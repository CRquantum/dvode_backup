!  Stiction-friction problem

!    Problem B3 in the following reference:
!    S. Thompson, A Collection of Test Problems for Ordinary
!    Differential Equations Solvers Which Have Provisions
!    for Rootfinding, ORNL/TM-9912, August, 1987.

!    Original Reference:
!    D. Ellison, "Efficient Automatic Integration of Ordinary
!    Differential Equations with Discontinuities," Math. Comp.
!    Simulation, Volume XXIII, 1981, pp. 190-196.

!                    Brief Problem Description

!    The position of a block sliding across a surface is tracked.
!    Six event functions are used to track the times at which
!    the block sticks and unsticks, and to handle discontinuities
!    in the forcing function. The integration is restarted on the
!    appropriate branch of the ode and the appropriate branch of
!    the forcing function at the event times.

!    For t <= 1.5, the odes are defined by:
!       y_1' = y_2
!       y_2' =:
!            (F_a - F_1 -F_2*y_2)/m if (y_2 = 0 and F_a >  F_s) or y_2 > 0
!            (F_a + F_1 -F_2*y_2)/m if (y_2 = 0 and F_a < -F_s) or y_2 < 0
!            0 if y_2 = 0 and |F_a| <= F_s

!    The initial conditions are:
!            y_1(0) = y_2(0) = 0.

!    The forcing function F_a is defined by:
!       F_a(t) =:
!             0  if 0 <= t < 0.1 or 1 <= t
!             5t if 0.1 <= t < 0.5
!             -t if 0.5 <= t < 1

!    The event functions are defined by:
!       g_1 =:
!              -1 if (y_2 = 0 and F_a > F_s) or y_2 > 0
!              +1 otherwise
!       g_2 =:
!              -1 if (y_2 = 0 and F_a < -F_s) or y_2 < 0
!              +1 otherwise
!       g_3 =:
!              -1 if y_2 = 0 and |F_a| <= F_s
!              +1 otherwise
!       g_4 = t - 0.1
!       g_5 = t - 0.5
!       g_6 = t - 1.0

MODULE demo_sf

!  Problem data and calculated values used throughout demo_sf:
   IMPLICIT NONE
   DOUBLE PRECISION C1, C2, C3, C4, C5, C6, C7, D1, D2, D3, &
    D4, D5, D6, D7, FA, FM, FS, F1, F2, T1, T2, T3, T4, T5, &
    T6, T7, XM
   INTEGER IF, ISWIT, JSWIT
!  IF is the branch of F_a(t).
!  ISWIT and JSWIT are the branch of y_2'(t).

 CONTAINS

   SUBROUTINE DERV_B3(NEQ,T,Y,DY)

!   Calculate the system derivatives.

      IMPLICIT NONE
      INTEGER  NEQ
      DOUBLE PRECISION DY, T, Y
      DIMENSION Y(NEQ), DY(NEQ)

      CALL FAOFT(T, FA, IF)

      DY(1) = Y(2)

      IF (ISWIT == 1) DY(2) =(FA - F1 - F2 * Y(2)) / FM
      IF (ISWIT == 2) DY(2) =(FA + F1 - F2 * Y(2)) / FM
      IF (ISWIT == 3) DY(2) = 0.0D0

      RETURN
   END SUBROUTINE DERV_B3

   SUBROUTINE GRES_B3(NEQ,T,Y,NG,G)

!   Calculate the residuals for the event functions.

      IMPLICIT NONE
      INTEGER NEQ, NG
      DOUBLE PRECISION G, T, Y
      DIMENSION Y(NEQ), G(NG)

      CALL FAOFT(T, FA, IF)

      JSWIT = 0

      IF (((ABS(Y(2))<=0.0D0).AND.(FA>FS)).OR.(Y(2)>0.0D0)) JSWIT = 1

      IF (((ABS(Y(2))<=0.0D0).AND.(FA<-FS)).OR.(Y(2)<0.0D0)) JSWIT = 2

      IF ((ABS(Y(2))<=0.0D0).AND.(ABS(FA)<=FS)) JSWIT = 3

      IF (JSWIT == 0) WRITE(6, 1)
    1 FORMAT(' JSWIT = 0 in GRES_B3. Stopping.')
      IF (JSWIT == 0) STOP

      G(1) = 1.0D0
      IF (((ABS(Y(2))<=0.0D0).AND.(FA>FS)).OR.(Y(2)>0.0D0)) G(1) = -1.0D0

      G(2) = 1.0D0
      IF (((ABS(Y(2))<=0.0D0).AND.(FA<-FS)).OR.(Y(2)<0.0D0)) G(2) = -1.0D0

      G(3) = 1.0D0
      IF ((ABS(Y(2))<=0.0D0).AND.(ABS(FA)<=FS)) G(3) = -1.0D0

      G(4) = T - 0.1D0
      G(5) = T - 0.5D0
      G(6) = T - 1.0D0

      RETURN
   END SUBROUTINE GRES_B3

   SUBROUTINE CHANGE_B3(JROOT,NG,NEQ,YROOT,TROOT)

!   Make problem changes at roots of the event functions.

      IMPLICIT NONE
      INTEGER I, JROOT, NEQ, NG
      DOUBLE PRECISION TROOT, YROOT
      DIMENSION JROOT(NG), YROOT(NEQ)

      DO 20 I = 1, 3
         IF (JROOT(I) == 1) YROOT(2) = 0.0D0
   20 END DO

!     Switch branches for the driving function FA(T).

      IF (JROOT(4) == 1) IF = 2
      IF (JROOT(5) == 1) IF = 3
      IF (JROOT(6) == 1) IF = 4

!     Switch branches for the ode.

      IF (ABS(TROOT - .166D0) <= .01D0) ISWIT = 1
      IF (ABS(TROOT - .705D0) <= .01D0) ISWIT = 3
      IF (ABS(TROOT - .830D0) <= .01D0) ISWIT = 2
      IF (ABS(TROOT - 1.03D0) <= .01D0) ISWIT = 3

      RETURN
   END SUBROUTINE CHANGE_B3

   SUBROUTINE FAOFT(T,FA,JF)

!   Define the driving function.

      IMPLICIT NONE
      INTEGER JF
      DOUBLE PRECISION FA, T

      GOTO (10, 20, 30, 40), JF

   10 CONTINUE
      FA = 0.0D0
      GOTO 50

   20 CONTINUE
      FA = 5.0D0 * T
      GOTO 50

   30 CONTINUE
      FA = - T
      GOTO 50

   40 CONTINUE
      FA = 0.0D0

   50 CONTINUE

      RETURN
   END SUBROUTINE FAOFT

   SUBROUTINE INITIAL_B3(NEQ,TOLD,TFINAL,TINC,YOLD)

!   Define the problem dependent data.

      IMPLICIT NONE
      INTEGER NEQ
      DOUBLE PRECISION TFINAL, TINC, TOLD, YOLD
      DIMENSION YOLD(NEQ)

      CALL EXACT_B3

      TOLD = 0.0D0
      TFINAL = 1.50D0
      TINC = .01D0
      YOLD(1) = 0.0D0
      YOLD(2) = 0.0D0

!     Define the problem-dependent parameters.
      FM = 0.64D0
      FS = 0.83D0
      F1 = 0.75D0
      F2 = 0.28D0

!     IF is used to denote the current segment for the
!     FA(T) driving function.
      IF = 1

!     The ode starts out on branch 3.
      ISWIT = 3

      RETURN
   END SUBROUTINE INITIAL_B3

   SUBROUTINE EXACT_B3

!   Calculate the exact solution.

      IMPLICIT NONE
      INTEGER IFLAG, INT
      DOUBLE PRECISION ARG, C, D, T, T3AE, T3B, T3C, T3R, T3RE, &
      Y1OFT, Y1OFT3, Y2OFT, YT4MD5
      DIMENSION C(7), D(7), T(7)
      LOGICAL IPRINT
!     EXTERNAL F3

      IPRINT = .FALSE.

      F1 = 0.75D0
      F2 = 0.28D0
      XM = 0.64D0

      INT = 1
      C1 = 0.0D0
      D1 = 0.0D0
      T1 = .166D0
      C(1) = C1
      D(1) = D1
      T(1) = T1
      IF (IPRINT) WRITE(6,1) INT, C1, D1, T1

      INT = 2
      C2 = - EXP(F2 * T1 / XM) *(5.0D0 * T1 / F2 - 5.0D0 * XM / F2**2 &
      - F1 / F2)
      D2 =(C2 * XM / F2) * EXP(- F2 * T1 / XM) -(5.0D0 * T1**2) &
      /(2.0D0 * F2) +(5.0D0 * XM * T1) / F2**2 + F1 * T1 / F2
      T2 = 0.5D0
      C(2) = C2
      D(2) = D2
      T(2) = T2
      IF (IPRINT) WRITE(6,1) INT, C2, D2, T2

      INT = 3
      Y1OFT =(- C2 * XM / F2) * EXP(- F2 * T2 / XM) +(5.0D0 * T2** &
      2) /(2.0D0 * F2) -(5.0D0 * XM * T2) / F2**2 - F1 * T2 / F2 + D2
      Y2OFT = C2 * EXP(- F2 * T2 / XM) + 5.0D0 * T2 / F2 - 5.0D0 * XM &
      / F2**2 - F1 / F2
      C3 = EXP(F2 * T2 / XM) *(Y2OFT + T2 / F2 - XM / F2**2 + F1 / F2)
      D3 = Y1OFT +(C3 * XM / F2) * EXP(- F2 * T2 / XM) + T2**2 / &
      (2.0D0 * F2) - XM * T2 / F2**2 + F1 * T2 / F2
      T3B = .70D0
      T3C = .71D0
      T3R = T3B
      T3RE = 0.0D0
      T3AE = 1.0D-13
      CALL DFZERO(F3, T3B, T3C, T3R, T3RE, T3AE, IFLAG)
      T3 = T3B
      C(3) = C3
      D(3) = D3
      T(3) = T3
      IF (IPRINT) WRITE(6,2) IFLAG
      IF (IPRINT) WRITE(6,1) INT, C3, D3, T3

      INT = 4
      C4 = 0.0D0
      D4 = 0.0D0
      T4 = 0.83D0
      C(4) = C4
      D(4) = D4
      T(4) = T4
      IF (IPRINT) WRITE(6,1) INT, C4, D4, T4

      INT = 5
      C5 = EXP(F2 * T4 / XM) *(T4 / F2 - XM / F2**2 - F1 / F2)
      Y1OFT3 =(- C3 * XM / F2) * EXP(- F2 * T3 / XM) - T3**2 / &
      (2.0D0 * F2) + XM * T3 / F2**2 - F1 * T3 / F2 + D3
      YT4MD5 =(- XM * C5 / F2) * EXP(- F2 * T4 / XM) - T4**2 / &
      (2.0D0 * F2) + XM * T4 / F2**2 + F1 * T4 / F2
      D5 = Y1OFT3 - YT4MD5
      T5 = 1.0D00
      C(5) = C5
      D(5) = D5
      T(5) = T5
      IF (IPRINT) WRITE(6,1) INT, C5, D5, T5

      INT = 6
      C6 = EXP(F2 / XM) *(C5 * EXP(- F2 / XM) - 1.0D0 / F2 + XM / F2**2)
      D6 =(XM / F2) *(C6 - C5) * EXP(- F2 / XM) - 1.0D0 /(2.0D0 * &
      F2) + XM / F2**2 + D5
      ARG = - F1 /(C6 * F2)
      T6 =(- XM / F2) * LOG(ARG)
      C(6) = C6
      D(6) = D6
      T(6) = T6
      IF (IPRINT) WRITE(6,1) INT, C6, D6, T6

      INT = 7
      C7 = 0.0D0
      D7 = 0.0D0
      T7 = 1.5D0
      C(7) = C7
      D(7) = D7
      T(7) = T7
      IF (IPRINT) WRITE(6,1) INT, C7, D7, T7

      IF (IPRINT) WRITE(6,3)(INT, C(INT), D(INT), T(INT), INT=1,7)

    1 FORMAT(' The parameters for interval ',I2, ' are: ',/,(2X,D25.15))
    2 FORMAT(' Return from DFZERO with IFLAG = ',I3)
    3 FORMAT(' Coefficients: ',/,(2X,I2,3D20.10))

      RETURN
   END SUBROUTINE EXACT_B3

   FUNCTION F3(T)

!   Residual routine used in the calculation of the root near 0.705.

      IMPLICIT NONE
      DOUBLE PRECISION T, F3

      F3 = C3 * EXP(- F2 * T / XM) - T / F2 + XM / F2**2 - F1 / F2

      RETURN
   END FUNCTION F3

   SUBROUTINE DFZERO(F,B,C,R,RE,AE,IFLAG)

!   Slatec rootfinder.

      IMPLICIT NONE
      DOUBLE PRECISION A, ACBS, ACMB, AE, AW, B, C, CMB, ER, FA, FB, FC, &
      FX, FZ, P, Q, R, RE, RW, T, TOL, Z
      DOUBLE PRECISION F
      INTEGER IC, IFLAG, KOUNT
      EXTERNAL F
      INTRINSIC EPSILON

!     ER IS two times the computer unit roundoff value.
      ER = 2.0D0 * EPSILON(1.0D0)

!     Initialize.

      Z = R
      IF (R <= MIN(B, C) .OR. R >= MAX(B, C)) Z = C
      RW = MAX(RE, ER)
      AW = MAX(AE, 0.0D0)
      IC = 0
      T = Z
      FZ = F(T)
      T = B
      FB = F(T)
      KOUNT = 2
!     IF (SIGN(1.0D0, FZ) == SIGN(1.0D0, FB)) GOTO 1
      IF (ABS(SIGN(1.0D0, FZ) - SIGN(1.0D0, FB)) <= 0.0D0) GOTO 1
      C = Z
      FC = FZ
      GOTO 2
!   1 IF (Z == C) GOTO 2
    1 IF (ABS(Z-C) <= 0.0D0) GOTO 2
      T = C
      FC = F(T)
      KOUNT = 3
!     IF (SIGN(1.0D0, FZ) == SIGN(1.0D0, FC)) GOTO 2
      IF (ABS(SIGN(1.0D0, FZ) - SIGN(1.0D0, FC)) <= 0.0D0) GOTO 2
      B = Z
      FB = FZ
    2 A = C
      FA = FC
      ACBS = ABS(B - C)
      FX = MAX(ABS(FB), ABS(FC))

    3 IF (ABS(FC) >= ABS(FB)) GOTO 4
!     Perform interchange.
      A = B
      FA = FB
      B = C
      FB = FC
      C = A
      FC = FA

    4 CMB = 0.5D0 *(C - B)
      ACMB = ABS(CMB)
      TOL = RW * ABS(B) + AW

!     Test stopping criterion and function count.

      IF (ACMB <= TOL) GOTO 10
      IF (ABS(FB) <= 0.0D0) GOTO 11
      IF (KOUNT >= 500) GOTO 14

!     Calculate new iterate implicitly as B+P/Q where we
!     arrange P >= 0. The implicit form is used to prevent
!     overflow.

      P =(B - A) * FB
      Q = FA - FB
      IF (P >= 0.D0) GOTO 5
      P = - P
      Q = - Q

!     Update a and check for satisfactory reduction in the size
!     of the bracketing interval. If not, perform bisection.

    5 A = B
      FA = FB
      IC = IC + 1
      IF (IC < 4) GOTO 6
      IF (8.0D0 * ACMB >= ACBS) GOTO 8
      IC = 0
      ACBS = ACMB

!     Test for too small a change.

    6 IF (P > ABS(Q) * TOL) GOTO 7

!     Increment by tolerance.

      B = B + SIGN(TOL, CMB)
      GOTO 9

!     Root ought to be between B and (C+B)/2.

    7 IF (P >= CMB * Q) GOTO 8

!     Use secant rule

      B = B + P / Q
      GOTO 9

!     Use bisection

    8 B = 0.5D0 *(C + B)

!     Have completed computation for new iterate B.

    9 T = B
      FB = F(T)
      KOUNT = KOUNT + 1

!     Decide whether next step is interpolation or extrapolation.

!     IF (SIGN(1.0D0, FB) /= SIGN(1.0D0, FC)) GOTO 3
      IF (ABS(SIGN(1.0D0, FB) - SIGN(1.0D0, FC)) > 0.0D0) GOTO 3
      C = A
      FC = FA
      GOTO 3

!     Finished. Process results for proper setting of IFLAG.

!  10 IF (SIGN(1.0D0, FB) == SIGN(1.0D0, FC)) GOTO 13
   10 IF (ABS(SIGN(1.0D0, FB) - SIGN(1.0D0, FC)) <= 0.0D0) GOTO 13
      IF (ABS(FB) > FX) GOTO 12
      IFLAG = 1
      RETURN
   11 IFLAG = 2
      RETURN
   12 IFLAG = 3
      RETURN
   13 IFLAG = 4
      RETURN
   14 IFLAG = 5

      RETURN
   END SUBROUTINE DFZERO

   SUBROUTINE TERROR_B3(TROOT,TERROR)

!   Calculate the error in the computed roots.

      IMPLICIT NONE
      DOUBLE PRECISION TERROR, TROOT

      TERROR = 1.0D10
      IF (ABS(0.1D0 - TROOT) <= 0.5D-2) TERROR = 0.1D0 - TROOT
      IF (ABS(0.5D0 - TROOT) <= 0.5D-2) TERROR = 0.5D0 - TROOT
      IF (ABS(1.0D0 - TROOT) <= 0.5D-2) TERROR = 1.0D0 - TROOT
      IF (ABS(T1 - TROOT) <= 0.5D-2) TERROR = T1 - TROOT
      IF (ABS(T3 - TROOT) <= 0.5D-2) TERROR = T3 - TROOT
      IF (ABS(T4 - TROOT) <= 0.5D-2) TERROR = T4 - TROOT
      IF (ABS(T6 - TROOT) <= 0.5D-2) TERROR = T6 - TROOT

      RETURN
   END SUBROUTINE TERROR_B3

   SUBROUTINE TRUE_B3(NEQ,T,EXACT)

!   Calculate the exact solution.

      IMPLICIT NONE
      INTEGER IDUM, NEQ
      DOUBLE PRECISION EXACT, T

      DIMENSION EXACT(NEQ)

      CALL YSOL_B3(T, IDUM, EXACT(1), EXACT(2))

      RETURN
   END SUBROUTINE TRUE_B3

   SUBROUTINE YSOL_B3(T,INT,Y1OFT,Y2OFT)

!   Routine used in the calculation of the exact solution.

      IMPLICIT NONE
      INTEGER INT
      DOUBLE PRECISION T, Y1OFT, Y2OFT

      INT = 1
      IF (T > T1) INT = 2
      IF (T > T2) INT = 3
      IF (T > T3) INT = 4
      IF (T > T4) INT = 5
      IF (T > T5) INT = 6
      IF (T > T6) INT = 7

      GOTO (100, 200, 300, 400, 500, 600, 700), INT

  100 CONTINUE
      Y1OFT = 0.0D0
      Y2OFT = 0.0D0
      GOTO 800

  200 CONTINUE
      Y1OFT =(- C2 * XM / F2) * EXP(- F2 * T / XM) +(5.0D0 * T**2) &
      /(2.0D0 * F2) -(5.0D0 * XM * T) / F2**2 - F1 * T / F2 + D2
      Y2OFT = C2 * EXP(- F2 * T / XM) + 5.0D0 * T / F2 - 5.0D0 * XM / &
      F2**2 - F1 / F2
      GOTO 800

  300 CONTINUE
      Y1OFT =(- C3 * XM / F2) * EXP(- F2 * T / XM) - T**2 /(2.0D0 * &
      F2) + XM * T / F2**2 - F1 * T / F2 + D3
      Y2OFT = C3 * EXP(- F2 * T / XM) - T / F2 + XM / F2**2 - F1 / F2
      GOTO 800

  400 CONTINUE
      Y1OFT =(- C3 * XM / F2) * EXP(- F2 * T3 / XM) - T3**2 / &
     (2.0D0 * F2) +(XM * T3) / F2**2 - F1 * T3 / F2 + D3
      Y2OFT = 0.0D0
      GOTO 800

  500 CONTINUE
      Y1OFT =(- XM * C5 / F2) * EXP(- F2 * T / XM) - T**2 /(2.0D0 * &
      F2) + XM * T / F2**2 + F1 * T / F2 + D5
      Y2OFT = C5 * EXP(- F2 * T / XM) - T / F2 + XM / F2**2 + F1 / F2
      GOTO 800

  600 CONTINUE
      Y1OFT =(- XM * C6 / F2) * EXP(- F2 * T / XM) + F1 * T / F2 + D6
      Y2OFT = C6 * EXP(- F2 * T / XM) + F1 / F2
      GOTO 800

  700 CONTINUE
      Y1OFT =(- XM * C6 / F2) * EXP(- F2 * T6 / XM) + F1 * T6 / F2 + D6
      Y2OFT = 0.0D0

  800 CONTINUE

      RETURN
   END SUBROUTINE YSOL_B3

END MODULE demo_sf

!******************************************************************

      PROGRAM Run_sf

      USE demo_sf 
      USE DVODE_F90_M

!     Type declarations.
      IMPLICIT NONE
!     Define the number of odes and the number of event functions.
      INTEGER, PARAMETER :: NEQ = 2, NG = 6
      INTEGER ITASK, ISTATE, JROOTS, ISTATS, I, TOTALS
      DOUBLE PRECISION ATOL, RTOL, RSTATS, T, TOUT, Y, TFINAL, TINC, &
      EXACT, ERRMAX, TERROR
      DIMENSION Y(NEQ), RSTATS(22), ISTATS(31), JROOTS(NG), &
      EXACT(NEQ), TOTALS(3)

      TYPE (VODE_OPTS) :: OPTIONS

!     Open the output file and the plot file.
      OPEN (UNIT=6,FILE='demosf.dat')
      OPEN (UNIT=7,FILE='demosfplot.dat')

!     Set the initial conditions and problem parameters.
      CALL INITIAL_B3(NEQ,T,TFINAL,TINC,Y)

!     Set the integration parameters.
      TOUT = TINC
      RTOL = 1.0D-10
      ATOL = 1.0D-10
      ITASK = 1
      ISTATE = 1
      ERRMAX = -1.0D0
      TOTALS(1:3) = 0

!     Set the VODE_F90 options.
      OPTIONS = SET_OPTS(RELERR=RTOL,ABSERR=ATOL,NEVENTS=NG)

!     Perform the integration.
10    CONTINUE
      CALL DVODE_F90(DERV_B3,NEQ,Y,T,TOUT,ITASK,ISTATE,OPTIONS,G_FCN=GRES_B3)

!     Gather the integration statistics for this problem.
      CALL GET_STATS(RSTATS,ISTATS,NG,JROOTS)

!     Stop the integration if an error occurred.
      IF (ISTATE < 0) THEN
        WRITE(6,*) ' The integration was not successful.'
        GOTO 20
      END IF

!     Write to the MATLAB plot file.
      WRITE(7,90001) TOUT, Y(1), Y(2)

!     Terminate the integration if we have reached the final output time.
      IF (T >= TFINAL) GOTO 20

!     Go back in for more if this is an output return.
      IF (ISTATE == 2) THEN
!       Calculate the error in the solution:
        CALL TRUE_B3(NEQ, TOUT, EXACT)
        DO I = 1, NEQ
           ERRMAX = MAX(ERRMAX,ABS(EXACT(I)-Y(I)))
        END DO
         TOUT = TOUT + TINC
         GOTO 10
      END IF

!     An event time was located.
      IF (ISTATE == 3) THEN
        WRITE(6,90005) T
        WRITE(6,90000) ISTATS(11), ISTATS(12), ISTATS(10)
        TOTALS(1) = TOTALS(1) + ISTATS(11)
        TOTALS(2) = TOTALS(2) + ISTATS(12)
        TOTALS(3) = TOTALS(3) + ISTATS(10)
!       Calculate the error in the solution.
        CALL TRUE_B3(NEQ, T, EXACT)
        DO I = 1, NEQ
           ERRMAX = MAX(ERRMAX,ABS(EXACT(I)-Y(I)))
        END DO
!       Calculate the error in the computed event time.
        CALL TERROR_B3(T, TERROR)
        WRITE(6,90004) TERROR
!       Make problem changes:
        CALL CHANGE_B3(JROOTS,NG,NEQ,Y,T)
!       Restart the integration at this event.
        ISTATE = 1
        TOUT = T + TINC
        GO TO 10
      END IF

20    CONTINUE

!     Write the integration final root finding statistics.
      TOTALS(1) = TOTALS(1) + ISTATS(11)
      TOTALS(2) = TOTALS(2) + ISTATS(12)
      TOTALS(3) = TOTALS(3) + ISTATS(10)
      WRITE(6,90003) TOTALS(1), TOTALS(2), TOTALS(3)
      WRITE(6,90002) ERRMAX
90000 FORMAT (' Steps = ',I5,' f-s = ',I5,' g-s =',I5)
90001 FORMAT (3D15.5)
90002 FORMAT (/' Maximum absolute error in computed solution = ', D15.5)
90003 FORMAT (/' Total Steps = ',I5,' Total f-s = ',I5,' Total g-s =',I5)
90004 FORMAT (' Error in computed root = ', D15.5)
90005 FORMAT (/' Event return for T = ', D15.5)

      STOP
      END PROGRAM Run_sf
