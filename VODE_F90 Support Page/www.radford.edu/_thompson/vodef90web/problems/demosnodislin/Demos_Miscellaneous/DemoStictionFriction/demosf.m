function [t,y] = demosf
% Matlab script to plot the solution for for the stiction-friction problem

temp = load('demosfplot.dat','-ascii');
t  = temp(:,1)
y1 = temp(:,2);
y2 = temp(:,3);

figure
plot(t,y1)
xlabel('t');
ylabel('y_1');
title(' Stiction-Friction Problem')

figure
plot(t,y2)
xlabel('t');
ylabel('y_2');
title('Stiction-Friction Problem')
