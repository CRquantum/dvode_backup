function [t,y] = demoknee
temp = load('demokneeplot.dat','-ascii');

t = temp(:,1);
y = temp(:,2);

plot(t,y)
title('Knee Problem Solution')
xlabel('t');
ylabel('y');