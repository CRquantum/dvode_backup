! VODE_F90 demonstration program
! Ruby laser oscillator

! Reference:
! G.D. Byrne and A.C. Hindmarsh, Stiff ODE Solvers: A Review of
! Current and Coming Attractions, J. Comp. Physics, Vol. 70,
! No. 1, May 1987, pp. 1-62.

    MODULE LASER_DEMO

      IMPLICIT NONE
      DOUBLE PRECISION ALPHA, BETA, GAMMA, RHO, SIGMA, TAU, N, PHI, DNDT, &
        DPHIDT, N_STEADY, PHI_STEADY

    CONTAINS

      SUBROUTINE DERIVS(NEQ,T,Y,YDOT)
        IMPLICIT NONE
        INTEGER NEQ
        DOUBLE PRECISION T, Y, YDOT
        DIMENSION Y(NEQ), YDOT(NEQ)

        N = Y(1)
        PHI = Y(2)
        DNDT = -N*(ALPHA*PHI+BETA) + GAMMA
        DPHIDT = PHI*(RHO*N-SIGMA) + TAU*(1.0D0+N)
        YDOT(1) = DNDT
        YDOT(2) = DPHIDT
        RETURN
      END SUBROUTINE DERIVS

      SUBROUTINE JACD(NEQ,T,Y,ML,MU,PD,NROWPD)
!     Subroutine to define the exact Jacobian for this problem
        IMPLICIT NONE
        INTEGER NEQ, ML, MU, NROWPD
        DOUBLE PRECISION T, Y, PD
        DIMENSION Y(NEQ), PD(NROWPD,NEQ)

        N = Y(1)
        PHI = Y(2)
        PD(1,1) = -(ALPHA*PHI+BETA)
        PD(2,1) = PHI*RHO + TAU
        PD(1,2) = -N*ALPHA
        PD(2,2) = RHO*N - SIGMA
        RETURN
      END SUBROUTINE JACD

    END MODULE LASER_DEMO

!******************************************************************

    PROGRAM DEMOLASER

      USE LASER_DEMO
      USE DVODE_F90_M

!     Type declarations:
      IMPLICIT NONE
      INTEGER, PARAMETER :: NEQ = 2, NTOUT = 901
      INTEGER ITASK, ISTATE, ISTATS, IOUT, SWITCH
      DOUBLE PRECISION RSTATS, T, TOUT, Y, RTOL, TFINAL, DELTAT, TSWITCH, &
        ABSERR_TOLERANCES
      DIMENSION Y(NEQ), RSTATS(22), ISTATS(31), ABSERR_TOLERANCES(NEQ)
      TYPE (VODE_OPTS) :: OPTIONS

!     Open the output file and the plot files:
      OPEN (UNIT=6,FILE='demolaser.dat')
      OPEN (UNIT=7,FILE='demolaserplot.dat')
!     Set  the problem parameters:
      ALPHA = 1.5D-18
      BETA = 2.5D-6
      GAMMA = 2.1D-6
      RHO = 0.6D0
      SIGMA = 0.18D0
      TAU = 0.016D0
      N = -1.0D0
      PHI = 0.0D0
!     Equilibrium solution:
      N_STEADY = 0.3D0 - 1.155D-14
      PHI_STEADY = 3.0D12 + 0.1798D0
!     Set the integration parameters:
      RTOL = 1.0D-6
      ABSERR_TOLERANCES(1) = 1.0D-9
      ABSERR_TOLERANCES(2) = 1.0D-6
      WRITE (6,90004) RTOL, ABSERR_TOLERANCES(1:NEQ)
!     Set the initial conditions:
      Y(1) = N
      Y(2) = PHI
!     Output points:
      T = 0.0D0
      TOUT = T
      TFINAL = 2.0D6
!     TFINAL = 0.7D6
      TSWITCH = 4.9D5
      SWITCH = 1
      DELTAT = (TFINAL-T)/REAL(NTOUT-1)
      WRITE (6,90001) TOUT, Y(1), Y(2)
      WRITE (7,90001) TOUT, Y(1), Y(2)
      IOUT = 1
      ITASK = 1
      ISTATE = 1
!     Start with stiff method:
      OPTIONS = SET_OPTS(DENSE_J=.TRUE.,USER_SUPPLIED_JACOBIAN=.TRUE., &
        RELERR=RTOL,ABSERR_VECTOR=ABSERR_TOLERANCES,MXSTEP=200000)
!     Perform the integration:
      DO IOUT = 2, NTOUT
        TOUT = TOUT + DELTAT
        CALL DVODE_F90(DERIVS,NEQ,Y,T,TOUT,ITASK,ISTATE,OPTIONS,J_FCN=JACD)
!        Gather and write the integration statistics for this problem:
        CALL GET_STATS(RSTATS,ISTATS)
!        Stop the integration if an error occurred:
        IF (ISTATE<0) THEN
          WRITE (6,90003) ISTATE
          STOP
        END IF
!        Print the solution and write the plot data file:
        WRITE (6,90001) TOUT, Y(1), Y(2)
        WRITE (7,90001) TOUT, Y(1), Y(2)
        IF (SWITCH==1 .AND. TOUT>=TSWITCH) THEN
!           Switch to nonstiff method:
          WRITE (6,90000) TOUT
          OPTIONS = SET_OPTS(RELERR=RTOL,MXSTEP=200000, &
            ABSERR_VECTOR=ABSERR_TOLERANCES)
          SWITCH = 0
          ISTATE = 3
        END IF
      END DO ! IOUT
!     Write the integration final integration statistics:
      WRITE (6,90002) ISTATS(11), ISTATS(12), ISTATS(13)
!     Format statements for this problem:
90000 FORMAT (' Switched to nonstiff method at T = ',D15.5)
90001 FORMAT (3D15.5)
90002 FORMAT (' Steps = ',I10,' f-s = ',I10,' J-s = ',I10)
90003 FORMAT (' An error occurred in VODE_F90. ISTATE = ',I3)
90004 FORMAT (/' Results for RTOL = ',D15.5/' ATOL(1) = ',D15.5, &
        '   ATOL(2) =',D15.5)

    END PROGRAM DEMOLASER
