function [t,y] = demolaser
% Matlab script to plot the solution for for the ruby oscillator problem

temp = load('demolaserplot.dat','-ascii');
t  = temp(:,1)
y1 = temp(:,2);
y2 = temp(:,3);

floor = 1e-15;
floor = -1;

figure
y1(:) = max(y1(:),floor);
semilogx(t,y1)
xlabel('log(t)');
ylabel('n');
title('Laser Oscillator Problem')

figure
y2(:) = max(y2(:),floor);
loglog(t,y2)
xlabel('log(t)');
ylabel('log(\phi)');
title('Laser Oscillator Problem')
