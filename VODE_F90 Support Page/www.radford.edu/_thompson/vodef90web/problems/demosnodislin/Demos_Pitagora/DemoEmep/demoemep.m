function [t,y] = demoemep
% Matlab script to plot the solution for the Emep
% Pitagora problem

temp = load('demoemepplot.dat','-ascii');
t    = temp(:,1);
NO   = temp(:,2);
NO2  = temp(:,3);
SO2  = temp(:,4);
CH4  = temp(:,5);
O3   = temp(:,6);
N205 = temp(:,7);

figure
plot(t,NO)
title('Emep Problem')
xlabel('t');
ylabel('NO(t)');

figure
plot(t,NO2)
title('Emep Problem')
xlabel('t');
ylabel('NO2(t)');

figure
plot(t,SO2)
title('Emep Problem')
xlabel('t');
ylabel('SO2(t)');

figure
plot(t,CH4)
title('Emep Problem')
xlabel('t');
ylabel('CH4(t)');

figure
plot(t,O3)
title('Emep Problem')
xlabel('t');
ylabel('O3(t)');

figure
plot(t,N205)
title('Emep Problem')
xlabel('t');
ylabel('N205(t)');
