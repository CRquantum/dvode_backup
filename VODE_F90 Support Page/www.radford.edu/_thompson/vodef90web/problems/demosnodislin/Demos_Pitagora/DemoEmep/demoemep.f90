! VODE_F90 demonstration program
! Test Set for IVP solvers
! http://www.dm.uniba.it/~testset/

! Problem EMEP
! Night_or_Day flag used to remove discontinuities at the
! switch times.

    MODULE EMEP_DEMO

      IMPLICIT NONE
      INTEGER SWITCH, NIGHT_OR_DAY
      DOUBLE PRECISION TSWITCH
      DIMENSION TSWITCH(0:9)

    CONTAINS

      SUBROUTINE DERIVS(NEQ,TIME,Y,DY)
!     Subroutine to evaluate dy/dt for this problem
        IMPLICIT NONE
        INTEGER NEQ
        DOUBLE PRECISION TIME, Y, DY
        DIMENSION Y(NEQ), DY(NEQ)
        DOUBLE PRECISION M, O2, XN2, RPATH3, RPATH4
        DOUBLE PRECISION S1, S2, S3, S4, S5, S6, S7, S8, S9, S10, S11

!=======================================================================

!     EMEP MSC-W OZONE MODEL CHEMISTRY

!=======================================================================

! Parameters
        INTEGER NSPEC, NRC, NDJ
        DOUBLE PRECISION HMIX
        PARAMETER (NSPEC=66,NRC=266,NDJ=16)
        PARAMETER (HMIX=1.2D5)
!     NSPEC : 66    number of species
!     NRC   : 266   size of rate constant array
!     NDJ   : 16    number of photolysis reactions
!     HMIX  : mixing height in cm
!     EMIS1..EMIS13 : emitted species

! Listing of species
!     Y(1:NSPEC) =
!        NO,     NO2,    SO2,    CO,     CH4,     C2H6,
!        NC4H10, C2H4,   C3H6,   OXYL,   HCHO,    CH3CHO,
!        MEK,    O3,     HO2,    HNO3,   H2O2,    H2,
!        CH3O2,  C2H5OH, SA,     CH3O2H, C2H5O2,  CH3COO,
!        PAN,    SECC4H, MEKO2,  R2OOH,  ETRO2,   MGLYOX,
!        PRRO2,  GLYOX,  OXYO2,  MAL,    MALO2,   OP,
!        OH,     OD,     NO3,    N2O5,   ISOPRE,  NITRAT,
!        ISRO2,  MVK,    MVKO2,  CH3OH,  RCO3H,   OXYO2H,
!        BURO2H, ETRO2H, PRRO2H, MEKO2H, MALO2H,  MACR,
!        ISNI,   ISRO2H, MARO2,  MAPAN,  CH2CCH3, ISONO3,
!        ISNIR,  MVKO2H, CH2CHR, ISNO3H, ISNIRH,  MARO2H

!=======================================================================

! Emissions in molec/(cm**2*s), of NO, NO2, SO2, CO, CH4, C2H6, NC4H10,
!    C2H4, C3H6, O-XYLENE, C2H5OH and ISOPRENE
        DOUBLE PRECISION EMIS1, EMIS2, EMIS3, EMIS4, EMIS5, EMIS6, EMIS7, &
          EMIS8, EMIS9, EMIS10, EMIS11, EMIS12, EMIS13
        DOUBLE PRECISION FRAC6, FRAC7, FRAC8, FRAC9, FRAC10, FRAC13
        DOUBLE PRECISION VEKT6, VEKT7, VEKT8, VEKT9, VEKT10, VEKT13
        DOUBLE PRECISION VMHC, EMNOX, EMHC, FACISO, FACHC, FACNOX

!   distribution of VOC emissions among species:
        PARAMETER (FRAC6=0.07689D0,FRAC7=0.41444D0,FRAC8=0.03642D0, &
          FRAC9=0.03827D0,FRAC10=0.24537D0,FRAC13=0.13957D0)
        PARAMETER (VEKT6=30.D0,VEKT7=58.D0,VEKT8=28.D0,VEKT9=42.D0, &
          VEKT10=106.D0,VEKT13=46.D0)
        PARAMETER (VMHC=1.D0/(FRAC6/VEKT6+FRAC7/VEKT7+FRAC8/VEKT8+FRAC9/VEKT9+ &
          FRAC10/VEKT10+FRAC13/VEKT13))

!   choose values for NOX and HC emissions in molecules/cm**2xs
        PARAMETER (EMNOX=2.5D11,EMHC=2.5D11)

!   rural case
        PARAMETER (FACISO=1.0D0,FACHC=1.0D0,FACNOX=1.0D0)

        PARAMETER (EMIS1=EMNOX*FACNOX,EMIS2=0.D0,EMIS3=EMNOX*FACNOX, &
          EMIS4=EMHC*10.D0*FACHC,EMIS5=0.D0,EMIS6=EMHC*FRAC6/VEKT6*VMHC*FACHC, &
          EMIS7=EMHC*FRAC7/VEKT7*VMHC*FACHC,EMIS8=EMHC*FRAC8/VEKT8*VMHC*FACHC, &
          EMIS9=EMHC*FRAC9/VEKT9*VMHC*FACHC,EMIS10=EMHC*FRAC10/VEKT10*VMHC* &
          FACHC,EMIS11=0.5D0*FACISO*EMHC,EMIS12=0.D0, &
          EMIS13=EMHC*FRAC13/VEKT13*VMHC*FACHC)


        DOUBLE PRECISION RC(NRC), DJ(NDJ), H2O

!=======================================================================

! Compute time-dependent EMEP coefficients
        CALL EMEPCF(TIME,RC,DJ,H2O)


        M = 2.55D19
        O2 = 5.2D18
        XN2 = 1.99D19
!..  pathways for decay of secc4h9o:
!..  Newer assumption, from Atkisnon , 1991
        RPATH3 = 0.65D0
        RPATH4 = 0.35D0
!=======================================================================


        DY(1) = DJ(3)*Y(2) + DJ(13)*Y(39) + RC(19)*Y(2)*Y(39) + EMIS1/HMIX - &
          (RC(5)*Y(36)+RC(11)*Y(14)+RC(17)*Y(15)+RC(72)*Y(23)+RC(79)*(Y(24)+Y( &
          57))+RC(15)*Y(39)+RC(60)*(Y(19)+Y(26)+Y(27)+Y(29)+Y(31)+Y(33)+Y( &
          35)+Y(43)+Y(45)+Y(59)+Y(61)+Y(60)))*Y(1)
        DY(2) = Y(1)*(RC(5)*Y(36)+RC(11)*Y(14)+RC(17)*Y(15)+RC(72)*Y(23)+RC(79 &
          )*(Y(24)+Y(57))+0.2D1*RC(15)*Y(39)) + RC(60)*Y(1)*(Y(19)+Y(26)+Y(27) &
          +Y(29)+Y(31)+Y(33)+Y(35)+Y(59)) + RC(60)*Y(1)*(0.86D0*Y(43)+0.19D1*Y &
          (61)+0.11D1*Y(60)+0.95D0*Y(45)) + DJ(14)*Y(39) + DJ(5)*Y(16) + &
          DJ(15)*Y(40) + RC(29)*Y(40) + RC(78)*(Y(25)+Y(58)) - &
          (DJ(3)+RC(12)*Y(14)+RC(20)*Y(39)+RC(21)*Y(37)+RC(48)+RC(77)*(Y( &
          24)+Y(57)))*Y(2)
        DY(3) = EMIS3/HMIX - (RC(39)*Y(37)+RC(40)*Y(19)+RC(47))*Y(3)
        DY(4) = EMIS4/HMIX + Y(37)*(RC(66)*Y(11)+2.D0*RC(221)*Y(32)+RC(222)*Y( &
          30)) + Y(14)*(0.44D0*RC(112)*Y(8)+0.4D0*RC(123)*Y(9)+0.5D-1*RC(160)* &
          Y(44)+0.5D-1*RC(150)*Y(41)) + Y(11)*(DJ(6)+DJ(7)+RC(69)*Y(39)) + &
          DJ(8)*Y(12) + DJ(11)*Y(30) + 2.D0*DJ(7)*Y(32) - RC(70)*Y(37)*Y(4)
        DY(5) = EMIS5/HMIX + 0.7D-1*RC(123)*Y(14)*Y(9) - RC(59)*Y(37)*Y(5)
        DY(6) = EMIS6/HMIX - RC(71)*Y(37)*Y(6)
        DY(7) = EMIS7/HMIX - RC(81)*Y(37)*Y(7)
        DY(8) = EMIS8/HMIX - (RC(109)*Y(37)+RC(112)*Y(14))*Y(8)
        DY(9) = EMIS9/HMIX + 0.7D-1*RC(150)*Y(14)*Y(41) - &
          (RC(123)*Y(14)+RC(125)*Y(37))*Y(9)
        DY(10) = EMIS10/HMIX - RC(234)*Y(37)*Y(10)
        DY(11) = Y(19)*(RC(60)*Y(1)+(2.D0*RC(61)+RC(62))*Y(19)+RC(80)*Y(24)+RC &
          (40)*Y(3)) + Y(37)*(RC(63)*Y(46)+RC(67)*Y(22)) + &
          Y(1)*RC(60)*(2.D0*Y(29)+Y(31)+0.74D0*Y(43)+0.266D0*Y(45)+ &
          0.15D0*Y(60)) + Y(14)*(0.5D0*RC(123)*Y(9)+RC(112)*Y(8)+0.7D0*RC(157) &
          *Y(56)+0.8D0*RC(160)*Y(44)+0.8D0*RC(150)*Y(41)) + 2.D0*DJ(7)*Y(32) + &
          DJ(16)*(Y(22)+0.156D1*Y(50)+Y(51)) - (RC(66)*Y(37)+DJ(6)+DJ(7)+RC(69 &
          )*Y(39)+RC(53))*Y(11)
        DY(12) = Y(1)*(RC(72)*Y(23)+RC(83)*Y(26)*RPATH4+RC(105)*Y(27)+RC(126)* &
          Y(31)+0.95D0*RC(162)*Y(61)+0.684D0*RC(154)*Y(45)) + &
          Y(37)*(RC(64)*Y(20)+RC(76)*Y(28)+RC(76)*Y(50)) + &
          0.5D0*RC(123)*Y(14)*Y(9) + 0.4D-1*RC(160)*Y(14)*Y(44) + &
          DJ(16)*(Y(28)+0.22D0*Y(50)+0.35D0*Y(49)+Y(51)+Y(52)) - &
          (DJ(8)+RC(75)*Y(37)+RC(53))*Y(12)
        DY(13) = RC(83)*Y(1)*Y(26)*RPATH3 + (0.65D0*DJ(16)+RC(76)*Y(37))*Y(49) &
          + RC(76)*Y(37)*Y(51) + (RC(159)*Y(1)+DJ(16))*Y(59) + &
          0.95D0*RC(162)*Y(1)*Y(61) - (DJ(9)+RC(86)*Y(37)+RC(53))*Y(13)
        DY(14) = RC(1)*Y(36) + RC(89)*Y(15)*Y(24) - (RC(11)*Y(1)+RC(12)*Y(2)+ &
          RC(13)*Y(37)+RC(14)*Y(15)+RC(49)+RC(112)*Y(8)+RC(123)*Y(9)+ &
          RC(157)*Y(56)+RC(160)*Y(44)+RC(150)*Y(41)+DJ(1)+DJ(2))*Y(14)
        S1 = Y(37)*(RC(13)*Y(14)+RC(31)*Y(17)+RC(33)*Y(18)+RC(39)*Y(3)+RC(63)* &
          Y(46)+RC(64)*Y(20)+RC(66)*Y(11)+RC(70)*Y(4)+RC(221)*Y(32)) + &
          Y(19)*(RC(40)*Y(3)+2.D0*RC(61)*Y(19)+0.5D0*RC(80)*Y(24)) + &
          DJ(11)*Y(30) + Y(1)*RC(60)*(Y(19)+Y(29)+Y(31)+Y(33)+Y(35)+0.95D0*Y( &
          45)+Y(26)*RPATH3+0.78D0*Y(43)+Y(59)+0.5D-1*Y(61)+0.8D0*Y(60)) + &
          RC(72)*Y(1)*Y(23) + DJ(8)*Y(12)
        DY(15) = S1 + 2.D0*DJ(6)*Y(11) + DJ(16)*(Y(22)+Y(28)+0.65D0*Y(49)+Y(50 &
          )+Y(51)+Y(48)+Y(53)) + Y(39)*(RC(26)*Y(17)+RC(69)*Y(11)) + &
          Y(14)*(0.12D0*RC(112)*Y(8)+0.28D0*RC(123)*Y(9)+0.6D-1*RC(160)*Y(44)) &
          + 0.6D-1*RC(150)*Y(14)*Y(41) - (RC(14)*Y(14)+RC(17)*Y(1)+RC(30)*Y(37 &
          )+2.D0*RC(36)*Y(15)+RC(65)*Y(19)+RC(74)*Y(23)+(RC(88)+RC( &
          89))*Y(24)+RC(85)*(Y(26)+Y(29)+Y(31)+Y(27)+Y(57)+Y(45)+Y(61)+Y( &
          59)+Y(33)+Y(35)+Y(43)+Y(60)))*Y(15)
        DY(16) = RC(21)*Y(2)*Y(37) + Y(39)*(RC(26)*Y(17)+RC(69)*Y(11)) - &
          (RC(35)*Y(37)+DJ(5)+RC(45))*Y(16)
        DY(17) = RC(36)*Y(15)**2 - (RC(31)*Y(37)+DJ(4)+RC(43)+RC(26)*Y(39)+RC( &
          47))*Y(17)
        DY(18) = DJ(7)*Y(11) + Y(14)*(0.13D0*RC(112)*Y(8)+0.7D-1*RC(123)*Y(9)) &
          - RC(33)*Y(37)*Y(18)
        DY(19) = Y(37)*(RC(59)*Y(5)+RC(68)*Y(22)) + Y(24)*(RC(79)*Y(1)+2.D0*RC &
          (94)*Y(24)) + DJ(8)*Y(12) + DJ(16)*Y(47) + &
          0.31D0*RC(123)*Y(14)*Y(9) - (RC(40)*Y(3)+RC(60)*Y(1)+2.D0*RC(61)*Y( &
          19)+2.D0*RC(62)*Y(19)+RC(65)*Y(15)+0.5D0*RC(80)*Y(24))*Y(19)
        DY(20) = EMIS13/HMIX - RC(64)*Y(37)*Y(20)
        DY(21) = (RC(40)*Y(19)+RC(39)*Y(37))*Y(3) + 0.5D-1*EMIS3/HMIX - &
          RC(51)*Y(21)
        DY(22) = RC(65)*Y(15)*Y(19) - (RC(43)+DJ(16)+(RC(67)+RC(68))*Y(37))*Y( &
          22)
        DY(23) = Y(37)*(RC(71)*Y(6)+RC(68)*Y(28)) + 0.35D0*DJ(16)*Y(49) + &
          RC(83)*Y(1)*Y(26)*RPATH4 + DJ(9)*Y(13) - (RC(72)*Y(1)+RC(74)*Y(15))* &
          Y(23)
        DY(24) = Y(37)*(RC(75)*Y(12)+RC(222)*Y(30)+RC(68)*Y(47)) + &
          RC(105)*Y(1)*Y(27) + RC(78)*Y(25) + DJ(11)*Y(30) + DJ(9)*Y(13) + &
          DJ(16)*Y(52) + 0.684D0*RC(154)*Y(1)*Y(45) - &
          (RC(77)*Y(2)+RC(79)*Y(1)+RC(80)*Y(19)+2.D0*RC(94)*Y(24)+(RC(88)+RC( &
          89))*Y(15))*Y(24)
        DY(25) = RC(77)*Y(24)*Y(2) - (RC(50)+RC(78))*Y(25)
        DY(26) = Y(37)*(RC(81)*Y(7)+RC(68)*Y(49)) - (RC(83)*Y(1)+RC(85)*Y(15)) &
          *Y(26)
        DY(27) = Y(37)*(RC(86)*Y(13)+RC(87)*Y(52)) - &
          (RC(105)*Y(1)+RC(85)*Y(15))*Y(27)
        DY(28) = RC(74)*Y(15)*Y(23) - ((RC(76)+RC(68))*Y(37)+DJ(16)+RC(52))*Y( &
          28)
        DY(29) = Y(37)*(RC(109)*Y(8)+RC(68)*Y(50)) - &
          (RC(110)*Y(1)+RC(85)*Y(15))*Y(29)
        DY(30) = RC(236)*Y(1)*Y(33) + RC(220)*Y(1)*Y(35) + &
          0.266D0*RC(154)*Y(1)*Y(45) + 0.82D0*RC(160)*Y(14)*Y(44) + &
          DJ(16)*(Y(48)+Y(53)) - (DJ(11)+RC(222)*Y(37))*Y(30)
        DY(31) = Y(37)*(RC(125)*Y(9)+RC(68)*Y(51)) - &
          (RC(126)*Y(1)+RC(85)*Y(15))*Y(31)
        DY(32) = RC(220)*Y(1)*Y(35) + DJ(16)*Y(53) - &
          (2.D0*DJ(7)+RC(221)*Y(37))*Y(32)
        DY(33) = Y(37)*(RC(234)*Y(10)+RC(235)*Y(48)) - &
          (RC(236)*Y(1)+RC(85)*Y(15))*Y(33)
        DY(34) = RC(236)*Y(1)*Y(33) + DJ(16)*Y(48) - RC(219)*Y(37)*Y(34)
        DY(35) = Y(37)*(RC(219)*Y(34)+RC(223)*Y(53)) - &
          (RC(220)*Y(1)+RC(85)*Y(15))*Y(35)
        DY(36) = DJ(1)*Y(14) + DJ(3)*Y(2) + DJ(14)*Y(39) + RC(7)*Y(38) + &
          0.2D0*RC(160)*Y(14)*Y(44) + 0.3D0*RC(150)*Y(14)*Y(41) - &
          (RC(1)+RC(5)*Y(1))*Y(36)
        S1 = 2.D0*RC(8)*H2O*Y(38) + Y(15)*(RC(14)*Y(14)+RC(17)*Y(1)) + &
          2.D0*DJ(4)*Y(17) + DJ(5)*Y(16)
        S2 = S1 + DJ(16)*(Y(22)+Y(28)+Y(47)+Y(49)+Y(50)+Y(52)+Y(48)+Y(53))
        S3 = S2 + Y(14)*(0.15D0*RC(123)*Y(9)+0.8D-1*RC(160)*Y(44))
        S4 = S3
        S6 = 0.55D0*RC(150)*Y(14)*Y(41)
        S8 = -1
        S11 = RC(222)*Y(30) + RC(75)*Y(12) + RC(81)*Y(7) + RC(87)*Y(52) + &
          RC(86)*Y(13) + RC(235)*Y(48) + RC(109)*Y(8) + RC(125)*Y(9) + &
          RC(234)*Y(10) + RC(223)*Y(53) + RC(219)*Y(34) + RC(31)*Y(17) + &
          RC(21)*Y(2) + RC(148)*Y(62) + RC(64)*Y(20) + RC(39)*Y(3) + &
          RC(71)*Y(6)
        S10 = S11 + RC(30)*Y(15) + RC(59)*Y(5) + RC(70)*Y(4) + RC(35)*Y(16) + &
          RC(13)*Y(14) + RC(221)*Y(32) + RC(68)*(Y(22)+Y(28)+Y(47)+Y(50)+Y(51) &
          +Y(49)) + RC(66)*Y(11) + RC(151)*Y(41) + RC(153)*Y(44) + &
          RC(63)*Y(46) + RC(33)*Y(18) + RC(158)*Y(54) + RC(146)*Y(63) + &
          RC(149)*(Y(65)+Y(66)) + RC(147)*Y(64) + RC(161)*Y(55)
        S11 = Y(37)
        S9 = S10*S11
        S7 = S8*S9
        S5 = S6 + S7
        DY(37) = S4 + S5
        DY(38) = DJ(2)*Y(14) - (RC(7)+RC(8)*H2O)*Y(38)
        DY(39) = (RC(29)+DJ(15))*Y(40) + RC(12)*Y(14)*Y(2) + &
          RC(35)*Y(37)*Y(16) - (RC(15)*Y(1)+RC(26)*Y(17)+RC(163)*Y(41)+RC(19)* &
          Y(2)+RC(20)*Y(2)+DJ(13)+DJ(14)+RC(69)*Y(11))*Y(39)
        DY(40) = RC(20)*Y(39)*Y(2) - (RC(29)+DJ(15)+RC(45))*Y(40)
        DY(41) = EMIS11/HMIX - (RC(151)*Y(37)+RC(163)*Y(39)+RC(150)*Y(14))*Y( &
          41)
        DY(42) = RC(45)*Y(16) + 2.D0*RC(44)*Y(40) - RC(51)*Y(42)
        DY(43) = Y(37)*(RC(151)*Y(41)+RC(156)*Y(56)) + &
          0.12D0*RC(152)*Y(1)*Y(43) - (RC(152)*Y(1)+RC(155)*Y(15))*Y(43)
        DY(44) = RC(60)*Y(1)*(0.42D0*Y(43)+0.5D-1*Y(60)) + &
          0.26D0*RC(150)*Y(14)*Y(41) - (RC(153)*Y(37)+RC(160)*Y(14))*Y(44)
        DY(45) = RC(153)*Y(44)*Y(37) + RC(148)*Y(37)*Y(62) - &
          (RC(154)*Y(1)+RC(85)*Y(15))*Y(45)
        DY(46) = RC(62)*Y(19)**2 - RC(63)*Y(37)*Y(46)
        DY(47) = RC(88)*Y(15)*Y(24) - (RC(68)*Y(37)+DJ(16)+RC(52))*Y(47)
        DY(48) = RC(85)*Y(15)*Y(33) - (RC(235)*Y(37)+DJ(16)+RC(52))*Y(48)
        DY(49) = RC(85)*Y(15)*Y(26) - ((RC(76)+RC(68))*Y(37)+DJ(16)+RC(52))*Y( &
          49)
        DY(50) = RC(85)*Y(15)*Y(29) - ((RC(76)+RC(68))*Y(37)+DJ(16)+RC(52))*Y( &
          50)
        DY(51) = RC(85)*Y(15)*Y(31) - ((RC(76)+RC(68))*Y(37)+DJ(16)+RC(52))*Y( &
          51)
        DY(52) = RC(85)*Y(15)*Y(27) - (RC(87)*Y(37)+DJ(16)+RC(52))*Y(52)
        DY(53) = RC(85)*Y(15)*Y(35) - (RC(223)*Y(37)+DJ(16)+RC(52))*Y(53)
        DY(54) = RC(60)*Y(1)*(0.32D0*Y(43)+0.1D0*Y(60)) + &
          0.67D0*RC(150)*Y(14)*Y(41) - RC(158)*Y(37)*Y(54)
        DY(55) = RC(60)*Y(1)*(0.14D0*Y(43)+0.5D-1*Y(45)+0.85D0*Y(60)) - &
          RC(161)*Y(37)*Y(55)
        DY(56) = RC(155)*Y(15)*Y(43) - (RC(156)*Y(37)+RC(157)*Y(14)+RC(52))*Y( &
          56)
        DY(57) = 0.5D0*RC(158)*Y(37)*Y(54) + RC(78)*Y(58) + &
          RC(149)*Y(37)*Y(66) - (RC(77)*Y(2)+RC(79)*Y(1)+RC(85)*Y(15))*Y(57)
        DY(58) = RC(77)*Y(57)*Y(2) - (RC(50)+RC(78))*Y(58)
        DY(59) = RC(79)*Y(1)*Y(57) + RC(146)*Y(37)*Y(63) - &
          (RC(159)*Y(1)+RC(85)*Y(15))*Y(59)
        DY(60) = RC(163)*Y(39)*Y(41) + RC(147)*Y(37)*Y(64) - &
          (RC(164)*Y(1)+RC(85)*Y(15))*Y(60)
        DY(61) = RC(161)*Y(37)*Y(55) + RC(149)*Y(37)*Y(65) - &
          (RC(162)*Y(1)+RC(85)*Y(15))*Y(61)
        DY(62) = RC(85)*Y(15)*Y(45) - (RC(148)*Y(37)+RC(52))*Y(62)
        DY(63) = RC(85)*Y(15)*Y(59) - (RC(146)*Y(37)+RC(52))*Y(63)
        DY(64) = RC(85)*Y(15)*Y(60) - (RC(147)*Y(37)+RC(52))*Y(64)
        DY(65) = RC(85)*Y(15)*Y(61) - (RC(149)*Y(37)+RC(52))*Y(65)
        DY(66) = RC(85)*Y(15)*Y(57) - (RC(149)*Y(37)+RC(52))*Y(66)
        RETURN
      END SUBROUTINE DERIVS

      SUBROUTINE JACD(NEQ,TIME,Y,ML,MU,JAC,NROWPD)
!     Subroutine to define the exact Jacobian for this problem
        IMPLICIT NONE
        INTEGER NEQ, ML, MU, NROWPD
        DOUBLE PRECISION TIME, Y, JAC
        DIMENSION Y(NEQ), JAC(NROWPD,NEQ)
        INTEGER I, J
        DOUBLE PRECISION M, O2, XN2, RPATH3, RPATH4
        DOUBLE PRECISION S1

! Parameters
        INTEGER NSPEC, NRC, NDJ
        DOUBLE PRECISION HMIX
        PARAMETER (NSPEC=66,NRC=266,NDJ=16)
        PARAMETER (HMIX=1.2D5)

        DOUBLE PRECISION EMIS1, EMIS2, EMIS3, EMIS4, EMIS5, EMIS6, EMIS7, &
          EMIS8, EMIS9, EMIS10, EMIS11, EMIS12, EMIS13
        DOUBLE PRECISION FRAC6, FRAC7, FRAC8, FRAC9, FRAC10, FRAC13
        DOUBLE PRECISION VEKT6, VEKT7, VEKT8, VEKT9, VEKT10, VEKT13
        DOUBLE PRECISION VMHC, EMNOX, EMHC, FACISO, FACHC, FACNOX

!   distribution of VOC emissions among species:
        PARAMETER (FRAC6=0.07689D0,FRAC7=0.41444D0,FRAC8=0.03642D0, &
          FRAC9=0.03827D0,FRAC10=0.24537D0,FRAC13=0.13957D0)
        PARAMETER (VEKT6=30.D0,VEKT7=58.D0,VEKT8=28.D0,VEKT9=42.D0, &
          VEKT10=106.D0,VEKT13=46.D0)
        PARAMETER (VMHC=1.D0/(FRAC6/VEKT6+FRAC7/VEKT7+FRAC8/VEKT8+FRAC9/VEKT9+ &
          FRAC10/VEKT10+FRAC13/VEKT13))

!   choose values for NOX and HC emissions in molecules/cm**2xs
        PARAMETER (EMNOX=2.5D11,EMHC=2.5D11)

!   rural case
        PARAMETER (FACISO=1.0D0,FACHC=1.0D0,FACNOX=1.0D0)

        PARAMETER (EMIS1=EMNOX*FACNOX,EMIS2=0.D0,EMIS3=EMNOX*FACNOX, &
          EMIS4=EMHC*10.D0*FACHC,EMIS5=0.D0,EMIS6=EMHC*FRAC6/VEKT6*VMHC*FACHC, &
          EMIS7=EMHC*FRAC7/VEKT7*VMHC*FACHC,EMIS8=EMHC*FRAC8/VEKT8*VMHC*FACHC, &
          EMIS9=EMHC*FRAC9/VEKT9*VMHC*FACHC,EMIS10=EMHC*FRAC10/VEKT10*VMHC* &
          FACHC,EMIS11=0.5D0*FACISO*EMHC,EMIS12=0.D0, &
          EMIS13=EMHC*FRAC13/VEKT13*VMHC*FACHC)


        DOUBLE PRECISION RC(NRC), DJ(NDJ), H2O

!=======================================================================

! Compute time-dependent EMEP coefficients
        CALL EMEPCF(TIME,RC,DJ,H2O)


        M = 2.55D19
        O2 = 5.2D18
        XN2 = 1.99D19
!..  pathways for decay of secc4h9o:
!..  Newer assumption, from Atkisnon , 1991
        RPATH3 = 0.65D0
        RPATH4 = 0.35D0
!=======================================================================

        DO I = 1, 66
          DO J = 1, 66
            JAC(I,J) = 0.0D0
          END DO
        END DO
        JAC(1,1) = -RC(5)*Y(36) - RC(11)*Y(14) - RC(17)*Y(15) - RC(72)*Y(23) - &
          RC(79)*(Y(24)+Y(57)) - RC(15)*Y(39) - RC(60)*(Y(19)+Y(26)+Y(27)+Y(29 &
          )+Y(31)+Y(33)+Y(35)+Y(43)+Y(45)+Y(59)+Y(61)+Y(60))
        JAC(1,2) = DJ(3) + RC(19)*Y(39)
        JAC(1,14) = -RC(11)*Y(1)
        JAC(1,15) = -RC(17)*Y(1)
        JAC(1,19) = -RC(60)*Y(1)
        JAC(1,23) = -RC(72)*Y(1)
        JAC(1,24) = -RC(79)*Y(1)
        JAC(1,26) = -RC(60)*Y(1)
        JAC(1,27) = -RC(60)*Y(1)
        JAC(1,29) = -RC(60)*Y(1)
        JAC(1,31) = -RC(60)*Y(1)
        JAC(1,33) = -RC(60)*Y(1)
        JAC(1,35) = -RC(60)*Y(1)
        JAC(1,36) = -RC(5)*Y(1)
        JAC(1,39) = DJ(13) + RC(19)*Y(2) - RC(15)*Y(1)
        JAC(1,43) = -RC(60)*Y(1)
        JAC(1,45) = -RC(60)*Y(1)
        JAC(1,57) = -RC(79)*Y(1)
        JAC(1,59) = -RC(60)*Y(1)
        JAC(1,60) = -RC(60)*Y(1)
        JAC(1,61) = -RC(60)*Y(1)
        JAC(2,1) = RC(5)*Y(36) + RC(11)*Y(14) + RC(17)*Y(15) + RC(72)*Y(23) + &
          RC(79)*(Y(24)+Y(57)) + 0.2D1*RC(15)*Y(39) + &
          RC(60)*(Y(19)+Y(26)+Y(27)+Y(29)+Y(31)+Y(33)+Y(35)+Y(59)) + &
          RC(60)*(0.86D0*Y(43)+0.19D1*Y(61)+0.11D1*Y(60)+0.95D0*Y(45))
        JAC(2,2) = -DJ(3) - RC(12)*Y(14) - RC(20)*Y(39) - RC(21)*Y(37) - &
          RC(48) - RC(77)*(Y(24)+Y(57))
        JAC(2,14) = RC(11)*Y(1) - RC(12)*Y(2)
        JAC(2,15) = RC(17)*Y(1)
        JAC(2,16) = DJ(5)
        JAC(2,19) = RC(60)*Y(1)
        JAC(2,23) = RC(72)*Y(1)
        JAC(2,24) = RC(79)*Y(1) - RC(77)*Y(2)
        JAC(2,25) = RC(78)
        JAC(2,26) = RC(60)*Y(1)
        JAC(2,27) = RC(60)*Y(1)
        JAC(2,29) = RC(60)*Y(1)
        JAC(2,31) = RC(60)*Y(1)
        JAC(2,33) = RC(60)*Y(1)
        JAC(2,35) = RC(60)*Y(1)
        JAC(2,36) = RC(5)*Y(1)
        JAC(2,37) = -RC(21)*Y(2)
        JAC(2,39) = 0.2D1*RC(15)*Y(1) + DJ(14) - RC(20)*Y(2)
        JAC(2,40) = RC(29) + DJ(15)
        JAC(2,43) = 0.86D0*RC(60)*Y(1)
        JAC(2,45) = 0.95D0*RC(60)*Y(1)
        JAC(2,57) = RC(79)*Y(1) - RC(77)*Y(2)
        JAC(2,58) = RC(78)
        JAC(2,59) = RC(60)*Y(1)
        JAC(2,60) = 0.11D1*RC(60)*Y(1)
        JAC(2,61) = 0.19D1*RC(60)*Y(1)
        JAC(3,3) = -RC(39)*Y(37) - RC(40)*Y(19) - RC(47)
        JAC(3,19) = -RC(40)*Y(3)
        JAC(3,37) = -RC(39)*Y(3)
        JAC(4,4) = -RC(70)*Y(37)
        JAC(4,8) = 0.44D0*RC(112)*Y(14)
        JAC(4,9) = 0.4D0*RC(123)*Y(14)
        JAC(4,11) = RC(66)*Y(37) + DJ(6) + DJ(7) + RC(69)*Y(39)
        JAC(4,12) = DJ(8)
        JAC(4,14) = 0.44D0*RC(112)*Y(8) + 0.4D0*RC(123)*Y(9) + &
          0.5D-1*RC(160)*Y(44) + 0.5D-1*RC(150)*Y(41)
        JAC(4,30) = DJ(11) + RC(222)*Y(37)
        JAC(4,32) = 2.D0*RC(221)*Y(37) + 2.D0*DJ(7)
        JAC(4,37) = RC(66)*Y(11) + 2.D0*RC(221)*Y(32) + RC(222)*Y(30) - &
          RC(70)*Y(4)
        JAC(4,39) = RC(69)*Y(11)
        JAC(4,41) = 0.5D-1*RC(150)*Y(14)
        JAC(4,44) = 0.5D-1*RC(160)*Y(14)
        JAC(5,5) = -RC(59)*Y(37)
        JAC(5,9) = 0.7D-1*RC(123)*Y(14)
        JAC(5,14) = 0.7D-1*RC(123)*Y(9)
        JAC(5,37) = -RC(59)*Y(5)
        JAC(6,6) = -RC(71)*Y(37)
        JAC(6,37) = -RC(71)*Y(6)
        JAC(7,7) = -RC(81)*Y(37)
        JAC(7,37) = -RC(81)*Y(7)
        JAC(8,8) = -RC(109)*Y(37) - RC(112)*Y(14)
        JAC(8,14) = -RC(112)*Y(8)
        JAC(8,37) = -RC(109)*Y(8)
        JAC(9,9) = -RC(123)*Y(14) - RC(125)*Y(37)
        JAC(9,14) = 0.7D-1*RC(150)*Y(41) - RC(123)*Y(9)
        JAC(9,37) = -RC(125)*Y(9)
        JAC(9,41) = 0.7D-1*RC(150)*Y(14)
        JAC(10,10) = -RC(234)*Y(37)
        JAC(10,37) = -RC(234)*Y(10)
        JAC(11,1) = Y(19)*RC(60) + RC(60)*(2.D0*Y(29)+Y(31)+0.74D0*Y(43)+ &
          0.266D0*Y(45)+0.15D0*Y(60))
        JAC(11,3) = RC(40)*Y(19)
        JAC(11,8) = RC(112)*Y(14)
        JAC(11,9) = 0.5D0*RC(123)*Y(14)
        JAC(11,11) = -RC(66)*Y(37) - DJ(6) - DJ(7) - RC(69)*Y(39) - RC(53)
        JAC(11,14) = 0.5D0*RC(123)*Y(9) + RC(112)*Y(8) + 0.7D0*RC(157)*Y(56) + &
          0.8D0*RC(160)*Y(44) + 0.8D0*RC(150)*Y(41)
        JAC(11,19) = RC(60)*Y(1) + 2*(2.D0*RC(61)+RC(62))*Y(19) + &
          RC(80)*Y(24) + RC(40)*Y(3)
        JAC(11,22) = Y(37)*RC(67) + DJ(16)
        JAC(11,24) = RC(80)*Y(19)
        JAC(11,29) = 2.D0*RC(60)*Y(1)
        JAC(11,31) = RC(60)*Y(1)
        JAC(11,32) = 2.D0*DJ(7)
        JAC(11,37) = RC(63)*Y(46) + RC(67)*Y(22) - RC(66)*Y(11)
        JAC(11,39) = -RC(69)*Y(11)
        JAC(11,41) = 0.8D0*RC(150)*Y(14)
        JAC(11,43) = 0.74D0*RC(60)*Y(1)
        JAC(11,44) = 0.8D0*RC(160)*Y(14)
        JAC(11,45) = 0.266D0*RC(60)*Y(1)
        JAC(11,46) = Y(37)*RC(63)
        JAC(11,50) = 0.156D1*DJ(16)
        JAC(11,51) = DJ(16)
        JAC(11,56) = 0.7D0*RC(157)*Y(14)
        JAC(11,60) = 0.15D0*RC(60)*Y(1)
        JAC(12,1) = RC(72)*Y(23) + RC(83)*Y(26)*RPATH4 + RC(105)*Y(27) + &
          RC(126)*Y(31) + 0.95D0*RC(162)*Y(61) + 0.684D0*RC(154)*Y(45)
        JAC(12,9) = 0.5D0*RC(123)*Y(14)
        JAC(12,12) = -DJ(8) - RC(75)*Y(37) - RC(53)
        JAC(12,14) = 0.5D0*RC(123)*Y(9) + 0.4D-1*RC(160)*Y(44)
        JAC(12,20) = Y(37)*RC(64)
        JAC(12,23) = RC(72)*Y(1)
        JAC(12,26) = Y(1)*RC(83)*RPATH4
        JAC(12,27) = RC(105)*Y(1)
        JAC(12,28) = RC(76)*Y(37) + DJ(16)
        JAC(12,31) = RC(126)*Y(1)
        JAC(12,37) = RC(64)*Y(20) + RC(76)*Y(28) + RC(76)*Y(50) - RC(75)*Y(12)
        JAC(12,44) = 0.4D-1*RC(160)*Y(14)
        JAC(12,45) = 0.684D0*RC(154)*Y(1)
        JAC(12,49) = 0.35D0*DJ(16)
        JAC(12,50) = RC(76)*Y(37) + 0.22D0*DJ(16)
        JAC(12,51) = DJ(16)
        JAC(12,52) = DJ(16)
        JAC(12,61) = 0.95D0*RC(162)*Y(1)
        JAC(13,1) = RC(83)*Y(26)*RPATH3 + RC(159)*Y(59) + 0.95D0*RC(162)*Y(61)
        JAC(13,13) = -DJ(9) - RC(86)*Y(37) - RC(53)
        JAC(13,26) = RC(83)*Y(1)*RPATH3
        JAC(13,37) = RC(76)*Y(49) + RC(76)*Y(51) - RC(86)*Y(13)
        JAC(13,49) = 0.65D0*DJ(16) + RC(76)*Y(37)
        JAC(13,51) = RC(76)*Y(37)
        JAC(13,59) = RC(159)*Y(1) + DJ(16)
        JAC(13,61) = 0.95D0*RC(162)*Y(1)
        JAC(14,1) = -RC(11)*Y(14)
        JAC(14,2) = -RC(12)*Y(14)
        JAC(14,8) = -RC(112)*Y(14)
        JAC(14,9) = -RC(123)*Y(14)
        JAC(14,14) = -RC(11)*Y(1) - RC(12)*Y(2) - RC(13)*Y(37) - &
          RC(14)*Y(15) - RC(49) - RC(112)*Y(8) - RC(123)*Y(9) - &
          RC(157)*Y(56) - RC(160)*Y(44) - RC(150)*Y(41) - DJ(1) - DJ(2)
        JAC(14,15) = RC(89)*Y(24) - RC(14)*Y(14)
        JAC(14,24) = RC(89)*Y(15)
        JAC(14,36) = RC(1)
        JAC(14,37) = -RC(13)*Y(14)
        JAC(14,41) = -RC(150)*Y(14)
        JAC(14,44) = -RC(160)*Y(14)
        JAC(14,56) = -RC(157)*Y(14)
        JAC(15,1) = RC(60)*(Y(19)+Y(29)+Y(31)+Y(33)+Y(35)+0.95D0*Y(45)+Y(26)* &
          RPATH3+0.78D0*Y(43)+Y(59)+0.5D-1*Y(61)+0.8D0*Y(60)) + RC(72)*Y(23) - &
          RC(17)*Y(15)
        JAC(15,3) = RC(40)*Y(19) + RC(39)*Y(37)
        JAC(15,4) = RC(70)*Y(37)
        JAC(15,8) = 0.12D0*RC(112)*Y(14)
        JAC(15,9) = 0.28D0*RC(123)*Y(14)
        JAC(15,11) = RC(66)*Y(37) + 2.D0*DJ(6) + RC(69)*Y(39)
        JAC(15,12) = DJ(8)
        JAC(15,14) = RC(13)*Y(37) + 0.12D0*RC(112)*Y(8) + &
          0.28D0*RC(123)*Y(9) + 0.6D-1*RC(160)*Y(44) + 0.6D-1*RC(150)*Y(41) - &
          RC(14)*Y(15)
        JAC(15,15) = -4.D0*RC(36)*Y(15) - RC(14)*Y(14) - RC(17)*Y(1) - &
          RC(30)*Y(37) - RC(65)*Y(19) - RC(74)*Y(23) - (RC(88)+RC(89))*Y(24) - &
          RC(85)*(Y(26)+Y(29)+Y(31)+Y(27)+Y(57)+Y(45)+Y(61)+Y(59)+Y(33)+Y(35)+ &
          Y(43)+Y(60))
        JAC(15,17) = RC(31)*Y(37) + RC(26)*Y(39)
        JAC(15,18) = Y(37)*RC(33)
        JAC(15,19) = RC(40)*Y(3) + 4.D0*RC(61)*Y(19) + 0.5D0*RC(80)*Y(24) + &
          RC(60)*Y(1) - RC(65)*Y(15)
        JAC(15,20) = Y(37)*RC(64)
        JAC(15,22) = DJ(16)
        JAC(15,23) = RC(72)*Y(1) - RC(74)*Y(15)
        JAC(15,24) = 0.5D0*RC(80)*Y(19) - (RC(88)+RC(89))*Y(15)
        JAC(15,26) = Y(1)*RC(60)*RPATH3 - RC(85)*Y(15)
        JAC(15,27) = -RC(85)*Y(15)
        JAC(15,28) = DJ(16)
        JAC(15,29) = RC(60)*Y(1) - RC(85)*Y(15)
        JAC(15,30) = DJ(11)
        JAC(15,31) = RC(60)*Y(1) - RC(85)*Y(15)
        JAC(15,32) = RC(221)*Y(37)
        JAC(15,33) = RC(60)*Y(1) - RC(85)*Y(15)
        JAC(15,35) = RC(60)*Y(1) - RC(85)*Y(15)
        JAC(15,37) = RC(13)*Y(14) + RC(31)*Y(17) + RC(33)*Y(18) + &
          RC(39)*Y(3) + RC(63)*Y(46) + RC(64)*Y(20) + RC(66)*Y(11) + &
          RC(70)*Y(4) + RC(221)*Y(32) - RC(30)*Y(15)
        JAC(15,39) = RC(26)*Y(17) + RC(69)*Y(11)
        JAC(15,41) = 0.6D-1*RC(150)*Y(14)
        JAC(15,43) = 0.78D0*RC(60)*Y(1) - RC(85)*Y(15)
        JAC(15,44) = 0.6D-1*RC(160)*Y(14)
        JAC(15,45) = 0.95D0*RC(60)*Y(1) - RC(85)*Y(15)
        JAC(15,46) = Y(37)*RC(63)
        JAC(15,48) = DJ(16)
        JAC(15,49) = 0.65D0*DJ(16)
        JAC(15,50) = DJ(16)
        JAC(15,51) = DJ(16)
        JAC(15,53) = DJ(16)
        JAC(15,57) = -RC(85)*Y(15)
        JAC(15,59) = RC(60)*Y(1) - RC(85)*Y(15)
        JAC(15,60) = 0.8D0*RC(60)*Y(1) - RC(85)*Y(15)
        JAC(15,61) = 0.5D-1*RC(60)*Y(1) - RC(85)*Y(15)
        JAC(16,2) = RC(21)*Y(37)
        JAC(16,11) = RC(69)*Y(39)
        JAC(16,16) = -RC(35)*Y(37) - DJ(5) - RC(45)
        JAC(16,17) = RC(26)*Y(39)
        JAC(16,37) = RC(21)*Y(2) - RC(35)*Y(16)
        JAC(16,39) = RC(26)*Y(17) + RC(69)*Y(11)
        JAC(17,15) = 2*RC(36)*Y(15)
        JAC(17,17) = -RC(31)*Y(37) - DJ(4) - RC(43) - RC(26)*Y(39) - RC(47)
        JAC(17,37) = -RC(31)*Y(17)
        JAC(17,39) = -RC(26)*Y(17)
        JAC(18,8) = 0.13D0*RC(112)*Y(14)
        JAC(18,9) = 0.7D-1*RC(123)*Y(14)
        JAC(18,11) = DJ(7)
        JAC(18,14) = 0.13D0*RC(112)*Y(8) + 0.7D-1*RC(123)*Y(9)
        JAC(18,18) = -Y(37)*RC(33)
        JAC(18,37) = -RC(33)*Y(18)
        JAC(19,1) = Y(24)*RC(79) - Y(19)*RC(60)
        JAC(19,3) = -RC(40)*Y(19)
        JAC(19,5) = RC(59)*Y(37)
        JAC(19,9) = 0.31D0*RC(123)*Y(14)
        JAC(19,12) = DJ(8)
        JAC(19,14) = 0.31D0*RC(123)*Y(9)
        JAC(19,15) = -RC(65)*Y(19)
        JAC(19,19) = -(2.D0*RC(61)+2.D0*RC(62))*Y(19) - RC(40)*Y(3) - &
          RC(60)*Y(1) - 2.D0*RC(61)*Y(19) - 2.D0*RC(62)*Y(19) - RC(65)*Y(15) - &
          0.5D0*RC(80)*Y(24)
        JAC(19,22) = RC(68)*Y(37)
        JAC(19,24) = RC(79)*Y(1) + 4.D0*RC(94)*Y(24) - 0.5D0*RC(80)*Y(19)
        JAC(19,37) = RC(59)*Y(5) + RC(68)*Y(22)
        JAC(19,47) = DJ(16)
        JAC(20,20) = -Y(37)*RC(64)
        JAC(20,37) = -RC(64)*Y(20)
        JAC(21,3) = RC(40)*Y(19) + RC(39)*Y(37)
        JAC(21,19) = RC(40)*Y(3)
        JAC(21,21) = -RC(51)
        JAC(21,37) = RC(39)*Y(3)
        JAC(22,15) = RC(65)*Y(19)
        JAC(22,19) = RC(65)*Y(15)
        JAC(22,22) = -RC(43) - DJ(16) - (RC(67)+RC(68))*Y(37)
        JAC(22,37) = -(RC(67)+RC(68))*Y(22)
        JAC(23,1) = RC(83)*Y(26)*RPATH4 - RC(72)*Y(23)
        JAC(23,6) = RC(71)*Y(37)
        JAC(23,13) = DJ(9)
        JAC(23,15) = -RC(74)*Y(23)
        JAC(23,23) = -RC(72)*Y(1) - RC(74)*Y(15)
        JAC(23,26) = Y(1)*RC(83)*RPATH4
        JAC(23,28) = RC(68)*Y(37)
        JAC(23,37) = RC(71)*Y(6) + RC(68)*Y(28)
        JAC(23,49) = 0.35D0*DJ(16)
        JAC(24,1) = RC(105)*Y(27) + 0.684D0*RC(154)*Y(45) - Y(24)*RC(79)
        JAC(24,2) = -RC(77)*Y(24)
        JAC(24,12) = RC(75)*Y(37)
        JAC(24,13) = DJ(9)
        JAC(24,15) = -(RC(88)+RC(89))*Y(24)
        JAC(24,19) = -RC(80)*Y(24)
        JAC(24,24) = -4.D0*RC(94)*Y(24) - RC(77)*Y(2) - RC(79)*Y(1) - &
          RC(80)*Y(19) - (RC(88)+RC(89))*Y(15)
        JAC(24,25) = RC(78)
        JAC(24,27) = RC(105)*Y(1)
        JAC(24,30) = DJ(11) + RC(222)*Y(37)
        JAC(24,37) = RC(75)*Y(12) + RC(222)*Y(30) + RC(68)*Y(47)
        JAC(24,45) = 0.684D0*RC(154)*Y(1)
        JAC(24,47) = RC(68)*Y(37)
        JAC(24,52) = DJ(16)
        JAC(25,2) = RC(77)*Y(24)
        JAC(25,24) = RC(77)*Y(2)
        JAC(25,25) = -RC(50) - RC(78)
        JAC(26,1) = -RC(83)*Y(26)
        JAC(26,7) = RC(81)*Y(37)
        JAC(26,15) = -RC(85)*Y(26)
        JAC(26,26) = -RC(83)*Y(1) - RC(85)*Y(15)
        JAC(26,37) = RC(81)*Y(7) + RC(68)*Y(49)
        JAC(26,49) = RC(68)*Y(37)
        JAC(27,1) = -RC(105)*Y(27)
        JAC(27,13) = RC(86)*Y(37)
        JAC(27,15) = -RC(85)*Y(27)
        JAC(27,27) = -RC(105)*Y(1) - RC(85)*Y(15)
        JAC(27,37) = RC(86)*Y(13) + RC(87)*Y(52)
        JAC(27,52) = RC(87)*Y(37)
        JAC(28,15) = RC(74)*Y(23)
        JAC(28,23) = RC(74)*Y(15)
        JAC(28,28) = -(RC(76)+RC(68))*Y(37) - DJ(16) - RC(52)
        JAC(28,37) = -(RC(76)+RC(68))*Y(28)
        JAC(29,1) = -RC(110)*Y(29)
        JAC(29,8) = RC(109)*Y(37)
        JAC(29,15) = -RC(85)*Y(29)
        JAC(29,29) = -RC(110)*Y(1) - RC(85)*Y(15)
        JAC(29,37) = RC(109)*Y(8) + RC(68)*Y(50)
        JAC(29,50) = RC(68)*Y(37)
        JAC(30,1) = RC(236)*Y(33) + RC(220)*Y(35) + 0.266D0*RC(154)*Y(45)
        JAC(30,14) = 0.82D0*RC(160)*Y(44)
        JAC(30,30) = -DJ(11) - RC(222)*Y(37)
        JAC(30,33) = RC(236)*Y(1)
        JAC(30,35) = RC(220)*Y(1)
        JAC(30,37) = -RC(222)*Y(30)
        JAC(30,44) = 0.82D0*RC(160)*Y(14)
        JAC(30,45) = 0.266D0*RC(154)*Y(1)
        JAC(30,48) = DJ(16)
        JAC(30,53) = DJ(16)
        JAC(31,1) = -RC(126)*Y(31)
        JAC(31,9) = RC(125)*Y(37)
        JAC(31,15) = -RC(85)*Y(31)
        JAC(31,31) = -RC(126)*Y(1) - RC(85)*Y(15)
        JAC(31,37) = RC(125)*Y(9) + RC(68)*Y(51)
        JAC(31,51) = RC(68)*Y(37)
        JAC(32,1) = RC(220)*Y(35)
        JAC(32,32) = -2.D0*DJ(7) - RC(221)*Y(37)
        JAC(32,35) = RC(220)*Y(1)
        JAC(32,37) = -RC(221)*Y(32)
        JAC(32,53) = DJ(16)
        JAC(33,1) = -RC(236)*Y(33)
        JAC(33,10) = RC(234)*Y(37)
        JAC(33,15) = -RC(85)*Y(33)
        JAC(33,33) = -RC(236)*Y(1) - RC(85)*Y(15)
        JAC(33,37) = RC(234)*Y(10) + RC(235)*Y(48)
        JAC(33,48) = RC(235)*Y(37)
        JAC(34,1) = RC(236)*Y(33)
        JAC(34,33) = RC(236)*Y(1)
        JAC(34,34) = -RC(219)*Y(37)
        JAC(34,37) = -RC(219)*Y(34)
        JAC(34,48) = DJ(16)
        JAC(35,1) = -RC(220)*Y(35)
        JAC(35,15) = -RC(85)*Y(35)
        JAC(35,34) = RC(219)*Y(37)
        JAC(35,35) = -RC(220)*Y(1) - RC(85)*Y(15)
        JAC(35,37) = RC(219)*Y(34) + RC(223)*Y(53)
        JAC(35,53) = RC(223)*Y(37)
        JAC(36,1) = -RC(5)*Y(36)
        JAC(36,2) = DJ(3)
        JAC(36,14) = DJ(1) + 0.2D0*RC(160)*Y(44) + 0.3D0*RC(150)*Y(41)
        JAC(36,36) = -RC(1) - RC(5)*Y(1)
        JAC(36,38) = RC(7)
        JAC(36,39) = DJ(14)
        JAC(36,41) = 0.3D0*RC(150)*Y(14)
        JAC(36,44) = 0.2D0*RC(160)*Y(14)
        JAC(37,1) = RC(17)*Y(15)
        JAC(37,2) = -RC(21)*Y(37)
        JAC(37,3) = -RC(39)*Y(37)
        JAC(37,4) = -RC(70)*Y(37)
        JAC(37,5) = -RC(59)*Y(37)
        JAC(37,6) = -RC(71)*Y(37)
        JAC(37,7) = -RC(81)*Y(37)
        JAC(37,8) = -RC(109)*Y(37)
        JAC(37,9) = 0.15D0*RC(123)*Y(14) - RC(125)*Y(37)
        JAC(37,10) = -RC(234)*Y(37)
        JAC(37,11) = -RC(66)*Y(37)
        JAC(37,12) = -RC(75)*Y(37)
        JAC(37,13) = -RC(86)*Y(37)
        JAC(37,14) = RC(14)*Y(15) + 0.15D0*RC(123)*Y(9) + &
          0.8D-1*RC(160)*Y(44) + 0.55D0*RC(150)*Y(41) - RC(13)*Y(37)
        JAC(37,15) = RC(14)*Y(14) + RC(17)*Y(1) - RC(30)*Y(37)
        JAC(37,16) = DJ(5) - RC(35)*Y(37)
        JAC(37,17) = 2.D0*DJ(4) - RC(31)*Y(37)
        JAC(37,18) = -Y(37)*RC(33)
        JAC(37,20) = -Y(37)*RC(64)
        JAC(37,22) = DJ(16) - RC(68)*Y(37)
        JAC(37,28) = DJ(16) - RC(68)*Y(37)
        JAC(37,30) = -RC(222)*Y(37)
        JAC(37,32) = -RC(221)*Y(37)
        JAC(37,34) = -RC(219)*Y(37)
        S1 = -RC(149)*(Y(65)+Y(66)) - RC(21)*Y(2) - RC(35)*Y(16) - &
          RC(87)*Y(52) - RC(147)*Y(64) - RC(75)*Y(12) - RC(148)*Y(62) - &
          RC(31)*Y(17) - RC(64)*Y(20) - RC(70)*Y(4) - RC(153)*Y(44) - &
          RC(86)*Y(13) - RC(13)*Y(14) - RC(109)*Y(8) - RC(235)*Y(48) - &
          RC(234)*Y(10) - RC(81)*Y(7)
        JAC(37,37) = S1 - RC(59)*Y(5) - RC(219)*Y(34) - RC(30)*Y(15) - &
          RC(221)*Y(32) - RC(63)*Y(46) - RC(66)*Y(11) - RC(222)*Y(30) - &
          RC(33)*Y(18) - RC(39)*Y(3) - RC(223)*Y(53) - RC(151)*Y(41) - &
          RC(71)*Y(6) - RC(158)*Y(54) - RC(161)*Y(55) - RC(125)*Y(9) - &
          RC(68)*(Y(22)+Y(28)+Y(47)+Y(50)+Y(51)+Y(49)) - RC(146)*Y(63)
        JAC(37,38) = 2.D0*RC(8)*H2O
        JAC(37,41) = 0.55D0*RC(150)*Y(14) - RC(151)*Y(37)
        JAC(37,44) = 0.8D-1*RC(160)*Y(14) - RC(153)*Y(37)
        JAC(37,46) = -Y(37)*RC(63)
        JAC(37,47) = DJ(16) - RC(68)*Y(37)
        JAC(37,48) = DJ(16) - RC(235)*Y(37)
        JAC(37,49) = DJ(16) - RC(68)*Y(37)
        JAC(37,50) = DJ(16) - RC(68)*Y(37)
        JAC(37,51) = -RC(68)*Y(37)
        JAC(37,52) = DJ(16) - RC(87)*Y(37)
        JAC(37,53) = DJ(16) - RC(223)*Y(37)
        JAC(37,54) = -RC(158)*Y(37)
        JAC(37,55) = -RC(161)*Y(37)
        JAC(37,62) = -RC(148)*Y(37)
        JAC(37,63) = -RC(146)*Y(37)
        JAC(37,64) = -RC(147)*Y(37)
        JAC(37,65) = -RC(149)*Y(37)
        JAC(37,66) = -RC(149)*Y(37)
        JAC(38,14) = DJ(2)
        JAC(38,38) = -RC(7) - RC(8)*H2O
        JAC(39,1) = -RC(15)*Y(39)
        JAC(39,2) = RC(12)*Y(14) - (RC(19)+RC(20))*Y(39)
        JAC(39,11) = -RC(69)*Y(39)
        JAC(39,14) = RC(12)*Y(2)
        JAC(39,16) = RC(35)*Y(37)
        JAC(39,17) = -RC(26)*Y(39)
        JAC(39,37) = RC(35)*Y(16)
        JAC(39,39) = -RC(15)*Y(1) - RC(26)*Y(17) - RC(163)*Y(41) - &
          RC(19)*Y(2) - RC(20)*Y(2) - DJ(13) - DJ(14) - RC(69)*Y(11)
        JAC(39,40) = RC(29) + DJ(15)
        JAC(39,41) = -RC(163)*Y(39)
        JAC(40,2) = RC(20)*Y(39)
        JAC(40,39) = RC(20)*Y(2)
        JAC(40,40) = -RC(29) - DJ(15) - RC(45)
        JAC(41,14) = -RC(150)*Y(41)
        JAC(41,37) = -RC(151)*Y(41)
        JAC(41,39) = -RC(163)*Y(41)
        JAC(41,41) = -RC(151)*Y(37) - RC(163)*Y(39) - RC(150)*Y(14)
        JAC(42,16) = RC(45)
        JAC(42,40) = 2.D0*RC(44)
        JAC(42,42) = -RC(51)
        JAC(43,1) = -0.88D0*RC(152)*Y(43)
        JAC(43,15) = -RC(155)*Y(43)
        JAC(43,37) = RC(151)*Y(41) + RC(156)*Y(56)
        JAC(43,41) = RC(151)*Y(37)
        JAC(43,43) = -0.88D0*RC(152)*Y(1) - RC(155)*Y(15)
        JAC(43,56) = RC(156)*Y(37)
        JAC(44,1) = RC(60)*(0.42D0*Y(43)+0.5D-1*Y(60))
        JAC(44,14) = 0.26D0*RC(150)*Y(41) - RC(160)*Y(44)
        JAC(44,37) = -RC(153)*Y(44)
        JAC(44,41) = 0.26D0*RC(150)*Y(14)
        JAC(44,43) = 0.42D0*RC(60)*Y(1)
        JAC(44,44) = -RC(153)*Y(37) - RC(160)*Y(14)
        JAC(44,60) = 0.5D-1*RC(60)*Y(1)
        JAC(45,1) = -RC(154)*Y(45)
        JAC(45,15) = -RC(85)*Y(45)
        JAC(45,37) = RC(153)*Y(44) + RC(148)*Y(62)
        JAC(45,44) = RC(153)*Y(37)
        JAC(45,45) = -RC(154)*Y(1) - RC(85)*Y(15)
        JAC(45,62) = RC(148)*Y(37)
        JAC(46,19) = 2*RC(62)*Y(19)
        JAC(46,37) = -RC(63)*Y(46)
        JAC(46,46) = -Y(37)*RC(63)
        JAC(47,15) = RC(88)*Y(24)
        JAC(47,24) = RC(88)*Y(15)
        JAC(47,37) = -RC(68)*Y(47)
        JAC(47,47) = -RC(68)*Y(37) - DJ(16) - RC(52)
        JAC(48,15) = RC(85)*Y(33)
        JAC(48,33) = RC(85)*Y(15)
        JAC(48,37) = -RC(235)*Y(48)
        JAC(48,48) = -RC(235)*Y(37) - DJ(16) - RC(52)
        JAC(49,15) = RC(85)*Y(26)
        JAC(49,26) = RC(85)*Y(15)
        JAC(49,37) = -(RC(76)+RC(68))*Y(49)
        JAC(49,49) = -(RC(76)+RC(68))*Y(37) - DJ(16) - RC(52)
        JAC(50,15) = RC(85)*Y(29)
        JAC(50,29) = RC(85)*Y(15)
        JAC(50,37) = -(RC(76)+RC(68))*Y(50)
        JAC(50,50) = -(RC(76)+RC(68))*Y(37) - DJ(16) - RC(52)
        JAC(51,15) = RC(85)*Y(31)
        JAC(51,31) = RC(85)*Y(15)
        JAC(51,37) = -(RC(76)+RC(68))*Y(51)
        JAC(51,51) = -(RC(76)+RC(68))*Y(37) - DJ(16) - RC(52)
        JAC(52,15) = RC(85)*Y(27)
        JAC(52,27) = RC(85)*Y(15)
        JAC(52,37) = -RC(87)*Y(52)
        JAC(52,52) = -RC(87)*Y(37) - DJ(16) - RC(52)
        JAC(53,15) = RC(85)*Y(35)
        JAC(53,35) = RC(85)*Y(15)
        JAC(53,37) = -RC(223)*Y(53)
        JAC(53,53) = -RC(223)*Y(37) - DJ(16) - RC(52)
        JAC(54,1) = RC(60)*(0.32D0*Y(43)+0.1D0*Y(60))
        JAC(54,14) = 0.67D0*RC(150)*Y(41)
        JAC(54,37) = -RC(158)*Y(54)
        JAC(54,41) = 0.67D0*RC(150)*Y(14)
        JAC(54,43) = 0.32D0*RC(60)*Y(1)
        JAC(54,54) = -RC(158)*Y(37)
        JAC(54,60) = 0.1D0*RC(60)*Y(1)
        JAC(55,1) = RC(60)*(0.14D0*Y(43)+0.5D-1*Y(45)+0.85D0*Y(60))
        JAC(55,37) = -RC(161)*Y(55)
        JAC(55,43) = 0.14D0*RC(60)*Y(1)
        JAC(55,45) = 0.5D-1*RC(60)*Y(1)
        JAC(55,55) = -RC(161)*Y(37)
        JAC(55,60) = 0.85D0*RC(60)*Y(1)
        JAC(56,14) = -RC(157)*Y(56)
        JAC(56,15) = RC(155)*Y(43)
        JAC(56,37) = -RC(156)*Y(56)
        JAC(56,43) = RC(155)*Y(15)
        JAC(56,56) = -RC(156)*Y(37) - RC(157)*Y(14) - RC(52)
        JAC(57,1) = -RC(79)*Y(57)
        JAC(57,2) = -RC(77)*Y(57)
        JAC(57,15) = -RC(85)*Y(57)
        JAC(57,37) = 0.5D0*RC(158)*Y(54) + RC(149)*Y(66)
        JAC(57,54) = 0.5D0*RC(158)*Y(37)
        JAC(57,57) = -RC(77)*Y(2) - RC(79)*Y(1) - RC(85)*Y(15)
        JAC(57,58) = RC(78)
        JAC(57,66) = RC(149)*Y(37)
        JAC(58,2) = RC(77)*Y(57)
        JAC(58,57) = RC(77)*Y(2)
        JAC(58,58) = -RC(50) - RC(78)
        JAC(59,1) = RC(79)*Y(57) - RC(159)*Y(59)
        JAC(59,15) = -RC(85)*Y(59)
        JAC(59,37) = RC(146)*Y(63)
        JAC(59,57) = RC(79)*Y(1)
        JAC(59,59) = -RC(159)*Y(1) - RC(85)*Y(15)
        JAC(59,63) = RC(146)*Y(37)
        JAC(60,1) = -RC(164)*Y(60)
        JAC(60,15) = -RC(85)*Y(60)
        JAC(60,37) = RC(147)*Y(64)
        JAC(60,39) = RC(163)*Y(41)
        JAC(60,41) = RC(163)*Y(39)
        JAC(60,60) = -RC(164)*Y(1) - RC(85)*Y(15)
        JAC(60,64) = RC(147)*Y(37)
        JAC(61,1) = -RC(162)*Y(61)
        JAC(61,15) = -RC(85)*Y(61)
        JAC(61,37) = RC(161)*Y(55) + RC(149)*Y(65)
        JAC(61,55) = RC(161)*Y(37)
        JAC(61,61) = -RC(162)*Y(1) - RC(85)*Y(15)
        JAC(61,65) = RC(149)*Y(37)
        JAC(62,15) = RC(85)*Y(45)
        JAC(62,37) = -RC(148)*Y(62)
        JAC(62,45) = RC(85)*Y(15)
        JAC(62,62) = -RC(148)*Y(37) - RC(52)
        JAC(63,15) = RC(85)*Y(59)
        JAC(63,37) = -RC(146)*Y(63)
        JAC(63,59) = RC(85)*Y(15)
        JAC(63,63) = -RC(146)*Y(37) - RC(52)
        JAC(64,15) = RC(85)*Y(60)
        JAC(64,37) = -RC(147)*Y(64)
        JAC(64,60) = RC(85)*Y(15)
        JAC(64,64) = -RC(147)*Y(37) - RC(52)
        JAC(65,15) = RC(85)*Y(61)
        JAC(65,37) = -RC(149)*Y(65)
        JAC(65,61) = RC(85)*Y(15)
        JAC(65,65) = -RC(149)*Y(37) - RC(52)
        JAC(66,15) = RC(85)*Y(57)
        JAC(66,37) = -RC(149)*Y(66)
        JAC(66,57) = RC(85)*Y(15)
        JAC(66,66) = -RC(149)*Y(37) - RC(52)
        RETURN
      END SUBROUTINE JACD

      SUBROUTINE INIT(NEQ,T,Y)
        IMPLICIT NONE
        INTEGER NEQ
        DOUBLE PRECISION T, Y(NEQ)
        INTEGER I
!     Establishment of initial conditions:
!     Completely arbitrary at this stage !
        DO I = 1, 13
          Y(I) = 1.D7
        END DO
        DO I = 14, NEQ
          Y(I) = 100.D0
        END DO
        Y(1) = 1.0D9
        Y(2) = 5.0D9
        Y(3) = 5.0D9
        Y(4) = 3.8D12
        Y(5) = 3.5D13
        Y(14) = 5.D11
        Y(38) = 1.D-3
        RETURN
      END SUBROUTINE INIT

      SUBROUTINE EMEPCF(TIME,RC,DJ,H2O)
        INTEGER NSPEC, NRC, NDJ
        DOUBLE PRECISION TIME, HMIX
        PARAMETER (NSPEC=66,NRC=266,NDJ=16)
        PARAMETER (HMIX=1.2D5)
        DOUBLE PRECISION RC(NRC), DJ(NDJ), H2O

! Compute time-dependent EMEP coefficients
!   RC: reaction coefficients
!   DJ: dissociation rate coefficient
!   H2O water vapour concentrations

! A and B: DJ=A*exp(-B*SEC)
!    SEC = 1/cos(THETA) where THETA is solar zenith angle
! T temperature in K

        DOUBLE PRECISION A(NDJ), B(NDJ), SEC, T

        INTEGER ITIMEH, I24HRS, I
        DOUBLE PRECISION TIMEH, TIMEOD, PI, XLHA, FI, DEKL, XQ, RH, XZ
        DOUBLE PRECISION M, O2, XN2, DELTA

        DATA A/1.23D-3, 2.00D-4, 1.45D-2, 2.20D-5, 3.00D-6, 5.40D-5, 6.65D-5, &
          1.35D-5, 2.43D-5, 5.40D-4, 2.16D-4, 5.40D-5, 3.53D-2, 8.94D-2, &
          3.32D-5, 2.27D-5/
        DATA B/0.60D0, 1.40D0, 0.40D0, 0.75D0, 1.25D0, 0.79D0, 0.60D0, 0.94D0, &
          0.88D0, 0.79D0, 0.79D0, 0.79D0, 0.081D0, 0.059D0, 0.57D0, 0.62D0/

        TIMEH = TIME/3600.D0
        ITIMEH = INT(TIMEH)
        I24HRS = ITIMEH/24 + 1
        TIMEOD = TIMEH - (I24HRS-1)*24.D0

! Meteorology

        PI = 4.D0*ATAN(1.0D0)
!   XLHA local hour angle
        XLHA = (1.D0+TIMEOD*3600.D0/4.32D4)*PI
!   FI (Norwegian PHI!) latitude, dekl solar declination
!   here latitude = 50 deg. N
        FI = 50.D0*PI/180.D0
        DEKL = 23.5D0*PI/180.D0
        SEC = 1.D0/(COS(XLHA)*COS(FI)*COS(DEKL)+SIN(FI)*SIN(DEKL))
!   def of temperature variation
!     XP=8.7D-5*TIMEOD*3600.D0-2.83
!     T=8.3D0*SIN(XP)+289.86
!   for simplicity
        T = 298.D0

!   def of water vapor concentration
        XQ = -7.93D-5*TIMEOD*3600.D0 + 2.43D0
        RH = 23.D0*SIN(XQ) + 66.5D0

        XZ = (597.3D0-0.57D0*(T-273.16D0))*18.D0/1.986D0* &
          (1.D0/T-1.D0/273.16D0)
        H2O = 6.1078D0*EXP(-XZ)*10.D0/(1.38D-16*T)*RH

! Calculate  values of photolysis rates DJ(1..16), based
!   upon RGD A & B coefficients and correction factors from HOUGH (1988)

        IF (TIMEOD<4.0D0 .OR. TIMEOD>=20.D0) THEN
!   in the dark:
          DO I = 1, NDJ
            DJ(I) = 1.D-40
          END DO
        ELSE
!   daytime:
          DO I = 1, NDJ
            DJ(I) = A(I)*EXP(-B(I)*SEC)
            IF (DJ(I)<0.0D0) STOP 'DJ'
          END DO
        END IF

! Set up chemical reaction rate coefficients:
!     16/6/92: inclusion of M, N2, O2 values in rate-constants
!     reaction rate coefficient definition. units: 2-body reactions
!     cm**3/(molecule x s), unimolecular 1.D0/s, 3-body
!     cm**6/(molecule**2 x s)

        M = 2.55D19
        O2 = 5.2D18
        XN2 = 1.99D19

        DO I = 1, NRC
          RC(I) = 0.D0
        END DO


!..A92, assuming 80% N2, 20% O2
        RC(1) = 5.7D-34*(T/300.0D0)**(-2.8D0)*M*O2
!..unchanged, as source of O+NO+O2 reaction rate unknown.
        RC(5) = 9.6D-32*(T/300.0D0)**(-1.6D0)*M
!..estimate from Demore(1990) (=A92) O(d)+N2 and DeMore O(D)+O2
        RC(7) = 2.0D-11*EXP(100.D0/T)*M
!.. A92    >>>>>
        RC(11) = 1.8D-12*EXP(-1370.D0/T)
        RC(12) = 1.2D-13*EXP(-2450.D0/T)
        RC(13) = 1.9D-12*EXP(-1000.D0/T)
        RC(14) = 1.4D-14*EXP(-600.D0/T)
        RC(15) = 1.8D-11*EXP(+110.D0/T)
        RC(17) = 3.7D-12*EXP(240.0D0/T)
!.. <<<<<<    A92
!..M.J (from Wayne et al.)
        RC(19) = 7.2D-14*EXP(-1414.D0/T)
!fix  rc(19) =7.2d-13*exp(-1414.D0/t)
!..M.J suggests that rc(27) be omitted:
!     rc(27) =8.5d-13*exp(-2450.D0/t)
!..  My change to get similar to A92 troe.
        RC(29) = 7.1D14*EXP(-11080.D0/T)
!..A92
        RC(30) = 4.8D-11*EXP(+250.D0/T)
!..A92, De More,1990 .. no change : oh + h2o2
        RC(31) = 2.9D-12*EXP(-160.D0/T)
!..A92 : oh + h2
        RC(33) = 7.7D-12*EXP(-2100.D0/T)
!..My, similar to DeMore et al complex : oh+hno3
        RC(35) = 1.0D-14*EXP(785.0D0/T)
!.. Mike Jenkin's suggestion for ho2 + ho2 reactions: (from DeMore et al
        RC(36) = 2.3D-13*EXP(600.D0/T)
        RC(36) = RC(36) + M*1.7D-33*EXP(1000.D0/T)
        RC(36) = RC(36)*(1.D0+1.4D-21*H2O*EXP(2200.D0/T))

!..A92
        RC(59) = 3.9D-12*EXP(-1885.D0/T)
! A92 : ch3o2 + no
        RC(60) = 4.2D-12*EXP(180.D0/T)
! A92 + A90 assumption that ka = 0.5D0 * k
        RC(61) = 5.5D-14*EXP(365.D0/T)
! A92 + A90 assumption that kb = 0.5D0 * k
        RC(62) = 5.5D-14*EXP(365.D0/T)
! A92
        RC(63) = 3.3D-12*EXP(-380.D0/T)
! A92 : ho2 + ch3o2
        RC(65) = 3.8D-13*EXP(780.D0/T)
! A92 new: ch3ooh + oh -> hcho + oh
        RC(67) = 1.0D-12*EXP(190.D0/T)
! A92 new: ch3ooh + oh -> ch3o2
        RC(68) = 1.9D-12*EXP(190.D0/T)
!.. A92
        RC(71) = 7.8D-12*EXP(-1020.D0/T)
! A92 new: c2h5o2 + ho2 -> c2h5ooh (r2ooh)
        RC(74) = 6.5D-13*EXP(650.D0/T)
!.. A92
        RC(75) = 5.6D-12*EXP(310.D0/T)
! TOR90 assumption w.r.t. rc(67) : c2h5ooh + oh -> ch3cho + oh
!     rc(76) = 5.8 * rc(67)
        RC(76) = 5.8D-12*EXP(190.D0/T)
!A92 : approximation to troe expression
        RC(78) = 1.34D16*EXP(-13330.D0/T)
! additional reactions :-
! A92 : ho2 + ch3coo2 -> rco3h
        RC(88) = 1.3D-13*EXP(1040.D0/T)
! A92 : ho2 + ch3coo2 -> rco2h + o3
        RC(89) = 3.0D-13*EXP(1040.D0/T)
!.. A92
        RC(94) = 2.8D-12*EXP(530.D0/T)
!.. D & J, gives results very close to A92
        RC(81) = 1.64D-11*EXP(-559.D0/T)

!.. A90
        RC(83) = RC(60)
        RC(105) = RC(60)
!A90
        RC(110) = RC(60)
!A90/PS  isnir + no
        RC(162) = RC(60)
!A90/PS  isono3 + no
        RC(164) = RC(60)

!.. From RGD, but very similar to A92 Troe
        RC(109) = 1.66D-12*EXP(474.D0/T)
!A92
        RC(112) = 1.2D-14*EXP(-2630.D0/T)
!A92
        RC(123) = 6.5D-15*EXP(-1880.D0/T)
!A90
        RC(126) = RC(60)
!..A90
        RC(220) = RC(60)
!..A90
        RC(236) = RC(60)
!ooooooooooooo   natural voc reactions 00000000000000000
!..A90  isoprene + o3 -> products
        RC(150) = 12.3D-15*EXP(-2013.D0/T)
!..A90  isoprene + oh -> isro2
        RC(151) = 2.54D-11*EXP(410.D0/T)
!A90  isoprene-RO2 + no
        RC(152) = RC(60)
!A90  methylvinylketone (mvk) + oh
        RC(153) = 4.13D-12*EXP(452.D0/T)
!A90  mvko2 + no
        RC(154) = RC(60)
!A90  macr + oh
        RC(158) = 1.86D-11*EXP(175.D0/T)
!A90  ch2cch3 + no
        RC(159) = RC(60)
!A90  mvk + o3
        RC(160) = 4.32D-15*EXP(-2016.D0/T)

!     rc(255) = 9.9d-16*exp(-731/t)
!.......................................................................

!oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
!     aerosol formation and depositions ..x
!     parameterization of heterogeneous loss in 1./s for humidities
!     less than 90%.  applied to hno3, h2o2, h2so4, ch3o2h.
        RC(43) = 5.D-6
        IF (RH>0.90D0) RC(43) = 1.D-4
        RC(44) = RC(43)
        RC(45) = RC(43)

!.. A92
        RC(8) = 2.2D-10
!.. My (?) , similar to A92 troe expression.
        RC(20) = 1.4D-12
!.. My (?) , similar to A92 troe expression.
        RC(21) = 1.4D-11
!.. RGD Note (=DeMore?)
        RC(26) = 4.1D-16
!.. RGD
        RC(39) = 1.35D-12
!.. RGD
        RC(40) = 4.0D-17
!.. A92, assuming all products are ch3cho
        RC(64) = 3.2D-12
!.. A92, with temperature dependance neglected.
        RC(66) = 9.6D-12
!.. A92 >>>>>>>>
        RC(69) = 5.8D-16
        RC(70) = 2.4D-13
        RC(72) = 8.9D-12
!.. <<<<<<<<<< A92
!.. A92 : approximation to troe expression
        RC(77) = 1.0D-11
!.. A92 : ch3coo2 + no
        RC(79) = 2.0D-11
!.. A92 : sum of two pathways
        RC(80) = 1.1D-11
!mj   rc(84) =2.5d-14
!ya   kho2ro2 : estimate of rate of ho2 + higher RO2, suggested by Yvonn
        RC(85) = 1.0D-11
!..A90, ignoring slight temperature dependance
        RC(86) = 1.15D-12
!..MJ suggestion.. combine oh + secc4h9o2, meko2 rates   =>
!     rc(87)= rc(68) + rc(86), approx. at 298
        RC(87) = 4.8D-12
!mj..new A92 : oh + ch3co2h -> ch3o2
        RC(90) = 8.0D-13
!.. Approximates to A92 Troe ...
        RC(125) = 2.86D-11
!.. rate for ch2chr + oh, = k68+k125 (propene), Yv.
        RC(146) = 3.2D-11
!.. rate for isono3h + oh, = k156 (isro2h), Yv.
        RC(147) = 2.0D-11
!.. MY GUESS rate of oh + mvko2h
        RC(148) = 2.2D-11
!.. MY GUESS rate of oh + other biogenic ooh
        RC(149) = 3.7D-11
!ya   kho2ro2 : estimate of rate of ho2 + higher RO2, suggested by Yvonn
!A90  isro2 + ho2
        RC(155) = RC(85)
!PS   isro2h + oh
        RC(156) = 2.0D-11
!PS   isro2h + o3
        RC(157) = 8.0D-18
!PS   isni + oh
        RC(161) = 3.35D-11
!PS   isopre + no3
        RC(163) = 7.8D-13
!ZZ   rc(163) =7.8d-16
!.. Unchanged, also in IVL scheme
        RC(219) = 2.0D-11
!..A92
        RC(221) = 1.1D-11
!..A92
        RC(222) = 1.7D-11
!..MJ suggestion.. combine oh + malo2h rates   =>
!     rc(223)= rc(68) + rc(219)
        RC(223) = 2.4D-11
!..A90
        RC(234) = 1.37D-11
!..MJ suggestion.. combine rc(68) with rc(234) =>
!     rc(235)= rc(68) + rc(234)
        RC(235) = 1.7D-11

!..............................................
!         deposition loss rate coefficients vd/hmix, vd in cm/s.
!         hno3     calculated     rc(46)
!         so2      0.5            rc(47)
!         h2o2     0.5            rc(47)
!         no2      0.2            rc(48)
!         o3       0.5            rc(49)
!         pan      0.2            rc(50)
!         h2so4    0.1            rc(51)
!... simple approx. for now - reduce all vg by 4 at night to allow
!    for surface inversion......
        DELTA = 1.0D0
        IF (NIGHT_OR_DAY==1) DELTA = 0.25D0
!     IF (timeod.ge.20.D0.or.timeod.lt.4.D0) delta = 0.25D0
!         if(timeod.ge.20.D0.or.timeod.le.4.D0) delta=0.25D0
!         if (night) delta=0.25D0
!     Changed:
        RC(46) = 2.0D0*DELTA/HMIX
        RC(47) = 0.5D0*DELTA/HMIX
        RC(48) = 0.2D0*DELTA/HMIX
        RC(49) = 0.5D0*DELTA/HMIX
        RC(50) = 0.2D0*DELTA/HMIX
        RC(51) = 0.1D0*DELTA/HMIX
!. dep. of organic peroxides = 0.5 cms-1
        RC(52) = 0.5D0*DELTA/HMIX
!. dep. of ketones, RCHO  = 0.3 cms-1
        RC(53) = 0.3D0*DELTA/HMIX

        RETURN
      END SUBROUTINE EMEPCF

      SUBROUTINE SOLUT(NEQ,T,Y)
        IMPLICIT NONE
        INTEGER NEQ
        DOUBLE PRECISION T, Y(NEQ)

        Y(1) = 0.25645805093601D+08
        Y(2) = 0.51461347708556D+11
        Y(3) = 0.23156799577319D+12
        Y(4) = 0.11309365994733D+14
        Y(5) = 0.34592853260350D+14
        Y(6) = 0.10272365509751D+12
        Y(7) = 0.85087355868443D+11
        Y(8) = 0.41312856746209D+10
        Y(9) = 0.12709378547180D+10
        Y(10) = 0.56328808904060D+10
        Y(11) = 0.82638215527479D+11
        Y(12) = 0.32935526589172D+11
        Y(13) = 0.10540581862144D+12
        Y(14) = 0.31503085853931D+13
        Y(15) = 0.24883839367388D+08
        Y(16) = 0.10975656154077D+11
        Y(17) = 0.13651965576432D+11
        Y(18) = 0.38520482305080D+12
        Y(19) = 0.11374627216097D+09
        Y(20) = 0.28509822059077D+11
        Y(21) = 0.37869336198310D+12
        Y(22) = 0.38323843339068D+10
        Y(23) = 0.48049399025208D+07
        Y(24) = 0.33905460811715D+08
        Y(25) = 0.34944529300020D+11
        Y(26) = 0.14905763708553D+08
        Y(27) = 0.10005678719131D+08
        Y(28) = 0.18567530620184D+10
        Y(29) = 0.22722061569502D+07
        Y(30) = 0.51683758837725D+10
        Y(31) = 0.23175959524799D+07
        Y(32) = 0.30351396570102D+10
        Y(33) = 0.59289794793138D+07
        Y(34) = 0.25152972906931D+10
        Y(35) = 0.45696782763907D+07
        Y(36) = 0.35165924336687D+01
        Y(37) = 0.40635893499335D+05
        Y(38) = 0.38043227330586D-36
        Y(39) = 0.55775907287099D+09
        Y(40) = 0.76845966195032D+09
        Y(41) = 0.53384917779330D+10
        Y(42) = 0.60486040253666D+12
        Y(43) = 0.42222372170072D+08
        Y(44) = 0.36818527669954D+10
        Y(45) = 0.58357315681736D+07
        Y(46) = 0.14565071591335D+10
        Y(47) = 0.88628686841955D+10
        Y(48) = 0.93387533127053D+09
        Y(49) = 0.35957535867357D+10
        Y(50) = 0.46224275841192D+09
        Y(51) = 0.23959100649045D+09
        Y(52) = 0.60809365864182D+10
        Y(53) = 0.77041082974937D+09
        Y(54) = 0.33862208607592D+10
        Y(55) = 0.18382643870322D+10
        Y(56) = 0.19732523461142D+10
        Y(57) = 0.13134039409286D+07
        Y(58) = 0.13444717645883D+10
        Y(59) = 0.25684831172265D+07
        Y(60) = 0.11318545919859D+10
        Y(61) = 0.45915379379973D+07
        Y(62) = 0.86093822830561D+09
        Y(63) = 0.35095609713139D+09
        Y(64) = 0.66823509701060D+09
        Y(65) = 0.32073564029578D+09
        Y(66) = 0.11449876348727D+09
        RETURN
      END SUBROUTINE SOLUT

    END MODULE EMEP_DEMO

!******************************************************************

    PROGRAM EMEP

      USE EMEP_DEMO
      USE DVODE_F90_M

!     Type declarations:
      IMPLICIT NONE
!     Number of odes and number of event functions for this problem:
      INTEGER, PARAMETER :: NEQ = 66, NTPLOT = 103
      INTEGER ITASK, ISTATE, ISTATS, I, ITPLOT, MTPLOT
      DOUBLE PRECISION ATOL, RTOL, RSTATS, T, TOUT, Y, EPS, TFINAL, DELTAT, &
        YOUT, ERRMAX
      DIMENSION Y(NEQ), RSTATS(22), ISTATS(31), YOUT(NEQ)

      TYPE (VODE_OPTS) :: OPTIONS

!     Open the output file and the plot files:
      OPEN (UNIT=6,FILE='demoemep.dat')
      OPEN (UNIT=7,FILE='demoemepplot.dat')
      EPS = 1D-6
      EPS = 1D-12
      WRITE (6,90008) EPS
!     Set the integration parameters:
      NIGHT_OR_DAY = 1
      SWITCH = 0
      TSWITCH(0) = 4D0*3600D0
      TSWITCH(1) = 20D0*3600D0
      DO I = 1, 4
        TSWITCH(2*I) = TSWITCH(0) + 24D0*3600D0*DBLE(I)
        TSWITCH(2*I+1) = TSWITCH(1) + 24D0*3600D0*DBLE(I)
      END DO
      WRITE (6,90000) (TSWITCH(I),I=0,9)
90000 FORMAT (' TSWITCH follows: ',/,(D15.5))
      T = 0.0D0
      CALL INIT(NEQ,T,Y)
      TFINAL = 5D0
      TFINAL = 417600D0
      TFINAL = TSWITCH(9)
      MTPLOT = NTPLOT
      MTPLOT = NTPLOT - 2
      DELTAT = TFINAL/DBLE(NTPLOT-2)
      DELTAT = MIN(DELTAT,TSWITCH(1))
      TOUT = DELTAT
      RTOL = EPS
      ATOL = 1D0
      ATOL = EPS
      ITASK = 4
      ISTATE = 1
!     Plot variables:
      ITPLOT = 1
      WRITE (7,90003) TOUT, Y(1), Y(2), Y(3), Y(5), Y(14), Y(40)
!     Set the VODE_F90 options:
      OPTIONS = SET_OPTS(DENSE_J=.TRUE.,USER_SUPPLIED_JACOBIAN=.TRUE., &
        RELERR=RTOL,ABSERR=ATOL,MXSTEP=100000,TCRIT=TSWITCH(SWITCH))
10    CONTINUE
!     Perform the integration:
      CALL DVODE_F90(DERIVS,NEQ,Y,T,TOUT,ITASK,ISTATE,OPTIONS,J_FCN=JACD)
!     Gather and write the integration statistics for this problem:
      CALL GET_STATS(RSTATS,ISTATS)
!     Stop the integration if an error occurred:
      IF (ISTATE<0) THEN
        WRITE (6,90006) ISTATE
        STOP
      END IF
      ITPLOT = ITPLOT + 1
      IF (ITPLOT<=MTPLOT+2) THEN
        WRITE (7,90003) TOUT, Y(1), Y(2), Y(3), Y(5), Y(14), Y(40)
      END IF
      IF (TOUT>=TFINAL) GO TO 20
!     IF (TOUT == TSWITCH(SWITCH)) THEN
      IF (ABS(TOUT-TSWITCH(SWITCH))<=0.0D0) THEN
        WRITE (6,90010) TOUT, SWITCH, ISTATE
!        Restart the integration at the switch times :
        ITASK = 4
        ISTATE = 1
        T = TOUT
        SWITCH = SWITCH + 1
        NIGHT_OR_DAY = -NIGHT_OR_DAY
        IF (SWITCH>9) THEN
          WRITE (6,90001)
90001     FORMAT (' Illegal value of SWITCH.')
        END IF
!        Need to reset the TCRIT time to the next switch point:
        OPTIONS = SET_OPTS(DENSE_J=.TRUE.,USER_SUPPLIED_JACOBIAN=.TRUE., &
          RELERR=RTOL,ABSERR=ATOL,MXSTEP=100000,TCRIT=TSWITCH(SWITCH))
        TOUT = TOUT + DELTAT
        TOUT = MIN(TOUT,TSWITCH(SWITCH))
        TOUT = MIN(TOUT,TFINAL)
        GO TO 10
      END IF
      IF (ITPLOT<=MTPLOT+1) THEN
        TOUT = TOUT + DELTAT
        TOUT = MIN(TOUT,TSWITCH(SWITCH))
        TOUT = MIN(TOUT,TFINAL)
        GO TO 10
      END IF
20    CONTINUE
!     Write the integration final root finding statistics for
!     this problem:
      WRITE (6,90005) ISTATS(11), ISTATS(12), ISTATS(13)
      CALL SOLUT(NEQ,TOUT,YOUT)
      ERRMAX = 0D0
      DO I = 1, NEQ
        IF (I/=36 .AND. I/=38) THEN
!           ERRMAX = MAX(ERRMAX,ABS(Y(I)-YOUT(I))/(ATOL+RTOL*ABS(YOUT(I))))
          ERRMAX = MAX(ERRMAX,ABS(Y(I)-YOUT(I))/(ATOL+ABS(YOUT(I))))
          WRITE (6,90002) I, Y(I), YOUT(I), ERRMAX
90002     FORMAT (I5,3D20.10)
        ELSE
          WRITE (6,90002) I, Y(I), YOUT(I), ERRMAX
        END IF
      END DO
      WRITE (6,90009) TOUT, ERRMAX
      STOP

!     Format statements for this problem:
90003 FORMAT ((7D25.15))
90005 FORMAT (' Steps = ',I4,' f-s = ',I4,' J-s = ',I4)
90006 FORMAT (' An error occurred in VODE_F90. ISTATE = ',I3)
90008 FORMAT (/' Results for ATOL = RTOL = ',D15.5,':')
90009 FORMAT (' Maximum relative error at t = ',D15.5,' is ',D15.5)
90010 FORMAT (' Time = ',D25.15,' Switch = ',I3,' ISTATE = ',I3)

    END PROGRAM EMEP
