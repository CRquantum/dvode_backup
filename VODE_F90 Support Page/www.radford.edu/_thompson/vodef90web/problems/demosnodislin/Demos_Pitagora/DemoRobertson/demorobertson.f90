! VODE_F90 demonstration program
! Nonnegativity of the solution components is enforced for the
! Robertson problem.

    MODULE ROBERTSON_DEMO

      IMPLICIT NONE

    CONTAINS

      SUBROUTINE DERIVS(NEQ,T,Y,DY)
!     Subroutine to evaluate dy/dt for this problem
        IMPLICIT NONE
        INTEGER NEQ
        DOUBLE PRECISION T, Y, DY
        DIMENSION Y(NEQ), DY(NEQ)

        DY(1) = -0.04D0*Y(1) + 1D4*Y(2)*Y(3)
        DY(2) = 0.04D0*Y(1) - 1D4*Y(2)*Y(3) - 3D7*Y(2)**2
        DY(3) = 3D7*Y(2)**2
        RETURN
      END SUBROUTINE DERIVS

      SUBROUTINE JACD(NEQ,T,Y,ML,MU,PD,NROWPD)
!     Subroutine to define the exact Jacobian for this problem
        IMPLICIT NONE
        INTEGER NEQ, ML, MU, NROWPD
        DOUBLE PRECISION T, Y, PD
        DIMENSION Y(NEQ), PD(NROWPD,NEQ)

        PD(1:NEQ,1:NEQ) = 0D0
        PD(1,1) = -0.04D0
        PD(1,2) = 1D4*Y(3)
        PD(1,3) = 1D4*Y(2)
        PD(2,1) = 0.04D0
        PD(2,2) = -1D4*Y(3) - 6D7*Y(2)
        PD(2,3) = -1D4*Y(2)
        PD(3,1) = 0D0
        PD(3,2) = 6D7*Y(2)
        PD(3,3) = 0D0
        RETURN
      END SUBROUTINE JACD

    END MODULE ROBERTSON_DEMO

!******************************************************************

    PROGRAM ROBERTSON

      USE ROBERTSON_DEMO
      USE DVODE_F90_M

!     Program declarations:
      IMPLICIT NONE
!     Number of odes for this problem:
      INTEGER, PARAMETER :: NEQ = 3
      INTEGER ITASK, ISTATE, IOUT, ISTATS, NOUT, I
      DOUBLE PRECISION RSTATS, T, TOUT, Y, DELTAT, AERRSEND, OVERRUN, TRATIO
      DIMENSION RSTATS(22), ISTATS(31)
      DIMENSION Y(NEQ), AERRSEND(NEQ), OVERRUN(NEQ)
      INTEGER BOUNDED_COMPONENTS
      DOUBLE PRECISION ABSERR_TOLERANCES, RELERR_TOLERANCES, LOWER_BOUNDS, &
        UPPER_BOUNDS
      DIMENSION RELERR_TOLERANCES(NEQ), ABSERR_TOLERANCES(NEQ), &
        BOUNDED_COMPONENTS(NEQ), LOWER_BOUNDS(NEQ), UPPER_BOUNDS(NEQ)

      TYPE (VODE_OPTS) :: OPTIONS

!     Define the output file and the plot file:
      OPEN (UNIT=6,FILE='demorobertson.dat')
      OPEN (UNIT=3,FILE='demorobertsonplot.dat')

!     Set the VODE_F90 parameters:
      RELERR_TOLERANCES(1:NEQ) = 1.0D-6
      ABSERR_TOLERANCES(1:NEQ) = 1.0D-6
      DO I = 1, NEQ
        BOUNDED_COMPONENTS(I) = I
      END DO
      LOWER_BOUNDS(1:NEQ) = 0.0D0
      UPPER_BOUNDS(1:NEQ) = 1.0D50
      ITASK = 1
      ISTATE = 1
!     Set the initial conditions:
      Y(1) = 1.0D0
      Y(2) = 0.0D0
      Y(3) = 0.0D0
!     Set the output point information for this problem:
      T = 0.0D0
      DELTAT = 0.4D-4
      TOUT = DELTAT
      NOUT = 100
      TRATIO = 10.0D0**(15.0D0/100.0D0)

!     Set the options to enforce bounds for this problem:
!     Note:
!     You can replace the first line with the following if you wish
!     to use an exact Jacobian:
!     OPTIONS=SET_OPTS(DENSE_J=.TRUE.,USER_SUPPLIED_JACOBIAN=.TRUE.,   &

      OPTIONS = SET_OPTS(DENSE_J=.TRUE.,RELERR_VECTOR=RELERR_TOLERANCES, &
        ABSERR_VECTOR=ABSERR_TOLERANCES,CONSTRAINED=BOUNDED_COMPONENTS, &
        CLOWER=LOWER_BOUNDS,CUPPER=UPPER_BOUNDS)

!     Perform the integration:
      DO IOUT = 1, NOUT
        CALL DVODE_F90(DERIVS,NEQ,Y,T,TOUT,ITASK,ISTATE,OPTIONS,J_FCN=JACD)
!        Gather and write the integration statistics for this problem:
        CALL GET_STATS(RSTATS,ISTATS)
        WRITE (6,90001) ISTATE, ISTATS(11), ISTATS(12), ISTATS(13)
!        Write the plot information for this problem:
        WRITE (3,90003) TOUT, Y(1), Y(2), Y(3)
!        Define the next output point:
        TOUT = TOUT*TRATIO
!        Stop the integration if an error occurred:
        IF (ISTATE<0) THEN
          PRINT *, ' The integration was not successful.'
          GO TO 10
        END IF
      END DO

!     Check the accuracy of the final solution for this problem:
      AERRSEND(1) = ABS(Y(1)-0.2083340149701255D-07)
      AERRSEND(2) = ABS(Y(2)-0.8333360770334713D-13)
      AERRSEND(3) = ABS(Y(3)-0.9999999791665050D+00)
      DO I = 1, NEQ
        OVERRUN(I) = 1.0D0
        IF (Y(I)/=0.0D0) OVERRUN(I) = AERRSEND(I)/Y(I)
      END DO
      WRITE (6,90000) T
      PRINT *, ' TIME = ', T, ' Y, AERRSEND, OVERRUN: '
      DO I = 1, NEQ
        WRITE (6,90002) Y(I), AERRSEND(I), OVERRUN(I)
        PRINT *, Y(I), AERRSEND(I), OVERRUN(I)
      END DO
      PRINT *, ' ISTATE = ', ISTATE
      PRINT *, ' The integration was successful.'

10    CONTINUE

!     Format statements for this problem:
90000 FORMAT (' TIME =',D12.4,' Y, AERRSEND, OVERRUN: ')
90001 FORMAT (/' ISTATE = ',I3,' No. steps =',I4,'  No. f-s =',I4, &
        ' No. J-s =',I4)
90002 FORMAT (3D15.5)
90003 FORMAT (4D15.5)
      STOP

    END PROGRAM ROBERTSON

