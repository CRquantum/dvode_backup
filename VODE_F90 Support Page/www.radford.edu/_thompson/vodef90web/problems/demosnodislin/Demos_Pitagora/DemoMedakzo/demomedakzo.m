function [t,y] = demomedakzo
% Matlab script to plot the solution for the demomedakzo problem

tempx = load('demomedakzoplotx.dat','-ascii');
tempt = load('demomedakzoplott.dat','-ascii');
tempuv = load('demomedakzoplotuv.dat','-ascii');
x = tempx(:);
t = tempt(:);
nxplot = size(x,1);
ntplot = size(t,1);
uvec = tempuv(:,1);
vvec = tempuv(:,2);
u = reshape(uvec,nxplot,ntplot);
v = reshape(vvec,nxplot,ntplot);

figure
title('Medakzo Problem. u(x,t)')
view(50,30)
surf(x,t,u)
xlabel('x');
ylabel('t');

figure
title('Medakzo Problem. v(x,t)')
view(50,30)
surf(x,t,v)
xlabel('x');
ylabel('t');
