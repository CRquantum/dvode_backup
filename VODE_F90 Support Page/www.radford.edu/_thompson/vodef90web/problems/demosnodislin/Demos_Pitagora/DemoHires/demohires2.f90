! VODE_F90 demonstration program
! Generate solution on [0,321.8122]

! Test Set for IVP solvers
! http://www.dm.uniba.it/~testset/
! Problem HIRES

    MODULE HIRES_DEMO

      IMPLICIT NONE

    CONTAINS

      SUBROUTINE DERIVS(NEQ,T,Y,YDOT)
!     Subroutine to evaluate dy/dt for this problem
        IMPLICIT NONE
        INTEGER NEQ
        DOUBLE PRECISION T, Y, YDOT
        DIMENSION Y(NEQ), YDOT(NEQ)

        YDOT(1) = -1.71D0*Y(1) + 0.43D0*Y(2) + 8.32D0*Y(3) + 0.0007D0
        YDOT(2) = 1.71D0*Y(1) - 8.75D0*Y(2)
        YDOT(3) = -10.03D0*Y(3) + 0.43D0*Y(4) + 0.035D0*Y(5)
        YDOT(4) = 8.32D0*Y(2) + 1.71D0*Y(3) - 1.12D0*Y(4)
        YDOT(5) = -1.745D0*Y(5) + 0.43D0*(Y(6)+Y(7))
        YDOT(6) = -280D0*Y(6)*Y(8) + 0.69D0*Y(4) + 1.71D0*Y(5) - 0.43D0*Y(6) + &
          0.69D0*Y(7)
        YDOT(7) = 280D0*Y(6)*Y(8) - 1.81D0*Y(7)
        YDOT(8) = -YDOT(7)
        RETURN
      END SUBROUTINE DERIVS

      SUBROUTINE JACD(NEQ,T,Y,ML,MU,PD,NROWPD)
!     Subroutine to define the dense Jacobian for this problem
        IMPLICIT NONE
        INTEGER NEQ, ML, MU, NROWPD
        DOUBLE PRECISION T, Y, PD
        DIMENSION Y(NEQ), PD(NROWPD,NEQ)

        PD(1:NEQ,1:NEQ) = 0D0
        PD(1,1) = -1.71D0
        PD(1,2) = 0.43D0
        PD(1,3) = 8.32D0
        PD(2,1) = 1.71D0
        PD(2,2) = -8.75D0
        PD(3,3) = -10.03D0
        PD(3,4) = 0.43D0
        PD(3,5) = 0.035D0
        PD(4,2) = 8.32D0
        PD(4,3) = 1.71D0
        PD(4,4) = -1.12D0
        PD(5,5) = -1.745D0
        PD(5,6) = 0.43D0
        PD(5,7) = 0.43D0
        PD(6,4) = 0.69D0
        PD(6,5) = 1.71D0
        PD(6,6) = -280D0*Y(8) - 0.43D0
        PD(6,7) = 0.69D0
        PD(6,8) = -280D0*Y(6)
        PD(7,6) = 280D0*Y(8)
        PD(7,7) = -1.81D0
        PD(7,8) = 280D0*Y(6)
        PD(8,6) = -280D0*Y(8)
        PD(8,7) = 1.81D0
        PD(8,8) = -280D0*Y(6)
        RETURN
      END SUBROUTINE JACD

      SUBROUTINE JACS(NEQ,T,Y,IA,JA,NZ,P)
!     Subroutine to define the sparse Jacobian for this problem (just
!     loads dense)
        IMPLICIT NONE
        INTEGER NEQ, IA, JA, NZ, ML, MU, COL, ROW, ISTART, I, NZSAVE, NROWPD
        DOUBLE PRECISION T, Y, P, PD(8,8)
        DIMENSION Y(*), IA(*), JA(*), P(*)

        IF (NZ<=0) THEN
          NZ = NEQ*NEQ
          NZSAVE = NZ
          RETURN
        END IF
        IF (NZ/=NZSAVE) WRITE (6,*) ' NZ UNEQUAL TO NZSAVE IN JACS'
        IF (NZ/=NZSAVE) STOP
        ML = NEQ
        MU = NEQ
        NROWPD = NEQ
        CALL JACD(NEQ,T,Y,ML,MU,PD,NROWPD)
        IA(1) = 1
        DO I = 1, NEQ
          IA(I+1) = IA(I) + NEQ
        END DO
        I = 0
        DO COL = 1, NEQ
          I = NEQ*(COL-1)
          DO ROW = 1, NEQ
            I = I + 1
            JA(I) = ROW
          END DO
        END DO
        DO COL = 1, NEQ
          ISTART = NEQ*(COL-1)
          P(ISTART+1:ISTART+NEQ) = PD(:,COL)
        END DO
        RETURN
      END SUBROUTINE JACS
    END MODULE HIRES_DEMO

!******************************************************************

    PROGRAM HIRES

      USE HIRES_DEMO
      USE DVODE_F90_M

!     Type declarations:
      IMPLICIT NONE
!     Number of odes and number of event functions for this problem:
      INTEGER, PARAMETER :: NEQ = 8
      INTEGER ITASK, ISTATE, ISTATS, I, IEPS
      DOUBLE PRECISION ATOL, RTOL, RSTATS, T, TOUT, Y, ERRMAX, YOUT, EPS
      DIMENSION Y(NEQ), RSTATS(22), ISTATS(31), YOUT(NEQ)

      TYPE (VODE_OPTS) :: OPTIONS

!     Open the output file and the plot files:
      OPEN (UNIT=6,FILE='demohires2.dat')
      EPS = 1D-2
      DO IEPS = 1, 4
        EPS = EPS/1D2
        WRITE (6,90003) EPS
!     Set the initial conditions:
        Y(1) = 1D0
        Y(2:7) = 0D0
        Y(8) = 0.0057D0
!     Set the integration parameters:
        T = 0.0D0
        TOUT = 321.8122D0
        RTOL = EPS
        ATOL = EPS
        ITASK = 1
        ISTATE = 1
!     Set the VODE_F90 options:
        OPTIONS = SET_OPTS(DENSE_J=.TRUE.,USER_SUPPLIED_JACOBIAN=.TRUE., &
          RELERR=RTOL,ABSERR=ATOL,MXSTEP=10000)
!     OPTIONS = SET_OPTS(SPARSE_J=.TRUE.,USER_SUPPLIED_JACOBIAN=.TRUE.,&
!     RELERR=RTOL,ABSERR=ATOL,MXSTEP=10000)
!     Perform the integration:
        CALL DVODE_F90(DERIVS,NEQ,Y,T,TOUT,ITASK,ISTATE,OPTIONS,J_FCN=JACD)
!                    J_FCN=JACS)
!     Gather and write the integration statistics for this problem:
        CALL GET_STATS(RSTATS,ISTATS)
!     Stop the integration if an error occurred:
        IF (ISTATE<0) THEN
          WRITE (6,90001) ISTATE
          STOP
        END IF
!     Write the integration final root finding statistics for
!     this problem:
        WRITE (6,90000) ISTATS(11), ISTATS(12), ISTATS(13)
        YOUT(1) = 0.7371312573325668D-3
        YOUT(2) = 0.1442485726316185D-3
        YOUT(3) = 0.5888729740967575D-4
        YOUT(4) = 0.1175651343283149D-2
        YOUT(5) = 0.2386356198831331D-2
        YOUT(6) = 0.6238968252742796D-2
        YOUT(7) = 0.2849998395185769D-2
        YOUT(8) = 0.2850001604814231D-2
        ERRMAX = 0.0D0
        DO I = 1, NEQ
          ERRMAX = MAX(ERRMAX,ABS(Y(I)-YOUT(I)))
        END DO
        WRITE (6,90002) ERRMAX
      END DO ! IEPS
!     Format statements for this problem:
90000 FORMAT (' Steps = ',I4,' f-s = ',I4,' J-s = ',I4)
90001 FORMAT (' An error occurred in VODE_F90. ISTATE = ',I3)
90002 FORMAT (' Maximum absolute error at t = 321.8122 = ',D15.5)
90003 FORMAT (/' Results for ATOL = RTOL = ',D15.5,':')

    END PROGRAM HIRES
