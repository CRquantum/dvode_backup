function [t,y] = demohires
% Matlab script to plot the solution for the Hires problem
temp = load('demohiresplot.dat','-ascii');
ntout = 100;
t  = zeros(ntout);
y2 = zeros(ntout);
y3 = zeros(ntout);
y4 = zeros(ntout);
y5 = zeros(ntout);
y6 = zeros(ntout);
y7 = zeros(ntout);
y8 = zeros(ntout);
for i = 1:ntout
   istart = 9 * (i - 1) + 1;
   t(i)  = temp(istart);
   y1(i) = temp(istart+1);
   y2(i) = temp(istart+2);
   y3(i) = temp(istart+3);
   y4(i) = temp(istart+4);
   y5(i) = temp(istart+5);
   y6(i) = temp(istart+6);
   y7(i) = temp(istart+7);
   y8(i) = temp(istart+8);
end

figure
plot(t,y1,t,y2,t,y3,t,y4,t,y5,t,y6,t,y7,t,y8)
title('Hires Problem')

figure
plot(t,y1)
title('y_1')

figure
plot(t,y2)
title('y_2')

figure
plot(t,y3)
title('y_3')

figure
plot(t,y4)
title('y_4')

figure
plot(t,y5)
title('y_5')

figure
plot(t,y6)
title('y_6')

figure
plot(t,y7)
title('y_7')

figure
plot(t,y8)
title('y_8')
