! VODE_F90 demonstration program
! Generate plots of the solution components

! Test Set for IVP solvers
! http://www.dm.uniba.it/~testset/
! Problem OREGO

    MODULE OREGO_DEMO

      IMPLICIT NONE

    CONTAINS

      SUBROUTINE DERIVS(NEQ,T,Y,YDOT)
!     Subroutine to evaluate dy/dt for this problem
        IMPLICIT NONE
        INTEGER NEQ
        DOUBLE PRECISION T, Y, YDOT
        DIMENSION Y(NEQ), YDOT(NEQ)

        YDOT(1) = 77.27D0*(Y(2)+Y(1)*(1.D0-8.375D-6*Y(1)-Y(2)))
        YDOT(2) = (Y(3)-(1.D0+Y(1))*Y(2))/77.27D0
        YDOT(3) = 0.161D0*(Y(1)-Y(3))
        RETURN
      END SUBROUTINE DERIVS

      SUBROUTINE JACD(NEQ,T,Y,ML,MU,PD,NROWPD)
!     Subroutine to define the exact Jacobian for this problem
        IMPLICIT NONE
        INTEGER NEQ, ML, MU, NROWPD
        DOUBLE PRECISION T, Y, PD
        DIMENSION Y(NEQ), PD(NROWPD,NEQ)

        PD(1:NEQ,1:NEQ) = 0D0
        PD(1,1) = 77.27D0*(1.D0-2.D0*8.375D-6*Y(1)-Y(2))
        PD(1,2) = 77.27D0*(1.D0-Y(1))
        PD(1,3) = 0.D0
        PD(2,1) = -Y(2)/77.27D0
        PD(2,2) = -(1.D0+Y(1))/77.27D0
        PD(2,3) = 1.D0/77.27D0
        PD(3,1) = .161D0
        PD(3,2) = .0D0
        PD(3,3) = -.161D0
        RETURN
      END SUBROUTINE JACD

    END MODULE OREGO_DEMO

!******************************************************************

    PROGRAM OREGO

      USE OREGO_DEMO
      USE DVODE_F90_M

!     Type declarations:
      IMPLICIT NONE
!     Number of odes and number of event functions for this problem:
      INTEGER, PARAMETER :: NEQ = 3, NTOUT = 2001
      INTEGER ITASK, ISTATE, ISTATS, I, IPLOT
      DOUBLE PRECISION ATOL, RTOL, RSTATS, T, TOUT, Y, EPS, TFINAL, DELTAT, &
        YOUT, ERRMAX
      DIMENSION Y(NEQ), RSTATS(22), ISTATS(31), YOUT(NEQ)

      TYPE (VODE_OPTS) :: OPTIONS

!     Open the output file and the plot files:
      OPEN (UNIT=6,FILE='demoorego.dat')
      OPEN (UNIT=7,FILE='demooregoplot.dat')
      EPS = 1D-10
      WRITE (6,90004) EPS
!     Set the initial conditions:
      Y(1) = 1D0
      Y(2) = 2D0
      Y(3) = 3D0
!     Set the integration parameters:
      T = 0.0D0
      TFINAL = 360D0
      DELTAT = TFINAL/DBLE(NTOUT-1)
      WRITE (6,90000) T, Y(1:NEQ)
      TOUT = DELTAT
      IPLOT = 1
      WRITE (7,90000) T, Y(1:NEQ)
      RTOL = EPS
      ATOL = EPS
      ITASK = 1
      ISTATE = 1
!     Set the VODE_F90 options:
      OPTIONS = SET_OPTS(DENSE_J=.TRUE.,USER_SUPPLIED_JACOBIAN=.TRUE., &
        RELERR=RTOL,ABSERR=ATOL,MXSTEP=10000)
10    CONTINUE
!     Perform the integration:
      CALL DVODE_F90(DERIVS,NEQ,Y,T,TOUT,ITASK,ISTATE,OPTIONS,J_FCN=JACD)
!     Gather and write the integration statistics for this problem:
      CALL GET_STATS(RSTATS,ISTATS)
!     Stop the integration if an error occurred:
      IF (ISTATE<0) THEN
        WRITE (6,90003) ISTATE
        STOP
      END IF
      IPLOT = IPLOT + 1
      IF (IPLOT<=NTOUT) THEN
        WRITE (6,90000) TOUT, Y(1:NEQ)
        WRITE (7,90000) T, Y(1:NEQ)
        TOUT = TOUT + DELTAT
        TOUT = MIN(TOUT,TFINAL)
        GO TO 10
      END IF
!     Write the integration final root finding statistics for
!     this problem:
      WRITE (6,90002) ISTATS(11), ISTATS(12), ISTATS(13)
      YOUT(1) = 0.1000814870318523D+001
      YOUT(2) = 0.1228178521549917D+004
      YOUT(3) = 0.1320554942846706D+003
      WRITE (6,90000) TOUT, YOUT(1:NEQ)
      ERRMAX = 0.0D0
      DO I = 1, NEQ
        ERRMAX = MAX(ERRMAX,ABS((Y(I)-YOUT(I))/YOUT(I)))
      END DO
      WRITE (6,90001) ERRMAX
!     Format statements for this problem:
90000 FORMAT (4D15.5)
90001 FORMAT (' Maximum relative error at t = 360 = ',D15.5)
90002 FORMAT (' Steps = ',I10,' f-s = ',I10,' J-s = ',I10)
90003 FORMAT (' An error occurred in VODE_F90. ISTATE = ',I3)
90004 FORMAT (/' Results for ATOL = RTOL = ',D15.5,':')

    END PROGRAM OREGO
