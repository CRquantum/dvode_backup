function [t,y] = demoorego
% Matlab script to plot the solution for Pitagora Orego problem

temp = load('demooregoplot.dat','-ascii');
t  = temp(:,1);
y1 = temp(:,2);
y2 = temp(:,3);
y3 = temp(:,4); 

figure
semilogy(t,y1,t,y2,t,y3)
xlabel('t');
ylabel('log(y)');
title('Orego Problem')

figure
semilogy(t,y1)
xlabel('t');
ylabel('log(y_1)');
title('Orego Problem')

figure
semilogy(t,y2)
xlabel('t');
ylabel('log(y_2)');
title('Orego Problem')

figure
semilogy(t,y3)
xlabel('t');
ylabel('log(y_3)');
title('Orego Problem')
