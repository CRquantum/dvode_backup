function [t,y] = demoe5
% Matlab script to plot the solution for Pitagora E5 problem

temp = load('demoe5plot.dat','-ascii');
t  = temp(:,1)
y1 = temp(:,2);
y2 = temp(:,3);
y3 = temp(:,4); 
y4 = temp(:,5); 

floor = 1e-15;

figure
y1(:) = max(y1(:),floor);
loglog(t,y1)
xlabel('log(t)');
ylabel('log(y_1)');
title('E5 Problem')

figure
y2(:) = max(y2(:),floor);
loglog(t,y2)
xlabel('log(t)');
ylabel('log(y_2)');
title('E5 Problem')

figure
y3(:) = max(y3(:),floor);
loglog(t,y3)
xlabel('log(t)');
ylabel('log(y_3)');
title('E5 Problem')

figure
y4(:) = max(y4(:),floor);
loglog(t,y4)
xlabel('log(t)');
ylabel('log(y_4)');
title('E5 Problem')
