! VODE_F90 demonstration program

! Test Set for IVP solvers
! http://www.dm.uniba.it/~testset/
! Problem E5

    MODULE E5_DEMO

      IMPLICIT NONE

    CONTAINS

      SUBROUTINE DERIVS(NEQ,T,Y,YDOT)
!     Subroutine to evaluate dy/dt for this problem
        IMPLICIT NONE
        INTEGER NEQ
        DOUBLE PRECISION T, Y, YDOT
        DIMENSION Y(NEQ), YDOT(NEQ)
        DOUBLE PRECISION PROD1, PROD2, PROD3, PROD4

        PROD1 = 7.89D-10*Y(1)
        PROD2 = 1.1D7*Y(1)*Y(3)
        PROD3 = 1.13D9*Y(2)*Y(3)
        PROD4 = 1.13D3*Y(4)
        YDOT(1) = -PROD1 - PROD2
        YDOT(2) = PROD1 - PROD3
        YDOT(4) = PROD2 - PROD4
        YDOT(3) = YDOT(2) - YDOT(4)
        RETURN
      END SUBROUTINE DERIVS

      SUBROUTINE JACD(NEQ,T,Y,ML,MU,PD,NROWPD)
!     Subroutine to define the exact Jacobian for this problem
        IMPLICIT NONE
        INTEGER NEQ, ML, MU, NROWPD
        DOUBLE PRECISION T, Y, PD
        DIMENSION Y(NEQ), PD(NROWPD,NEQ)
        DOUBLE PRECISION A, B, CM, C

        A = 7.89D-10
        B = 1.1D7
        CM = 1.13D9
        C = 1.13D3
        PD(1,1) = -A - B*Y(3)
        PD(1,2) = 0.D0
        PD(1,3) = -B*Y(1)
        PD(1,4) = 0.D0
        PD(2,1) = A
        PD(2,2) = -CM*Y(3)
        PD(2,3) = -CM*Y(2)
        PD(2,4) = 0.D0
        PD(3,1) = A - B*Y(3)
        PD(3,2) = -CM*Y(3)
        PD(3,3) = -B*Y(1) - CM*Y(2)
        PD(3,4) = C
        PD(4,1) = B*Y(3)
        PD(4,2) = 0.D0
        PD(4,3) = B*Y(1)
        PD(4,4) = -C
        RETURN
      END SUBROUTINE JACD

    END MODULE E5_DEMO

!******************************************************************

    PROGRAM E5

      USE E5_DEMO
      USE DVODE_F90_M

!     Type declarations:
      IMPLICIT NONE
!     Note: The final integration time is 10**(ILAST).
      INTEGER, PARAMETER :: IFIRST = -3, ILAST = 3
      INTEGER, PARAMETER :: NEQ = 4, NTOUT = 9*(ILAST-IFIRST) + 2
      INTEGER ITASK, ISTATE, ISTATS, I, J, IPLOT
      DOUBLE PRECISION RSTATS, T, TOUT, Y, EPS, TPLOT, YOUT, ERRMAX, &
        RELERR_TOLERANCES, ABSERR_TOLERANCES, TI
      DIMENSION Y(NEQ), RSTATS(22), ISTATS(31), TPLOT(NTOUT), YOUT(NEQ)
      DIMENSION RELERR_TOLERANCES(NEQ), ABSERR_TOLERANCES(NEQ)
      TYPE (VODE_OPTS) :: OPTIONS

!     Open the output file and the plot files:
      OPEN (UNIT=6,FILE='demoe5.dat')
      OPEN (UNIT=7,FILE='demoe5plot.dat')
      EPS = 1D-13
      WRITE (6,90004) EPS
!     Set the initial conditions:
      Y(1) = 1.76D-3
      Y(2) = 0.0D0
      Y(3) = 0.0D0
      Y(4) = 0.0D0
!     Set the integration parameters:
      T = 0.0D0
      WRITE (6,90000) T, Y(1:NEQ)
      WRITE (7,90000) T, Y(1:NEQ)
      IPLOT = 1
      TPLOT(1) = T
      DO I = IFIRST, ILAST
        IPLOT = IPLOT + 1
        IF (I<0) THEN
          TPLOT(IPLOT) = 1D0/10D0**(-I)
        ELSE IF (I>0) THEN
          TPLOT(IPLOT) = 10D0**I
        ELSE
          TPLOT(IPLOT) = 1D0
        END IF
        TI = TPLOT(IPLOT)
        IF (I<ILAST) THEN
          DO J = 2, 9
            IPLOT = IPLOT + 1
            TPLOT(IPLOT) = DBLE(J)*TI
          END DO
        END IF
      END DO
      RELERR_TOLERANCES(1:NEQ) = EPS
      ABSERR_TOLERANCES(1:NEQ) = EPS
      ITASK = 1
      ISTATE = 1
!     Set the VODE_F90 options:
      OPTIONS = SET_OPTS(DENSE_J=.TRUE.,USER_SUPPLIED_JACOBIAN=.TRUE., &
        MXSTEP=100000,RELERR_VECTOR=RELERR_TOLERANCES, &
        ABSERR_VECTOR=ABSERR_TOLERANCES)
!     Perform the integration:
      DO IPLOT = 2, NTOUT
        TOUT = TPLOT(IPLOT)
        CALL DVODE_F90(DERIVS,NEQ,Y,T,TOUT,ITASK,ISTATE,OPTIONS,J_FCN=JACD)
!        Gather and write the integration statistics for this problem:
        CALL GET_STATS(RSTATS,ISTATS)
!        Stop the integration if an error occurred:
        IF (ISTATE<0) THEN
          WRITE (6,90003) ISTATE
          STOP
        END IF
!        Save the plot data:
        WRITE (6,90000) TOUT, Y(1:NEQ)
        WRITE (7,90000) TOUT, Y(1:NEQ)
      END DO ! IPLOT
!     Write the integration final integration statistics:
      WRITE (6,90002) ISTATS(11), ISTATS(12), ISTATS(13)
!     Compare with the reference solution:
      YOUT(1:NEQ) = 0D0
      IF (ILAST==13) THEN
        YOUT(1) = 0.1152903278711829D-290
        YOUT(2) = 0.8867655517642120D-022
        YOUT(3) = 0.8854814626268838D-022
        YOUT(4) = 0.0000000000000000D+000
      END IF
      IF (ILAST==3) THEN
        YOUT(1) = 1.618076919919600D-03
        YOUT(2) = 1.382236955418478D-10
        YOUT(3) = 8.251573436034144D-12
        YOUT(4) = 1.299721221058136D-10
      END IF
      WRITE (6,90000) TOUT, YOUT(1:NEQ)
      ERRMAX = 0.0D0
      DO I = 1, NEQ
        ERRMAX = MAX(ERRMAX,ABS(Y(I)-YOUT(I)))
      END DO
      WRITE (6,90001) ERRMAX

!     Format statements for this problem:
90000 FORMAT (5D15.5)
90001 FORMAT (' Maximum absolute error at tfinal = ',D15.5)
90002 FORMAT (' Steps = ',I10,' f-s = ',I10,' J-s = ',I10)
90003 FORMAT (' An error occurred in VODE_F90. ISTATE = ',I3)
90004 FORMAT (/' Results for ATOL = RTOL = ',D15.5,':')

    END PROGRAM E5
