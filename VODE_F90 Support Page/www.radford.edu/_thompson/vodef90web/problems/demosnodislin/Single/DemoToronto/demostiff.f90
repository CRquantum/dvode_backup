! DVODE_F90 demonstration program

! Toronto Stiff Test Set

! Reference:
! ALGORITHM 648, COLLECTED ALGORITHMS FROM ACM.
! TRANSACTIONS ON MATHEMATICAL SOFTWARE,
! VOL. 13, NO. 1, P. 28

! Caution:
! The original test routines are altered in this version.
! Some subroutine arguments, COMMON variables, and local
! variables have been moved to the following global header.
! You should obtain the original test suite from netlib
! if you plan to use the routines for other purposes.

    MODULE STIFFSET

! Note:
! In this version ID and IID are defined in the main
! program demostiff (not in the Toronto routines).
! The other parameters are obtained by calling IVALU
! with a modified argument list.

      IMPLICIT NONE
      INTEGER IWT, N, ID, IID, NFCN, NJAC, NLUD
      REAL W, DY
      DIMENSION W(20), DY(400)

    CONTAINS

      SUBROUTINE DERIVS(NEQ,T,Y,YDOT)
        IMPLICIT NONE
        INTEGER NEQ
        REAL T, Y, YDOT
        DIMENSION Y(NEQ), YDOT(NEQ)

        CALL FCN(T,Y,YDOT)
        RETURN
      END SUBROUTINE DERIVS

      SUBROUTINE JACD(NEQ,T,Y,ML,MU,PD,NROWPD)
        IMPLICIT NONE
        INTEGER NEQ, ML, MU, NROWPD
        REAL T, Y, PD
        DIMENSION Y(NEQ), PD(NROWPD,NEQ)

        CALL PDERV(T,Y)
        CALL LOADPD(NROWPD,NEQ,PD)
        RETURN
      END SUBROUTINE JACD

      SUBROUTINE LOADPD(NROWPD,NEQ,PD)
        IMPLICIT NONE
        INTEGER NEQ, NROWPD, I, J
        REAL PD
        DIMENSION PD(NROWPD,NEQ)

        DO J = 1, NEQ
          DO I = 1, NEQ
            PD(I,J) = DY(I+(J-1)*NROWPD)
          END DO
        END DO
        RETURN
      END SUBROUTINE LOADPD

      SUBROUTINE IVALU(XSTART,XEND,HBEGIN,HMAX,Y)

!      ROUTINE TO PROVIDE THE INITIAL VALUES REQUIRED TO SPECIFY
!      THE MATHEMATICAL PROBLEM AS WELL AS VARIOUS PROBLEM
!      PARAMETERS REQUIRED BY THE TESTING PACKAGE. THE APPROPRIATE
!      SCALING VECTOR IS ALSO INITIALISED IN CASE THIS OPTION IS
!      SELECTED.

!      PARAMETERS (OUTPUT)
!         N      - DIMENSION OF THE PROBLEM
!         XSTART - INITIAL VALUE OF THE INDEPENDENT VARIABLE
!         XEND   - FINAL VALUE OF THE INDEPENDENT VARIABLE
!         HBEGIN - APPROPRIATE STARTING STEPSIZE
!         Y      - VECTOR OF INITIAL CONDITIONS FOR THE DEPENDENT
!                  VARIABLES
!         W      - VECTOR OF WEIGHTS USED TO SCALE THE PROBLEM IF
!                  THIS OPTION IS SELECTED.

!      PARAMETER  (INPUT)
!         IWT    - FLAG TO INDICATE IF SCALED OPTION IS SELECTED
!         ID     - FLAG IDENTIFYING WHICH EQUATION IS BEING SOLVED

        IMPLICIT NONE

!     .. Scalar Arguments ..
        REAL HBEGIN, HMAX, XEND, XSTART
!     .. Array Arguments ..
        REAL Y(20)
!     .. Local Scalars ..
        REAL XS
        INTEGER I, IOUT, ITMP
!     .. Data statements ..
        DATA XS/0.E0/

!     .. Executable Statements ..

        XSTART = XS
!     GOTO (40, 80, 120, 160, 20, 20, 20, 20, 20, 20, 200, 220, 220,    &
!     220, 220, 20, 20, 20, 20, 20, 360, 400, 400, 400, 400, 20, 20, 20,&
!     20, 20, 540, 580, 600, 640, 660, 680, 20, 20, 20, 20, 700, 740,   &
!     760, 780, 800, 20, 20, 20, 20, 20, 840, 860, 880, 900, 920) ID
        GO TO (20,40,60,80,10,10,10,10,10,10,100,110,110,110,110,10,10,10,10, &
          10,180,200,200,200,200,10,10,10,10,10,270,290,300,320,330,340,10,10, &
          10,10,350,370,380,390,400,10,10,10,10,10,420,430,440,450,460) ID
10      IOUT = 6
        WRITE (IOUT,FMT=90000) ID
        STOP

!     PROBLEM CLASS A - LINEAR WITH REAL EIGENVALUES

20      CONTINUE
!     PROBLEM A1
        N = 4
        W(1) = 0.100E+01
        W(2) = 0.100E+01
        W(3) = 0.100E+01
        W(4) = 0.100E+01
        XEND = 20.E0
        HBEGIN = 1.0E-2
        HMAX = 20.E0
        DO 30 I = 1, N
          Y(I) = 1.0E0
30      END DO
        GO TO 470

40      CONTINUE
!     PROBLEM A2
        N = 9
        W(1) = 0.100E+00
        W(2) = 0.200E+00
        W(3) = 0.300E+00
        W(4) = 0.400E+00
        W(5) = 0.500E+00
        W(6) = 0.600E+00
        W(7) = 0.700E+00
        W(8) = 0.800E+00
        W(9) = 0.900E+00
        XEND = 120.E0
        HBEGIN = 5.E-4
        HMAX = 120.E0
        DO 50 I = 1, N
          Y(I) = 0.E0
50      END DO
        GO TO 470

60      CONTINUE
!     PROBLEM A3
        N = 4
        W(1) = 0.100E+01
        W(2) = 0.100E+01
        W(3) = 0.782E+01
        W(4) = 0.100E+01
        HBEGIN = 1.E-5
        XEND = 20.E0
        HMAX = 20.E0
        DO 70 I = 1, N
          Y(I) = 1.E0
70      END DO
        GO TO 470

80      CONTINUE
!     PROBLEM A4
        N = 10
        W(1) = 0.100E+01
        W(2) = 0.100E+01
        W(3) = 0.100E+01
        W(4) = 0.100E+01
        W(5) = 0.100E+01
        W(6) = 0.100E+01
        W(7) = 0.100E+01
        W(8) = 0.100E+01
        W(9) = 0.100E+01
        W(10) = 0.100E+01
        XEND = 1.E0
        HBEGIN = 1.E-5
        HMAX = 1.E0
        DO 90 I = 1, N
          Y(I) = 1.E0
90      END DO
        GO TO 470

!     PROBLEM CLASS B - LINEAR WITH NON-REAL EIGENVALUES

100     CONTINUE
!     PROBLEM B1
        N = 4
        W(1) = 0.100E+01
        W(2) = 0.859E+01
        W(3) = 0.100E+01
        W(4) = 0.322E+02
        XEND = 20.E0
        HBEGIN = 7.E-3
        HMAX = 20.E0
        Y(1) = 1.E0
        Y(2) = 0.E0
        Y(3) = 1.E0
        Y(4) = 0.E0
        GO TO 470

110     CONTINUE
!     PROBLEM B2, B3, B4, B5
        N = 6
        ITMP = IID - 1
        GO TO (120,130,140,150) ITMP
120     CONTINUE
        W(1) = 0.100E+01
        W(2) = 0.100E+01
        W(3) = 0.100E+01
        W(4) = 0.100E+01
        W(5) = 0.100E+01
        W(6) = 0.100E+01
        GO TO 160
130     CONTINUE
        W(1) = 0.100E+01
        W(2) = 0.100E+01
        W(3) = 0.100E+01
        W(4) = 0.100E+01
        W(5) = 0.100E+01
        W(6) = 0.100E+01
        GO TO 160
140     CONTINUE
        W(1) = 0.112E+01
        W(2) = 0.100E+01
        W(3) = 0.100E+01
        W(4) = 0.100E+01
        W(5) = 0.100E+01
        W(6) = 0.100E+01
        GO TO 160
150     CONTINUE
        W(1) = 0.131E+01
        W(2) = 0.112E+01
        W(3) = 0.100E+01
        W(4) = 0.100E+01
        W(5) = 0.100E+01
        W(6) = 0.100E+01
160     CONTINUE
        XEND = 20.E0
        HBEGIN = 1.E-2
        HMAX = 20.E0
        DO 170 I = 1, N
          Y(I) = 1.E0
170     END DO
        GO TO 470

!     PROBLEM CLASS C - NON-LINEAR COUPLING FROM
!                       STEADY STATE TO TRANSIENT

180     CONTINUE
!     PROBLEM C1
        N = 4
        W(1) = 0.102E+01
        W(2) = 0.103E+01
        W(3) = 0.100E+01
        W(4) = 0.100E+01
        XEND = 20.E0
        HBEGIN = 1.E-2
        HMAX = 20.E0
        DO 190 I = 1, N
          Y(I) = 1.E0
190     END DO
        GO TO 470

200     CONTINUE
!     PROBLEM C2, C3, C4, C5
        N = 4
        ITMP = IID - 1
        GO TO (210,220,230,240) ITMP
210     CONTINUE
        W(1) = 0.200E+01
        W(2) = 0.100E+01
        W(3) = 0.100E+01
        W(4) = 0.100E+01
        GO TO 250
220     CONTINUE
        W(1) = 0.200E+01
        W(2) = 0.100E+01
        W(3) = 0.100E+01
        W(4) = 0.100E+01
        GO TO 250
230     CONTINUE
        W(1) = 0.200E+01
        W(2) = 0.400E+01
        W(3) = 0.200E+02
        W(4) = 0.420E+03
        GO TO 250
240     CONTINUE
        W(1) = 0.200E+01
        W(2) = 0.800E+01
        W(3) = 0.136E+03
        W(4) = 0.371E+05
250     CONTINUE
        XEND = 20.E0
        HBEGIN = 1.E-2
        HMAX = 20.E0
        DO 260 I = 1, N
          Y(I) = 1.E0
260     END DO
        GO TO 470

!     PROBLEM CLASS D - NON-LINEAR WITH REAL EIGENVALUES

270     CONTINUE
!     PROBLEM D1
        N = 3
        W(1) = 0.223E+02
        W(2) = 0.271E+02
        W(3) = 0.400E+03
        XEND = 400.E0
        HBEGIN = 1.7E-2
        HMAX = 400.E0
        DO 280 I = 1, N
          Y(I) = 0.E0
280     END DO
        GO TO 470

290     CONTINUE
!     PROBLEM D2
        N = 3
        W(1) = 0.100E+01
        W(2) = 0.365E+00
        W(3) = 0.285E+02
        XEND = 40.E0
        HBEGIN = 1.E-5
        HMAX = 40.E0
        Y(1) = 1.E0
        Y(2) = 0.E0
        Y(3) = 0.E0
        GO TO 470

300     CONTINUE
!     PROBLEM D3
        N = 4
        W(1) = 0.100E+01
        W(2) = 0.100E+01
        W(3) = 0.360E+00
        W(4) = 0.485E+00
        XEND = 20.E0
        HBEGIN = 2.5E-5
        HMAX = 20.E0
        DO 310 I = 1, 2
          Y(I) = 1.E0
          Y(I+2) = 0.E0
310     END DO
        GO TO 470

320     CONTINUE
!     PROBLEM D4
        N = 3
        W(1) = 0.100E+01
        W(2) = 0.142E+01
        W(3) = 0.371E-05
        XEND = 50.E0
        HBEGIN = 2.9E-4
        HMAX = 50.E0
        Y(1) = 1.E0
        Y(2) = 1.E0
        Y(3) = 0.E0
        GO TO 470

330     CONTINUE
!     PROBLEM D5
        N = 2
        W(1) = 0.992E+00
        W(2) = 0.984E+00
        XEND = 1.E2
        HBEGIN = 1.E-4
        HMAX = 1.E2
        Y(1) = 0.E0
        Y(2) = 0.E0
        GO TO 470

340     CONTINUE
!     PROBLEM D6
        N = 3
        W(1) = 0.100E+01
        W(2) = 0.148E+00
        W(3) = 0.577E-07
        XEND = 1.E0
        HBEGIN = 3.3E-8
        HMAX = 1.E0
        Y(1) = 1.E0
        Y(2) = 0.E0
        Y(3) = 0.E0
        GO TO 470

!     PROBLEM CLASS E - NON-LINEAR WITH NON-REAL EIGENVALUES

350     CONTINUE
!     PROBLEM E1
        N = 4
        W(1) = 0.100E-07
        W(2) = 0.223E-06
        W(3) = 0.132E-04
        W(4) = 0.171E-02
        XEND = 1.E0
        HBEGIN = 6.8E-3
        HMAX = 1.E0
        DO 360 I = 1, N
          Y(I) = 0.E0
360     END DO
        GO TO 470

370     CONTINUE
!     PROBLEM E2
        N = 2
        W(1) = 0.202E+01
        W(2) = 0.764E+01
        XEND = 1.E1
        HBEGIN = 1.E-3
        HMAX = 1.E1
        Y(1) = 2.E0
        Y(2) = 0.E0
        GO TO 470

380     CONTINUE
!     PROBLEM E3
        N = 3
        W(1) = 0.163E+01
        W(2) = 0.160E+01
        W(3) = 0.263E+02
        XEND = 5.E2
        HBEGIN = .2E-1
        HMAX = 5.E2
        Y(1) = 1.E0
        Y(2) = 1.E0
        Y(3) = 0.E0
        GO TO 470

390     CONTINUE
!     PROBLEM E4
        N = 4
        W(1) = 0.288E+02
        W(2) = 0.295E+02
        W(3) = 0.155E+02
        W(4) = 0.163E+02
        XEND = 1.E3
        HBEGIN = 1.E-3
        HMAX = 1.E3
        Y(1) = 0.E0
        Y(2) = -2.E0
        Y(3) = -1.E0
        Y(4) = -1.E0
        GO TO 470

400     CONTINUE
!     PROBLEM E5
        N = 4
        W(1) = 0.176E-02
        W(2) = 0.146E-09
        W(3) = 0.827E-11
        W(4) = 0.138E-09
        XEND = 1.E3
        HBEGIN = 5.E-5
        HMAX = 1.E3
        Y(1) = 1.76E-3
        DO 410 I = 2, N
          Y(I) = 0.E0
410     END DO
        GO TO 470

!     PROBLEM CLASS F - CHEMICAL KINETICS EQUATIONS

420     CONTINUE
!     PROBLEM F1
        N = 4
        W(1) = 0.121E+04
        W(2) = 0.835E-01
        W(3) = 0.121E+04
        W(4) = 0.100E+00
        HMAX = 1.E3
        HBEGIN = 1.E-4
        XEND = 1.E3
        Y(1) = 761.E0
        Y(2) = 0.E0
        Y(3) = 600.E0
        Y(4) = .1E0
        GO TO 470

430     CONTINUE
!     PROBLEM F2
        N = 2
        W(1) = 0.100E+01
        W(2) = 0.253E-02
        HMAX = 240.E0
        HBEGIN = 1.E-2
        XEND = 240.E0
        Y(1) = 1.0E0
        Y(2) = 0.E0
        GO TO 470

440     CONTINUE
!     PROBLEM F3
        N = 5
        W(1) = 0.400E-05
        W(2) = 0.100E-05
        W(3) = 0.374E-08
        W(4) = 0.765E-06
        W(5) = 0.324E-05
        HBEGIN = 1.E-6
        HBEGIN = 1.E-10
        HMAX = 100.E0
        XEND = 100.E0
        Y(1) = 4.E-6
        Y(2) = 1.E-6
        Y(3) = 0.0E0
        Y(4) = 0.0E0
        Y(5) = 0.0E0
        GO TO 470

450     CONTINUE
!     PROBLEM F4
        N = 3
        W(1) = 0.118E+06
        W(2) = 0.177E+04
        W(3) = 0.313E+05
        HBEGIN = 1.E-3
        HMAX = 50.E0
        XEND = 300.E0
        Y(1) = 4.E0
        Y(2) = 1.1E0
        Y(3) = 4.E0
        GO TO 470

460     CONTINUE
!     PROBLEM F5
        N = 4
        W(1) = 0.336E-06
        W(2) = 0.826E-02
        W(3) = 0.619E-02
        W(4) = 0.955E-05
        HBEGIN = 1.E-7
        HMAX = 100.E0
        XEND = 100.E0
        Y(1) = 3.365E-7
        Y(2) = 8.261E-3
        Y(3) = 1.642E-3
        Y(4) = 9.380E-6
470     CONTINUE
        IF (IWT<0) GO TO 490
        DO 480 I = 1, N
          Y(I) = Y(I)/W(I)
480     END DO
490     CONTINUE
        RETURN

90000   FORMAT (' AN INVALID INTERNAL PROBLEM ID OF ',I4, &
          ' WAS FOUND BY THE IVALU ROUTINE',' RUN TERMINATED. CHECK THE DATA.' &
          )
      END SUBROUTINE IVALU

      SUBROUTINE EVALU(Y)

!     ROUTINE TO PROVIDE THE 'TRUE' SOLUTION OF THE DIFFERENTIAL
!     EQUATION EVALUATED AT THE ENDPOINT OF THE INTEGRATION.

!     1986 REVISION:  SOME VERY SMALL CONSTANTS HAVE BEEN RECAST IN THE
!     (NOT SO SMALL CONST)/(1.E38) TO AVOID COMPILE-TIME UNDERFLOW ERROR
!     IT IS ASSUMED 1E+38 WON'T OVERFLOW.
!     PARAMETER  (OUTPUT)
!        Y      - THE TRUE SOLUTION VECTOR EVALUATED AT THE ENDPOINT

!     PARAMETERS (INPUT)
!        N      - DIMENSION OF THE PROBLEM
!        W      - VECTOR OF WEIGHTS USED TO SCALE THE PROBLEM
!                 IF THIS OPTION IS SELECTED
!        IWT    - FLAG USED TO SIGNAL WHEN THE SCALED PROBLEM IS
!                 BEING SOLVED
!        ID     - FLAG USED TO INDICATE WHICH EQUATION IS BEING
!                 SOLVED

        IMPLICIT NONE

!     .. Parameters ..
        REAL TENE38
        PARAMETER (TENE38=1.E38)
!     .. Array Arguments ..
        REAL Y(20)
!     .. Local Scalars ..
        INTEGER I

!     .. Executable Statements ..

!     GOTO (20, 40, 60, 80, 620, 620, 620, 620, 620, 620, 100, 120, 140,&
!     160, 180, 620, 620, 620, 620, 620, 200, 220, 240, 260, 280, 620,  &
!     620, 620, 620, 620, 300, 320, 340, 360, 380, 400, 620, 620, 620,  &
!     620, 420, 440, 460, 480, 500, 620, 620, 620, 620, 620, 520, 540,  &
!     560, 580, 600, 620, 620, 620, 620, 620) ID
        GO TO (10,20,30,40,310,310,310,310,310,310,50,60,70,80,90,310,310,310, &
          310,310,100,110,120,130,140,310,310,310,310,310,150,160,170,180,190, &
          200,310,310,310,310,210,220,230,240,250,310,310,310,310,310,260,270, &
          280,290,300,310,310,310,310,310) ID
        GO TO 310

!     PROBLEM CLASS A

!     PROBLEM A1
10      Y(1) = 4.539992969929191E-05
        Y(2) = 2.061153036149920E-09
        Y(3) = 2.823006338263857E-18/TENE38
        Y(4) = 5.235792540515384E-14/TENE38
        GO TO 310

!     PROBLEM A2
20      Y(1) = 9.999912552999704E-02
        Y(2) = 1.999982511586291E-01
        Y(3) = 2.999975543202422E-01
        Y(4) = 3.999971057541257E-01
        Y(5) = 4.999969509963023E-01
        Y(6) = 5.999971057569546E-01
        Y(7) = 6.999975543256127E-01
        Y(8) = 7.999982511659962E-01
        Y(9) = 8.999991255386128E-01
        GO TO 310

!     PROBLEM A3
30      Y(1) = -1.353352661867235E-03
        Y(2) = 1.368526917891521E-02
        Y(3) = 1.503725348455117E+00
        Y(4) = 1.353352832366099E-01
        GO TO 310

!     PROBLEM A4
40      Y(1) = 3.678794411714325E-01
        Y(2) = 1.265870722340194E-14
        Y(3) = 1.911533219339204E-04/TENE38
        Y(4) = 2.277441666729596E-17/TENE38
        Y(5) = 0.0E0
        Y(6) = 0.0E0
        Y(7) = 0.0E0
        Y(8) = 0.0E0
        Y(9) = 0.0E0
        Y(10) = 0.0E0
        GO TO 310

!     PROBLEM CLASS B

!     PROBLEM B1
50      Y(1) = 1.004166730990124E-09
        Y(2) = 1.800023280346500E-08
        Y(3) = 0.0E0
        Y(4) = -6.042962877027475E-03/TENE38/TENE38
        GO TO 310

!     PROBLEM B2
60      Y(1) = 6.181330838820067E-31
        Y(2) = 8.963657877626303E-31
        Y(3) = 2.738406773453261E-27
        Y(4) = 2.061153063164016E-09
        Y(5) = 4.539992973654118E-05
        Y(6) = 1.353352832365270E-01
        GO TO 310

!     PROBLEM B3
70      Y(1) = -1.076790816984970E-28
        Y(2) = 5.455007683862160E-28
        Y(3) = 2.738539964946867E-27
        Y(4) = 2.061153071123456E-09
        Y(5) = 4.539992974611305E-05
        Y(6) = 1.353352832365675E-01
        GO TO 310

!     PROBLEM B4
80      Y(1) = 1.331242472678293E-22
        Y(2) = -2.325916064237926E-22
        Y(3) = 1.517853928534857E-35
        Y(4) = 2.061152428936651E-09
        Y(5) = 4.539992963392291E-05
        Y(6) = 1.353352832363442E-01
        GO TO 310

!     PROBLEM B5
90      Y(1) = -3.100634584292190E-14
        Y(2) = 3.862788998076547E-14
        Y(3) = 1.804851385304217E-35
        Y(4) = 2.061153622425655E-09
        Y(5) = 4.539992976246673E-05
        Y(6) = 1.353352832366126E-01
        GO TO 310

!     PROBLEM CLASS C

!     PROBLEM C1
100     Y(1) = 4.003223925456179E-04
        Y(2) = 4.001600000000000E-04
        Y(3) = 4.000000000000000E-04
        Y(4) = 2.000000000000000E-02
        GO TO 310

!     PROBLEM C2
110     Y(1) = 1.999999997938994E+00
        Y(2) = 3.999999990839974E-02
        Y(3) = 4.001599991537078E-02
        Y(4) = 4.003201271914461E-02
        GO TO 310

!     PROBLEM C3
120     Y(1) = 1.999999997939167E+00
        Y(2) = 3.999999990840744E-01
        Y(3) = 4.159999990793773E-01
        Y(4) = 4.333055990159567E-01
        GO TO 310

!     PROBLEM C4
130     Y(1) = 1.999999997938846E+00
        Y(2) = 3.999999990839318E+00
        Y(3) = 1.999999991637941E+01
        Y(4) = 4.199999965390368E+02
        GO TO 310

!     PROBLEM C5
140     Y(1) = 1.999999997938846E+00
        Y(2) = 7.999999981678634E+00
        Y(3) = 1.359999993817714E+02
        Y(4) = 3.712799965967762E+04
        GO TO 310

!     PROBLEM CLASS D

!     PROBLEM D1
150     Y(1) = 2.224222010616901E+01
        Y(2) = 2.711071334484136E+01
        Y(3) = 3.999999999999999E+02
        GO TO 310

!     PROBLEM D2
160     Y(1) = 7.158270687193941E-01
        Y(2) = 9.185534764557338E-02
        Y(3) = 2.841637457458413E+01
        GO TO 310

!     PROBLEM D3
170     Y(1) = 6.397604446889910E-01
        Y(2) = 5.630850708287990E-03
        Y(3) = 3.602395553110090E-01
        Y(4) = 3.170647969903515E-01
        GO TO 310

!     PROBLEM D4
180     Y(1) = 5.976546980673215E-01
        Y(2) = 1.402343408546138E+00
        Y(3) = -1.893386540441913E-06
        GO TO 310

!     PROBLEM D5
190     Y(1) = -9.916420698713913E-01
        Y(2) = 9.833363588544478E-01
        GO TO 310

!     PROBLEM D6
200     Y(1) = 8.523995440749948E-01
        Y(2) = 1.476003981941319E-01
        Y(3) = 5.773087333950041E-08
        GO TO 310

!     PROBLEM CLASS E

!     PROBLEM E1
210     Y(1) = 1.000000000000012E-08
        Y(2) = -1.625323873316817E-19
        Y(3) = 2.025953375595861E-17
        Y(4) = -1.853149807630002E-15
        GO TO 310

!     PROBLEM E2
220     Y(1) = -1.158701266031984E+00
        Y(2) = 4.304698089780476E-01
        GO TO 310

!     PROBLEM E3
230     Y(1) = 4.253052197643089E-03
        Y(2) = 5.317019548450387E-03
        Y(3) = 2.627647748753926E+01
        GO TO 310

!     PROBLEM E4
240     Y(1) = 1.999999977523654E+01
        Y(2) = -2.000000022476345E+01
        Y(3) = -2.247634567084293E-07
        Y(4) = 2.247634567084293E-07
        GO TO 310

!     PROBLEM E5
250     Y(1) = 1.618076919919600E-03
        Y(2) = 1.382236955418478E-10
        Y(3) = 8.251573436034144E-12
        Y(4) = 1.299721221058136E-10
        GO TO 310

!     PROBLEM CLASS F

!     PROBLEM F1
260     Y(1) = 1.211129474696585E+03
        Y(2) = 1.271123619113051E-05
        Y(3) = 1.208637804660361E+03
        Y(4) = 3.241981171933418E-04
        GO TO 310

!     PROBLEM F2
270     Y(1) = 3.912699122292088E-01
        Y(2) = 1.329964166084866E-03
        GO TO 310

!     PROBLEM F3
280     Y(1) = 3.235910070806680E-13
        Y(2) = 2.360679774997897E-07
        Y(3) = 7.639319089351045E-14
        Y(4) = 7.639319461070194E-07
        Y(5) = 3.236067653908783E-06
        GO TO 310

!     PROBLEM F4
290     Y(1) = 4.418303324022590E+00
        Y(2) = 1.290244712916425E+00
        Y(3) = 3.019282584050490E+00
        GO TO 310

!     PROBLEM F5
300     Y(1) = 1.713564284690712E-07
        Y(2) = 3.713563071160676E-03
        Y(3) = 6.189271785267793E-03
        Y(4) = 9.545143571530929E-06
310     CONTINUE
        IF (IWT<0) GO TO 330
        DO 320 I = 1, N
          Y(I) = Y(I)/W(I)
320     END DO
330     CONTINUE
        RETURN
      END SUBROUTINE EVALU

      SUBROUTINE FCN(X,Y,YP)

!     ROUTINE TO EVALUATE THE DERIVATIVE F(X,Y) CORRESPONDING TO
!     THE DIFFERENTIAL EQUATION:
!                    DY/DX = F(X,Y) .
!     THE ROUTINE STORES THE VECTOR OF DERIVATIVES IN YP(*). THE
!     PARTICULAR EQUATION BEING INTEGRATED IS INDICATED BY THE
!     VALUE OF THE FLAG ID WHICH IS PASSED THROUGH COMMON. THE
!     DIFFERENTIAL EQUATION IS SCALED BY THE WEIGHT VECTOR W(*)
!     IF THIS OPTION HAS BEEN SELECTED (IF SO IT IS SIGNALLED
!     BY THE FLAG IWT).

        IMPLICIT NONE

!     .. Scalar Arguments ..
        REAL X
!     .. Array Arguments ..
        REAL Y(20), YP(20)
!     .. Local Scalars ..
        REAL F, Q, S, SUM, T, TEMP, XTEMP
        INTEGER I
!     .. Local Arrays ..
        REAL BPARM(4), CPARM(4), VECT1(4), VECT2(4), YTEMP(20)
!     .. Data statements ..
        DATA BPARM/3.E0, 8.E0, 25.E0, 1.E2/
        DATA CPARM/1.E-1, 1.E0, 1.E1, 2.E1/

!     .. Executable Statements ..

        NFCN = NFCN + 1
        IF (IWT<0) GO TO 20
        DO 10 I = 1, N
          YTEMP(I) = Y(I)
          Y(I) = Y(I)*W(I)
10      END DO
20      CONTINUE
        GO TO (30,40,60,70,320,320,320,320,320,320,90,100,100,100,100,320,320, &
          320,320,320,110,120,120,120,120,320,320,320,320,320,130,140,150,160, &
          170,180,320,320,320,320,190,200,210,220,260,320,320,320,320,320,270, &
          280,290,300,310) ID
        GO TO 320

!     PROBLEM CLASS A - LINEAR WITH REAL EIGENVALUES

!     PROBLEM A1
30      YP(1) = -.5E0*Y(1)
        YP(2) = -1.E0*Y(2)
        YP(3) = -1.E2*Y(3)
        YP(4) = -9.E1*Y(4)
        GO TO 320

!     PROBLEM A2
40      YP(1) = -1.8E3*Y(1) + 9.E2*Y(2)
        DO 50 I = 2, 8
          YP(I) = Y(I-1) - 2.E0*Y(I) + Y(I+1)
50      END DO
        YP(9) = 1.E3*Y(8) - 2.E3*Y(9) + 1.E3
        GO TO 320

!     PROBLEM A3
60      YP(1) = -1.E4*Y(1) + 1.E2*Y(2) - 1.E1*Y(3) + 1.E0*Y(4)
        YP(2) = -1.E3*Y(2) + 1.E1*Y(3) - 1.E1*Y(4)
        YP(3) = -1.E0*Y(3) + 1.E1*Y(4)
        YP(4) = -1.E-1*Y(4)
        GO TO 320

!     PROBLEM A4
70      DO 80 I = 1, 10
          YP(I) = -REAL(I)**5*Y(I)
80      END DO
        GO TO 320

!     PROBLEM CLASS B - LINEAR WITH NON-REAL EIGENVALUES

!     PROBLEM B1
90      YP(1) = -Y(1) + Y(2)
        YP(2) = -1.E2*Y(1) - Y(2)
        YP(3) = -1.E2*Y(3) + Y(4)
        YP(4) = -1.E4*Y(3) - 1.E2*Y(4)
        GO TO 320

!     PROBLEMS B2, B3, B4, B5
100     YP(1) = -1.E1*Y(1) + BPARM(IID-1)*Y(2)
        YP(2) = -BPARM(IID-1)*Y(1) - 1.E1*Y(2)
        YP(3) = -4.E0*Y(3)
        YP(4) = -1.E0*Y(4)
        YP(5) = -.5E0*Y(5)
        YP(6) = -.1E0*Y(6)
        GO TO 320

!     PROBLEM CLASS C - NON-LINEAR COUPLING FROM
!                       STEADY STATE TO TRANSIENT

!     PROBLEM C1
110     YP(1) = -Y(1) + (Y(2)*Y(2)+Y(3)*Y(3)+Y(4)*Y(4))
        YP(2) = -1.E1*Y(2) + 1.E1*(Y(3)*Y(3)+Y(4)*Y(4))
        YP(3) = -4.E1*Y(3) + 4.E1*Y(4)*Y(4)
        YP(4) = -1.E2*Y(4) + 2.E0
        GO TO 320

!     PROBLEMS C2, C3, C4, C5
120     YP(1) = -Y(1) + 2.E0
        YP(2) = -1.E1*Y(2) + CPARM(IID-1)*Y(1)*Y(1)
        YP(3) = -4.E1*Y(3) + (Y(1)*Y(1)+Y(2)*Y(2))*CPARM(IID-1)*4.E0
        YP(4) = (Y(1)*Y(1)+Y(2)*Y(2)+Y(3)*Y(3))*CPARM(IID-1)*1.E1 - 1.E2*Y(4)
        GO TO 320

!     PROBLEM CLASS D - NON-LINEAR WITH REAL EIGENVALUES

!     PROBLEM D1
130     YP(1) = .2E0*Y(2) - .2E0*Y(1)
        YP(2) = 1.E1*Y(1) - (6.E1-.125E0*Y(3))*Y(2) + .125E0*Y(3)
        YP(3) = 1.E0
        GO TO 320

!     PROBLEM D2
140     YP(1) = -.04E0*Y(1) + .01E0*Y(2)*Y(3)
        YP(2) = 4.E2*Y(1) - 1.E2*Y(2)*Y(3) - 3.E3*Y(2)**2
        YP(3) = 3.E1*Y(2)**2
        GO TO 320

!     PROBLEM D3
150     YP(1) = Y(3) - 1.E2*Y(1)*Y(2)
        YP(3) = -YP(1)
        YP(4) = -Y(4) + 1.E4*Y(2)**2
        YP(2) = YP(1) - YP(4) + Y(4) - 1.E4*Y(2)**2
        GO TO 320

!     PROBLEM D4
160     YP(1) = -.013E0*Y(1) - 1.E3*Y(1)*Y(3)
        YP(2) = -2.5E3*Y(2)*Y(3)
        YP(3) = YP(1) + YP(2)
        GO TO 320

!     PROBLEM D5
170     XTEMP = .01E0 + Y(1) + Y(2)
        YP(1) = .01E0 - XTEMP*(1.E0+(Y(1)+1.E3)*(Y(1)+1.E0))
        YP(2) = .01E0 - XTEMP*(1.E0+Y(2)**2)
        GO TO 320

!     PROBLEM D6
180     YP(1) = -Y(1) + 1.E8*Y(3)*(1.E0-Y(1))
        YP(2) = -1.E1*Y(2) + 3.E7*Y(3)*(1.E0-Y(2))
        YP(3) = -YP(1) - YP(2)
        GO TO 320

!     PROBLEM CLASS E - NON-LINEAR WITH NON-REAL EIGENVALUES

!     PROBLEM E1
190     YP(1) = Y(2)
        YP(2) = Y(3)
        YP(3) = Y(4)
        YP(4) = (Y(1)**2-SIN(Y(1))-1.E8)*Y(1) + (Y(2)*Y(3)/(Y(1)**2+1.E0)-4.E6 &
          )*Y(2) + (1.E0-6.E4)*Y(3) + (1.E1*EXP(-Y(4)**2)-4.E2)*Y(4) + 1.E0
        GO TO 320

!     PROBLEM E2
200     YP(1) = Y(2)
        YP(2) = 5.E0*Y(2) - 5.E0*Y(1)*Y(1)*Y(2) - Y(1)
        GO TO 320

!     PROBLEM E3
210     YP(1) = -55.E0*Y(1) - Y(3)*Y(1) + 65.E0*Y(2)
        YP(2) = .785E-1*Y(1) - .785E-1*Y(2)
        YP(3) = .1E0*Y(1)
        GO TO 320

!     PROBLEM E4
220     SUM = Y(1) + Y(2) + Y(3) + Y(4)
        DO 230 I = 1, 4
          VECT2(I) = -Y(I) + .5E0*SUM
230     END DO
        VECT1(1) = .5E0*(VECT2(1)**2-VECT2(2)**2)
        VECT1(2) = VECT2(1)*VECT2(2)
        VECT1(3) = VECT2(3)**2
        VECT1(4) = VECT2(4)**2
        TEMP = -1.E1*VECT2(1) - 1.E1*VECT2(2)
        VECT2(2) = 1.E1*VECT2(1) - 1.E1*VECT2(2)
        VECT2(1) = TEMP
        VECT2(3) = 1.E3*VECT2(3)
        VECT2(4) = 1.E-2*VECT2(4)
        SUM = 0.E0
        DO 240 I = 1, 4
          SUM = SUM + VECT1(I) - VECT2(I)
240     END DO
        DO 250 I = 1, 4
          YP(I) = VECT2(I) - VECT1(I) + .5E0*SUM
250     END DO
        GO TO 320

!     PROBLEM E5
260     XTEMP = -7.89E-10*Y(1)
        YP(1) = XTEMP - 1.1E7*Y(1)*Y(3)
        YP(2) = -XTEMP - 1.13E9*Y(2)*Y(3)
        YP(4) = 1.1E7*Y(1)*Y(3) - 1.13E3*Y(4)
        YP(3) = YP(2) - YP(4)
        GO TO 320

!     PROBLEM CLASS F - CHEMICAL KINETICS EQUATIONS

!     PROBLEM F1
270     TEMP = 6.E-3*EXP(20.7E0-1.5E4/Y(1))
        YP(1) = 1.3E0*(Y(3)-Y(1)) + 1.04E4*TEMP*Y(2)
        YP(2) = 1.88E3*(Y(4)-Y(2)*(1.E0+TEMP))
        YP(3) = 1752.E0 - 269.E0*Y(3) + 267.E0*Y(1)
        YP(4) = .1E0 + 320.E0*Y(2) - 321.E0*Y(4)
        GO TO 320

!     PROBLEM F2
280     YP(1) = -Y(1) - Y(1)*Y(2) + 294.E0*Y(2)
        YP(2) = Y(1)*(1.E0-Y(2))/98.E0 - 3.E0*Y(2)
        GO TO 320

!     PROBLEM F3
290     YP(1) = -1.0E7*Y(2)*Y(1) + 1.E1*Y(3)
        YP(2) = -1.0E7*Y(2)*Y(1) - 1.E7*Y(2)*Y(5) + 1.E1*Y(3) + 1.E1*Y(4)
        YP(3) = 1.0E7*Y(2)*Y(1) - 1.001E4*Y(3) + 1.E-3*Y(4)
        YP(4) = 1.E4*Y(3) - 1.0001E1*Y(4) + 1.E7*Y(2)*Y(5)
        YP(5) = 1.E1*Y(4) - 1.E7*Y(2)*Y(5)
        GO TO 320

!     PROBLEM F4
300     S = 77.27E0
        T = 0.161E0
        Q = 8.375E-6
        F = 1.E0
        YP(1) = S*(Y(2)-Y(1)*Y(2)+Y(1)-Q*Y(1)*Y(1))
        YP(2) = (-Y(2)-Y(1)*Y(2)+F*Y(3))/S
        YP(3) = T*(Y(1)-Y(3))
        GO TO 320

!     PROBLEM F5
310     YP(1) = -3.E11*Y(1)*Y(2) + 1.2E8*Y(4) - 9.E11*Y(1)*Y(3)
        YP(2) = -3.E11*Y(1)*Y(2) + 2.E7*Y(4)
        YP(3) = -9.E11*Y(1)*Y(3) + 1.E8*Y(4)
        YP(4) = 3.E11*Y(1)*Y(2) - 1.2E8*Y(4) + 9.E11*Y(1)*Y(3)
320     CONTINUE
        IF (IWT<0) GO TO 340
        DO 330 I = 1, N
          YP(I) = YP(I)/W(I)
          Y(I) = YTEMP(I)
330     END DO
340     CONTINUE
        RETURN
      END SUBROUTINE FCN

      SUBROUTINE PDERV(X,Y)

!     ROUTINE TO EVALUATE THE JACOBIAN MATRIX OF PARTIAL DERIVATIVES
!     CORRESPONDING TO THE DIFFERENTIAL EQUATION:
!                   DY/DX = F(X,Y).
!     THE N**2 ELEMENTS OF THE ARRAY DY(*) ARE ASSIGNED THE VALUE OF
!     THE JACOBIAN MATRIX WITH ELEMENT I+(J-1)*N BEING ASSIGNED THE
!     VALUE OF DF(I)/DY(J). THE PARTICULAR EQUATION BEING INTEGRATED
!     IS INDICATED BY THE VALUE OF THE FLAG ID WHICH IS PASSED THROUGH
!     COMMON. IF A SCALED DIFFERENTIAL EQUATION IS BEING SOLVED (AS
!     SIGNALLED IWT) THE ELEMENTS OF THE JACOBIAN ARE SCALED ACCORDING-
!     LY BY THE WEIGHT VECTOR W(*).

        IMPLICIT NONE

!     .. Scalar Arguments ..
        REAL X
!     .. Array Arguments ..
        REAL Y(20)
!     .. Local Scalars ..
        REAL F, Q, S, SUM, T, TEMP, XTEMP1, XTEMP2, XTEMP3
        INTEGER I, ITMP, J, L
!     .. Local Arrays ..
        REAL BPARM(4), CPARM(4), VECT2(4), YTEMP(20)
!     .. Data statements ..
        DATA BPARM/3.E0, 8.E0, 25.E0, 1.E2/
        DATA CPARM/1.E-1, 1.E0, 1.E1, 2.E1/

!     .. Executable Statements ..

        NJAC = NJAC + 1
        IF (IWT<0) GO TO 20
        DO 10 I = 1, N
          YTEMP(I) = Y(I)
          Y(I) = Y(I)*W(I)
10      END DO
20      CONTINUE
        GO TO (30,50,80,100,490,490,490,490,490,490,130,150,150,150,150,490, &
          490,490,490,490,170,190,190,190,190,490,490,490,490,490,210,220,230, &
          240,260,270,490,490,490,490,290,310,320,330,420,490,490,490,490,490, &
          440,450,460,470,480) ID
        GO TO 490

!     PROBLEM CLASS A - LINEAR WITH REAL EIGENVALUES

!     PROBLEM A1
30      DO 40 I = 1, 16
          DY(I) = 0.E0
40      END DO
        DY(1) = -.5E0
        DY(6) = -1.E0
        DY(11) = -1.E2
        DY(16) = -9.E1
        GO TO 490

!     PROBLEM A2
50      DO 60 I = 1, 81
          DY(I) = 0.E0
60      END DO
        DO 70 I = 2, 62, 10
          DY(I) = 1.E0
          DY(I+9) = -2.E0
          DY(I+18) = 1.E0
70      END DO
        DY(1) = -1.8E3
        DY(10) = 9.E2
        DY(72) = 1.E3
        DY(81) = -2.E3
        GO TO 490

!     PROBLEM A3
80      DO 90 I = 1, 16
          DY(I) = 0.E0
90      END DO
        DY(1) = -1.E4
        DY(5) = 1.E2
        DY(6) = -1.E3
        DY(9) = -1.E1
        DY(10) = 1.E1
        DY(11) = -1.E0
        DY(13) = 1.E0
        DY(14) = -1.E1
        DY(15) = 1.E1
        DY(16) = -1.E-1
        GO TO 490

!     PROBLEM A4
100     DO 110 I = 1, 100
          DY(I) = 0.E0
110     END DO
        DO 120 I = 1, 10
          DY((I-1)*10+I) = -REAL(I)**5
120     END DO
        GO TO 490

!     PROBLEM CLASS B - LINEAR WITH NON-REAL EIGENVALUES

!     PROBLEM B1
130     DO 140 I = 1, 16
          DY(I) = 0.E0
140     END DO
        DY(1) = -1.E0
        DY(2) = -1.E2
        DY(5) = 1.E0
        DY(6) = -1.E0
        DY(11) = -1.E2
        DY(12) = -1.E4
        DY(15) = 1.E0
        DY(16) = -1.E2
        GO TO 490

!     PROBLEMS B2, B3, B4, B5
150     DO 160 I = 1, 36
          DY(I) = 0.E0
160     END DO
        DY(1) = -1.E1
        DY(2) = -BPARM(IID-1)
        DY(7) = BPARM(IID-1)
        DY(8) = -1.E1
        DY(15) = -4.E0
        DY(22) = -1.E0
        DY(29) = -.5E0
        DY(36) = -.1E0
        GO TO 490

!     PROBLEM CLASS C - NON-LINEAR COUPLING FROM
!                       STEADY STATE TO TRANSIENT

!     PROBLEM C1
170     DO 180 I = 1, 16
          DY(I) = 0.E0
180     END DO
        DY(1) = -1.E0
        DY(5) = 2.E0*Y(2)
        DY(6) = -1.E1
        DY(9) = 2.E0*Y(3)
        DY(10) = 2.E1*Y(3)
        DY(11) = -4.E1
        DY(13) = 2.E0*Y(4)
        DY(14) = 2.E1*Y(4)
        DY(15) = 8.E1*Y(4)
        DY(16) = -1.E2
        GO TO 490

!     PROBLEMS C2, C3, C4, C5
190     DO 200 I = 1, 16
          DY(I) = 0.E0
200     END DO
        DY(1) = -1.E0
        DY(2) = 2.E0*Y(1)*CPARM(IID-1)
        DY(3) = 8.E0*Y(1)*CPARM(IID-1)
        DY(4) = 2.E1*Y(1)*CPARM(IID-1)
        DY(6) = -1.E1
        DY(7) = 8.E0*Y(2)*CPARM(IID-1)
        DY(8) = 2.E1*Y(2)*CPARM(IID-1)
        DY(11) = -4.E1
        DY(12) = 2.E1*Y(3)*CPARM(IID-1)
        DY(16) = -1.E2
        GO TO 490

!     PROBLEM CLASS D - NON-LINEAR WITH REAL EIGENVALUES

!     PROBLEM D1
210     DY(1) = -.2E0
        DY(2) = 1.E1
        DY(3) = 0.E0
        DY(4) = .2E0
        DY(5) = -6.E1 + .125E0*Y(3)
        DY(6) = 0.E0
        DY(7) = 0.E0
        DY(8) = .125E0*Y(2) + .125E0
        DY(9) = 0.E0
        GO TO 490

!     PROBLEM D2
220     DY(1) = -4.E-2
        DY(2) = 4.E2
        DY(3) = 0.E0
        DY(4) = 1.E-2*Y(3)
        DY(5) = -1.E2*Y(3) - 6.E3*Y(2)
        DY(6) = 6.E1*Y(2)
        DY(7) = .1E-1*Y(2)
        DY(8) = -1.E2*Y(2)
        DY(9) = 0.E0
        GO TO 490

!     PROBLEM D3
230     DY(1) = -1.E2*Y(2)
        DY(2) = DY(1)
        DY(3) = -DY(1)
        DY(4) = 0.E0
        DY(5) = -1.E2*Y(1)
        DY(7) = -DY(5)
        DY(8) = 2.E4*Y(2)
        DY(6) = DY(5) - DY(8)
        DY(6) = DY(6) - 2.E4*Y(2)
        DY(9) = 1.E0
        DY(10) = 1.E0
        DY(11) = -1.E0
        DY(12) = 0.E0
        DY(13) = 0.E0
        DY(14) = 2.E0
        DY(15) = 0.E0
        DY(16) = -1.E0
        GO TO 490

!     PROBLEM D4
240     DY(1) = -.013E0 - 1.E3*Y(3)
        DY(2) = 0.E0
        DY(4) = 0.E0
        DY(5) = -2.5E3*Y(3)
        DY(7) = -1.E3*Y(1)
        DY(8) = -2.5E3*Y(2)
        DO 250 I = 3, 9, 3
          DY(I) = DY(I-1) + DY(I-2)
250     END DO
        GO TO 490

!     PROBLEM D5
260     XTEMP1 = Y(1) + 1.E3
        XTEMP2 = Y(1) + 1.E0
        XTEMP3 = .01E0 + Y(1) + Y(2)
        DY(2) = -(1.E0+Y(2)**2)
        DY(3) = -(1.E0+XTEMP1*XTEMP2)
        DY(1) = -(-DY(3)+XTEMP3*(XTEMP1+XTEMP2))
        DY(4) = -(2.E0*XTEMP3*Y(2)-DY(2))
        GO TO 490

!     PROBLEM D6
270     DY(1) = -1.E0 - 1.E8*Y(3)
        DY(2) = 0.E0
        DY(4) = 0.E0
        DY(5) = -1.E1 - 3.E7*Y(3)
        DY(7) = 1.E8*(1.E0-Y(1))
        DY(8) = 3.E7*(1.E0-Y(2))
        DO 280 I = 3, 9, 3
          DY(I) = -DY(I-2) - DY(I-1)
280     END DO
        GO TO 490

!     PROBLEM CLASS E - NON-LINEAR WITH NON-REAL EIGENVALUES

!     PROBLEM E1
290     DO 300 I = 1, 16
          DY(I) = 0.E0
300     END DO
        DY(5) = 1.E0
        DY(10) = 1.E0
        DY(15) = 1.E0
        XTEMP1 = Y(1)
        XTEMP2 = Y(2)/(XTEMP1**2+1.E0)**2
        DY(4) = 3.E0*XTEMP1**2 - XTEMP1*COS(XTEMP1) - SIN(XTEMP1) - 1.E8 - &
          2.E0*XTEMP1*Y(2)*Y(3)*XTEMP2
        DY(8) = 2.E0*Y(3)*Y(2)/(1.E0+Y(1)**2) - 4.E6
        DY(12) = Y(2)*Y(2)/(1.E0+Y(1)**2) + 1.E0 - 6.E4
        DY(16) = 1.E1*EXP(-Y(4)**2)*(1.E0-2.E0*Y(4)**2) - 4.E2
        GO TO 490

!     PROBLEM E2
310     DY(1) = 0.E0
        DY(2) = -1.E1*Y(1)*Y(2) - 1.E0
        DY(3) = 1.E0
        DY(4) = 5.E0 - 5.E0*Y(1)*Y(1)
        GO TO 490

!     PROBLEM E3
320     DY(1) = -55.E0 - Y(3)
        DY(2) = .785E-1
        DY(3) = 0.1E0
        DY(4) = 65.E0
        DY(5) = -.785E-1
        DY(6) = 0.E0
        DY(7) = -Y(1)
        DY(8) = 0.E0
        DY(9) = 0.E0
        GO TO 490

!     PROBLEM E4
330     SUM = Y(1) + Y(2) + Y(3) + Y(4)
        DO 340 I = 1, 4
          VECT2(I) = -Y(I) + .5E0*SUM
340     END DO
        DO 350 I = 1, 16
          DY(I) = 0.E0
350     END DO
        DY(1) = VECT2(1) + 1.E1
        DY(2) = VECT2(2) - 1.E1
        DY(5) = -DY(2)
        DY(6) = DY(1)
        DY(11) = 2.E0*VECT2(3) - 1.E3
        DY(16) = 2.E0*VECT2(4) - 1.E-2
        DO 380 I = 1, 4
          SUM = 0.E0
          DO 360 J = 1, 4
            L = I + (J-1)*4
            SUM = SUM + DY(L)
360       END DO
          DO 370 J = 1, 4
            L = I + (J-1)*4
            DY(L) = -DY(L) + .5E0*SUM
370       END DO
380     END DO
        DO 410 J = 1, 4
          SUM = 0.E0
          DO 390 I = 1, 4
            L = I + (J-1)*4
            SUM = SUM + DY(L)
390       END DO
          DO 400 I = 1, 4
            L = I + (J-1)*4
            DY(L) = -DY(L) + .5E0*SUM
400       END DO
410     END DO
        GO TO 490

!     PROBLEM E5
420     DY(1) = -7.89E-10 - 1.1E7*Y(3)
        DY(2) = 7.89E-10
        DY(4) = 1.1E7*Y(3)
        DY(5) = 0.E0
        DY(6) = -1.13E9*Y(3)
        DY(8) = 0.E0
        DY(9) = -1.1E7*Y(1)
        DY(10) = -1.13E9*Y(2)
        DY(12) = -DY(9)
        DY(13) = 0.E0
        DY(14) = 0.E0
        DY(16) = -1.13E3
        DO 430 I = 3, 15, 4
          DY(I) = DY(I-1) - DY(I+1)
430     END DO
        GO TO 490

!     PROBLEM CLASS F - CHEMICAL KINETICS EQUATIONS

!     PROBLEM F1
440     TEMP = 90.E0*EXP(20.7E0-1.5E4/Y(1))/Y(1)**2
        DY(1) = -1.3E0 + 1.04E4*TEMP*Y(2)
        DY(2) = -1.88E3*Y(2)*TEMP
        DY(3) = 267.E0
        DY(4) = 0.E0
        TEMP = 6.E-3*EXP(20.7E0-1.5E4/Y(1))
        DY(5) = 1.04E4*TEMP
        DY(6) = -1.88E3*(1.E0+TEMP)
        DY(7) = 0.E0
        DY(8) = 320.E0
        DY(9) = 1.3E0
        DY(10) = 0.E0
        DY(11) = -269.E0
        DY(12) = 0.0E0
        DY(13) = 0.0E0
        DY(14) = 1.88E3
        DY(15) = 0.0E0
        DY(16) = -321.0E0
        GO TO 490

!     PROBLEM F2
450     DY(1) = -1.E0 - Y(2)
        DY(2) = (1.E0-Y(2))/98.E0
        DY(3) = -Y(1) + 294.E0
        DY(4) = -Y(1)/98.E0 - 3.E0
        GO TO 490

!     PROBLEM F3
460     DY(1) = -1.E7*Y(2)
        DY(2) = -1.E7*Y(2)
        DY(3) = 1.E7*Y(2)
        DY(4) = 0.0E0
        DY(5) = 0.0E0
        DY(6) = -1.E7*Y(1)
        DY(7) = -1.E7*Y(1) - 1.E7*Y(5)
        DY(8) = 1.E7*Y(1)
        DY(9) = 1.E7*Y(5)
        DY(10) = -1.E7*Y(5)
        DY(11) = 1.E1
        DY(12) = 1.E1
        DY(13) = -1.001E4
        DY(14) = 1.E4
        DY(15) = 0.0E0
        DY(16) = 0.0E0
        DY(17) = 1.E1
        DY(18) = 1.E-3
        DY(19) = -1.0001E1
        DY(20) = 1.E1
        DY(21) = 0.0E0
        DY(22) = -1.E7*Y(2)
        DY(23) = 0.0E0
        DY(24) = 1.E7*Y(2)
        DY(25) = -1.0E7*Y(2)
        GO TO 490

!     PROBLEM F4
470     S = 77.27E0
        T = 0.161E0
        Q = 8.375E-6
        F = 1.E0
        DY(1) = S*(-Y(2)+1.E0-2.E0*Q*Y(1))
        DY(2) = -Y(2)/S
        DY(3) = T
        DY(4) = S*(1.E0-Y(1))
        DY(5) = (-1.E0-Y(1))/S
        DY(6) = 0.E0
        DY(7) = 0.E0
        DY(8) = F/S
        DY(9) = -T
        GO TO 490

!     PROBLEM F5
480     DY(1) = -3.E11*Y(2) - 9.E11*Y(3)
        DY(2) = -3.E11*Y(2)
        DY(3) = -9.E11*Y(3)
        DY(4) = 3.E11*Y(2) + 9.E11*Y(3)
        DY(5) = -3.E11*Y(1)
        DY(6) = -3.E11*Y(1)
        DY(7) = 0.0E0
        DY(8) = 3.E11*Y(1)
        DY(9) = -9.E11*Y(1)
        DY(10) = 0.0E0
        DY(11) = -9.E11*Y(1)
        DY(12) = 9.E11*Y(1)
        DY(13) = 1.2E8
        DY(14) = 2.E7
        DY(15) = 1.E8
        DY(16) = -1.2E8
490     CONTINUE
        IF (IWT<0) GO TO 520
        DO 510 I = 1, N
          Y(I) = YTEMP(I)
          DO 500 J = 1, N
            ITMP = I + (J-1)*N
            DY(ITMP) = DY(ITMP)*W(J)/W(I)
500       END DO
510     END DO
520     CONTINUE
        RETURN
      END SUBROUTINE PDERV

    END MODULE STIFFSET

!******************************************************************

    PROGRAM DEMOSTIFF

      USE STIFFSET
      USE DVODE_F90_M

      IMPLICIT NONE
      INTEGER ITASK, ISTATE, ISTATS, NEQ, I, CLASS, PROBLEM, MYID, ITEST
      REAL RSTATS, T, TOUT, HBEGIN, HBOUND, TBEGIN, TEND, Y, EPS, YINIT, &
        YFINAL, RELERR_TOLERANCES, ABSERR_TOLERANCES, AERROR
      LOGICAL USEW, USEHBEGIN
      DIMENSION Y(20), RSTATS(22), ISTATS(31), YINIT(20), YFINAL(20), MYID(55)
      DIMENSION RELERR_TOLERANCES(20), ABSERR_TOLERANCES(20), AERROR(20)
      TYPE (VODE_OPTS) :: OPTIONS
      DATA MYID/1, 2, 3, 4, 0, 0, 0, 0, 0, 0, 11, 12, 13, 14, 15, 0, 0, 0, 0, &
        0, 21, 22, 23, 24, 25, 0, 0, 0, 0, 0, 31, 32, 33, 34, 35, 36, 0, 0, 0, &
        0, 41, 42, 43, 44, 45, 0, 0, 0, 0, 0, 51, 52, 53, 54, 55/

      OPEN (UNIT=6,FILE='demostiff.dat')

      DO 20 ITEST = 1, 55
        ID = MYID(ITEST)
        IID = MOD(ID,10)
        IF (ID==0) GO TO 20
        WRITE (6,90010)

        CLASS = ID/10
        PROBLEM = ID - 10*CLASS
        IF (CLASS==0) THEN
          WRITE (6,90000) PROBLEM
        ELSE IF (CLASS==1) THEN
          WRITE (6,90001) PROBLEM
        ELSE IF (CLASS==2) THEN
          WRITE (6,90002) PROBLEM
        ELSE IF (CLASS==3) THEN
          WRITE (6,90003) PROBLEM
        ELSE IF (CLASS==4) THEN
          WRITE (6,90004) PROBLEM
        ELSE IF (CLASS==5) THEN
          WRITE (6,90005) PROBLEM
        END IF

!     Scale the odes?
        USEW = .TRUE.
!     Use the IVALU starting step size?
        USEHBEGIN = .TRUE.

        IWT = -1
        IF (USEW) IWT = 1
        CALL IVALU(TBEGIN,TEND,HBEGIN,HBOUND,YINIT)
        IF ( .NOT. USEHBEGIN) HBEGIN = 0.0E0
        NEQ = N
        T = TBEGIN
        TOUT = TEND
        Y(1:NEQ) = YINIT(1:NEQ)
        EPS = 1E-5
        RELERR_TOLERANCES(1:NEQ) = EPS
        ABSERR_TOLERANCES(1:NEQ) = EPS
        WRITE (6,90007) ID, TBEGIN, TEND, HBEGIN, HBOUND, IWT, N, EPS, &
          Y(1:NEQ)

        ITASK = 1
        ISTATE = 1
        OPTIONS = SET_OPTS(DENSE_J=.TRUE.,USER_SUPPLIED_JACOBIAN=.TRUE., &
          RELERR_VECTOR=RELERR_TOLERANCES(1:NEQ),ABSERR_VECTOR= &
          ABSERR_TOLERANCES(1:NEQ),MXSTEP=100000,H0=HBEGIN,HMAX=HBOUND)
10      CONTINUE
        CALL DVODE_F90(DERIVS,NEQ,Y,T,TOUT,ITASK,ISTATE,OPTIONS,J_FCN=JACD)

        IF (ISTATE<0) THEN
          WRITE (6,90006) ISTATE
          STOP
        END IF
        CALL GET_STATS(RSTATS,ISTATS)
        WRITE (6,90009) ISTATS(11), ISTATS(12), ISTATS(13)
        IF (TOUT<TEND) GO TO 10
        CALL EVALU(YFINAL)
        DO I = 1, NEQ
          AERROR(I) = ABS(Y(I)-YFINAL(I))
        END DO
        WRITE (6,90008) (I,Y(I),YFINAL(I),AERROR(I),I=1,NEQ)

20    CONTINUE ! End of ITEST Loop

!     Format statements for this problem:

90000 FORMAT (' Class/Problem = A',I1)
90001 FORMAT (' Class/Problem = B',I1)
90002 FORMAT (' Class/Problem = C',I1)
90003 FORMAT (' Class/Problem = D',I1)
90004 FORMAT (' Class/Problem = E',I1)
90005 FORMAT (' Class/Problem = F',I1)
90006 FORMAT (' An error occurred in VODE_F90. ISTATE = ',I3)
90007 FORMAT (' Problem ID       = ',I3,/,' Initial time     = ',E15.5,/, &
        ' Final time       = ',E15.5,/,' Initial stepsize = ',E15.5,/, &
        ' Maximum stepsize = ',E15.5,/,' IWT flag         = ',I3,/, &
        ' Number of odes   = ',I3,/,' Error tolerance  = ',E15.5,/, &
        ' Initial solution = ',/,(E15.5))
90008 FORMAT (' Computed and reference solutions and absolute', &
        ' errors follow:',/,(I3,3E15.5))
90009 FORMAT (' Steps = ',I10,' f-s = ',I10,' J-s = ',I10)
90010 FORMAT (' _________________________________________')
      STOP
    END PROGRAM DEMOSTIFF
