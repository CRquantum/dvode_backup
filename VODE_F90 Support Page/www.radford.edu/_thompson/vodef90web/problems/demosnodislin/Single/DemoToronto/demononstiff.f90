! DVODE_F90 demonstration program

! Toronto Nonstiff Test Set

! Reference:
! ALGORITHM 648, COLLECTED ALGORITHMS FROM ACM.
! TRANSACTIONS ON MATHEMATICAL SOFTWARE,
! VOL. 13, NO. 1, P. 28

! Caution:
! This version of the test set is intended for use only
! in conjunction with this demo program for DVODE_F90.
! The original test routines are altered in this version.
! Some subroutine arguments, COMMON variables, and local
! variables have been moved to the following global header.
! You should obtain the original test suite from netlib
! if you plan to use the routines for another solver.

!******************************************************************

    MODULE NONSTIFFSET

! Note:
! In this version ID is defined in the main program
! demostiff. The other parameters are obtained by
! calling IVALU with a modified argument list.

      IMPLICIT NONE
      INTEGER IWT, N, ID, NFCN
      REAL W
      DIMENSION W(51)

    CONTAINS

      SUBROUTINE DERIVS(NEQ,T,Y,YDOT)
        IMPLICIT NONE
        INTEGER NEQ
        REAL T, Y, YDOT
        DIMENSION Y(NEQ), YDOT(NEQ)

        CALL FCN(T,Y,YDOT)
        RETURN
      END SUBROUTINE DERIVS

      SUBROUTINE IVALU(XSTART,XEND,HBEGIN,HMAX,Y)

!      ROUTINE TO PROVIDE THE INITIAL VALUES REQUIRED TO SPECIFY
!      THE MATHEMATICAL PROBLEM AS WELL AS VARIOUS PROBLEM
!      PARAMETERS REQUIRED BY THE TESTING PACKAGE. THE APPROPRIATE
!      SCALING VECTOR IS ALSO INITIALISED IN CASE THIS OPTION IS
!      SELECTED.

!      PARAMETERS (OUTPUT)
!      N      - DIMENSION OF THE PROBLEM
!      XSTART - INITIAL VALUE OF THE INDEPENDENT VARIABLE
!      XEND   - FINAL VALUE OF THE INDEPENDENT VARIABLE
!      HBEGIN - APPROPRIATE STARTING STEPSIZE
!      Y      - VECTOR OF INITIAL CONDITIONS FOR THE DEPENDENT
!               VARIABLES
!      WT     - VECTOR OF WEIGHTS USED TO SCALE THE PROBLEM IF
!               THIS OPTION IS SELECTED.

!      PARAMETER  (INPUT)
!      IWT    - FLAG TO INDICATE IF SCALED OPTION IS SELESTED
!      ID     - FLAG IDENTIFYING WHICH EQUATION IS BEING SOLVED

        IMPLICIT NONE

!     .. Scalar Arguments ..
        REAL HBEGIN, HMAX, XEND, XSTART
!     INTEGER ID, IWT, N
!     .. Array Arguments ..
        REAL Y(51)
!     REAL W(51)
!     .. Local Scalars ..
        REAL E, HB, HM, XE, XS
        INTEGER I, IOUT
!     .. Data statements ..
        DATA HM, HB, XS, XE/20.0E0, 1.0E0, 0.0E0, 20.0E0/

!     .. Executable Statements ..

        HMAX = HM
        HBEGIN = HB
        XSTART = XS
        XEND = XE
!     GOTO (40, 60, 80, 100, 120, 20, 20, 20, 20, 20, 140, 160, 180,    &
!     200, 220, 20, 20, 20, 20, 20, 240, 280, 320, 360, 400, 20, 20, 20,&
!     20, 20, 420, 420, 420, 420, 420, 20, 20, 20, 20, 20, 540, 560,    &
!     580, 600, 620, 20, 20, 20, 20, 20, 640, 660, 680, 700, 720) ID
        GO TO (20,30,40,50,60,10,10,10,10,10,70,80,90,100,110,10,10,10,10,10, &
          120,140,160,180,200,10,10,10,10,10,210,210,210,210,210,10,10,10,10, &
          10,270,280,290,300,310,10,10,10,10,10,320,330,340,350,360) ID
10      IOUT = 6
        WRITE (IOUT,FMT=90000) ID
        STOP

!     PROBLEM CLASS A

!     1:
20      CONTINUE
!     PROBLEM A1
        N = 1
        W(1) = 0.100E+01
        Y(1) = 1.0E0
        GO TO 370
!     2:
30      CONTINUE
!     PROBLEM A2
        N = 1
        W(1) = 0.100E+01
        Y(1) = 1.0E0
        GO TO 370
!     3:
40      CONTINUE
!     PROBLEM A3
        N = 1
        W(1) = 0.271E+01
        Y(1) = 1.0E0
        GO TO 370
!     4:
50      CONTINUE
!     PROBLEM A4
        N = 1
        W(1) = 0.177E+02
        Y(1) = 1.0E0
        GO TO 370
!     5:
60      CONTINUE
!     PROBLEM A5
        N = 1
        W(1) = 0.620E+01
        Y(1) = 4.0E0
        GO TO 370

!     PROBLEM CLASS B

!     11:
70      CONTINUE
!     PROBLEM B1
        N = 2
        W(1) = 0.425E+01
        W(2) = 0.300E+01
        Y(1) = 1.0E0
        Y(2) = 3.0E0
        GO TO 370
!     12:
80      CONTINUE
!     PROBLEM B2
        N = 3
        W(1) = 0.200E+01
        W(2) = 0.100E+01
        W(3) = 0.100E+01
        Y(1) = 2.0E0
        Y(2) = 0.0E0
        Y(3) = 1.0E0
        GO TO 370
!     13:
90      CONTINUE
!     PROBLEM B3
        N = 3
        W(1) = 0.100E+01
        W(2) = 0.519E+00
        W(3) = 0.947E+00
        Y(1) = 1.0E0
        Y(2) = 0.0E0
        Y(3) = 0.0E0
        GO TO 370
!     14:
100     CONTINUE
!     PROBLEM B4
        N = 3
        W(1) = 0.300E+01
        W(2) = 0.220E+01
        W(3) = 0.100E+01
        Y(1) = 3.0E0
        Y(2) = 0.0E0
        Y(3) = 0.0E0
        GO TO 370
!     15:
110     CONTINUE
!     PROBLEM B5
        N = 3
        W(1) = 0.100E+01
        W(2) = 0.100E+01
        W(3) = 0.100E+01
        Y(1) = 0.0E0
        Y(2) = 1.0E0
        Y(3) = 1.0E0
        GO TO 370

!     PROBLEM CLASS C

!     21:
120     CONTINUE
!     PROBLEM C1
        N = 10
        W(1) = 0.100E+01
        W(2) = 0.368E+00
        W(3) = 0.271E+00
        W(4) = 0.224E+00
        W(5) = 0.195E+00
        W(6) = 0.175E+00
        W(7) = 0.161E+00
        W(8) = 0.149E+00
        W(9) = 0.139E+00
        W(10) = 0.998E+00
        Y(1) = 1.0E0
        DO 130 I = 2, N
          Y(I) = 0.0E0
130     END DO
        GO TO 370
!     22:
140     CONTINUE
!     PROBLEM C2
        N = 10
        W(1) = 0.100E+01
        W(2) = 0.250E+00
        W(3) = 0.148E+00
        W(4) = 0.105E+00
        W(5) = 0.818E-01
        W(6) = 0.669E-01
        W(7) = 0.566E-01
        W(8) = 0.491E-01
        W(9) = 0.433E-01
        W(10) = 0.100E+01
        Y(1) = 1.0E0
        DO 150 I = 2, N
          Y(I) = 0.0E0
150     END DO
        GO TO 370
!     23:
160     CONTINUE
!     PROBLEM C3
        N = 10
        W(1) = 0.100E+01
        W(2) = 0.204E+00
        W(3) = 0.955E-01
        W(4) = 0.553E-01
        W(5) = 0.359E-01
        W(6) = 0.252E-01
        W(7) = 0.184E-01
        W(8) = 0.133E-01
        W(9) = 0.874E-02
        W(10) = 0.435E-02
        Y(1) = 1.0E0
        DO 170 I = 2, N
          Y(I) = 0.0E0
170     END DO
        GO TO 370
!     24:
180     CONTINUE
!     PROBLEM C4
        N = 51
        W(1) = 0.100E+01
        W(2) = 0.204E+00
        W(3) = 0.955E-01
        W(4) = 0.553E-01
        W(5) = 0.359E-01
        W(6) = 0.252E-01
        W(7) = 0.186E-01
        W(8) = 0.143E-01
        W(9) = 0.113E-01
        W(10) = 0.918E-02
        W(11) = 0.760E-02
        W(12) = 0.622E-02
        W(13) = 0.494E-02
        W(14) = 0.380E-02
        W(15) = 0.284E-02
        W(16) = 0.207E-02
        W(17) = 0.146E-02
        W(18) = 0.101E-02
        W(19) = 0.678E-03
        W(20) = 0.444E-03
        W(21) = 0.283E-03
        W(22) = 0.177E-03
        W(23) = 0.107E-03
        W(24) = 0.637E-04
        W(25) = 0.370E-04
        W(26) = 0.210E-04
        W(27) = 0.116E-04
        W(28) = 0.631E-05
        W(29) = 0.335E-05
        W(30) = 0.174E-05
        W(31) = 0.884E-06
        W(32) = 0.440E-06
        W(33) = 0.215E-06
        W(34) = 0.103E-06
        W(35) = 0.481E-07
        W(36) = 0.221E-07
        W(37) = 0.996E-08
        W(38) = 0.440E-08
        W(39) = 0.191E-08
        W(40) = 0.814E-09
        W(41) = 0.340E-09
        W(42) = 0.140E-09
        W(43) = 0.564E-10
        W(44) = 0.224E-10
        W(45) = 0.871E-11
        W(46) = 0.334E-11
        W(47) = 0.126E-11
        W(48) = 0.465E-12
        W(49) = 0.169E-12
        W(50) = 0.600E-13
        W(51) = 0.189E-13
        Y(1) = 1.0E0
        DO 190 I = 2, N
          Y(I) = 0.0E0
190     END DO
        GO TO 370
!     25:
200     CONTINUE
!     PROBLEM C5
        N = 30
        W(1) = 0.545E+01
        W(2) = 0.471E+01
        W(3) = 0.203E+01
        W(4) = 0.664E+01
        W(5) = 0.834E+01
        W(6) = 0.346E+01
        W(7) = 0.113E+02
        W(8) = 0.172E+02
        W(9) = 0.748E+01
        W(10) = 0.302E+02
        W(11) = 0.411E+01
        W(12) = 0.144E+01
        W(13) = 0.244E+02
        W(14) = 0.284E+02
        W(15) = 0.154E+02
        W(16) = 0.764E+00
        W(17) = 0.661E+00
        W(18) = 0.284E+00
        W(19) = 0.588E+00
        W(20) = 0.366E+00
        W(21) = 0.169E+00
        W(22) = 0.388E+00
        W(23) = 0.190E+00
        W(24) = 0.877E-01
        W(25) = 0.413E-01
        W(26) = 0.289E+00
        W(27) = 0.119E+00
        W(28) = 0.177E+00
        W(29) = 0.246E+00
        W(30) = 0.319E-01
        Y(1) = 3.42947415189E0
        Y(2) = 3.35386959711E0
        Y(3) = 1.35494901715E0
        Y(4) = 6.64145542550E0
        Y(5) = 5.97156957878E0
        Y(6) = 2.18231499728E0
        Y(7) = 11.2630437207E0
        Y(8) = 14.6952576794E0
        Y(9) = 6.27960525067E0
        Y(10) = -30.1552268759E0
        Y(11) = 1.65699966404E0
        Y(12) = 1.43785752721E0
        Y(13) = -21.1238353380E0
        Y(14) = 28.4465098142E0
        Y(15) = 15.3882659679E0
        Y(16) = -.557160570446E0
        Y(17) = .505696783289E0
        Y(18) = .230578543901E0
        Y(19) = -.415570776342E0
        Y(20) = .365682722812E0
        Y(21) = .169143213293E0
        Y(22) = -.325325669158E0
        Y(23) = .189706021964E0
        Y(24) = .0877265322780E0
        Y(25) = -.0240476254170E0
        Y(26) = -.287659532608E0
        Y(27) = -.117219543175E0
        Y(28) = -.176860753121E0
        Y(29) = -.216393453025E0
        Y(30) = -.0148647893090E0
        GO TO 370

!     PROBLEM CLASS D

210     CONTINUE
!     PROBLEM D1, D2, D3, D4, D5
        E = .2E0*(REAL(ID)-3.E1) - .1E0
        N = 4
        IF (ID/=31) GO TO 220
        W(1) = 0.110E+01
        W(2) = 0.995E+00
        W(3) = 0.101E+01
        W(4) = 0.111E+01
220     IF (ID/=32) GO TO 230
        W(1) = 0.130E+01
        W(2) = 0.954E+00
        W(3) = 0.105E+01
        W(4) = 0.136E+01
230     IF (ID/=33) GO TO 240
        W(1) = 0.150E+01
        W(2) = 0.866E+00
        W(3) = 0.115E+01
        W(4) = 0.173E+01
240     IF (ID/=34) GO TO 250
        W(1) = 0.170E+01
        W(2) = 0.714E+00
        W(3) = 0.140E+01
        W(4) = 0.238E+01
250     IF (ID/=35) GO TO 260
        W(1) = 0.190E+01
        W(2) = 0.436E+00
        W(3) = 0.229E+01
        W(4) = 0.436E+01
260     CONTINUE
        Y(1) = 1.0E0 - E
        Y(2) = 0.0E0
        Y(3) = 0.0E0
        Y(4) = SQRT((1.0E0+E)/(1.0E0-E))
        GO TO 370

!     PROBLEM CLASS E

!     41:
270     CONTINUE
!     PROBLEM E1
        N = 2
        W(1) = 0.679E+00
        W(2) = 0.478E+00
        E = .79788456080286536E0
        Y(1) = E*.84147098480789651E0
        Y(2) = E*.11956681346419146E0
        GO TO 370
!     42:
280     CONTINUE
!     PROBLEM E2
        N = 2
        W(1) = 0.201E+01
        W(2) = 0.268E+01
        Y(1) = 2.0E0
        Y(2) = 0.0E0
        GO TO 370
!     43:
290     CONTINUE
!     PROBLEM E3
        N = 2
        W(1) = 0.116E+01
        W(2) = 0.128E+01
        Y(1) = 0.0E0
        Y(2) = 0.0E0
        GO TO 370
!     44:
300     CONTINUE
!     PROBLEM E4
        N = 2
        W(1) = 0.340E+02
        W(2) = 0.277E+00
        Y(1) = 3.E1
        Y(2) = 0.E0
        GO TO 370
!     45:
310     CONTINUE
!     PROBLEM E5
        N = 2
        W(1) = 0.141E+02
        W(2) = 0.240E+01
        Y(1) = 0.0E0
        Y(2) = 0.0E0
        GO TO 370

!     PROBLEM CLASS F

!     51:
320     CONTINUE
!     PROBLEM F1
        N = 2
        W(1) = 0.129E+02
        W(2) = 0.384E+02
        Y(1) = 0.0E0
        Y(2) = 0.0E0
        HMAX = 1.0E0
        GO TO 370
!     52:
330     CONTINUE
!     PROBLEM F2
        N = 1
        W(1) = 0.110E+03
        Y(1) = 110.0E0
        HMAX = 1.0E0
        GO TO 370
!     53:
340     CONTINUE
!     PROBLEM F3
        N = 2
        W(1) = 0.131E+01
        W(2) = 0.737E+00
        Y(1) = 0.0E0
        Y(2) = 0.0E0
        HMAX = 1.0E0
        HBEGIN = 0.9E0
        GO TO 370
!     54:
350     CONTINUE
!     PROBLEM F4
        N = 1
        W(1) = 0.152E+01
        Y(1) = 1.0E0
        HMAX = 10.0E0
        GO TO 370
!     55:
360     CONTINUE
!     PROBLEM F5
        N = 1
        W(1) = 0.100E+01
        Y(1) = 1.0E0
        HMAX = 20.0E0
370     CONTINUE
        IF (IWT<0.) GO TO 390
        DO 380 I = 1, N
          Y(I) = Y(I)/W(I)
380     END DO
390     CONTINUE
        RETURN

90000   FORMAT ('AN INVALID INTERNAL PROBLEM ID OF ',I4, &
          ' WAS FOUND BY THE IVALU ROUTINE',/ &
          ' RUN TERMINATED. CHECK THE DATA.')
      END SUBROUTINE IVALU

      SUBROUTINE EVALU(Y)

!     ROUTINE TO PROVIDE THE 'TRUE' SOLUTION OF THE DIFFERENTIAL
!     EQUATION EVALUATED AT THE ENDPOINT OF THE INTEGRATION.

!     PARAMETER  (OUTPUT)
!        Y      - THE TRUE SOLUTION VECTOR EVALUATED AT THE ENDPOINT

!     PARAMETERS (INPUT)
!        N      - DIMENSION OF THE PROBLEM
!        W      - VECTOR OF WEIGHTS USED TO SCALE THE PROBLEM
!                 IF THIS OPTION IS SELECTED
!        IWT    - FLAG USED TO SIGNAL WHEN THE SCALED PROBLEM IS
!                 BEING SOLVED
!        ID     - FLAG USED TO INDICATE WHICH EQUATION IS BEING
!                 SOLVED

        IMPLICIT NONE

!     .. Scalar Arguments ..
!     INTEGER ID, IWT, N
!     .. Array Arguments ..
        REAL Y(51)
!     REAL W(51)
!     .. Local Scalars ..
        INTEGER I

!     .. Executable Statements ..

!     GOTO (20, 40, 60, 80, 100, 620, 620, 620, 620, 620, 120, 140, 160,&
!     180, 200, 620, 620, 620, 620, 620, 220, 240, 260, 280, 300, 620,  &
!     620, 620, 620, 620, 320, 340, 360, 380, 400, 620, 620, 620, 620,  &
!     620, 420, 440, 460, 480, 500, 620, 620, 620, 620, 620, 520, 540,  &
!     560, 580, 600) ID
        GO TO (10,20,30,40,50,310,310,310,310,310,60,70,80,90,100,310,310,310, &
          310,310,110,120,130,140,150,310,310,310,310,310,160,170,180,190,200, &
          310,310,310,310,310,210,220,230,240,250,310,310,310,310,310,260,270, &
          280,290,300) ID
        GO TO 310

!     PROBLEM CLASS A

!     1:
!        PROBLEM A1
10      Y(1) = 2.061153353012535E-09
        GO TO 310
!     2:
!        PROBLEM A2
20      Y(1) = 2.182178902359887E-01
        GO TO 310
!     3:
!        PROBLEM A3
30      Y(1) = 2.491650271850414E+00
        GO TO 310
!     4:
!        PROBLEM A4
40      Y(1) = 1.773016648131483E+01
        GO TO 310
!     5:
!        PROBLEM A5
50      Y(1) = -7.887826688964196E-01
        GO TO 310

!     PROBLEM CLASS B

!     11:
!        PROBLEM B1
60      Y(1) = 6.761876008576667E-01
        Y(2) = 1.860816099640036E-01
        GO TO 310
!     12:
!        PROBLEM B2
70      Y(1) = 1.000000001030576E+00
        Y(2) = 1.000000000000000E+00
        Y(3) = 9.999999989694235E-01
        GO TO 310
!     13:
!        PROBLEM B3
80      Y(1) = 2.061153488557776E-09
        Y(2) = 5.257228022048349E-02
        Y(3) = 9.474277177183630E-01
        GO TO 310
!     14:
!        PROBLEM B4
90      Y(1) = 9.826950928005993E-01
        Y(2) = 2.198447081694832E+00
        Y(3) = 9.129452507276399E-01
        GO TO 310
!     15:
!        PROBLEM B5
100     Y(1) = -9.396570798729192E-01
        Y(2) = -3.421177754000779E-01
        Y(3) = 7.414126596199957E-01
        GO TO 310

!     PROBLEM CLASS C

!     21:
!        PROBLEM C1
110     Y(1) = 2.061153622240064E-09
        Y(2) = 4.122307244619555E-08
        Y(3) = 4.122307244716968E-07
        Y(4) = 2.748204829855288E-06
        Y(5) = 1.374102414941961E-05
        Y(6) = 5.496409659803266E-05
        Y(7) = 1.832136553274552E-04
        Y(8) = 5.234675866508716E-04
        Y(9) = 1.308668966628220E-03
        Y(10) = 9.979127409508656E-01
        GO TO 310
!     22:
!        PROBLEM C2
120     Y(1) = 2.061153577984930E-09
        Y(2) = 2.061153573736588E-09
        Y(3) = 2.061153569488245E-09
        Y(4) = 2.061153565239902E-09
        Y(5) = 2.061153560991560E-09
        Y(6) = 2.061153556743217E-09
        Y(7) = 2.061153552494874E-09
        Y(8) = 2.061153548246532E-09
        Y(9) = 2.061153543998189E-09
        Y(10) = 9.999999814496180E-01
        GO TO 310
!     23:
!        PROBLEM C3
130     Y(1) = 2.948119211022058E-03
        Y(2) = 5.635380154844266E-03
        Y(3) = 7.829072515926013E-03
        Y(4) = 9.348257908594937E-03
        Y(5) = 1.007943610301970E-02
        Y(6) = 9.982674171429909E-03
        Y(7) = 9.088693332766085E-03
        Y(8) = 7.489115195185912E-03
        Y(9) = 5.322964130953349E-03
        Y(10) = 2.762434379029886E-03
        GO TO 310
!     24:
!        PROBLEM C4
140     Y(1) = 3.124111453721466E-03
        Y(2) = 6.015416842150318E-03
        Y(3) = 8.470021834842650E-03
        Y(4) = 1.033682931733337E-02
        Y(5) = 1.153249572873923E-02
        Y(6) = 1.204549525737964E-02
        Y(7) = 1.192957068015293E-02
        Y(8) = 1.128883207111195E-02
        Y(9) = 1.025804501391024E-02
        Y(10) = 8.982017581934167E-03
        Y(11) = 7.597500902492453E-03
        Y(12) = 6.219920556824985E-03
        Y(13) = 4.935916341009131E-03
        Y(14) = 3.801432544256119E-03
        Y(15) = 2.844213677587894E-03
        Y(16) = 2.069123394222672E-03
        Y(17) = 1.464687282843915E-03
        Y(18) = 1.009545263941126E-03
        Y(19) = 6.779354330227017E-04
        Y(20) = 4.437815269118510E-04
        Y(21) = 2.833264542938954E-04
        Y(22) = 1.765005798796805E-04
        Y(23) = 1.073342592697238E-04
        Y(24) = 6.374497601777217E-05
        Y(25) = 3.698645309704183E-05
        Y(26) = 2.097466832643746E-05
        Y(27) = 1.162956710412555E-05
        Y(28) = 6.306710405783322E-06
        Y(29) = 3.346286430868515E-06
        Y(30) = 1.737760074184334E-06
        Y(31) = 8.835366904275847E-07
        Y(32) = 4.399520411127637E-07
        Y(33) = 2.146181897152360E-07
        Y(34) = 1.025981211654928E-07
        Y(35) = 4.807864068784215E-08
        Y(36) = 2.209175152474847E-08
        Y(37) = 9.956251263138180E-09
        Y(38) = 4.402193653748924E-09
        Y(39) = 1.910149382204028E-09
        Y(40) = 8.135892921473050E-10
        Y(41) = 3.402477118549235E-10
        Y(42) = 1.397485617545782E-10
        Y(43) = 5.638575303049199E-11
        Y(44) = 2.235459707956947E-11
        Y(45) = 8.710498036398032E-12
        Y(46) = 3.336554275346643E-12
        Y(47) = 1.256679567784939E-12
        Y(48) = 4.654359053128788E-13
        Y(49) = 1.693559145599857E-13
        Y(50) = 5.996593816663054E-14
        Y(51) = 1.891330702629865E-14
        GO TO 310
!     25:
!        PROBLEM C5
150     Y(1) = -4.792730224323733E+00
        Y(2) = -2.420550725448973E+00
        Y(3) = -9.212509306014886E-01
        Y(4) = -4.217310404035213E+00
        Y(5) = 7.356202947498970E+00
        Y(6) = 3.223785985421212E+00
        Y(7) = 4.035559443262270E+00
        Y(8) = 1.719865528670555E+01
        Y(9) = 7.478910794233703E+00
        Y(10) = -2.998759326324844E+01
        Y(11) = -4.107310937550929E+00
        Y(12) = -9.277008321754407E-01
        Y(13) = -2.442125302518482E+01
        Y(14) = 2.381459045746554E+01
        Y(15) = 1.492096306951359E+01
        Y(16) = 3.499208963063806E-01
        Y(17) = -5.748487687912825E-01
        Y(18) = -2.551694020879149E-01
        Y(19) = -5.237040978903326E-01
        Y(20) = -2.493000463579661E-01
        Y(21) = -8.045341642044464E-02
        Y(22) = -3.875289237334110E-01
        Y(23) = 5.648603288767891E-02
        Y(24) = 3.023606472143342E-02
        Y(25) = 4.133856546712445E-02
        Y(26) = -2.862393029841379E-01
        Y(27) = -1.183032405136207E-01
        Y(28) = -1.511986457359206E-01
        Y(29) = -2.460068894318766E-01
        Y(30) = -3.189687411323877E-02
        GO TO 310

!     PROBLEM CLASS D

!     31:
!        PROBLEM D1
160     Y(1) = 2.198835352008397E-01
        Y(2) = 9.427076846341813E-01
        Y(3) = -9.787659841058176E-01
        Y(4) = 3.287977990962036E-01
        GO TO 310
!     32:
!        PROBLEM D2
170     Y(1) = -1.777027357140412E-01
        Y(2) = 9.467784719905892E-01
        Y(3) = -1.030294163192969E+00
        Y(4) = 1.211074890053952E-01
        GO TO 310
!     33:
!        PROBLEM D3
180     Y(1) = -5.780432953035361E-01
        Y(2) = 8.633840009194193E-01
        Y(3) = -9.595083730380727E-01
        Y(4) = -6.504915126712089E-02
        GO TO 310
!     34:
!        PROBLEM D4
190     Y(1) = -9.538990293416394E-01
        Y(2) = 6.907409024219432E-01
        Y(3) = -8.212674270877433E-01
        Y(4) = -1.539574259125825E-01
        GO TO 310
!     35:
!        PROBLEM D5
200     Y(1) = -1.295266250987574E+00
        Y(2) = 4.003938963792321E-01
        Y(3) = -6.775390924707566E-01
        Y(4) = -1.270838154278686E-01
        GO TO 310

!     PROBLEM CLASS E

!     41:
!        PROBLEM E1
210     Y(1) = 1.456723600728308E-01
        Y(2) = -9.883500195574063E-02
        GO TO 310
!     42:
!        PROBLEM E2
220     Y(1) = 2.008149762174948E+00
        Y(2) = -4.250887527320057E-02
        GO TO 310
!     43:
!        PROBLEM E3
230     Y(1) = -1.004178858647128E-01
        Y(2) = 2.411400132095954E-01
        GO TO 310
!     44:
!        PROBLEM E4
240     Y(1) = 3.395091444646555E+01
        Y(2) = 2.767822659672869E-01
        GO TO 310
!     45:
!        PROBLEM E5
250     Y(1) = 1.411797390542629E+01
        Y(2) = 2.400000000000002E+00
        GO TO 310

!     PROBLEM CLASS F

!     51:
!        PROBLEM F1
260     Y(1) = -1.294460621213470E1
        Y(2) = -2.208575158908672E-15
        GO TO 310
!     52:
!        PROBLEM F2
270     Y(1) = 70.03731057008607E0
        GO TO 310
!     53:
!        PROBLEM F3
280     Y(1) = -3.726957553088175E-1
        Y(2) = -6.230137949234190E-1
        GO TO 310
!     54:
!        PROBLEM F4
290     Y(1) = 9.815017249707434E-11
        GO TO 310
!     55:
!        PROBLEM F5
300     Y(1) = 1.0E0
310     CONTINUE
        IF (IWT<0) GO TO 330
        DO 320 I = 1, N
          Y(I) = Y(I)/W(I)
320     END DO
330     CONTINUE
        RETURN
      END SUBROUTINE EVALU

      SUBROUTINE FCN(X,Y,YP)

!     ROUTINE TO EVALUATE THE DERIVATIVE F(X,Y) CORRESPONDING TO
!     THE DIFFERENTIAL EQUATION:
!                    DY/DX = F(X,Y) .
!     THE ROUTINE STORES THE VECTOR OF DERIVATIVES IN YP(*). THE
!     PARTICULAR EQUATION BEING INTEGRATED IS INDICATED BY THE
!     VALUE OF THE FLAG ID WHICH IS PASSED THROUGH COMMON. THE
!     DIFFERENTIAL EQUATION IS SCALED BY THE WEIGHT VECTOR W(*)
!     IF THIS OPTION HAS BEEN SELECTED (IF SO IT IS SIGNALLED
!     BY THE FLAG IWT).

        IMPLICIT NONE

!     .. Scalar Arguments ..
        REAL X
!     .. Array Arguments ..
        REAL Y(51), YP(51)
!     .. Local Scalars ..
        REAL C1, C2, D, EX, K2, M0, P, TEMP
        INTEGER I, I3, I3M2, ITEMP, J, L, LL, MM
!     .. Local Arrays ..
        REAL M(5), Q(5,5), R(5), YTEMP(51)
!     .. Data statements ..
!     THE FOLLOWING DATA IS FOR PROBLEM C5 AND DEFINES THE MASSES
!     OF THE 5 OUTER PLANETS ETC. IN SOLAR UNITS.
!     K2 IS THE GRAVITATIONAL CONSTANT.
!     THE NEXT DATA IS FOR PROBLEMS F1 AND F5.
!     C1 IS PI**2 + 0.1**2 AND C2 IS SUM I**(4/3) FOR I=1 TO 19.
        DATA M0/1.00000597682E0/, M/.954786104043E-3, .285583733151E-3, &
          .437273164546E-4, .517759138449E-4, .277777777778E-5/
        DATA K2/2.95912208286E0/
        DATA EX, C1, C2/.33333333333333333E0, 9.879604401089358E0, &
          438.4461015267790E0/

!     .. Executable Statements ..

        NFCN = NFCN + 1
        IF (IWT<0) GO TO 20
        DO 10 I = 1, N
          YTEMP(I) = Y(I)
          Y(I) = Y(I)*W(I)
10      END DO
20      CONTINUE
        GO TO (30,40,50,60,70,430,430,430,430,430,80,90,100,110,120,430,430, &
          430,430,430,130,150,170,190,210,430,430,430,430,430,290,290,290,290, &
          290,430,430,430,430,430,300,310,320,330,340,430,430,430,430,430,350, &
          370,390,400,420) ID
        GO TO 430

!     PROBLEM CLASS A

!     1:
!        PROBLEM A1
30      YP(1) = -Y(1)
        GO TO 430
!     2:
!        PROBLEM A2
40      YP(1) = -.5E0*Y(1)*Y(1)*Y(1)
        GO TO 430
!     3:
!        PROBLEM A3
50      YP(1) = Y(1)*COS(X)
        GO TO 430
!     4:
!        PROBLEM A4
60      YP(1) = (1.0E0-Y(1)/20.0E0)*Y(1)/4.E0
        GO TO 430
!     5:
!        PROBLEM A5
70      YP(1) = (Y(1)-X)/(Y(1)+X)
        GO TO 430

!     PROBLEM CLASS B

!     11:
!        PROBLEM B1
80      D = Y(1) - Y(1)*Y(2)
        YP(1) = D + D
        YP(2) = -(Y(2)-Y(1)*Y(2))
        GO TO 430
!     12:
!        PROBLEM B2
90      YP(1) = -Y(1) + Y(2)
        YP(3) = Y(2) - Y(3)
        YP(2) = -YP(1) - YP(3)
        GO TO 430
!     13:
!        PROBLEM B3
100     D = Y(2)*Y(2)
        YP(1) = -Y(1)
        YP(2) = Y(1) - D
        YP(3) = D
        GO TO 430
!     14:
!        PROBLEM B4
110     D = SQRT(Y(1)*Y(1)+Y(2)*Y(2))
        YP(1) = -Y(2) - Y(1)*Y(3)/D
        YP(2) = Y(1) - Y(2)*Y(3)/D
        YP(3) = Y(1)/D
        GO TO 430
!     15:
!        PROBLEM B5
120     YP(1) = Y(2)*Y(3)
        YP(2) = -Y(1)*Y(3)
        YP(3) = -.51E0*Y(1)*Y(2)
        GO TO 430

!     PROBLEM CLASS C

!     21:
!        PROBLEM C1
130     YP(1) = -Y(1)
        DO 140 I = 2, 9
          YP(I) = Y(I-1) - Y(I)
140     END DO
        YP(10) = Y(9)
        GO TO 430
!     22:
!        PROBLEM C2
150     YP(1) = -Y(1)
        DO 160 I = 2, 9
          YP(I) = REAL(I-1)*Y(I-1) - REAL(I)*Y(I)
160     END DO
        YP(10) = 9.0E0*Y(9)
        GO TO 430
!     23:
!        PROBLEM C3
170     YP(1) = -2.0E0*Y(1) + Y(2)
        DO 180 I = 2, 9
          YP(I) = Y(I-1) - 2.0E0*Y(I) + Y(I+1)
180     END DO
        YP(10) = Y(9) - 2.0E0*Y(10)
        GO TO 430
!     24:
!        PROBLEM C4
190     YP(1) = -2.0E0*Y(1) + Y(2)
        DO 200 I = 2, 50
          YP(I) = Y(I-1) - 2.0E0*Y(I) + Y(I+1)
200     END DO
        YP(51) = Y(50) - 2.0E0*Y(51)
        GO TO 430
!     25:
!        PROBLEM C5
210     I = 0
        DO 250 L = 3, 15, 3
          I = I + 1
          P = Y(L-2)**2 + Y(L-1)**2 + Y(L)**2
          R(I) = 1.0E0/(P*SQRT(P))
          J = 0
          DO 240 LL = 3, 15, 3
            J = J + 1
            IF (LL/=L) GO TO 220
            GO TO 230
!              THEN
220         P = (Y(L-2)-Y(LL-2))**2 + (Y(L-1)-Y(LL-1))**2 + (Y(L)-Y(LL))**2
            Q(I,J) = 1.0E0/(P*SQRT(P))
            Q(J,I) = Q(I,J)
230         CONTINUE
240       END DO
250     END DO
        I3 = 0
        DO 280 I = 1, 5
          I3 = I3 + 3
          I3M2 = I3 - 2
          DO 270 LL = I3M2, I3
            MM = LL - I3
            YP(LL) = Y(LL+15)
            P = 0.0E0
            DO 260 J = 1, 5
              MM = MM + 3
              IF (J/=I) P = P + M(J)*(Y(MM)*(Q(I,J)-R(J))-Y(LL)*Q(I,J))
260         END DO
            YP(LL+15) = K2*(-(M0+M(I))*Y(LL)*R(I)+P)
270       END DO
280     END DO
        GO TO 430

!     PROBLEM CLASS D

!     31:32:33:34:35:
!        PROBLEMS D1, D2, D3, D4, D5
290     YP(1) = Y(3)
        YP(2) = Y(4)
        D = Y(1)*Y(1) + Y(2)*Y(2)
        D = SQRT(D*D*D)
        YP(3) = -Y(1)/D
        YP(4) = -Y(2)/D
        GO TO 430

!     PROBLEM CLASS E

!     41:
!        PROBLEM E1
300     YP(1) = Y(2)
        YP(2) = -(Y(2)/(X+1.0E0)+(1.0E0-.25E0/(X+1.0E0)**2)*Y(1))
        GO TO 430
!     42:
!        PROBLEM E2
310     YP(1) = Y(2)
        YP(2) = (1.0E0-Y(1)*Y(1))*Y(2) - Y(1)
        GO TO 430
!     43:
!        PROBLEM E3
320     YP(1) = Y(2)
        YP(2) = Y(1)**3/6.0E0 - Y(1) + 2.0E0*SIN(2.78535E0*X)
        GO TO 430
!     44:
!        PROBLEM E4
330     YP(1) = Y(2)
        YP(2) = .032E0 - .4E0*Y(2)*Y(2)
        GO TO 430
!     45:
!        PROBLEM E5
340     YP(1) = Y(2)
        YP(2) = SQRT(1.0E0+Y(2)*Y(2))/(25.0E0-X)
        GO TO 430

!     PROBLEM CLASS F

!     51:
!        PROBLEM F1
350     YP(1) = Y(2)
        YP(2) = .2E0*Y(2) - C1*Y(1)
        ITEMP = INT(X)
        IF ((ITEMP/2)*2==ITEMP) GO TO 360
        YP(2) = YP(2) - 1.0E0
        GO TO 430
360     YP(2) = YP(2) + 1.0E0
        GO TO 430
!     52:
!        PROBLEM F2
370     ITEMP = INT(X)
        IF ((ITEMP/2)*2==ITEMP) GO TO 380
        YP(1) = 55.0E0 - .50E0*Y(1)
        GO TO 430
380     YP(1) = 55.0E0 - 1.50E0*Y(1)
        GO TO 430
!     53:
!        PROBLEM F3
390     YP(1) = Y(2)
        YP(2) = .01E0*Y(2)*(1.0E0-Y(1)**2) - Y(1) - ABS(SIN( &
          3.1415926535897932E0*X))
        GO TO 430
!     54:
!        PROBLEM F4
400     IF (X>10.) GO TO 410
        TEMP = X - 5.0E0
        YP(1) = -2.0E0/21.0E0 - 120.0E0*TEMP/(1.0E0+4.0E0*TEMP**2)**16
        GO TO 430
410     YP(1) = -2.0E0*Y(1)
        GO TO 430
!     55:
!        PROBLEM F5
420     YP(1) = Y(1)*(4.0E0/(3.0E0*C2))*(SIGN(ABS(X- &
          1.0E0)**EX,X-1.0E0)+SIGN(ABS(X-2.0E0)**EX,X-2.0E0)+SIGN(ABS(X- &
          3.0E0)**EX,X-3.0E0)+SIGN(ABS(X-4.0E0)**EX,X-4.0E0)+SIGN(ABS(X- &
          5.0E0)**EX,X-5.0E0)+SIGN(ABS(X-6.0E0)**EX,X-6.0E0)+SIGN(ABS(X- &
          7.0E0)**EX,X-7.0E0)+SIGN(ABS(X-8.0E0)**EX,X-8.0E0)+SIGN(ABS(X- &
          9.0E0)**EX,X-9.0E0)+SIGN(ABS(X-10.0E0)**EX,X-10.0E0)+SIGN(ABS(X- &
          11.0E0)**EX,X-11.0E0)+SIGN(ABS(X-12.0E0)**EX,X-12.0E0)+SIGN(ABS(X- &
          13.0E0)**EX,X-13.0E0)+SIGN(ABS(X-14.0E0)**EX,X-14.0E0)+SIGN(ABS(X- &
          15.0E0)**EX,X-15.0E0)+SIGN(ABS(X-16.0E0)**EX,X-16.0E0)+SIGN(ABS(X- &
          17.0E0)**EX,X-17.0E0)+SIGN(ABS(X-18.0E0)**EX,X-18.0E0)+SIGN(ABS(X- &
          19.0E0)**EX,X-19.0E0))
430     CONTINUE
        IF (IWT<0) GO TO 450
        DO 440 I = 1, N
          YP(I) = YP(I)/W(I)
          Y(I) = YTEMP(I)
440     END DO
450     CONTINUE
        RETURN
      END SUBROUTINE FCN

    END MODULE NONSTIFFSET

!******************************************************************

    PROGRAM DEMONONSTIFF

      USE NONSTIFFSET
      USE DVODE_F90_M

      IMPLICIT NONE
      INTEGER ITASK, ISTATE, ISTATS, NEQ, I, CLASS, PROBLEM, MYID, ITEST
      REAL RSTATS, T, TOUT, HBEGIN, HBOUND, TBEGIN, TEND, Y, EPS, YINIT, &
        YFINAL, RELERR_TOLERANCES, ABSERR_TOLERANCES, AERROR
      LOGICAL USEW, USEHBEGIN
      DIMENSION Y(51), RSTATS(22), ISTATS(31), YINIT(51), YFINAL(51), MYID(55)
      DIMENSION RELERR_TOLERANCES(51), ABSERR_TOLERANCES(51), AERROR(51)
      TYPE (VODE_OPTS) :: OPTIONS
      DATA MYID/1, 2, 3, 4, 5, 0, 0, 0, 0, 0, 11, 12, 13, 14, 15, 0, 0, 0, 0, &
        0, 21, 22, 23, 24, 25, 0, 0, 0, 0, 0, 31, 32, 33, 34, 35, 0, 0, 0, 0, &
        0, 41, 42, 43, 44, 45, 0, 0, 0, 0, 0, 51, 52, 53, 54, 55/

      OPEN (UNIT=6,FILE='demononstiff.dat')

      DO 20 ITEST = 1, 55
        ID = MYID(ITEST)
        IF (ID==0) GO TO 20
        WRITE (6,90010)

        CLASS = ID/10
        PROBLEM = ID - 10*CLASS
        IF (CLASS==0) THEN
          WRITE (6,90000) PROBLEM
        ELSE IF (CLASS==1) THEN
          WRITE (6,90001) PROBLEM
        ELSE IF (CLASS==2) THEN
          WRITE (6,90002) PROBLEM
        ELSE IF (CLASS==3) THEN
          WRITE (6,90003) PROBLEM
        ELSE IF (CLASS==4) THEN
          WRITE (6,90004) PROBLEM
        ELSE IF (CLASS==5) THEN
          WRITE (6,90005) PROBLEM
        END IF

!     Scale the odes?
        USEW = .TRUE.
!     Use the IVALU starting step size?
        USEHBEGIN = .TRUE.
        IWT = -1
        IF (USEW) IWT = 1
        CALL IVALU(TBEGIN,TEND,HBEGIN,HBOUND,YINIT)
        IF ( .NOT. USEHBEGIN) HBEGIN = 0.0E0
        NEQ = N
        T = TBEGIN
        TOUT = TEND
        Y(1:NEQ) = YINIT(1:NEQ)
        EPS = 1E-5
        RELERR_TOLERANCES(1:NEQ) = EPS
        ABSERR_TOLERANCES(1:NEQ) = EPS
        WRITE (6,90007) ID, TBEGIN, TEND, HBEGIN, HBOUND, IWT, N, EPS, &
          Y(1:NEQ)


        ITASK = 1
        ISTATE = 1
        OPTIONS = SET_OPTS(RELERR_VECTOR=RELERR_TOLERANCES(1:NEQ), &
          ABSERR_VECTOR=ABSERR_TOLERANCES(1:NEQ),MXSTEP=100000,H0=HBEGIN, &
          HMAX=HBOUND)
10      CONTINUE
        CALL DVODE_F90(DERIVS,NEQ,Y,T,TOUT,ITASK,ISTATE,OPTIONS)

        IF (ISTATE<0) THEN
          WRITE (6,90006) ISTATE
          STOP
        END IF
        CALL GET_STATS(RSTATS,ISTATS)
        WRITE (6,90009) ISTATS(11), ISTATS(12)
        IF (TOUT<TEND) GO TO 10
        CALL EVALU(YFINAL)
        DO I = 1, NEQ
          AERROR(I) = ABS(Y(I)-YFINAL(I))
        END DO
        WRITE (6,90008) (I,Y(I),YFINAL(I),AERROR(I),I=1,NEQ)

20    CONTINUE ! End of ITEST Loop

!     Format statements for this problem:

90000 FORMAT (' Class/Problem = A',I1)
90001 FORMAT (' Class/Problem = B',I1)
90002 FORMAT (' Class/Problem = C',I1)
90003 FORMAT (' Class/Problem = D',I1)
90004 FORMAT (' Class/Problem = E',I1)
90005 FORMAT (' Class/Problem = F',I1)
90006 FORMAT (' An error occurred in VODE_F90. ISTATE = ',I3)
90007 FORMAT (' Problem ID       = ',I3,/,' Initial time     = ',E15.5,/, &
        ' Final time       = ',E15.5,/,' Initial stepsize = ',E15.5,/, &
        ' Maximum stepsize = ',E15.5,/,' IWT flag         = ',I3,/, &
        ' Number of odes   = ',I3,/,' Error tolerance  = ',E15.5,/, &
        ' Initial solution = ',/,(E15.5))
90008 FORMAT (' Computed and reference solutions and absolute', &
        ' errors follow:',/,(I3,3E15.5))
90009 FORMAT (' Steps = ',I10,' f-s = ',I10)
90010 FORMAT (' _________________________________________')
      STOP
    END PROGRAM DEMONONSTIFF
