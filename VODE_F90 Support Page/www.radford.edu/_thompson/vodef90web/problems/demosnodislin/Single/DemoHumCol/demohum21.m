function [t,y] = demohum21
% Matlab script to plot the solution for the packed
% humidification problem

temp = load('demohum21plot.dat','-ascii');
tp  = temp(:,1);
tlp = temp(:,2);
vp  = temp(:,3);

figure
plot(tp,tlp)
title('Humidification Column: Inlet Temperature')
xlabel('t (hr)');
ylabel('TL(0,t) (C)');

figure
plot(tp,vp)
title('Humidification Column: Velocity')
xlabel('t (hr)');
ylabel('V(t) (mols/hr)')
