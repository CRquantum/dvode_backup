! DVODE_F90 demonstration program
! Root finding is used to build the return maps for the coupled
! harmonic oscillator problem in the Shampine/Gladwell/Thompson
! book.

    MODULE HARMONIC_DEMO

      IMPLICIT NONE

    CONTAINS

      SUBROUTINE DERIVS(NEQ,T,Y,YDOT)
!     Subroutine to evaluate dy/dt for this problem
        IMPLICIT NONE
        INTEGER NEQ
        REAL T, Y, YDOT, A, B
        DIMENSION Y(NEQ), YDOT(NEQ)

        A = 3.12121212E0
        B = 2.11111111E0
        YDOT(1) = A*Y(3)
        YDOT(2) = B*Y(4)
        YDOT(3) = -A*Y(1)
        YDOT(4) = -B*Y(2)
        RETURN
      END SUBROUTINE DERIVS

      SUBROUTINE GEVENTS(NEQ,T,Y,NG,GOUT)
!     Subroutine to evaluate g(t,y) for this problem
        IMPLICIT NONE
        INTEGER NEQ, NG
        REAL T, Y, GOUT
        DIMENSION Y(NEQ), GOUT(NG)

        GOUT(1) = Y(2)
        GOUT(2) = Y(3)
        RETURN
      END SUBROUTINE GEVENTS

    END MODULE HARMONIC_DEMO

!******************************************************************

    PROGRAM HARMONIC

      USE HARMONIC_DEMO
      USE DVODE_F90_M

!     Type declarations:
      IMPLICIT NONE
!     Number of odes and number of event functions for this problem:
      INTEGER, PARAMETER :: NEQ = 4, NG = 2
      INTEGER ITASK, ISTATE, JROOTS, ISTATS, FOUND, NOUT7, NOUT8
      REAL ATOL, RTOL, RSTATS, T, TOUT, Y
      DIMENSION Y(NEQ), RSTATS(22), ISTATS(31), JROOTS(NG), FOUND(NG)

      TYPE (VODE_OPTS) :: OPTIONS

!     Open the output file and the plot files:
      OPEN (UNIT=6,FILE='demoharmonic.dat')
      OPEN (UNIT=7,FILE='demoharmonicplot1.dat')
      OPEN (UNIT=8,FILE='demoharmonicplot2.dat')

!     Number of roots found:
      NOUT7 = 0
      NOUT8 = 0
      FOUND(1:NG) = 0
!     Set the initial conditions:
      Y(1:NEQ) = 5.0E0
!     Set the integration parameters:
      T = 0.0E0
      TOUT = 65.0E0
      RTOL = 1.0E-6
      ATOL = 1.0E-6
      ITASK = 1
      ISTATE = 1

!     Set the VODE_F90 options:
      OPTIONS = SET_OPTS(RELERR=RTOL,ABSERR=ATOL,NEVENTS=NG)

10    CONTINUE

!     Perform the integration:
      CALL DVODE_F90(DERIVS,NEQ,Y,T,TOUT,ITASK,ISTATE,OPTIONS,G_FCN=GEVENTS)

!     Gather and write the integration statistics for this problem:
      CALL GET_STATS(RSTATS,ISTATS,NG,JROOTS)
      WRITE (6,90005) T, Y(1), Y(2), Y(3), Y(4)

!     Stop the integration if an error occurred:
      IF (ISTATE<0) THEN
        PRINT *, ' The integration was not successful.'
        GO TO 30
      END IF

!     Terminate the integration if we have reached the final output time:
      IF (ISTATE==2) GO TO 20

!     A root was found:
      FOUND(1:2) = FOUND(1:2) + JROOTS(1:2)
      IF (JROOTS(1)==1) THEN
        NOUT7 = NOUT7 + 1
        WRITE (7,90002) Y(1), Y(3)
      END IF
      IF (JROOTS(2)==1) THEN
        NOUT8 = NOUT8 + 1
        WRITE (8,90002) Y(1), Y(2)
      END IF
      WRITE (6,90004) JROOTS(1), JROOTS(2)
      ISTATE = 2

!     Continue the integration from this root:
      GO TO 10

20    CONTINUE

!     Write the integration final root finding statistics for
!     this problem:
      WRITE (6,90003) ISTATS(11), ISTATS(12), ISTATS(13), ISTATS(10)
      WRITE (6,90000) FOUND(1), FOUND(2)
      PRINT *, ' Number of roots found: ', FOUND(1), FOUND(2)
      IF (FOUND(1)/=43 .OR. FOUND(2)/=65) THEN
        WRITE (6,90001)
        PRINT *, ' The correct number of roots was not found:'
      ELSE
        PRINT *, ' The correct number of roots was found:'
      END IF
      STOP
30    WRITE (6,90006) ISTATE

!     Format statements for this problem:
90000 FORMAT (/' Number of roots found: ',2I5)
90001 FORMAT (/' The correct number of roots was not found: ',2I5)
90002 FORMAT (2D15.5)
90003 FORMAT (/' Steps = ',I4,' f-s = ',I4,' J-s = ',I4,' g-s =',I4/)
90004 FORMAT (5X,' The above line is a root,JROOTS = ',2I5)
90005 FORMAT (' t = ',E12.4,' Y = ',4E14.5)
90006 FORMAT (/' An error occurred in DVODE_F90. ISTATE = ',I3)
      STOP

    END PROGRAM HARMONIC
