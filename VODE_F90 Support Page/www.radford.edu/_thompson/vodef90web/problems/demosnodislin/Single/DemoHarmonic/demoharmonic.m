function [t,y] = demoharmonic
% Matlab script to plot the return maps for the
% harmonic oscillator problem

temp1 = load('demoharmonicplot1.dat','-ascii');
y13 = temp1(:,1:end);

temp2 = load('demoharmonicplot2.dat','-ascii');
y12 = temp2(:,1:end);

figure
plot(y13(:,1),y13(:,2),'*')
title('Harmonic Oscillators: y_3 vs y_1 when y_2 = 0')
axis([-8,8,-8,8])

figure
plot(y12(:,1),y12(:,2),'*')
title('Harmonic Oscillators: y_2 vs y_1 when y_3 = 0')
axis([-8,8,-8,8])
