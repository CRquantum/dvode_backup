    MODULE DEMOAR2

! VODE_F90 demonstration program

! Example Problem.

! The following is a simple example problem, with the coding
! needed for its solution using root finding by DVODE_F90.
! The problem is from chemical kinetics, and consists of the
! following three rate equations:
!     dy1/dt = -.04*y1 + 1.e4*y2*y3
!     dy2/dt = .04*y1 - 1.e4*y2*y3 - 3.e7*y2**2
!     dy3/dt = 3.e7*y2**2
! on the interval from t = 0.0 to t = 4.e10, with initial
! conditions y1 = 1.0, y2 = y3 = 0. The problem is stiff.
! In addition, we want to find the values of t, y1, y2,
! and y3 at which:
!   (1) y1 reaches the value 1.d-4, and
!   (2) y3 reaches the value 1.d-2.

! The following coding solves this problem with DVODE_F90,
! printing results at t = .4, 4., ..., 4.e10, and at the
! computed roots. It uses ITOL = 2 and ATOL much smaller
! for y2 than y1 or y3 because y2 has much smaller values.
! At the end of the run, statistical quantities of interest
! are printed (see optional outputs in the full description
! below).

    CONTAINS

      SUBROUTINE FEX(NEQ,T,Y,YDOT)
        IMPLICIT NONE
        INTEGER NEQ
        REAL T, Y, YDOT
        DIMENSION Y(3), YDOT(3)

        YDOT(1) = -0.04E0*Y(1) + 1.0E4*Y(2)*Y(3)
        YDOT(3) = 3.0E7*Y(2)*Y(2)
        YDOT(2) = -YDOT(1) - YDOT(3)
        RETURN
      END SUBROUTINE FEX

      SUBROUTINE GEX(NEQ,T,Y,NG,GOUT)
        IMPLICIT NONE
        INTEGER NEQ, NG
        REAL T, Y, GOUT
        DIMENSION Y(3), GOUT(2)

        GOUT(1) = Y(1) - 1.0E-4
        GOUT(2) = Y(3) - 1.0E-2
        RETURN
      END SUBROUTINE GEX

    END MODULE DEMOAR2

!******************************************************************

    PROGRAM RUNDEMOAR2

      USE DVODE_F90_M
      USE DEMOAR2

      IMPLICIT NONE
      INTEGER ITASK, ISTATE, NG, NEQ, IOUT, JROOT, ISTATS
      REAL ATOL, RTOL, RSTATS, T, TOUT, Y
      DIMENSION Y(3), ATOL(3), RSTATS(22), ISTATS(31), JROOT(2)
      TYPE (VODE_OPTS) :: OPTIONS

      OPEN (UNIT=6,FILE='demoar2.dat')
      NEQ = 3
      Y(1) = 1.0E0
      Y(2) = 0.0E0
      Y(3) = 0.0E0
      T = 0.0E0
      TOUT = 0.4E0
      RTOL = 1.0E-6
      ATOL(1) = 1.0E-8
      ATOL(2) = 1.0E-12
      ATOL(3) = 1.0E-8
      ITASK = 1
      ISTATE = 1
      NG = 2
      OPTIONS = SET_OPTS(DENSE_J=.TRUE.,RELERR=RTOL,ABSERR_VECTOR=ATOL, &
        NEVENTS=NG)
      DO 20 IOUT = 1, 11
10      CONTINUE
        CALL DVODE_F90(FEX,NEQ,Y,T,TOUT,ITASK,ISTATE,OPTIONS,G_FCN=GEX)
        CALL GET_STATS(RSTATS,ISTATS,NG,JROOT)
        WRITE (6,90000) T, Y(1), Y(2), Y(3)
90000   FORMAT (' At t =',E12.4,'   Y =',3E14.6)
        IF (ISTATE<0) GO TO 30
        IF (ISTATE==2) GO TO 20
        WRITE (6,90001) JROOT(1), JROOT(2)
90001   FORMAT (5X,' The above line is a root, JROOT =',2I5)
        ISTATE = 2
        GO TO 10
20    TOUT = TOUT*10.0E0
      WRITE (6,90002) ISTATS(11), ISTATS(12), ISTATS(13), ISTATS(10)
90002 FORMAT (/' No. steps =',I4,'  No. f-s =',I6,'  No. J-s =',I4, &
        '  No. g-s =',I4/)
      STOP
30    WRITE (6,90003) ISTATE
90003 FORMAT (///' Error halt.. ISTATE =',I3)
      STOP

    END PROGRAM RUNDEMOAR2
