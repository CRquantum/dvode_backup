! DVODE_F90 demonstration program
! This problem solves Bill Schiesser's convective cooling of a polymer
! sheet problem. (See problem description in INITAL.) Solution bounds
! are enforced. This program illustrates how to use DSS/2 in connection
! with DVODE_F90. It is modelled after the original f77 program for
! this problem.

    PROGRAM DEMOP11

      USE DVODE_F90_M

!     DEMOP11 CALLS THE FOLLOWING SUBROUTINES:
!       (1) INITAL TO DEFINE THE ODE INITAL CONDITIONS
!       (2) SET_OPTS TO DEFINE THE DVODE INTEGRATIONS
!       (3) DVODE_F90 TO INTEGRATE THE ODES
!       (4) GET_STATS TO OBTAIN INTEGRATION STATISTICS
!       (5) PRINT TO PRINT THE SOLUTION

      IMPLICIT NONE
      REAL T, T0, TP, TV, TOUT, ATOL, RTOL, RSTATS, YMIN, YMAX
      INTEGER NST, NFE, LENRW, LENIW, NNI, NCFN, NETF, NFEA, ISTATS, ISTATE, &
        ITASK, IOUT, NJE, NOUT, NLU, IDX, I, ML, MU, J, NPLOT
      DIMENSION ATOL(1), RTOL(1), RSTATS(22), ISTATS(30), IDX(301), YMIN(301), &
        YMAX(301)
      LOGICAL SPARSE, DENSE, BANDED
      TYPE (VODE_OPTS) :: OPTIONS

!     THE FOLLOWING CODING IS FOR 301 ORDINARY DIFFERENTIAL EQUATIONS
!     (ODES). IF MORE ODES ARE TO BE INTEGRATED, ALL OF THE  301*S
!     SHOULD BE CHANGED TO THE REQUIRED NUMBER.

      INTEGER NSTOP, NORUN
      REAL Y, F
      COMMON /T/T, NSTOP, NORUN/Y/Y(301)/F/F(301)
!     NOTE: Y AND F ARE THE SAME AS THE LOCAL VARIABLES IN INITAL
!     AND DERV.

!     THE NUMBER OF ODES IS IN COMMON /N/ FOR USE IN THE PROBLEM
!     SUBROUTINES:
      INTEGER N
      COMMON /N/N

!     COMMON AREA TO PROVIDE THE INPUT/OUTPUT UNIT NUMBERS
!     TO OTHER SUBROUTINES:
      INTEGER NI, NO
      COMMON /IO/NI, NO

!     LOCAL SOLUTION AND DERIVATIVE:
!     REAL YDOT
!     DIMENSION YDOT(301)
      REAL YV
      DIMENSION YV(301)

!     COMMUNICATE SOME THINGS BETWEEN DEMOP11 AND THE SUBROUTINES:
      REAL ZMESH, SPLOT
      LOGICAL BOUNDS, HEAT_TRANSFER, MANUAL, T0TESAME
      COMMON /DEMOP11DAT/BOUNDS, HEAT_TRANSFER, MANUAL, T0TESAME, ZMESH(301), &
        SPLOT(301)

!     PLOT ARRAYS:
!     REAL TPLOT
!     DIMENSION TPLOT(101)
      REAL YPLOT
      DIMENSION YPLOT(301,101)

!     Note:
!     Since the problem subroutines are not in a module, it is
!     necessary to declare the derivative subroutine as EXTERNAL:
      EXTERNAL FCN

!     DEFINE THE INPUT/OUTPUT UNIT NUMBERS:
      NI = 5
      NO = 6

!     OPEN THE OUTPUT FILE AND THE PLOT FILES:
      OPEN (NO,FILE='demop11.dat')
      OPEN (7,FILE='demop11plot.dat')
      OPEN (8,FILE='demop11mesh.dat')

!     NUMBER OF ODES:
!     N = 41
      N = 101
      IF (N>301) STOP

!*********************************************************

!     COMPUTE SPATIAL DERIVATIVE BY FIVE-POINT CENTERED OR
!     BY FIVE-POINT BIASED UPWIND DIFFERENCES:
!        CENTERED: NORUN = 1
!        UPWIND:   NORUN = 2
!     NORUN = 1
      NORUN = 2

!     ENFORCE SOLUTION BOUNDS:
!        YES: BOUNDS = .TRUE.
!        NO:  BOUNDS = .FALSE. (Original program)
!        Use FALSE if want to compare the Matlab plot
!        (If FALSE, will see the temperature above 400
!        near the beginning of the integration)
      BOUNDS = .TRUE.

!     INCLUDE HEAT TRANSFER:
!        YES: HEAT_TRANSFER = .TRUE. (Original program)
!        NO:  HEAT_TRANSFER = .FALSE.
      HEAT_TRANSFER = .TRUE.

!     MANUALLY LIMIT TEMPERATURES NOT TO EXCEED TE=400 IN DERV:
!        YES: MANUAL = .TRUE.
!        NO:  MANUAL = .FALSE. (Original program)
      MANUAL = .FALSE.

!     T0 AND TE THE SAME:
!        YES: T0TESAME = .TRUE.
!        NO:  T0TESAME = .FALSE. (Original program)
      T0TESAME = .FALSE.
!*********************************************************

!     SET DVODE_F90 JACOBIAN OPTION (dense, banded, sparse, nonstiff)
!     (Set only one of these TRUE.):
      SPARSE = .FALSE.
      DENSE = .FALSE.
      BANDED = .TRUE.

      IF (DENSE) THEN
        PRINT *, 'The dense Jacobian option will be used.'
      ELSE IF (BANDED) THEN
        PRINT *, 'The banded Jacobian option will be used.'
      ELSE IF (SPARSE) THEN
        PRINT *, 'The sparse Jacobian option will be used.'
      ELSE
        PRINT *, 'iThe nonstiff option will be used.'
      END IF

!     JACOBIAN BANDWIDTHS:
      ML = 4
      MU = 4

!     ABSOLUTE AND RELATIVE ERROR TOLERANCES:
      ATOL(1) = 1.0E-6
      RTOL(1) = 1.0E-6

!     INITIAL TIME AND OUTPUT INCREMENT:
      T0 = 0.0E0
      TP = 0.1E0
      T = T0
      TV = T0

!     NUMBER OF OUTPUT TIMES:
!     INTEGRATE TO T = 40:
!     Use this for the full solution:
!     NOUT = 101
!     Use this to see the bounds enforced near the beginning
!     in the Matlab plot:
      NOUT = 6
      NOUT = MIN(NOUT,101)

!     SET THE INITIAL CONDITIONS (THIS SETS THE Y VECTOR TOO):
      CALL INITAL

!     SET THE INITIAL DERIVATIVES (FOR POSSIBLE PRINTING):
      CALL DERV

!     PRINT THE INITIAL CONDITIONS:
      CALL PRINT(NI,NO)

!     SET THE INITIAL CONDITIONS FOR SUBROUTINE DVODE_F90:
      IF (BOUNDS) THEN
        YV(1:N) = 400.0E0 - Y(1:N)
        YV(1) = 0.0E0
      ELSE
        YV(1:N) = Y(1:N)
        YV(1) = 400.0E0
      END IF

!     WRITE(NO, 184) (I,YV(I),I=1,N)

!     ENFORCE NEGATIVITY FOR ALL COMPONENTS:
      IF (BOUNDS) THEN
        DO I = 1, N
          IDX(I) = I
          YMIN(I) = 0.0E0
          YMAX(I) = 1E10
        END DO
        WRITE (NO,90002)
      ELSE
        WRITE (NO,90003)
      END IF

!     INITIALIZE THE INTEGRATION FLAGS:
      ITASK = 1
      ISTATE = 1

!     SET REMAINING VODE_F90 OPTIONS:
      IF (BOUNDS) THEN
        OPTIONS = SET_OPTS(SPARSE_J=SPARSE,DENSE_J=DENSE,BANDED_J=BANDED, &
          LOWER_BANDWIDTH=ML,UPPER_BANDWIDTH=MU,ABSERR=ATOL(1),RELERR=RTOL(1), &
          MXSTEP=5000,CONSTRAINED=IDX(1:N),CLOWER=YMIN(1:N),CUPPER=YMAX(1:N))
      ELSE
        OPTIONS = SET_OPTS(SPARSE_J=SPARSE,DENSE_J=DENSE,BANDED_J=BANDED, &
          LOWER_BANDWIDTH=ML,UPPER_BANDWIDTH=MU,ABSERR=ATOL(1),RELERR=RTOL(1), &
          MXSTEP=5000)
      END IF

      NPLOT = 0
      DO IOUT = 1, NOUT
        IF (NPLOT<101) NPLOT = NPLOT + 1
        IF (IOUT<=5) THEN
          TOUT = TV + TP
        ELSE
          TOUT = 40.0E0
        END IF

!        PERFORM THE INTEGRATION:
        CALL DVODE_F90(FCN,N,YV,TV,TOUT,ITASK,ISTATE,OPTIONS)

!        GATHER THE INTEGRATION STATISTICS FOR THIS CALL:
        CALL GET_STATS(RSTATS,ISTATS)

!        CHECK IF AN ERROR OCCURRED:
!        IF (ISTATE < 0) GOTO 175

!        TRANSFER THE SOLUTION VARIABLE TO LOCAL STORAGE:
        IF (BOUNDS) THEN
          Y(1:N) = 400.0E0 - YV(1:N)
          Y(1) = 400.0E0
        ELSE
          Y(1:N) = YV(1:N)
          Y(1) = 400.0E0
        END IF

!        PRINT THE SOLUTION:
        T = TV
        CALL PRINT(NI,NO)
        IF (ISTATE<0) GO TO 10

!        STORE SOLUTION FOR PLOTTING:
!        TPLOT(NPLOT) = T
        YPLOT(1:N,NPLOT) = Y(1:N)

      END DO

!     Write the solution to a file for subsequent plotting in Matlab:
      DO I = 1, N
        WRITE (7,90000) (YPLOT(I,J),J=1,6)
      END DO
90000 FORMAT (6E12.4)
10    CONTINUE

!     CHECK IF AN ERROR OCCURRED IN VODE_F90:
      IF (ISTATE<0) THEN
        PRINT *, 'DVODE_F90 returned ISTATE = ', ISTATE
        WRITE (NO,*) 'DVODE_F90 returned ISTATE = ', ISTATE
!        STOP
      END IF

!     PRINT THE FINAL INTEGRATION STATISTICS:
      NST = ISTATS(11)
      NFE = ISTATS(12)
      NJE = ISTATS(13)
      NLU = ISTATS(19)
      LENRW = ISTATS(17)
      LENIW = ISTATS(18)
      NNI = ISTATS(20)
      NCFN = ISTATS(21)
      NETF = ISTATS(22)
      NFEA = NFE
      NFEA = NFE - NJE
      WRITE (NO,90001) LENRW, LENIW, NST, NFE, NFEA, NJE, NLU, NNI, NCFN, NETF
!     WRITE(NO, 183) (I,YV(I),I=1,N)

!     FORMATS:
90001 FORMAT ('   Final statistics for this run:'/'   RWORK size =',I4, &
        '   IWORK size =',I8/'   Number of steps =',I8/'   Number of f-s   =', &
        I8/'   (excluding J-s) =',I8/'   Number of J-s   =', &
        I8/'   Number of LU-s  =',I5/'   Number of nonlinear iterations =', &
        I8/'   Number of nonlinear convergence failures =', &
        I8/'   Number of error test failures =',I8/)
90002 FORMAT (' Nonnegativity enforced.')
90003 FORMAT (' Nonnegativity not enforced.')
90004 FORMAT (' Final DVODE_F90 Solution:',/,(I5,E15.5))
90005 FORMAT (' Initial DVODE_F90 Solution:',/,(I5,E15.5))

!     TERMINATE EXECUTION:
      PRINT *, ' '
      IF (ISTATE==2) THEN
        PRINT *, ' The integration was successful. '
      ELSE
        PRINT *, ' The integration was not successful. '
      END IF
      STOP

    END PROGRAM DEMOP11

    SUBROUTINE FCN(NDUM,TV,YV,YDOT)

      IMPLICIT REAL (A-H,O-Z)

!     SUBROUTINE FCN IS AN INTERFACE BETWEEN SUBROUTINES DVODE_F90
!     AND DERV

!     NOTE THAT THE SIZE OF ARRAYS Y AND F IN THE FOLLOWING COMMON
!     AREA IS ACTUALLY SET BY THE CORRESPONDING COMMON STATEMENT
!     IN MAIN  PROGRAM DEMOP11:
      COMMON /T/T, NSTOP, NORUN/Y/Y(301)/F/F(301)

!     THE NUMBER OF ODES IS AVAILABLE THROUGH COMMON /N/:
      COMMON /N/N

!     DEPENDENT VARIABLE AND DERIVATIVE VECTORS:
      DIMENSION YV(*), YDOT(*)

!     COMMUNICATE SOME THINGS BETWEEN DEMOP11 AND THE SUBROUTINES:
      REAL ZMESH, SPLOT
      LOGICAL BOUNDS, HEAT_TRANSFER, MANUAL, T0TESAME
      COMMON /DEMOP11DAT/BOUNDS, HEAT_TRANSFER, MANUAL, T0TESAME, ZMESH(301), &
        SPLOT(301)

!     TRANSFER THE INDEPENDENT VARIABLE AND DEPENDENT VARIABLE VECTOR
!     FOR USE IN SUBROUTINE DERV:
      T = TV
      IF (BOUNDS) THEN
        Y(1:N) = 400.0E0 - YV(1:N)
      ELSE
        Y(1:N) = YV(1:N)
      END IF

!     EVALUATE THE DERIVATIVE VECTOR:
      CALL DERV

!     TRANSFER THE DERIVATIVE VECTOR FOR USE BY SUBROUTINE DVODE_F90:
      IF (BOUNDS) THEN
        YDOT(1:N) = -F(1:N)
      ELSE
        YDOT(1:N) = F(1:N)
      END IF

      RETURN
    END SUBROUTINE FCN
!_______________________________________________________________________

    SUBROUTINE INITAL

      IMPLICIT REAL (A-H,O-Z)

!     CONVECTIVE COOLING OF A MOVING POLYMER SHEET

!     CONSIDER THE CONVECTIVE COOLING OF A MOVING POLYMER SHEET ILLUS-
!     TRATED BELOW
!                             +----- DZ -----+
!        ...........................................................
!            +                .              .
!            .                .              .
!            D    V, TP  ----+.    TP(Z,T)   . ----+ V, TP
!            .         AT Z   .              .            AT Z+DZ
!            +                .       .      .
!        ...........................................................
!      Z = 0                  Z       .     Z+DZ                 Z = ZL
!                                     .
!                                     +

!                                    TA

!     AN ENERGY BALANCE ON THE INCREMENTAL SECTION OF THE POLYMER SHEET
!     GIVES

!        (DZ*W*D*RHO*CP*TP ) = DZ*W*V*RHO*CP*TP
!                         T                    AT Z

!                            - DZ*W*V*RHO*CP*TP
!                                              AT Z+DZ

!                            + 2*W*DZ*U*(TA - TP)

!     THE 2 IN THE HEAT TRANSFER TERM ACCOUNTS FOR COOLING ON BOTH
!     FLAT SURFACES OF THE POLYMER.

!     DIVISION BY D*W*DZ*RHO*CP FOLLOWED BY DZ ---+ 0 GIVES

!        TP  = -V*TP  + 2*U*/(D*CP*RHO)*(TA - TP)                  (1)
!          T        Z

!     THE BOUNDARY AND INITIAL CONDITIONS FOR EQUATION (1) ARE

!        TP(0,T) = TE, TP(Z,0) = T0                             (2)(3)

!     WHERE

!        TP        POLYMER TEMPERATURE

!        TA        AMBIENT TEMPERATURE

!        T         TIME

!        Z         POSITION ALONG THE POLYMER

!        V         VELOCITY OF THE POLYMER SHEET

!        D         THICKNESS OF THE POLYMER SHEET

!        W         WIDTH OF THE POLYMER SHEET

!        U         HEAT TRANSFER COEFFICIENT BETWEEN THE FLAT SURFACES
!                  OF THE POLYMER SHEET AND THE SURROUNDING COOLING
!                  MEDIUM

!        CP        HEAT CAPACITY OF THE POLYMER

!        RHO       DENSITY OF THE POLYMER

!     THE PROBLEM, THEN, IS TO COMPUTE TP(Z,T) TO DETERMINE IF THE
!     POLYMER IS COOLED SUFFICIENTLY AT Z = ZL WHERE ZL IS THE LENGTH
!     OF THE COOLING SECTION.

!     THE NUMERICAL VALUES OF THE MODEL PARAMETERS ARE

!        V = 10 CM/SEC

!        D = 0.5 CM

!        CP = 0.8 CAL/GM-C

!        RHO = 1.2 GM/CM**3

!        U = 0.1 CAL/SEC-CM**2-C

!        TE = 400 C

!        T0 = 25 C

!        TA = 25

!        ZL = 100 CM

!     THE STEADY STATE NUMERICAL AND ANALYTICAL SOLUTIONS ARE COMPARED
!     IN SUBROUTINE PRINT.  THE STEADY STATE ANALYTICAL SOLUTION TO
!     EQUATIONS (1) TO (3) CAN BE OBTAINED IN THE FOLLOWING WAY.

!     AT STEADY STATE, EQUATION (1) REDUCES TO

!        V*DTP/DZ = 2*U/(D*RHO*CP)*(TA - TP)

!     WHICH CAN BE REARRANGED TO

!        DTP/(TA - TP) = E*DZ, E = 2*U*/(D*RHO*CP*V)

!     INTEGRATION OF BOTH SIDES GIVES

!          TP                     Z
!         INT DTP/(TP - TA) = -E*INT DX
!          TE                     0

!     EVALUATION OF THE INTEGRALS AT THE LIMITS GIVES

!       LN((TP - TA)/(TE - TA)) = -E*Z

!     OR

!        (TP - TA)/(TE - TA) = EXP(-E*Z)

!     OR

!        TP = TA + (TE - TA)*EXP(-E*Z)

!     THIS STEADY STATE ANALYTICAL SOLUTION CAN BE COMPARED WITH THE
!     STEADY STATE NUMERICAL SOLUTION.

!     FOR THE SPECIAL CASE U = 0, E = 0 IN THE PRECEDING SOLUTION WHICH
!     REDUCES TO

!        TP = TE

!     AS EXPECTED.

!     THE NUMERICAL SOLUTION IS PROGRAMMED IN SUBROUTINE DERV FOR TWO
!     APPROXIMATIONS OF THE SPATIAL DERIVATIVE, TP , IN EQUATION (1)
!                                                 Z
!        (1)  A FIVE-POINT CENTERED APPROXIMATION USED IN SUBROUTINE
!             DSS004.

!        (2)  A FIVE-POINT BIASED UPWIND APPROXIMATION USED IN SUBROU-
!             TINE DSS020.

!     AN INTERESTING EXPERIMENT WITH THIS PROGRAM IS TO SET THE HEAT
!     TRANSFER COEFFICIENT, U, TO ZERO.  FOR THIS CASE, EQUATION (1)
!     REDUCES TO THE ADVECTION EQUATION, WHICH, SUPERFICIALLY, IS
!     QUITE SIMPLE, BUT ACTUALLY IS DIFFICULT TO SOLVE NUMERICALLY
!     BECAUSE IT PROPAGATES DISCONTINUITIES.  THESE DISCONTINUITIES
!     PRODUCE DIFFUSION AND OSCILLATION WHICH ARE EVIDENT IN THE
!     NUMERICAL SOLUTIONS.

!     IF FUNCTIONS OF T LESS STRINGENT THAN DISCONTINUITIES ARE APPLIED
!     AS BOUNDARY CONDITIONS (AT Z = 0), THE FIVE-POINT BIASED UPWIND
!     APPROXIMATION WILL GENERALLY GIVE BETTER SOLUTIONS THAN THE FIVE-
!     POINT CENTERED APPROXIMATION.  THIS POINT HAS BEEN STUDIED EXTEN-
!     SIVELY, AND A TUTORIAL PAPER IS AVAILABLE FROM W. E. SCHIESSER.

!     ONE EASY WAY TO REMOVE DISCONTINUITIES FROM THE PROBLEM (EQUATIONS
!     (1) TO (3)) IS TO SET T0 = TE, FOR WHICH THE FOLLOWING PROGRAM
!     PRODUCES A SMOOTH SOLUTION EVEN FOR U = 0.  OTHERWISE (T0 NE TE),
!Note by ST: works

!     FOR U = 0, THE PROBLEM IS ESSENTIALLY IMPOSSIBLE SINCE TP  IN EQ-
!                                                              Z
!     UATION (1) IS INFINITE.  HOWEVER, EVEN FOR THIS CASE, A NUMERICAL
!     SOLUTION IS COMPUTED BY THE PROGRAM WHICH CAN BE COMPARED WITH THE
!     EXACT SOLUTION, I.E., FOR U = 0, EQUATION (1) BECOMES THE ADVEC-
!     TION EQUATION,

!        TP  + V*TP  = 0
!          T       Z

!     WHICH, FOR THE INITIAL AND BOUNDARY CONDITIONS

!        TP(Z,0) = T0, TP(0,T) = T0 + (TE - T0)*H(T)

!     HAS THE EXACT SOLUTION

!        TP(Z,T) = T0 + (TE - T0)*H(T - Z/V)

!     WHERE H(T) IS THE HEAVISIDE UNIT STEP FUNCTION

!               0, T LT 0

!        H(T) =

!               1, T GT 0

!     THIS EXACT SOLUTION FOR U = 0 (A STEP FUNCTION TRAVELING AT
!     THE VELOCITY V) CAN BE COMPARED WITH THE NUMERICAL SOLUTION.
!     IN PARTICULAR, AT Z = ZL, THE EXACT SOLUTION IS JUST A STEP AT
!     T = ZL/V FROM T0 TO TE, AND, OF COURSE, THE NUMERICAL SOLUTION
!     WILL ONLY APPROXIMATE THIS EXACT SOLUTION.

!     ONE OTHER IMPORTANT PARAMETER (BESIDE THE HEAT TRANSFER COEFFI-
!     CIENT, U) CAN BE STUDIED WITH THE FOLLOWING PROGRAM, THE NUMBER
!     OF GRID POINTS N.  IN THE PRESENT CASE, 301 WAS SELECTED RATHER
!     ARBITRARILY.

      COMMON /T/T, NSTOP, NORUN/Y/TP(301)/F/TPT(301)/S/TPZ(301)/C/V, D, CP, &
        RHO, U, TE, T0, TA, ZL, E, IP

!     THE NUMBER OF ODES IS AVAILABLE THROUGH COMMON /N/:
      COMMON /N/N

!     COMMUNICATE SOME THINGS BETWEEN DEMOP11 AND THE SUBROUTINES:
      REAL ZMESH, SPLOT
      LOGICAL BOUNDS, HEAT_TRANSFER, MANUAL, T0TESAME
      COMMON /DEMOP11DAT/BOUNDS, HEAT_TRANSFER, MANUAL, T0TESAME, ZMESH(301), &
        SPLOT(301)

!     SET THE MODEL PARAMETERS
      V = 10.0E0
      D = 0.5E0
      CP = 0.8E0
      RHO = 1.2E0

      IF (HEAT_TRANSFER) THEN
        U = 0.10E0
      ELSE
        U = 0.0E0
      END IF

      TE = 400.0E0

      IF (T0TESAME) THEN
        T0 = TE
      ELSE
        T0 = 25.0E0
      END IF

      TA = 25.0E0
      ZL = 101.0E0
!     N = 301

!     COMPUTE THE SPATIAL EIGENVALUE E
      E = 2.0E0*U/(D*RHO*CP*V)

!     MODEL INITIAL CONDITIONS
      DO 10 I = 1, N
        TP(I) = T0
10    END DO

!     COMPUTE THE INITIAL DERIVATIVES
      CALL DERV
      IP = 0

!     THE NUMERICAL SOLUTION FOR THE SECOND RUN
      DZ = ZL/FLOAT(N-1)
      DO 20 I = 1, N
        ZMESH(I) = FLOAT(I-1)*DZ
        SPLOT(I) = TA + (TE-TA)*EXP(-E*ZMESH(I))
20    END DO
!     ZMESH(1) = 0.0E0
!     ZMESH(N) = ZL
      WRITE (8,90000) (ZMESH(I),SPLOT(I),I=1,N)
90000 FORMAT ((2E12.4))

      RETURN
    END SUBROUTINE INITAL

    SUBROUTINE DERV

      IMPLICIT REAL (A-H,O-Z)

      COMMON /T/T, NSTOP, NORUN/Y/TP(301)/F/TPT(301)/S/TPZ(301)/C/V, D, CP, &
        RHO, U, TE, T0, TA, ZL, E, IP

!     THE NUMBER OF ODES IS AVAILABLE THROUGH COMMON /N/:
      COMMON /N/N

!     COMMUNICATE SOME THINGS BETWEEN DEMOP11 AND THE SUBROUTINES:
      REAL ZMESH, SPLOT
      LOGICAL BOUNDS, HEAT_TRANSFER, MANUAL, T0TESAME
      COMMON /DEMOP11DAT/BOUNDS, HEAT_TRANSFER, MANUAL, T0TESAME, ZMESH(301), &
        SPLOT(301)

!     BOUNDARY CONDITION
      TP(1) = TE
      TPT(1) = 0.0E0

!     MANUALLY LIMIT THE TEMPERATURES:
      IF (MANUAL) THEN
        TP(1:N) = MIN(TP(1:N),TE)
      END IF

      IF (NORUN==1) THEN
!        COMPUTE THE SPATIAL DERIVATIVE BY FIVE-POINT CENTERED
!        DIFFERENCES
        CALL DSS004(0.0E0,ZL,N,TP,TPZ)
      ELSE
!        COMPUTE THE SPATIAL DERIVATIVE BY FIVE-POINT BIASED UPWIND
!        DIFFERENCES:
        CALL DSS020(0.0E0,ZL,N,TP,TPZ,V)
      END IF

!     PDE (1)
      DO 10 I = 2, N
        TPT(I) = -V*TPZ(I) + 2.0E0*U/(D*RHO*CP)*(TA-TP(I))
10    END DO

      RETURN
    END SUBROUTINE DERV

    SUBROUTINE PRINT(NI,NO)

      IMPLICIT REAL (A-H,O-Z)

      COMMON /T/T, NSTOP, NORUN/Y/TP(301)/F/TPT(301)/S/TPZ(301)/C/V, D, CP, &
        RHO, U, TE, T0, TA, ZL, E, IP

!     THE NUMBER OF ODES IS AVAILABLE THROUGH COMMON /N/:
      COMMON /N/N

!     COMMUNICATE SOME THINGS BETWEEN DEMOP11 AND THE SUBROUTINES:
      REAL ZMESH, SPLOT
      LOGICAL BOUNDS, HEAT_TRANSFER, MANUAL, T0TESAME
      COMMON /DEMOP11DAT/BOUNDS, HEAT_TRANSFER, MANUAL, T0TESAME, ZMESH(301), &
        SPLOT(301)

!     DIMENSION THE ARRAYS FOR THE PLOTTED SOLUTION
!     DIMENSION TIP(101), TPP(2, 101)

!     PRINT A HEADING FOR THE NUMERICAL SOLUTION
      IP = IP + 1
      IF (IP==1) WRITE (NO,90000)
90000 FORMAT (' ',//,9X,'T',4X,' TP(0,T)',4X,'TP(ZL,T)')

!     IF (((IP-1)/10 * 10) .EQ. (IP-1)) WRITE (NO, 2) T, TP(1), TP(N)
90001 FORMAT (F10.2,2F12.2)

!     STORE THE SOLUTION FOR PLOTTING
!     TIP(IP) = T
!     TPP(NORUN, IP) = TP(N)

!     PLOT THE SOLUTION AT THE END OF THE SECOND RUN
!     IF (IP.LT.101) RETURN
!     IF (NORUN.LT.2) RETURN
!     CALL TPLOTS(NORUN,IP,TIP,TPP)

!     IF (IP/=1 .AND. IP/=101) RETURN
!     COMPUTE AND PRINT THE ANALYTICAL STEADY STATE SOLUTION ALONG WITH
!     THE NUMERICAL SOLUTION FOR THE SECOND RUN
      DZ = ZL/FLOAT(N-1)
      Z = -DZ
      WRITE (NO,90002)
      WRITE (NO,*) ' The second column corresponds to T = ', T
90002 FORMAT (' ',//,' STEADY STATE AND COMPUTED SOLUTIONS',//,'         Z', &
        '    TP(ANAL)','     TP(NUM)')
      DO 10 I = 1, N
        Z = Z + DZ
!        TPA = TA + (TE-TA) * DEXP( - E * Z)
        TPA = TA + (TE-TA)*EXP(-E*Z)
        WRITE (NO,90003) Z, TPA, TP(I)
90003   FORMAT (F10.1,2F12.1)
10    END DO
    END SUBROUTINE PRINT
