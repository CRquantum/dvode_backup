! VODE_F90 demonstration program
! Root finding is used to build the return maps for the
! Francescini strange attractor problem. The maps are
! constructed for seven values of the Reynolds' Number R.

! Reference for Problem:
! VALTER FRANCESCHINI, BIFURCATIONS OF TORI AND PHASE
! LOCKING IN A DISSIPATIVE SYSTEM OF DIFFERENTIAL
! EQUATIONS, PHYSICA 6D (1983), PP. 285-304.

    MODULE FRAN_DEMO

      IMPLICIT NONE
      REAL RPAR(7)

    CONTAINS

      SUBROUTINE DERIVS(NEQ,T,Y,YDOT)
!     Subroutine to evaluate dy/dt for this problem
        IMPLICIT NONE
        INTEGER NEQ
        REAL T, Y, YDOT
        REAL R, CON1, CON3, CON4, CON7
        DIMENSION Y(NEQ), YDOT(NEQ)

        R = RPAR(1)
        CON1 = RPAR(3)
        CON3 = RPAR(4)
        CON4 = RPAR(5)
        CON7 = RPAR(7)
        YDOT(1) = -2.0E0*Y(1) + CON4*(Y(2)*Y(3)+Y(4)*Y(5))
        YDOT(2) = -9.0E0*Y(2) + CON3*(Y(1)*Y(3)+Y(6)*Y(7))
        YDOT(3) = -5.0E0*Y(3) + 9.0E0*Y(1)*Y(7) - CON7*Y(1)*Y(2) + R
        YDOT(4) = -5.0E0*Y(4) - CON1*Y(1)*Y(5)
        YDOT(5) = -Y(5) - CON3*Y(1)*Y(4)
        YDOT(6) = -8.0E0*Y(6) - CON4*Y(2)*Y(7)
        YDOT(7) = -5.0E0*Y(7) + CON1*Y(2)*Y(6) - 9.0E0*Y(1)*Y(3)
        RETURN
      END SUBROUTINE DERIVS

      SUBROUTINE GEVENTS(NEQ,T,Y,NG,GOUT)
!     Subroutine to evaluate g(t,y) for this problem
        IMPLICIT NONE
        INTEGER NEQ, NG
        REAL T, Y, GOUT
        DIMENSION Y(NEQ), GOUT(NG)

        GOUT(1) = Y(1)
        RETURN
      END SUBROUTINE GEVENTS

    END MODULE FRAN_DEMO

!******************************************************************

    PROGRAM FRAN

      USE FRAN_DEMO
      USE DVODE_F90_M

!     Type declarations:
      IMPLICIT NONE
!     Number of odes and number of event functions for this problem:
      INTEGER, PARAMETER :: NEQ = 7, NG = 1
      INTEGER ITASK, ISTATE, JROOTS, ISTATS, FOUND, NOUT, IRVAL, MATFILE
      REAL ATOL, RTOL, RSTATS, T, TOUT, Y, TSETTLE, CON1, R
      DIMENSION Y(NEQ), RSTATS(22), ISTATS(31), JROOTS(NG), FOUND(NG)
      REAL, DIMENSION (7) :: RVALS = (/ 269E0, 299.5E0, 304E0, 310E0, &
        326.25E0, 347E0, 400E0/)
      TYPE (VODE_OPTS) :: OPTIONS

!     Open the output file and the plot files:
      OPEN (UNIT=6,FILE='demofran.dat')
      OPEN (UNIT=7,FILE='demofranplot.dat')
      OPEN (UNIT=21,FILE='demofranplot1.dat')
      OPEN (UNIT=22,FILE='demofranplot2.dat')
      OPEN (UNIT=23,FILE='demofranplot3.dat')
      OPEN (UNIT=24,FILE='demofranplot4.dat')
      OPEN (UNIT=25,FILE='demofranplot5.dat')
      OPEN (UNIT=26,FILE='demofranplot6.dat')
      OPEN (UNIT=27,FILE='demofranplot7.dat')

      DO IRVAL = 1, 7

!     Problem Reynold's Number:
        R = RVALS(IRVAL)
        MATFILE = 20 + IRVAL
!     Let the orbit settle out this long before plotting the attractor:
        TSETTLE = 600.0E0
        RPAR(1) = SQRT(5.0E0)
        RPAR(2) = TSETTLE
        RPAR(1) = R
        RPAR(2) = TSETTLE
        CON1 = SQRT(5.0E0)
        RPAR(3) = CON1
        RPAR(4) = 3.0E0*CON1
        RPAR(5) = 4.0E0*CON1
        RPAR(6) = 5.0E0*CON1
        RPAR(7) = 7.0E0*CON1

!     Number of roots found:
        NOUT = 0
        FOUND(1:NG) = 0
!     Set the initial conditions:
        Y(1) = 0.0E0
        Y(2) = -0.77942E0
        Y(3) = -0.92148E0
        Y(4) = 2.96994E0
        Y(5) = 8.39260E0
        Y(6) = 2.02003E0
        Y(7) = -4.88997E0
!     Set the integration parameters:
        T = 0.0E0
        TOUT = 1000.0E0
        RTOL = 1.0E-6
        ATOL = 1.0E-6
        ITASK = 1
        ISTATE = 1

!     Set the VODE_F90 options:
        OPTIONS = SET_OPTS(RELERR=RTOL,ABSERR=ATOL,NEVENTS=NG)

10      CONTINUE

!     Perform the integration:
        CALL DVODE_F90(DERIVS,NEQ,Y,T,TOUT,ITASK,ISTATE,OPTIONS,G_FCN=GEVENTS)

!     Gather and write the integration statistics for this problem:
        CALL GET_STATS(RSTATS,ISTATS,NG,JROOTS)

!     Stop the integration if an error occurred:
        IF (ISTATE<0) THEN
          PRINT *, ' The integration was not successful.'
          GO TO 30
        END IF

!     Terminate the integration if we have reached the final output time:
        IF (ISTATE==2) GO TO 20

!     A root was found:
        IF (ISTATE==3) THEN
          FOUND(1:NG) = FOUND(1:NG) + JROOTS(1:NG)
!        Plot a section of the return map (use symmetry for the rest):
          IF ((Y(5)>0.0E0) .AND. (Y(3)<0.0E0) .AND. (Y( &
              6)>0.0E0) .AND. (TOUT>TSETTLE)) THEN
            IF (JROOTS(1)==1) THEN
              NOUT = NOUT + 1
              WRITE (7,90001) Y(3), Y(6)
!              Write the plot data to the Matlab file:
              WRITE (MATFILE,90001) Y(3), Y(6)
            END IF
          END IF
!        WRITE(6,75) JROOTS(1)
          ISTATE = 2
!        Continue the integration from this root:
          GO TO 10
        END IF

20      CONTINUE

!     Write the integration final root finding statistics for
!     this problem:
        WRITE (6,90002) ISTATS(11), ISTATS(12), ISTATS(13), ISTATS(10)
        WRITE (6,90000) FOUND(1)
        PRINT *, ' For Reynolds Number = ', R
        PRINT *, ' Number of roots found: ', FOUND(1)

      END DO ! IRVAL
      STOP
30    WRITE (6,90004) ISTATE

!     Format statements for this problem:
90000 FORMAT (' Number of roots found: ',I10)
90001 FORMAT (2D25.15)
90002 FORMAT (/' Steps = ',I10,' f-s = ',I10,' J-s = ',I10,' g-s =',I10/)
90003 FORMAT (5X,' The above line is a root,JROOTS = ',I10)
90004 FORMAT (/' An error occurred in VODE_F90. ISTATE = ',I3)
      STOP

    END PROGRAM FRAN
