function [t,y] = demofran
% Matlab script to plot the solution for the demofran problem

temp1 = load('demofranplot1.dat','-ascii');
y3 = temp1(:,1);
y6 = temp1(:,2);
figure
plot(y3,y6,'.')
title('Franceshini Attractor. R = 269')
xlabel('y_3')
ylabel('y_6')

temp2 = load('demofranplot2.dat','-ascii');
y3 = temp2(:,1);
y6 = temp2(:,2);
figure
plot(y3,y6,'.')
title('Franceshini Attractor. R = 299.5')
xlabel('y_3')
ylabel('y_6')

temp3 = load('demofranplot3.dat','-ascii');
y3 = temp3(:,1);
y6 = temp3(:,2);
figure
plot(y3,y6,'.')
title('Franceshini Attractor. R = 304')
xlabel('y_3')
ylabel('y_6')

temp4 = load('demofranplot4.dat','-ascii');
y3 = temp4(:,1);
y6 = temp4(:,2);
figure
plot(y3,y6,'.')
title('Franceshini Attractor. R = 310')
xlabel('y_3')
ylabel('y_6')

temp5 = load('demofranplot5.dat','-ascii');
y3 = temp5(:,1);
y6 = temp5(:,2);
figure
plot(y3,y6,'.')
title('Franceshini Attractor. R = 326.25')
xlabel('y_3')
ylabel('y_6')

temp6 = load('demofranplot6.dat','-ascii');
y3 = temp6(:,1);
y6 = temp6(:,2);
figure
plot(y3,y6,'.')
title('Franceshini Attractor. R = 347')
xlabel('y_3')
ylabel('y_6')

temp7 = load('demofranplot7.dat','-ascii');
y3 = temp7(:,1);
y6 = temp7(:,2);
figure
plot(y3,y6,'.')
title('Franceshini Attractor. R = 400')
xlabel('y_3')
ylabel('y_6')
