! VODE_F90 demonstration program
! Diurnal kinetics problem. See the reference in the file foodwebdoc.

    MODULE DEMO_DIURN

      IMPLICIT NONE

!     Problem parameters and arrays:

      INTEGER MX, MZ, MX2

      REAL Q1, Q2, Q3, Q4, A3, A4, FREQ, C3, DX, DZ, COX, COZ, ACOX, DCH, &
        DCV0, CCALAN, CDALAN

      DIMENSION CCALAN(800), CDALAN(800)

    CONTAINS

      SUBROUTINE FCN(NEQ,T,CC,CDOT)

!     DIRECT THE CALCULATION OF THE TIME DERIVATIVES.

        IMPLICIT NONE
        INTEGER NEQ
        REAL T, CC, CDOT
        DIMENSION CC(NEQ), CDOT(NEQ)

!     LOAD THE SOLUTION INTO LOCAL STORAGE.

        CCALAN(1:NEQ) = CC(1:NEQ)
!     CALL DCOPY(NEQ, CC, 1, CCALAN, 1)

!     DEFINE THE TIME DERIVATIVES.

        CALL DERV(T)

!     LOAD THE TIME DERIVATIVES INTO INTEGRATOR ARRAY.

        CDOT(1:NEQ) = CDALAN(1:NEQ)
!     CALL DCOPY(NEQ, CDALAN, 1, CDOT, 1)

        RETURN
      END SUBROUTINE FCN

      SUBROUTINE INITAL(YINIT)

        IMPLICIT NONE
        INTEGER I, J, K, IZ, IY, NEQ
        REAL YINIT, VEL, Z, Z1, CZ, X, X1, CX, CXCZ, HALFDA, PI

        INCLUDE 'demodiurndoc'

        DIMENSION YINIT(*)

        DCH = 1.0E-2
        DCV0 = 1.0E-5
        Q1 = 1.63E-16
        Q2 = 4.66E-16
        A3 = 22.62E0
        A4 = 7.601E0
        C3 = 3.7E16
        PI = 3.1415926535898E0
        HALFDA = 4.32E4
        VEL = 1.0E-3
        MX2 = MX*2
!     midy = mz * mx - mx
        DX = 20.0E0/REAL(MX-1)
        DZ = 20.0E0/REAL(MZ-1)
        COX = DCH/DX**2
        ACOX = VEL/(2.0E0*DX)
        COZ = 1.0E0/DZ**2
        FREQ = PI/HALFDA

        DO 20 K = 1, MZ
          Z = 30.0E0 + REAL(K-1)*DZ
          Z1 = 0.1E0*(Z-40.0E0)
          Z1 = Z1**2
          CZ = 1.0E0 - Z1 + 0.5E0*Z1**2
          IZ = 2*MX*(K-1)
          DO 10 J = 1, MX
            X = REAL(J-1)*DX
            X1 = 0.1E0*(X-10.0E0)
            X1 = X1**2
            CX = 1.0E0 - X1 + 0.5E0*X1**2
            IY = IZ + 2*(J-1)
            CXCZ = CX*CZ
            YINIT(IY+1) = 1.0E6*CXCZ
            YINIT(IY+2) = 1.0E12*CXCZ
10        END DO
20      END DO

        NEQ = 2*MX*MZ
        DO 30 I = 1, NEQ
          CCALAN(I) = YINIT(I)
30      END DO

        RETURN
      END SUBROUTINE INITAL

      SUBROUTINE DERV(T)

!     this is the single precision version of subroutine urnder,
!     which computes the derivative of y and returns it in ydot.

        IMPLICIT NONE
        INTEGER J, K, IZ, IY1, IY2
        REAL T, RKIN1, RKIN2, DCVU1, DCVU2, DCVL1, DCVL2, ACH1, ACH2, C1, C2, &
          CZDL, CZDU, ZL, ZU, DCHR1, DCHR2, DCHL1, DCHL2, DCVAL

        CALL DIURN(T)

        DO 80 K = 1, MZ
          ZL = 30.0E0 + (K-1.5E0)*DZ
          ZU = ZL + DZ
          CALL DCV(ZL,DCVAL)
          CZDL = COZ*DCVAL
          CALL DCV(ZU,DCVAL)
          CZDU = COZ*DCVAL
          IZ = MX2*(K-1)
          DO 70 J = 1, MX
            IY1 = IZ + 2*(J-1) + 1
            IY2 = IY1 + 1
            C1 = CCALAN(IY1)
            C2 = CCALAN(IY2)
            CALL CHEMR(C1,C2,RKIN1,RKIN2)
            IF (K/=1) GO TO 10
            DCVU1 = CCALAN(IY1+MX2) - C1
            DCVU2 = CCALAN(IY2+MX2) - C2
            DCVL1 = -DCVU1
            DCVL2 = -DCVU2
            GO TO 30
10          CONTINUE
            IF (K/=MZ) GO TO 20
            DCVL1 = C1 - CCALAN(IY1-MX2)
            DCVL2 = C2 - CCALAN(IY2-MX2)
            DCVU1 = -DCVL1
            DCVU2 = -DCVL2
            GO TO 30
20          CONTINUE
            DCVL1 = C1 - CCALAN(IY1-MX2)
            DCVL2 = C2 - CCALAN(IY2-MX2)
            DCVU1 = CCALAN(IY1+MX2) - C1
            DCVU2 = CCALAN(IY2+MX2) - C2
30          CONTINUE
            ACH1 = 0.0E0
            ACH2 = 0.0E0
            IF (J/=1) GO TO 40
            DCHR1 = CCALAN(IY1+2) - C1
            DCHR2 = CCALAN(IY2+2) - C2
            DCHL1 = -DCHR1
            DCHL2 = -DCHR2
            GO TO 60
40          CONTINUE
            IF (J/=MX) GO TO 50
            DCHL1 = C1 - CCALAN(IY1-2)
            DCHL2 = C2 - CCALAN(IY2-2)
            DCHR1 = -DCHL1
            DCHR2 = -DCHL2
            GO TO 60
50          CONTINUE
            DCHL1 = C1 - CCALAN(IY1-2)
            DCHL2 = C2 - CCALAN(IY2-2)
            DCHR1 = CCALAN(IY1+2) - C1
            DCHR2 = CCALAN(IY2+2) - C2
            ACH1 = CCALAN(IY1+2) - CCALAN(IY1-2)
            ACH2 = CCALAN(IY2+2) - CCALAN(IY2-2)
60          CONTINUE
            CDALAN(IY1) = (CZDU*DCVU1-CZDL*DCVL1) + COX*(DCHR1-DCHL1) + &
              ACOX*ACH1 + RKIN1
            CDALAN(IY2) = (CZDU*DCVU2-CZDL*DCVL2) + COX*(DCHR2-DCHL2) + &
              ACOX*ACH2 + RKIN2
70        END DO
80      END DO

        RETURN
      END SUBROUTINE DERV

      SUBROUTINE DIURN(T)

!     this is the single precision version of subroutine diurn,
!     which computes the 2 diurnal rate functions at a given t.

        IMPLICIT NONE
        REAL T, S

        S = SIN(FREQ*T)
        IF (S<=0.0E0) GO TO 10
        Q3 = EXP(-A3/S)
        Q4 = EXP(-A4/S)
        RETURN
10      Q3 = 0.0E0
        Q4 = 0.0E0

        RETURN
      END SUBROUTINE DIURN

      SUBROUTINE DCV(Z,DCVAL)

!     this is the single precision version of function routine
!     dcv, which computes the vertical diffusion coefficient
!     at a given z.

        IMPLICIT NONE
        REAL ARG, Z, DCVAL

        ARG = 0.2E0*Z
        DCVAL = DCV0*EXP(ARG)

        RETURN
      END SUBROUTINE DCV

      SUBROUTINE CHEMR(C1,C2,R1,R2)

        IMPLICIT NONE
        REAL C1, C2, R1, R2, QQ1, QQ2, QQ3, QQ4

!     this is the single precision version of subroutine chemr,
!     which computes the 2 chemical kinetics rates at one
!     spatial point.

        QQ1 = Q1*C1*C3
        QQ2 = Q2*C1*C2
        QQ3 = Q3*C3
        QQ4 = Q4*C2
        R1 = -QQ1 - QQ2 + 2.0E0*QQ3 + QQ4
        R2 = QQ1 - QQ2 - QQ4

        RETURN
      END SUBROUTINE CHEMR

      SUBROUTINE INTURN(YINIT)

        IMPLICIT NONE
        INTEGER J, K, IZ, IY
        REAL YINIT, HALFDA, PI, VEL, Z, Z1, CZ, X, X1, CX, CXCZ

        INCLUDE 'demodiurndoc'

        DIMENSION YINIT(*)

        DCH = 1.0E-2
        DCV0 = 1.0E-5
        Q1 = 1.63E-16
        Q2 = 4.66E-16
        A3 = 22.62E0
        A4 = 7.601E0
        C3 = 3.7E16
        PI = 3.1415926535898E0
        HALFDA = 4.32E4
        VEL = 1.0E-3
        MX2 = MX*2
!     midy = mz * mx - mx
        DX = 20.0E0/REAL(MX-1)
        DZ = 20.0E0/REAL(MZ-1)
        COX = DCH/DX**2
        ACOX = VEL/(2.0E0*DX)
        COZ = 1.0E0/DZ**2
        FREQ = PI/HALFDA

        DO 20 K = 1, MZ
          Z = 30.0E0 + REAL(K-1)*DZ
          Z1 = 0.1E0*(Z-40.0E0)
          Z1 = Z1**2
          CZ = 1.0E0 - Z1 + 0.5E0*Z1**2
          IZ = 2*MX*(K-1)
          DO 10 J = 1, MX
            X = REAL(J-1)*DX
            X1 = 0.1E0*(X-10.0E0)
            X1 = X1**2
            CX = 1.0E0 - X1 + 0.5E0*X1**2
            IY = IZ + 2*(J-1)
            CXCZ = CX*CZ
            YINIT(IY+1) = 1.0E6*CXCZ
            YINIT(IY+2) = 1.0E12*CXCZ
10        END DO
20      END DO

        RETURN
      END SUBROUTINE INTURN

      SUBROUTINE URNDER(NEQ,T,Y,YDOT)

!     this is the single precision version of subroutine urnder,
!     which computes the derivative of y and returns it in ydot.

        IMPLICIT NONE
        INTEGER NEQ, J, K, IY1, IY2, IZ
        REAL T, Y, YDOT, ZL, ZU, CZDL, CZDU, C1, C2, DCVU1, DCVU2, DCVL1, &
          DCVL2, ACH1, ACH2, DCHR1, DCHR2, DCHL1, DCHL2, RKIN1, RKIN2, DCVAL
        DIMENSION Y(*), YDOT(*)

        CALL DIURN(T)

        DO 80 K = 1, MZ
          ZL = 30.0E0 + (K-1.5E0)*DZ
          ZU = ZL + DZ
          CALL DCV(ZL,DCVAL)
          CZDL = COZ*DCVAL
          CALL DCV(ZU,DCVAL)
          CZDU = COZ*DCVAL
          IZ = MX2*(K-1)
          DO 70 J = 1, MX
            IY1 = IZ + 2*(J-1) + 1
            IY2 = IY1 + 1
            C1 = Y(IY1)
            C2 = Y(IY2)
            CALL CHEMR(C1,C2,RKIN1,RKIN2)
            IF (K/=1) GO TO 10
            DCVU1 = Y(IY1+MX2) - C1
            DCVU2 = Y(IY2+MX2) - C2
            DCVL1 = -DCVU1
            DCVL2 = -DCVU2
            GO TO 30
10          CONTINUE
            IF (K/=MZ) GO TO 20
            DCVL1 = C1 - Y(IY1-MX2)
            DCVL2 = C2 - Y(IY2-MX2)
            DCVU1 = -DCVL1
            DCVU2 = -DCVL2
            GO TO 30
20          CONTINUE
            DCVL1 = C1 - Y(IY1-MX2)
            DCVL2 = C2 - Y(IY2-MX2)
            DCVU1 = Y(IY1+MX2) - C1
            DCVU2 = Y(IY2+MX2) - C2
30          CONTINUE
            ACH1 = 0.0E0
            ACH2 = 0.0E0
            IF (J/=1) GO TO 40
            DCHR1 = Y(IY1+2) - C1
            DCHR2 = Y(IY2+2) - C2
            DCHL1 = -DCHR1
            DCHL2 = -DCHR2
            GO TO 60
40          CONTINUE
            IF (J/=MX) GO TO 50
            DCHL1 = C1 - Y(IY1-2)
            DCHL2 = C2 - Y(IY2-2)
            DCHR1 = -DCHL1
            DCHR2 = -DCHL2
            GO TO 60
50          CONTINUE
            DCHL1 = C1 - Y(IY1-2)
            DCHL2 = C2 - Y(IY2-2)
            DCHR1 = Y(IY1+2) - C1
            DCHR2 = Y(IY2+2) - C2
            ACH1 = Y(IY1+2) - Y(IY1-2)
            ACH2 = Y(IY2+2) - Y(IY2-2)
60          CONTINUE
            YDOT(IY1) = (CZDU*DCVU1-CZDL*DCVL1) + COX*(DCHR1-DCHL1) + &
              ACOX*ACH1 + RKIN1
            YDOT(IY2) = (CZDU*DCVU2-CZDL*DCVL2) + COX*(DCHR2-DCHL2) + &
              ACOX*ACH2 + RKIN2
70        END DO
80      END DO

        RETURN
      END SUBROUTINE URNDER

    END MODULE DEMO_DIURN

!******************************************************************

    PROGRAM DIURN

      USE DVODE_F90_M
      USE DEMO_DIURN

      IMPLICIT NONE

!     Note:
!     NEQMAX is the maximum allowable number of odes. If NEQMAX is
!     increased, be sure to make the same increase in the comprb
!     file.
      INTEGER, PARAMETER :: NEQMAX = 800

      REAL T, TOUT, ATOL, RTOL, RSTATS, Y
      INTEGER NST, NFE, LENRW, LENIW, NNI, NCFN, NETF, NFEA, ISTATS, ISTATE, &
        ITASK, IOUT, NJE, NOUT, NLU, NPLOT, NEQ
      DIMENSION ATOL(1), RTOL(1), RSTATS(22), ISTATS(31)
      LOGICAL SPARSE
      TYPE (VODE_OPTS) :: OPTIONS
!     SOLUTION AND DERIVATIVE:
      DIMENSION Y(NEQMAX)

!     OPEN THE OUTPUT FILE:
      OPEN (6,FILE='demodiurn.dat')

!     NEQ = NUMBER OF ODES:
      MX = 20
      MZ = 20
      NEQ = 2*MX*MZ
      IF (NEQ>NEQMAX) THEN
        WRITE (6,90004)
        STOP
      END IF

!     INITIAL SOLUTION:
      CALL INITAL(Y)

!     SET DVODE_F90 JACOBIAN OPTION:
      SPARSE = .TRUE.

!     ABSOLUTE AND RELATIVE ERROR TOLERANCES:
      ATOL(1) = 1.0E-5
      RTOL(1) = 1.0E-5

!     INITIAL TIME AND FIRST OUTPUT POINT:
      T = 0.0D0
      TOUT = 1.0E-8

!     NUMBER OF OUTPUT TIMES:
      NOUT = 12
      NOUT = MIN(NOUT,101)

!     INITIALIZE THE INTEGRATION FLAGS:
      ITASK = 1
      ISTATE = 1

!     SET REMAINING VODE_F90 OPTIONS:

      OPTIONS = SET_OPTS(SPARSE_J=SPARSE,ABSERR=ATOL(1),RELERR=RTOL(1), &
        MXSTEP=100000,NZSWAG=20000)
      NPLOT = 0

!     PERFORM THE INTEGRATION:

      DO IOUT = 1, NOUT

        CALL DVODE_F90(FCN,NEQ,Y,T,TOUT,ITASK,ISTATE,OPTIONS)

!        GATHER THE INTEGRATION STATISTICS FOR THIS CALL:
        CALL GET_STATS(RSTATS,ISTATS)

!        CHECK IF AN ERROR OCCURRED:
        IF (ISTATE<0) GO TO 10

        IF (NPLOT<101) NPLOT = NPLOT + 1
        TOUT = 10.0D0*TOUT

      END DO

10    CONTINUE

!     CHECK IF AN ERROR OCCURRED IN VODE_F90:
      IF (ISTATE<0) THEN
        PRINT *, 'DVODE_F90 returned ISTATE = ', ISTATE
        WRITE (6,*) 'DVODE_F90 returned ISTATE = ', ISTATE
      END IF

!     PRINT THE FINAL INTEGRATION STATISTICS:
      NST = ISTATS(11)
      NFE = ISTATS(12)
      NJE = ISTATS(13)
      NLU = ISTATS(19)
      LENRW = ISTATS(17)
      LENIW = ISTATS(18)
      NNI = ISTATS(20)
      NCFN = ISTATS(21)
      NETF = ISTATS(22)
      NFEA = NFE
      NFEA = NFE - NJE
      WRITE (6,90000) LENRW, LENIW, NST, NFE, NFEA, NJE, NLU, NNI, NCFN, NETF
      WRITE (6,90002) TOUT
      WRITE (6,90003) NEQ
!     WRITE(6,182) (I,Y(I),I=1,NEQ)

      CALL RELEASE_ARRAYS

!     FORMATS:
90000 FORMAT ('  Final statistics for this run:'/'   RWORK size =',I8, &
        '   IWORK size =',I8/'   Number of steps =',I8/'   Number of f-s  =', &
        I8/'   (excluding J-s) =',I8/'   Number of J-s  =', &
        I8/'   Number of LU-s =',I5/'   Number of nonlinear iterations =', &
        I8/'   Number of nonlinear convergence failures =', &
        I8/'   Number of error test failures =',I8/)
90001 FORMAT (' Final DVODE_F90 Solution:',/,(I5,E15.5))
90002 FORMAT (' Final integration time = ',E15.5)
90003 FORMAT (' Number of odes = ',I10)
90004 FORMAT (' NEQ is larger than NEQMAX.')

!     TERMINATE EXECUTION:
      PRINT *, ' '
      IF (ISTATE==2) THEN
        PRINT *, ' The integration was successful. '
      ELSE
        PRINT *, ' The integration was not successful. '
      END IF
      STOP
    END PROGRAM DIURN
