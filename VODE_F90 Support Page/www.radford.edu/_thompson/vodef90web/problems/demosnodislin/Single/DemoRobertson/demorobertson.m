function [t,y] = demorobertson
% Matlab script to plot the solution for Robertson's problem
temp = load('demorobertsonplot.dat','-ascii');

t = temp(:,1)
y1 = temp(:,2);
y2 = 1e4 * temp(:,3);
y3 = temp(:,4); 

figure
semilogx(t,y1,t,y2,t,y3)
title('Robertson Problem')

figure
semilogx(t,y1)
title('y_1')

figure
semilogx(t,y2)
title('y_2')

figure
semilogx(t,y3)
title('y_3')