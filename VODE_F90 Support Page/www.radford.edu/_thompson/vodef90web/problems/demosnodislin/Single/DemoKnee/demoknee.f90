! DEMONSTRATION PROGRAM FOR THE DVODE_F90 PACKAGE.
! Dahlquist's knee problem
! Nonnegativity of the solution is enforced to handle the bend
! in the solution at t = 1.

! Problem References:

!   G. Dahlquist, L. Edsberg, G. Skollermo, and G. Soderlind,
!   Are the numerical methods and software satisfactory
!   for chemical kinetics?, pp. 149--164 in J. Hinze, ed., Numerical
!   Integration of Differential Equations and Large Linear Systems,
!   Lecture Notes in Math. #968, Springer, New York, 1982.

!   L.F. Shampine, S. Thompson, J.A. Kierzenka, and G.D. Byrne,
!   Non-negative Solutions of ODEs, Applied Mathematics and
!   Computation, 2005.

    MODULE KNEE_DEMO

      IMPLICIT NONE
      REAL EPSILON

    CONTAINS

      SUBROUTINE DERIVS(NEQ,T,YSOL,YDOT)
        IMPLICIT NONE
        INTEGER NEQ
        REAL T, Y, YDOT, YSOL
        DIMENSION YSOL(NEQ), YDOT(NEQ)

        Y = YSOL(1)
        YDOT(1) = (1.0E0/EPSILON)*((1.0E0-T)*Y-Y*Y)
        RETURN
      END SUBROUTINE DERIVS

      SUBROUTINE JACD(NEQ,T,Y,ML,MU,PD,NROWPD)
        IMPLICIT NONE
        INTEGER NEQ, ML, MU, NROWPD
        REAL T, Y, PD
        DIMENSION Y(NEQ), PD(NROWPD,NEQ)

        PD(1,1) = (1.0E0/EPSILON)*(1.0E0-T-2.0E0*Y(1))
        RETURN
      END SUBROUTINE JACD

    END MODULE KNEE_DEMO

!******************************************************************

    PROGRAM KNEE

      USE KNEE_DEMO
      USE DVODE_F90_M

      IMPLICIT NONE
      LOGICAL NONNEG
      INTEGER ITASK, ISTATE, NEQ, IOUT, ISTATS, NOUT, IDX, IJAC
      REAL ATOL, RTOL, RSTATS, T, TOUT, Y, DELTAT, ERROR, LB, UB
      DIMENSION RSTATS(22), ISTATS(31), ERROR(1), Y(1), ATOL(1), RTOL(1), &
        IDX(1), LB(1), UB(1)
      TYPE (VODE_OPTS) :: OPTIONS

      OPEN (UNIT=6,FILE='demoknee.dat')
      OPEN (UNIT=7,FILE='demokneeplot.dat')

!     Impose nonnegativity on the solution:
      NONNEG = .TRUE.

!     Define the problem parameter:
      EPSILON = 1.0E-6

!     Exact Jacobian:
      IJAC = 2
!     Numerical Jacobian:
      IJAC = 1

      NEQ = 1
      Y(1) = 1.0E0
      T = 0.0E0
      DELTAT = 0.02E0
      TOUT = DELTAT
      NOUT = 100
      WRITE (6,90003) T, Y(1)
      WRITE (7,90003) T, Y(1)
      RTOL(1) = 1.0E-6
      ATOL(1) = 1.0E-6
      ITASK = 1
      ISTATE = 1
      IDX(1) = 1
      LB(1) = 0.0E0
      UB(1) = 1E30
      IF (NONNEG) THEN
        IF (IJAC==1) THEN
          OPTIONS = SET_OPTS(DENSE_J=.TRUE.,RELERR=RTOL(1),ABSERR=ATOL(1), &
            CONSTRAINED=IDX,CLOWER=LB,CUPPER=UB)
        ELSE
          OPTIONS = SET_OPTS(DENSE_J=.TRUE.,RELERR=RTOL(1),ABSERR=ATOL(1), &
            CONSTRAINED=IDX,CLOWER=LB,CUPPER=UB,USER_SUPPLIED_JACOBIAN=.TRUE.)
        END IF
      ELSE
        IF (IJAC==1) THEN
          OPTIONS = SET_OPTS(DENSE_J=.TRUE.,RELERR=RTOL(1),ABSERR=ATOL(1))
        ELSE
          OPTIONS = SET_OPTS(DENSE_J=.TRUE.,RELERR=RTOL(1),ABSERR=ATOL(1), &
            USER_SUPPLIED_JACOBIAN=.TRUE.)
        END IF
      END IF

      DO IOUT = 1, NOUT
        CALL DVODE_F90(DERIVS,NEQ,Y,T,TOUT,ITASK,ISTATE,OPTIONS,J_FCN=JACD)
        CALL GET_STATS(RSTATS,ISTATS)
        TOUT = TOUT + DELTAT
        WRITE (6,90003) T, Y(1)
        WRITE (7,90003) T, Y(1)
        IF (ISTATE<0) THEN
          WRITE (5,90000) ISTATE
          GO TO 10
        END IF
      END DO ! End of output loop

      WRITE (6,90001) ISTATE, ISTATS(11), ISTATS(12), ISTATS(13)
      ERROR(1) = ABS(Y(1))
      WRITE (6,90002) T, Y(1), ERROR(1)
      PRINT *, Y(1), ERROR(1)
      PRINT *, ' ISTATE/IJAC = ', ISTATE, IJAC
10    CONTINUE

90000 FORMAT (' An error occurred in DVODE. ISTATE = ')
90001 FORMAT (' ISTATE    = ',I3,/,' No. steps = ',I4,/,' No. f-s   = ',I4,/, &
        ' No. J-s   = ',I4)
90002 FORMAT (' t         = ',E15.5,/,' y(t)      = ',E15.5,/,' Error     = ', &
        E15.5)
90003 FORMAT (2E15.5)

      STOP
    END PROGRAM KNEE
