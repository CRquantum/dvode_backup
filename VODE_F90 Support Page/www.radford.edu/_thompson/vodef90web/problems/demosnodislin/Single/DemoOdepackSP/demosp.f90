    MODULE DEMOSP

! VODE_F90 DEMONSTRATION PROGRAM

    CONTAINS

      SUBROUTINE F1(NEQ,T,Y,YDOT)
        IMPLICIT NONE
        INTEGER NEQ
        REAL T, Y, YDOT
        DIMENSION Y(2), YDOT(2)

        YDOT(1) = Y(2)
        YDOT(2) = 3.0E0*(1.0E0-Y(1)*Y(1))*Y(2) - Y(1)
        RETURN
      END SUBROUTINE F1

      SUBROUTINE JAC1(NEQ,T,Y,ML,MU,PD,NROWPD)
        IMPLICIT NONE
        INTEGER NEQ, ML, MU, NROWPD
        REAL T, Y, PD
        DIMENSION Y(2), PD(NROWPD,2)

        PD(1,1) = 0.0E0
        PD(1,2) = 1.0E0
        PD(2,1) = -6.0E0*Y(1)*Y(2) - 1.0E0
        PD(2,2) = 3.0E0*(1.0E0-Y(1)*Y(1))
        RETURN
      END SUBROUTINE JAC1

      SUBROUTINE EDIT1(NEQ,Y,T,ERM,IOUT)
        IMPLICIT NONE
        INTEGER NEQ, I, IOUT
        REAL Y, T, ER, ERM, YT, EXACT
        DIMENSION Y(NEQ)
        DIMENSION EXACT(4,2)
        DATA EXACT/0.1680102443E+01, -0.4845395244E-07, -0.1680103060E+01, &
          0.1116805988E-06, -0.2910560902E+00, -0.3168716128E+01, &
          0.2910593695E+00, 0.3168716327E+01/

        ERM = 0.0E0
        IF (ABS(T)<=0.0E0) RETURN
        DO I = 1, NEQ
          YT = EXACT(IOUT,I)
          ER = ABS(Y(I)-YT)
          ERM = MAX(ERM,ER)
        END DO
        RETURN
      END SUBROUTINE EDIT1

      SUBROUTINE F2(NEQ,T,Y,YDOT)
        IMPLICIT NONE
        INTEGER NEQ, I, J, K, NG
        REAL T, Y, YDOT, ALPH1, ALPH2, D
        DIMENSION Y(NEQ), YDOT(NEQ)
        DATA ALPH1/1.0E0/, ALPH2/1.0E0/, NG/5/

        DO 10 J = 1, NG
          DO 10 I = 1, NG
            K = I + (J-1)*NG
            D = -2.0E0*Y(K)
            IF (I/=1) D = D + Y(K-1)*ALPH1
            IF (J/=1) D = D + Y(K-NG)*ALPH2
10      YDOT(K) = D
        RETURN
      END SUBROUTINE F2

      SUBROUTINE JAC2(NEQ,T,Y,ML,MU,PD,NROWPD)
        IMPLICIT NONE
        INTEGER NEQ, ML, MU, NROWPD, J, MBAND, MU1, MU2, NG
        REAL T, Y, PD, ALPH1, ALPH2
        DIMENSION Y(NEQ), PD(NROWPD,NEQ)
        DATA ALPH1/1.0E0/, ALPH2/1.0E0/, NG/5/

        MBAND = ML + MU + 1
        MU1 = MU + 1
        MU2 = MU + 2
        DO 10 J = 1, NEQ
          PD(MU1,J) = -2.0E0
          PD(MU2,J) = ALPH1
10      PD(MBAND,J) = ALPH2
        DO 20 J = NG, NEQ, NG
20      PD(MU2,J) = 0.0E0
        RETURN
      END SUBROUTINE JAC2

      SUBROUTINE EDIT2(Y,T,ERM)
        IMPLICIT NONE
        INTEGER I, J, K, NG
        REAL Y, T, ERM, ALPH1, ALPH2, A1, A2, ER, EX, YT
        DIMENSION Y(25)
        DATA ALPH1/1.0E0/, ALPH2/1.0E0/, NG/5/

        ERM = 0.0E0
        IF (T==0.0E0) RETURN
        EX = 0.0E0
        IF (T<=30.0E0) EX = EXP(-2.0E0*T)
        A2 = 1.0E0
        DO 20 J = 1, NG
          A1 = 1.0E0
          DO 10 I = 1, NG
            K = I + (J-1)*NG
            YT = T**(I+J-2)*EX*A1*A2
            ER = ABS(Y(K)-YT)
            ERM = MAX(ERM,ER)
            A1 = A1*ALPH1/REAL(I)
10        END DO
          A2 = A2*ALPH2/REAL(J)
20      END DO
        RETURN
      END SUBROUTINE EDIT2

      SUBROUTINE F3(NEQ,T,Y,YDOT)
        IMPLICIT NONE
        INTEGER NEQ, J, L, M
        REAL T, Y, YDOT
        DIMENSION Y(NEQ), YDOT(NEQ)
        INTEGER IASAVE, JASAVE, IGRID, NZSAVE
        REAL PSAVE
        COMMON /IAJAP/IASAVE(10), JASAVE(27), PSAVE(27), IGRID, NZSAVE

        YDOT(1:NEQ) = 0.0E0
        DO M = 1, IGRID
          DO L = 1, IGRID
            J = L + (M-1)*IGRID
            IF (M/=1) YDOT(J-IGRID) = YDOT(J-IGRID) + Y(J)
            IF (L/=1) YDOT(J-1) = YDOT(J-1) + Y(J)
            YDOT(J) = YDOT(J) - 4.0E0*Y(J)
            IF (L/=IGRID) YDOT(J+1) = YDOT(J+1) + Y(J)
          END DO
        END DO
        RETURN
      END SUBROUTINE F3

      SUBROUTINE JAC3(NEQ,T,Y,IA,JA,NZ,P)
        IMPLICIT NONE
        INTEGER NEQ, IA, JA, NZ
        REAL T, Y, P
        DIMENSION Y(*), IA(*), JA(*), P(*)
        INTEGER IASAVE, JASAVE, IGRID, NZSAVE
        REAL PSAVE
        COMMON /IAJAP/IASAVE(10), JASAVE(27), PSAVE(27), IGRID, NZSAVE

        IF (NZ<=0) THEN
          NZ = NZSAVE
          RETURN
        END IF
        IF (NZ/=NZSAVE) PRINT *, ' NZ UNEQUAL TO NZSAVE IN JAC3'
        IF (NZ/=NZSAVE) STOP
        IA(1:NEQ+1) = IASAVE(1:NEQ+1)
        JA(1:NZ) = JASAVE(1:NZ)
        P(1:NZ) = PSAVE(1:NZ)
        RETURN
      END SUBROUTINE JAC3

      SUBROUTINE EDIT3(NEQ,Y,IOUT,ERM)
        IMPLICIT NONE
        INTEGER IOUT, I, NEQ
        REAL Y, ERM, ER, YEX
        DIMENSION Y(*), YEX(9,3)
        INTEGER IASAVE, JASAVE, IGRID, NZSAVE
        REAL PSAVE
        COMMON /IAJAP/IASAVE(10), JASAVE(27), PSAVE(27), IGRID, NZSAVE
        DATA YEX/6.687279E-01, 9.901910E-01, 7.603061E-01, 8.077979E-01, &
          1.170226E+00, 8.810605E-01, 5.013331E-01, 7.201389E-01, &
          5.379644E-01, 1.340488E-01, 1.917157E-01, 1.374034E-01, &
          1.007882E-01, 1.437868E-01, 1.028010E-01, 3.844343E-02, &
          5.477593E-02, 3.911435E-02, 1.929166E-02, 2.735444E-02, &
          1.939611E-02, 1.055981E-02, 1.496753E-02, 1.060897E-02, &
          2.913689E-03, 4.128975E-03, 2.925977E-03/

        ERM = 0.0E0
        DO I = 1, NEQ
          ER = ABS(Y(I)-YEX(I,IOUT))
          ERM = MAX(ERM,ER)
        END DO
        RETURN
      END SUBROUTINE EDIT3

      SUBROUTINE SSOUT3(NEQ,LOUT)
        INTEGER NEQ, LOUT
        INTEGER IASAVE, JASAVE, IGRID, NZSAVE
        REAL PSAVE
        COMMON /IAJAP/IASAVE(10), JASAVE(27), PSAVE(27), IGRID, NZSAVE

        WRITE (LOUT,90000) (IASAVE(I),I=1,NEQ+1)
90000   FORMAT (/' STRUCTURE DESCRIPTOR ARRAY IASAVE ='/(20I4))
        WRITE (LOUT,90001) (JASAVE(I),I=1,NZSAVE)
90001   FORMAT (/' STRUCTURE DESCRIPTOR ARRAY JASAVE ='/(20I4))
        RETURN
      END SUBROUTINE SSOUT3

    END MODULE DEMOSP

!******************************************************************

    PROGRAM RUNDEMOSP

      USE DVODE_F90_M
      USE DEMOSP

! VODE_F90 IS USED TO SOLVE THREE OF THE ODEPACK DEMO PROBLEMS, ONE
! WITH A FULL JACOBIAN, ONE WITH A BANDED JACOBIAN, AND ONE WITH A
! SPARSE JACOBIAN. THE SPARSE SOLUTION OPTION IS USED FOR EACH PROBLEM.
! EACH PROBLEM IS SOLVED FOR NEGATIVE AND POSITIVE VALUES OF MF.

      IMPLICIT NONE
      INTEGER I, IOPAR, IOUT, ISTATE, ITASK, ITOL, ISTATS, LENIW, LENRW, LOUT, &
        MBAND, METH, MF, MITER, ML, MU, NCFN, NEQ, NERR, NETF, NFE, NFEA, NJE, &
        NLU, NNI, NOUT, NQU, NST, MOSS, J, K, L, M, NTURB, MFSIGN
      REAL ATOL, DTOUT, ER, ERM, ERO, HU, RTOL, RSTATS, T, TOUT, TOUT1, Y, &
        FMIN, DTURB
      DIMENSION Y(25), RSTATS(22), ISTATS(31), ATOL(1), RTOL(1), DTURB(1)
      TYPE (VODE_OPTS) :: OPTIONS
      INTEGER IASAVE, JASAVE, IGRID, NZSAVE
      REAL PSAVE
      INTEGER IAUSER, JAUSER, NIAUSER, NJAUSER
      COMMON /IAJAP/IASAVE(10), JASAVE(27), PSAVE(27), IGRID, NZSAVE
      COMMON /IAJAUSER/IAUSER(10), JAUSER(27), NIAUSER, NJAUSER

      OPEN (UNIT=6,FILE='demosp.dat')
      LOUT = 6
      TOUT1 = 1.39283880203E0
      DTOUT = 2.214773875E0
      NERR = 0
      ITOL = 1
      RTOL(1) = 0.0E0
      ATOL(1) = 1.0E-6
      FMIN = 0.0E0
      NTURB = 1
      DTURB(1) = 0.0E0

      DO MFSIGN = 1, -1, -2

!     FIRST PROBLEM (NONSTIFF AND STIFF DENSE OPTIONS)

        NEQ = 2
        NOUT = 4
        WRITE (LOUT,90000) NEQ, ITOL, RTOL(1), ATOL(1)
90000   FORMAT (/' DENSE DEMONSTRATION PROGRAM FROM THE DVODE PACKAGE:'/ &
          ' PROBLEM 1. VAN DER POL OSCILLATOR.'/ &
          '   XDOTDOT - 3*(1-X**2)*XDOT + X = 0,','   X(0) = 2, XDOT(0) = 0'/ &
          '   NEQ =',I2/'   ITOL =',I3,'   RTOL =',E10.1,'   ATOL =',E10.1)
        MOSS = 2
        METH = 2
        MITER = 7
        MF = (100*MOSS+10*METH+MITER)*MFSIGN
        WRITE (LOUT,90001) MF
90001   FORMAT ('   MF =',I4/6X,'T',15X,'X',15X,'XDOT',7X,'NQ',6X,'H')
        T = 0.0E0
        Y(1) = 2.0E0
        Y(2) = 0.0E0
        ITASK = 1
        ISTATE = 1
        TOUT = TOUT1
        ERO = 0.0E0
        OPTIONS = SET_OPTS(METHOD_FLAG=MF,ABSERR=ATOL(1),RELERR=RTOL(1))
        DO 10 IOUT = 1, NOUT
          CALL DVODE_F90(F1,NEQ,Y,T,TOUT,ITASK,ISTATE,OPTIONS,JAC1)
          CALL GET_STATS(RSTATS,ISTATS)
          HU = RSTATS(11)
          NQU = ISTATS(14)
          WRITE (LOUT,90002) T, Y(1), Y(2), NQU, HU
90002     FORMAT (1X,E15.5,E16.5,E14.3,I5,E14.3)
          IF (ISTATE<0) GO TO 20
          IOPAR = IOUT - 2*(IOUT/2)
          IF (IOPAR/=0) GO TO 10
          ER = ABS(Y(1))/ATOL(1)
!        CALL EDIT1 (NEQ, Y, T, ER, IOUT)
!        ER = ER / ATOL(1)
          ERO = MAX(ERO,ER)
          IF (ER<10000.0E0) GO TO 10
          WRITE (LOUT,90003)
90003     FORMAT (' WARNING. ERROR EXCEEDS 10000 * TOLERANCE')
          NERR = NERR + 1
10      TOUT = TOUT + DTOUT
20      CONTINUE
        IF (ISTATE<0) NERR = NERR + 1
        NST = ISTATS(11)
        NFE = ISTATS(12)
        NJE = ISTATS(13)
        NLU = ISTATS(19)
        LENRW = ISTATS(17)
        LENIW = ISTATS(18)
        NNI = ISTATS(20)
        NCFN = ISTATS(21)
        NETF = ISTATS(22)
        NFEA = NFE
        IF (MITER==2) NFEA = NFE - NEQ*NJE
        IF (MITER==3) NFEA = NFE - NJE
        WRITE (LOUT,90004) LENRW, LENIW, NST, NFE, NFEA, NJE, NLU, NNI, NCFN, &
          NETF, ERO
90004   FORMAT ('   FINAL STATISTICS FOR THIS RUN:'/'   RWORK SIZE =',I4, &
          '   IWORK SIZE =',I4/'   NUMBER OF STEPS =', &
          I5/'   NUMBER OF F-S   =',I5/'   (EXCLUDING J-S) =', &
          I5/'   NUMBER OF J-S   =',I5/'   NUMBER OF LU-S  =', &
          I5/'   NUMBER OF NONLINEAR ITERATIONS =',I5/ &
          '   NUMBER OF NONLINEAR CONVERGENCE FAILURES =', &
          I5/'   NUMBER OF ERROR TEST FAILURES =',I5/'   ERROR OVERRUN =', &
          E10.2)

!     SECOND PROBLEM (NONSTIFF AND STIFF BANDED OPTIONS)

        NEQ = 25
        ML = 5
        MU = 0
        MBAND = ML + MU + 1
        NOUT = 5
        WRITE (LOUT,90005) NEQ, ML, MU, ITOL, RTOL(1), ATOL(1)
90005   FORMAT (/' BANDED DEMONSTRATION PROGRAM FROM THE DVODE PACKAGE:'/ &
          '   PROBLEM 2. YDOT = A * Y , WHERE'/ &
          '   A IS A BANDED LOWER TRIANGULAR MATRIX'/ &
          '   DERIVED FROM 2-D ADVECTION PDE'/'   NEQ =',I3,'   ML =',I2, &
          '   MU =',I2/'   ITOL =',I3,'   RTOL =',E10.1,'   ATOL =',E10.1)
        MOSS = 2
        METH = 2
        MITER = 7
        MF = (100*MOSS+10*METH+MITER)*MFSIGN
        WRITE (LOUT,90006) MF
90006   FORMAT ('   MF =',I4/6X,'T',13X,'MAX.ERR.',5X,'NQ',6X,'H')
        T = 0.0E0
        DO 30 I = 2, NEQ
30      Y(I) = 0.0E0
        Y(1) = 1.0E0
        ITASK = 1
        ISTATE = 1
        TOUT = 0.01E0
        ERO = 0.0E0
        OPTIONS = SET_OPTS(METHOD_FLAG=MF,ABSERR=ATOL(1),RELERR=RTOL(1))
        DO 40 IOUT = 1, NOUT
          CALL DVODE_F90(F2,NEQ,Y,T,TOUT,ITASK,ISTATE,OPTIONS,JAC2)
          CALL GET_STATS(RSTATS,ISTATS)
          CALL EDIT2(Y,T,ERM)
          HU = RSTATS(11)
          NQU = ISTATS(14)
          WRITE (LOUT,90007) T, ERM, NQU, HU
90007     FORMAT (1X,E15.5,E14.3,I5,E14.3)
          IF (ISTATE<0) GO TO 50
          ER = ERM/ATOL(1)
          ERO = MAX(ERO,ER)
          IF (ER<=1000.0E0) GO TO 40
          WRITE (LOUT,90003)
          NERR = NERR + 1
40      TOUT = TOUT*10.0E0
50      CONTINUE
        IF (ISTATE<0) NERR = NERR + 1
        NST = ISTATS(11)
        NFE = ISTATS(12)
        NJE = ISTATS(13)
        NLU = ISTATS(19)
        LENRW = ISTATS(17)
        LENIW = ISTATS(18)
        NNI = ISTATS(20)
        NCFN = ISTATS(21)
        NETF = ISTATS(22)
        NFEA = NFE
        IF (MITER==5) NFEA = NFE - MBAND*NJE
        IF (MITER==3) NFEA = NFE - NJE
        WRITE (LOUT,90004) LENRW, LENIW, NST, NFE, NFEA, NJE, NLU, NNI, NCFN, &
          NETF, ERO

!     THIRD PROBLEM (SPARSE OPTIONS)

        DO MOSS = 2, 0, -1
!        SOLVE THE PROBLEM YDOT = A*Y, WHERE A IS THE 9 BY 9
!        SPARSE MATRIX
!                  -4  1     1
!                   1 -4  1     1
!                      1 -4        1
!                           -4  1     1
!          A =               1 -4  1     1
!                               1 -4        1
!                                    -4  1
!                                     1 -4  1
!                                        1 -4
!        THE INITIAL CONDITIONS ARE  Y(0) = (1, 2, 3, ... , 9).
!        OUTPUT IS PRINTED AT T = 1, 2, AND 3.

          IGRID = 3
          NEQ = IGRID**2
          DO I = 1, NEQ
            Y(I) = I
          END DO
!        JACOBIAN STRUCTURE ARRAYS:
          IASAVE(1) = 1
          K = 1
          DO M = 1, IGRID
            DO L = 1, IGRID
              J = L + (M-1)*IGRID
              IF (M>1) THEN
                JASAVE(K) = J - IGRID
                K = K + 1
              END IF
              IF (L>1) THEN
                JASAVE(K) = J - 1
                K = K + 1
              END IF
              JASAVE(K) = J
              K = K + 1
              IF (L<IGRID) THEN
                JASAVE(K) = J + 1
                K = K + 1
              END IF
              IASAVE(J+1) = K
            END DO
          END DO
          K = 0
!        SPARSE JACOBIAN:
          DO J = 1, NEQ
            M = (J-1)/IGRID + 1
            L = J - (M-1)*IGRID
!           IF (M /= 1) P(J-IGRID) = 1.0E0
            IF (M/=1) THEN
              K = K + 1
              PSAVE(K) = 1.0E0
            END IF
!           IF (L /= 1) P(J-1) = 1.0E0
            IF (L/=1) THEN
              K = K + 1
              PSAVE(K) = 1.0E0
            END IF
!           P(J) = - 4.0E0
            K = K + 1
            PSAVE(K) = -4.0E0
!           IF (L /= IGRID) P(J+1) = 1.0E0
            IF (L/=IGRID) THEN
              K = K + 1
              PSAVE(K) = 1.0E0
            END IF
          END DO
          NZSAVE = 27
90008     FORMAT (' IA: ',/,(I5))
90009     FORMAT (' JA/P: ',/,(I5,F10.5))
          NOUT = 3
          IF (MOSS==2) WRITE (LOUT,90010) NEQ, ITOL, RTOL(1), ATOL(1)
          IF (MOSS==1) WRITE (LOUT,90011) NEQ, ITOL, RTOL(1), ATOL(1)
          IF (MOSS==0) WRITE (LOUT,90012) NEQ, ITOL, RTOL(1), ATOL(1)
90010     FORMAT (/' SPARSE DEMONSTRATION PROGRAM FOR LSODES PACKAGE:'/ &
            ' PROBLEM 3. YDOT = A * Y, WHERE A IS A SPARSE MATRIX'/ &
            '   NUMERICAL JACOBIAN OPTION USED (MOSS = 2)'/'   NEQ =',I3, &
            /'   ITOL =',I3,'   RTOL =',E10.1,'   ATOL =',E10.1)
90011     FORMAT (/' SPARSE DEMONSTRATION PROGRAM FOR LSODES PACKAGE'/ &
            ' PROBLEM 3. YDOT = A * Y, WHERE A IS A SPARSE MATRIX'/ &
            '   EXACT JACOBIAN OPTION USED (MOSS = 1)'/'   NEQ =',I3, &
            /'   ITOL =',I3,'   RTOL =',E10.1,'   ATOL =',E10.1)
90012     FORMAT (/' SPARSE DEMONSTRATION PROGRAM FOR LSODES PACKAGE'/ &
            ' PROBLEM 3. YDOT = A * Y, WHERE A IS A SPARSE MATRIX'/ &
            '   USER SUPPLIED JACOBIAN OPTION USED (MOSS = 0)'/'   NEQ =',I3, &
            /'   ITOL =',I3,'   RTOL =',E10.1,'   ATOL =',E10.1)
          IF (MOSS==2) THEN
            METH = 2
            MITER = 7
          END IF
          IF (MOSS==1) THEN
            METH = 2
            MITER = 6
          END IF
          IF (MOSS==0) THEN
            METH = 2
            MITER = 7
            NIAUSER = 10
            NJAUSER = 27
            IAUSER = IASAVE
            JAUSER = JASAVE
          END IF
          MF = (100*MOSS+10*METH+MITER)*MFSIGN
          WRITE (LOUT,90013) MF
90013     FORMAT ('   MF =',I4/6X,'T',13X,'MAX.ERR.',5X,'NQ',6X,'H')
          T = 0.0E0
          DO I = 1, NEQ
            Y(I) = I
          END DO
          ITASK = 1
          ISTATE = 1
          TOUT = 1.0E0
          ERO = 0.0E0
          OPTIONS = SET_OPTS(METHOD_FLAG=MF,ABSERR=ATOL(1),RELERR=RTOL(1))
          IF (MOSS==0) THEN
            CALL SET_IAJA(F3,NEQ,T,Y,FMIN,NTURB,DTURB,IAUSER,NIAUSER,JAUSER, &
              NJAUSER)
          ELSE
            CALL SET_IAJA(F3,NEQ,T,Y,FMIN,NTURB,DTURB)
          END IF
          DO 60 IOUT = 1, NOUT
            CALL DVODE_F90(F3,NEQ,Y,T,TOUT,ITASK,ISTATE,OPTIONS,JAC3)
            CALL GET_STATS(RSTATS,ISTATS)
            CALL EDIT3(NEQ,Y,IOUT,ERM)
            HU = RSTATS(11)
            NQU = ISTATS(14)
            WRITE (LOUT,90014) T, ERM, NQU, HU
90014       FORMAT (1X,E15.5,E14.3,I5,E14.3)
            IF (ISTATE<0) GO TO 70
            ER = ERM/ATOL(1)
            ERO = MAX(ERO,ER)
            IF (ER<=1000.0E0) GO TO 60
            WRITE (LOUT,90003)
            NERR = NERR + 1
60        TOUT = TOUT + 1.0E0
70        CONTINUE
          IF (ISTATE<0) NERR = NERR + 1
          NST = ISTATS(11)
          NFE = ISTATS(12)
          NJE = ISTATS(13)
          NLU = ISTATS(19)
          LENRW = ISTATS(17)
          LENIW = ISTATS(18)
          NNI = ISTATS(20)
          NCFN = ISTATS(21)
          NETF = ISTATS(22)
          NFEA = NFE
          IF (MITER==5) NFEA = NFE - MBAND*NJE
          IF (MITER==3) NFEA = NFE - NJE
          WRITE (LOUT,90004) LENRW, LENIW, NST, NFE, NFEA, NJE, NLU, NNI, &
            NCFN, NETF, ERO
        END DO

!     END OF MFSIGN DO:
      END DO

      WRITE (LOUT,90015) NERR
90015 FORMAT (/' NUMBER OF ERRORS ENCOUNTERED =',I3)

      STOP

    END PROGRAM RUNDEMOSP
