function [t,y] = demoflow
% Matlab script to plot the solution for the demoflow problem

tempz   = load('demoflowplotz.dat',  '-ascii');
tempt   = load('demoflowplott.dat',  '-ascii');
temptrg = load('demoflowplottrg.dat','-ascii');
tempgin = load('demoflowplotgin.dat','-ascii');
z = tempz(:);
t = tempt(:);
gin = tempgin(:);
nzplot = size(z,1);
ntplot = size(t,1);
tfplot  = temptrg(:,1);
rhoplot = temptrg(:,2);
gplot   = temptrg(:,3);
TF  = reshape(tfplot, nzplot,ntplot);
RHO = reshape(rhoplot,nzplot,ntplot);
G   = reshape(gplot,  nzplot,ntplot);
size(TF);
size(RHO);
size(G);

figure

surf(t,z,TF)
title('Flow Problem: Fluid Temperature')
xlabel('t');
ylabel('z');

figure
surf(t,z,RHO)
title('Flow Problem: Fluid Density')
xlabel('t');
ylabel('z');

figure
surf(t,z,G)
title('Flow Problem: Mass Flow Rate')
xlabel('t');
ylabel('z');

figure
plot(t,gin)
title('Flow Problem: Inlet Mass Flow Rate')
xlabel('t');
ylabel('G');

