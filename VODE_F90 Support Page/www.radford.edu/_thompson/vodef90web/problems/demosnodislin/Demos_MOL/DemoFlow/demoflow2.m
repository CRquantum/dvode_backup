function [t,y] = demoflow2
% Matlab script to plot the solution for the demoflow2 problem

tempz   = load('demoflow2plotz.dat',  '-ascii');
tempt   = load('demoflow2plott.dat',  '-ascii');
temptrg = load('demoflow2plottrg.dat','-ascii');
tempgin = load('demoflow2plotgin.dat','-ascii');
z = tempz(:);
t = tempt(:);
gin = tempgin(:);
nzplot = size(z,1);
ntplot = size(t,1);
tfplot  = temptrg(:,1);
rhoplot = temptrg(:,2);
gplot   = temptrg(:,3);
TF  = reshape(tfplot, nzplot,ntplot);
RHO = reshape(rhoplot,nzplot,ntplot);
G   = reshape(gplot,  nzplot,ntplot);
size(TF);
size(RHO);
size(G);

figure

surf(t,z,TF)
title('Fluid Temperature')
xlabel('t');
ylabel('z');

figure
surf(t,z,RHO)
title('Fluid Density')
xlabel('t');
ylabel('z');

figure
surf(t,z,G)
title('Mass Flow Rate')
xlabel('t');
ylabel('z');

figure
plot(t,gin)
title('Oulet Mass Flow Rate')
xlabel('t');
ylabel('G');

