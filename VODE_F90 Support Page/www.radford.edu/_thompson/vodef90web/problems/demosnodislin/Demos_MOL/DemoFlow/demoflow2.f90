! VODE_F90 demonstration program
! Solution of Euler equations for the subcooled region
! in a water reactor using the method of pseudo
! characteristics.

! In this version the boundary conditions set are the
! temterature and flow rate at zmin and the density at
! zmax. Constant water property values are used.

! Reference for the problem:
! Benchmark Fluid Flow Problems for Continuous Simulation
! Languages, Computers and Mathematics with Applications,
! Volume 12A, Number 3, 1986,pp. 345--352, S. Thompson
! and P. G. Tuttle.

    MODULE DEMO_FLOW2

      IMPLICIT NONE

!     Problem parameters and arrays:

!     COMMON AREA TO ESTABLISH LINKAGE WITH PROBLEM-DEPENDENT
!     SUBROUTINES.

      INTEGER NPT, ISTEAM, M, MP1

      DOUBLE PRECISION XKFAC, GA, SINANG, PHI, ZMIN, ZMAX, PH, AF, P0, G0, &
        ABSOLT, TF0, TFE, PHIMIN, PHIMAX, TF, RHO, VEL, DTF, DRHO, DVEL, &
        DZTF0, DZRHO0, DZVEL0, DZTFP, DZRHOP, DZVELP, DZTFM, DZRHOM, DZVELM, &
        VELPA, VELMA, BETA1, XK1, SPEED1, CSUBP1, DG, DZG0, DZGP, DZGM, TPDE, &
        RPDE, GPDE, DELTAZ, G, VPHI, Z, D1, D2, D3, E1, E2, E3, E2E3, F11, &
        F12, F13, F21, F22, F23, F31, F32, F33

      DIMENSION TF(201), RHO(201), VEL(201), DTF(201), DRHO(201), DVEL(201), &
        DZTF0(201), DZRHO0(201), DZVEL0(201), DZTFP(201), DZRHOP(201), &
        DZVELP(201), DZTFM(201), DZRHOM(201), DZVELM(201), VELPA(201), &
        VELMA(201), BETA1(201), XK1(201), SPEED1(201), CSUBP1(201), DG(201), &
        DZG0(201), DZGP(201), DZGM(201), TPDE(201), RPDE(201), GPDE(201), &
        G(201), VPHI(201), Z(201)

    CONTAINS

      SUBROUTINE FCN(NEQ,T,Y,YDOT)

!     DIRECT THE CALCULATION OF THE TIME DERIVATIVES

        IMPLICIT NONE
        INTEGER NEQ
        DOUBLE PRECISION T, Y, YDOT
        DIMENSION Y(NEQ), YDOT(NEQ)

!     THE ACTUAL CALCULATION OF THE DERIVATIVES IS
!     DIRECTED BY THE FOLLOWING SUBROUTINE.

        CALL DERVAL(Y(1),YDOT(1),Y(M+1),YDOT(M+1),Y(2*M+1),YDOT(2*M+1),T)

        RETURN
      END SUBROUTINE FCN

      SUBROUTINE DERVAL(AG,AGD,AT,ATD,AR,ARD,T)

!     DIRECT THE CALCULATION OF THE TIME DERIVATIVES.

        IMPLICIT NONE
        DOUBLE PRECISION AG, AGD, AT, ATD, AR, ARD, T
        DIMENSION AG(1), AGD(1), AT(1), ATD(1), AR(1), ARD(1)

!     LOAD THE SOLUTION INTO LOCAL STORAGE.

        CALL VARSET(AG,AT,AR)

!     LOAD BOUNDARY CONDITIONS AT THIS POINT IF APPLICABLE.

!     CALCULATE PROPERTIES IF APPLICABLE.

!     DEFINE THE SPATIAL DERIVATIVES.

        CALL SPATEL

!     DEFINE THE TIME DERIVATIVES.

        CALL PSEUDO

!     LOAD THE TIME DERIVATIVES INTO INTEGRATOR ARRAY.

        CALL DYSET(AGD,ATD,ARD)

        RETURN
      END SUBROUTINE DERVAL

      SUBROUTINE DYSET(AGD,ATD,ARD)

!     LOAD LOCAL TIME DERIVATIVES INTO THE INTEGRATOR ARRAY.

        IMPLICIT NONE
        DOUBLE PRECISION AGD, ATD, ARD
        DIMENSION AGD(1), ATD(1), ARD(1)

        ATD(1:M) = DTF(2:MP1)
        ARD(1:M) = DRHO(1:M)
        AGD(1:M) = DG(2:MP1)

        RETURN
      END SUBROUTINE DYSET

      SUBROUTINE INITAL(YINIT)

!     INITIALIZE THE PROBLEM.

!     THIS SUBROUTINE SHOULD BE CALLED BEFORE THE
!     INTEGRATION IS STARTED. WHEN INITAL IS CALLED,
!     THE NECESSARY PROBLEM PARAMETERS WILL BE
!     INITIALIZED. ALSO,THE INITIAL VALUES FOR THE
!     ODE SOLVER WILL BE RETURNED IN THE ARRAY YINIT.
!     THE TOTAL NUMBER OF ODE*S IS NEQ = 3*MVAL.
!     YINIT MUST BE DIMENSIONED IN THE CALLING
!     PROGRAM FOR AT LEAST NEQ.

        IMPLICIT NONE
        INTEGER I
        DOUBLE PRECISION YINIT, DELZ, G0VAL, DELTA
        DIMENSION YINIT(1)

        NPT = 2
        PHIMIN = 1.1D5
        PHIMAX = 1.1D5
        PHI = 1.1D5
        G0VAL = 270.9D0
        MP1 = M + 1
        XKFAC = 10.0D0
        GA = 9.80665D0
        SINANG = 1.0D0
        ZMIN = 0.0D0
!     SATURATION BOUNDARY:
        ZMAX = 2.0953D0
        PH = 7.97318D+2
        AF = 3.82760D0
        P0 = 7.4050D+6
        G0 = G0VAL
        ABSOLT = 273.15D0
        TF0 = 255.0D0
        TFE = 289.0D0

!     DEFINE THE SPATIAL MESH.

        Z(1) = ZMIN
        DELZ = (ZMAX-ZMIN)/M
        DO I = 2, M
          Z(I) = ZMIN + (I-1)*DELZ
        END DO
        Z(MP1) = ZMAX

!     DEFINE THE INITIAL GUESSES FOR RHO AND TF,AND
!     LOAD THE PROPERTIES.

        RHO(1:MP1) = .7343499327D3
        DELTA = (TFE-TF0)/REAL(M)
        DO I = 1, MP1
          TF(I) = TF0 + REAL(I-1)*DELTA
        END DO
        CALL SETIC

!     DEFINE THE INITIAL GUESS FOR THE FLOW
!     RATES AND VELOCITIES.

        DO I = 1, MP1
          G(I) = G0
          VEL(I) = G(I)/RHO(I)
        END DO

!     DEFINE THE INITIAL CONDITIONS FOR THE
!     ODE SOLVER.

        DO I = 1, M
          YINIT(M+I) = TF(I+1)
          YINIT(2*M+I) = RHO(I)
          YINIT(I) = G(I+1)
        END DO

        RETURN
      END SUBROUTINE INITAL

      SUBROUTINE VARSET(AG,AT,AR)

!     LOAD THE INTEGRATOR SOLUTION INTO LOCAL STORAGE.

        IMPLICIT NONE
        DOUBLE PRECISION AG, AT, AR
        DIMENSION AG(1), AT(1), AR(1)

        TF(2:MP1) = AT(1:M)
        RHO(1:M) = AR(1:M)
        G(2:MP1) = AG(1:M)

        RETURN
      END SUBROUTINE VARSET

      SUBROUTINE PSEUDO

!     USE GAUSSIAN ELIMINATION TO SOLVE THE EQUATIONS
!     IN CHARACTERISTIC FORM FOR THE TIME DERIVATIVES.

!     DECOMP/SOLVE SOLUTION REPLACED BY THE ANALYTICAL
!     SOLUTION

        IMPLICIT NONE
        INTEGER I

        DO I = 1, MP1

!     SET UP THE 3 BY 3 LINEAR SYSTEM FOR THIS
!     SPATIAL NODE.

          F11 = SPEED1(I)*SPEED1(I)*BETA1(I)*(TF(I)+ABSOLT)
          F12 = 0.0D0
          F13 = -RHO(I)*CSUBP1(I)
          F21 = -G(I)*XK1(I)*SPEED1(I) + 1.0D0
          F22 = RHO(I)*XK1(I)*SPEED1(I)
          F23 = RHO(I)*BETA1(I)
          F31 = G(I)*XK1(I)*SPEED1(I) + 1.0D0
          F32 = -F22
          F33 = F23

          D1 = 0.0D0
          D2 = -XKFAC*G(I)*ABS(G(I)/RHO(I)) - RHO(I)*GA*SINANG
          D3 = (SPEED1(I)*SPEED1(I)*VPHI(I)*PH*XK1(I))/(CSUBP1(I)*AF)

          E1 = F11*D1 + F12*D2 + F13*D3 - VEL(I)*(F11*DZRHO0(I)+F12*DZG0(I)+ &
            F13*DZTF0(I))
          E2 = F21*D1 + F22*D2 + F23*D3 - VELPA(I)*(F21*DZRHOP(I)+F22*DZGP(I)+ &
            F23*DZTFP(I))
          E3 = F31*D1 + F32*D2 + F33*D3 - VELMA(I)*(F31*DZRHOM(I)+F32*DZGM(I)+ &
            F33*DZTFM(I))

          E2E3 = (E2+E3)*0.5D0

!        SOLVE THE 3 BY 3 SYSTEM OF LINEAR EQUATIONS FOR THIS SPATIAL
!        NODE IN ORDER TO DEFINE THE TIME DERIVATIVES FOR THIS
!        SPATIAL NODE. THIS VERSION USES THE HAND CODED SOLUTION.

          DTF(I) = (F11*E2E3-E1)/(RHO(I)*(CSUBP1(I)+F11))
          DRHO(I) = E2E3 - RHO(I)*BETA1(I)*DTF(I)
          DG(I) = (-E3+F31*DRHO(I)+RHO(I)*BETA1(I)*DTF(I))/F22

        END DO

!     ZERO THE TIME DERIVATIVES CORRESPONDING TO THE
!     BOUNDARY CONDITIONS.

        DTF(1) = 0.0D0
        DRHO(MP1) = 0.0D0
        DG(1) = 0.0D0

        RETURN
      END SUBROUTINE PSEUDO

      SUBROUTINE SETIC

        IMPLICIT NONE
        INTEGER I
        DOUBLE PRECISION XKAVG, BEAVG, SPAVG, CSAVG

        DATA XKAVG/0.171446272015689D-08/

        DATA BEAVG/0.213024626664637D-02/

        DATA SPAVG/0.108595374561510D+04/

        DATA CSAVG/0.496941623289027D+04/

!     PROPERTIES CONSTANT IN SPACE.

        DO I = 1, MP1
          VPHI(I) = PHIMIN + (REAL(I-1)*(PHIMAX-PHIMIN)/REAL(M))
          XK1(I) = XKAVG
          BETA1(I) = BEAVG
          SPEED1(I) = SPAVG
          CSUBP1(I) = CSAVG
        END DO

        RETURN
      END SUBROUTINE SETIC

      SUBROUTINE SPATEL

!     DEFINE THE SPATIAL DERIVATIVES.

        IMPLICIT NONE
        INTEGER I

        DO I = 1, MP1
          VEL(I) = G(I)/RHO(I)
          VELPA(I) = VEL(I) + SPEED1(I)
          VELMA(I) = VEL(I) - SPEED1(I)
        END DO

        DELTAZ = Z(2) - Z(1)

!     V CHARACTERISTIC.

        CALL UPWIND(RHO(1),DZRHO0(1),VEL(1),MP1,DELTAZ,NPT)
        CALL UPWIND(G(1),DZG0(1),VEL(1),MP1,DELTAZ,NPT)
        CALL UPWIND(TF(1),DZTF0(1),VEL(1),MP1,DELTAZ,NPT)

!     V + A  CHARACTERISTIC.

        CALL UPWIND(RHO(1),DZRHOP(1),VELPA(1),MP1,DELTAZ,NPT)
        CALL UPWIND(G(1),DZGP(1),VELPA(1),MP1,DELTAZ,NPT)
        CALL UPWIND(TF(1),DZTFP(1),VELPA(1),MP1,DELTAZ,NPT)

!     V - A CHARACTERISTIC.

        CALL UPWIND(RHO(1),DZRHOM(1),VELMA(1),MP1,DELTAZ,NPT)
        CALL UPWIND(G(1),DZGM(1),VELMA(1),MP1,DELTAZ,NPT)
        CALL UPWIND(TF(1),DZTFM(1),VELMA(1),MP1,DELTAZ,NPT)

        RETURN
      END SUBROUTINE SPATEL

      SUBROUTINE UPWIND(U,UX,V,NX,DX,N0)

!     UPWIND DIFFERENCING ROUTINE.

!     APPROXIMATE DU/DX IN TERMS LIKE V(DU/DX) BY BACKWARD
!     DIFFERENCES IF V IS POSITIVE AND BY FORWARD DIFFERENCES
!     IF V IS NEGATIVE.

!     N0 MAY BE 2,3,OR 4 (IN WHICH CASE,2-POINT,3-POINT,
!     OR 4-POINT DIFFERENCES WILL BE USED,RESPECTIVELY).
!     U IS THE DEPENDENT VARIABLE TO BE DIFFERENCED.
!     NX IS THE NUMBER OF POINTS IN THE SPATIAL GRID
!     CORRESPONDING TO U.
!     DX IS THE(EQUAL) DISTANCE BETWEEN POINTS IN
!     THE SPATIAL MESH.

!     THIS SUBROUTINE WAS LIFTED FROM MIKE CARVER'S
!     FORSIM PACKAGE.

        IMPLICIT NONE
        INTEGER NX, N0, N1, N2, N3, NO, I
        DOUBLE PRECISION U, UX, V, DX
        DIMENSION U(NX), UX(NX), V(NX)

        N1 = NX - 1
        N2 = N1 - 1
        N3 = N2 - 1
        NO = N0 - 1

!     BACKWARD DIFFERENCES.

        GO TO (10,20,30), NO
10      DO I = 2, NX
          UX(I) = (U(I)-U(I-1))/DX
        END DO
        GO TO 40
20      DO I = 3, NX
          UX(I) = (1.5D0*U(I)-2.D0*U(I-1)+.5D0*U(I-2))/DX
        END DO
        GO TO 40
30      DO I = 4, NX
          UX(I) = (-2.0D0*U(I-3)+9.0D0*U(I-2)-18.0D0*U(I-1)+11.0D0*U(I))/ &
            (6.0D0*DX)
        END DO
        UX(3) = (1.5D0*U(3)-2.0D0*U(2)+.5D0*U(1))/DX
40      UX(1) = (U(2)-U(1))/DX
        UX(2) = (U(2)-U(1))/DX

!     FORWARD DIFFERENCES (APPLIED ONLY IF V .LT. 0).

        GO TO (50,60,70), NO
50      DO I = 1, N1
          IF (V(I)<0.0D0) UX(I) = (U(I+1)-U(I))/DX
        END DO
        GO TO 80
60      DO I = 1, N2
          IF (V(I)<0.0D0) UX(I) = (-1.5D0*U(I)+2.0D0*U(I+1)-.5D0*U(I+2))/DX
        END DO
        GO TO 80
70      DO I = 1, N3
          IF (V(I)<0.0D0) UX(I) = (-11.0D0*U(I)+18.0D0*U(I+1)-9.0D0*U(I+2)+ &
            2.0D0*U(I+3))/(6.0D0*DX)
        END DO
        IF (V(N2)<0.0D0) UX(N2) = (-1.5D0*U(N2)+2.0D0*U(N1)-.5D0*U(NX))/DX
80      IF (V(N1)<0.0D0) UX(N1) = (U(NX)-U(N1))/DX
        IF (V(NX)<0.0D0) UX(NX) = UX(N1)

        RETURN
      END SUBROUTINE UPWIND

    END MODULE DEMO_FLOW2

!******************************************************************

! Note:
! You can play one of two games with this program.
! Play Game A below to see the steady state results.
! Play Game B to see the oscillations in the plots
! of the spatial flow rate and the inlet flow rate
! during the initial transient.

    PROGRAM FLOW2

      USE DVODE_F90_M
      USE DEMO_FLOW2

      IMPLICIT NONE

!     Note:
!     NEQMAX is the maximum allowable number of odes..
!     If NEQMAX is increased,be sure to make the same
!     increase in the header declarations.
      INTEGER, PARAMETER :: NEQMAX = 600

      INTEGER, PARAMETER :: NZPLOT = 201, NTPLOT = 1001

      INTEGER NST, NFE, LENRW, LENIW, NNI, NCFN, NETF, NFEA, ISTATS, ISTATE, &
        ITASK, IOUT, NJE, NOUT, NLU, NPLOT, NEQ, I, J
      DOUBLE PRECISION T, TOUT, ATOL, RTOL, RSTATS, Y, DY, DERMAX, ZPLOT, &
        TPLOT, TFPLOT, RHOPLOT, GPLOT, GIPLOT, DELTAT, TFINAL, GERROR
      DIMENSION ATOL(1), RTOL(1), RSTATS(22), ISTATS(31), ZPLOT(NZPLOT), &
        TPLOT(NTPLOT), TFPLOT(NZPLOT,NTPLOT), RHOPLOT(NZPLOT,NTPLOT), &
        GPLOT(NZPLOT,NTPLOT), GIPLOT(NTPLOT)
      LOGICAL SPARSE, GAMEA
!     SOLUTION AND DERIVATIVE:
      DIMENSION Y(NEQMAX), DY(NEQMAX)

      TYPE (VODE_OPTS) :: OPTIONS

!     OPEN THE OUTPUT FILE AND THE MATLAB PLOT FILES:
      OPEN (6,FILE='demoflow2.dat')
      OPEN (7,FILE='demoflow2plotz.dat')
      OPEN (8,FILE='demoflow2plott.dat')
      OPEN (9,FILE='demoflow2plottrg.dat')
      OPEN (10,FILE='demoflow2plotgin.dat')

!     M+1 = NUMBER OF SPATIAL NODES AND NEQ = NUMBER OF ODES:
      M = 200
      M = 20
      NEQ = 3*M

      IF (M>=NZPLOT) THEN
        WRITE (6,90005)
        STOP
      END IF
      IF (NEQ>NEQMAX) THEN
        WRITE (6,90006)
        STOP
      END IF

!     INITIAL SOLUTION:
      CALL INITAL(Y)

!     SET DVODE_F90 JACOBIAN OPTION:
      SPARSE = .TRUE.

!     ABSOLUTE AND RELATIVE ERROR TOLERANCES:
      ATOL(1) = 1.0D-5
      RTOL(1) = 1.0D-5

!     INITIAL INTEGRATION TIME:
      T = 0.0D0

!     USE GAMEA = .FALSE. TO PLAY GAME B:
      GAMEA = .FALSE.
      GAMEA = .TRUE.
      IF (GAMEA) THEN
!        Game A: USE THIS FOR THE FULL STEADY STATE RUN:
        NOUT = 14
        NOUT = MIN(NOUT,NTPLOT)
        TFINAL = 1.0D5
        TOUT = 1.0D-8
      ELSE
!        Game B: USE THIS TO SEE THE OSCILLATIONS IN THE FLOW RATE
!        DURING THE INITIAL TRANSIENT:
        NOUT = 1001
        NOUT = MIN(NOUT,NTPLOT)
        TFINAL = 2.0D-2
        DELTAT = TFINAL/REAL(NOUT-1)
        TOUT = DELTAT
      END IF

!     INITIALIZE THE INTEGRATION FLAGS:
      ITASK = 1
      ISTATE = 1

      WRITE (6,90009) (G(I),RHO(I),TF(I),I=1,MP1)

!     SET REMAINING VODE_F90 OPTIONS:

      OPTIONS = SET_OPTS(SPARSE_J=SPARSE,ABSERR=ATOL(1),RELERR=RTOL(1), &
        MXSTEP=100000,NZSWAG=20000)

!     INITIALIZE THE PLOT COUNTER:
      NPLOT = 0

!     PERFORM THE INTEGRATION:

      DO IOUT = 1, NOUT

        CALL DVODE_F90(FCN,NEQ,Y,T,TOUT,ITASK,ISTATE,OPTIONS)

!        GATHER THE INTEGRATION STATISTICS FOR THIS CALL:
        CALL GET_STATS(RSTATS,ISTATS)

!        CHECK IF AN ERROR OCCURRED:
        IF (ISTATE<0) GO TO 10

!        CHECK THE MAGNITUDE OF THE DERIVATIVE AND THE SPATIAL
!        DISCRETIZATION ERROR IN THE INLET FLOW RATE:
        CALL FCN(NEQ,TOUT,Y,DY)
        DERMAX = MAXVAL(ABS(DY(1:NEQ)))
!        WRITE(6,185) TOUT,DERMAX
        GERROR = 100.0D0*ABS((G(MP1)-G0)/G0)
!        WRITE(6,190) GERROR

!        Update the plot point counter:
        IF (NPLOT<NTPLOT) NPLOT = NPLOT + 1

!        Save the plot data:
        IF (NPLOT<=NTPLOT) THEN
          TPLOT(IOUT) = TOUT
          TFPLOT(1:MP1,IOUT) = TF(1:MP1)
          RHOPLOT(1:MP1,IOUT) = RHO(1:MP1)
          GPLOT(1:MP1,IOUT) = G(1:MP1)
          GIPLOT(IOUT) = G(MP1)
        END IF

!        UPDATE THE OUTPUT TIME:
        IF (GAMEA) THEN
          TOUT = 10.0D0*TOUT
        ELSE
          TOUT = TOUT + DELTAT
        END IF

      END DO

10    CONTINUE

!     CHECK IF AN ERROR OCCURRED IN VODE_F90:
      IF (ISTATE<0) THEN
        PRINT *, 'DVODE_F90 returned ISTATE = ', ISTATE
        WRITE (6,*) 'DVODE_F90 returned ISTATE = ', ISTATE
      END IF

!     PRINT THE FINAL INTEGRATION STATISTICS:
      NST = ISTATS(11)
      NFE = ISTATS(12)
      NJE = ISTATS(13)
      NLU = ISTATS(19)
      LENRW = ISTATS(17)
      LENIW = ISTATS(18)
      NNI = ISTATS(20)
      NCFN = ISTATS(21)
      NETF = ISTATS(22)
      NFEA = NFE
      NFEA = NFE - NJE
      WRITE (6,90000) LENRW, LENIW, NST, NFE, NFEA, NJE, NLU, NNI, NCFN, NETF

      IF (GAMEA) THEN
        TOUT = TOUT/10.0D0
      ELSE
        TOUT = TOUT - DELTAT
      END IF

      WRITE (6,90001) TOUT
      WRITE (6,90010) (G(I),RHO(I),TF(I),I=1,MP1)
      WRITE (6,90002) NEQ
      WRITE (6,90003) TOUT, DERMAX
      WRITE (6,90008) GERROR

!     Write the Matlab plot file:
      ZPLOT(1:MP1) = Z(1:MP1)
      WRITE (7,90005) (ZPLOT(I),I=1,MP1)
      DO I = 1, NPLOT
        WRITE (8,90005) TPLOT(I)
        WRITE (10,90005) GIPLOT(I)
        DO J = 1, MP1
          WRITE (9,90004) TFPLOT(J,I), RHOPLOT(J,I), GPLOT(J,I)
        END DO
      END DO

      CALL RELEASE_ARRAYS

!     FORMATS:
90000 FORMAT (' Final statistics for this run:'/'   RWORK size      = ',I8, &
        '   IWORK size =',I8/'   Number of steps = ', &
        I8/'   Number of f-s   = ',I8/'   (excluding J-s) = ', &
        I8/'   Number of J-s   = ',I8/'   Number of LU-s  = ', &
        I8/'   Number of nonlinear iterations           = ', &
        I8/'   Number of nonlinear convergence failures = ', &
        I8/'   Number of error test failures            = ',I8/)
90001 FORMAT (' Final integration time = ',D15.5)
90002 FORMAT (' Number of odes = ',I10)
90003 FORMAT (' At t = ',D15.5,' dermax = ',D15.5)
90004 FORMAT (3D20.10)
90005 FORMAT (D25.15)
90006 FORMAT (' M+1 is larger than NZPLOT.')
90008 FORMAT (' % Spatial discretization error in outlet flow rate = ',F10.2)
90009 FORMAT (' Initial Solution:',/,(3D15.5))
90010 FORMAT (' Final Solution:',/,(3D15.5))
!              Note:

!     TERMINATE EXECUTION:
      PRINT *, ' '
      IF (ISTATE==2) THEN
        PRINT *, ' The integration was successful. '
      ELSE
        PRINT *, ' The integration was not successful. '
      END IF
      STOP
    END PROGRAM FLOW2
