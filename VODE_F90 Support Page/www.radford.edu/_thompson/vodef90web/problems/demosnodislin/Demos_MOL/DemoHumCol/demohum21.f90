! VODE_F90 demonstration program
! This program solves the packed humidification column problem from the
! following reference. (See the problem description in INITAL in this
! program.). This program uses sparse option with structure changes.
! This program illustrates how to use DSS/2 in connection with VODE_F90.
! It is modelled after the original f77 program for this problem from
! the reference.

! Reference for this problem:
! C.A. Silebi and W.E. Schiesser, Dynamic Modeling of Transport
! Process Systems, Academic Press, 1992.

    PROGRAM DEMO_HUM21

      USE DVODE_F90_M

!     DEMO_HUM21 CALLS THE FOLLOWING SUBROUTINES:
!       (1) INITAL TO DEFINE THE ODE INITAL CONDITIONS
!       (2) SET_OPTS TO DEFINE THE DVODE INTEGRATIONS
!       (3) DVODE_F90 TO INTEGRATE THE ODES
!       (4) GET_STATS TO OBTAIN INTEGRATION STATISTICS
!       (5) PRINT TO PRINT THE SOLUTION

      IMPLICIT NONE
      DOUBLE PRECISION T, T0, TP, TV, TOUT, ATOL, RTOL, RSTATS
      INTEGER NST, NFE, LENRW, LENIW, NNI, NCFN, NETF, NFEA, ISTATS, ISTATE, &
        ITASK, IOUT, NJE, NOUT, NLU
      DIMENSION RSTATS(22), ISTATS(31)
      TYPE (VODE_OPTS) :: OPTIONS

!     THE FOLLOWING CODING IS FOR 64 ORDINARY DIFFERENTIAL EQUATIONS
!     (ODES). IF MORE ODES ARE TO BE INTEGRATED, ALL OF THE  64*S
!     SHOULD BE CHANGED TO THE REQUIRED NUMBER. ALSO CHANGE THE 21*S.

      INTEGER NSTOP, NORUN, MIXIT
      DOUBLE PRECISION Y, F
      COMMON /T/T, NSTOP, NORUN/Y/Y(64)/F/F(64)/MIX/MIXIT
!     NOTE: Y AND F ARE THE SAME AS THE LOCAL VARIABLES IN INITAL
!     AND DERV.

!     THE NUMBER OF DIFFERENTIAL EQUATIONS IS IN COMMON /N/ FOR USE
!     IN THE PROBLEM SUBROUTINES:
      INTEGER NEQN
      COMMON /N/NEQN

!     COMMON/IO/ CONTAINS THE INPUT/OUTPUT UNIT (DEVICE) NUMBERS:
      INTEGER NI, NO
      COMMON /IO/NI, NO

      DOUBLE PRECISION YV
      DIMENSION YV(64)

      LOGICAL SPARSE, RECOMPUTE_SPARSITY, MYPICTURES

!     Note:
!     Since the problem subroutines are not in a module, it is
!     necessary to declare the derivative subroutine as EXTERNAL:
      EXTERNAL FCN

!     DEFINE THE INPUT UNIT NUMBER:
      NI = 5
!     DEFINE THE OUTPUT UNIT NUMBER:
      NO = 6
!     OPEN THE OUTPUT FILE AND THE PLOT FILE:
      OPEN (NO,FILE='demohum21.dat')
      OPEN (7,FILE='demohum21plot.dat')

!     USE ENGLISH UNITS:
      NORUN = 1
!     USE METRIC UNITS:
      NORUN = 2

!     USE NODEWISE ORDERING OF THE SOLUTION VARIABLES:
      MIXIT = 0
!     USE EQUATION ORDERING OF THE SOLUTION VARIABLES:
      MIXIT = 1

!     NUMBER OF ODES:
      NEQN = 64

!     ABSOLUTE AND RELATIVE ERROR TOLERANCES:
      RTOL = 1.0D-6
      ATOL = 1.0D-6

!     INITIAL, OUTPUT, AND FINAL INTEGRATION TIMES:
      T0 = 0.0D0
      TP = 0.005D0
      T = T0
      TV = T0

!     NUMBER OF OUTPUT TIMES:
!     INTEGRATE TO T = 1.0:
!     NOUT = 200
!     INTEGRATE TO T = 0.5:
      NOUT = 100

!     HAVE VODE_F90 RECOMPUTE THE SPARSITY STRUCTURE THE
!     SECOND TIME IT IS CALLED:
      RECOMPUTE_SPARSITY = .TRUE.

!     COMPUTE THE SPARSITY STRUCTURE AT ALL OUTPUT POINTS FOR
!     THE HECK OF IT (DOESN'T AFFECT WHAT VODE_F90 DOES):
      MYPICTURES = .FALSE.

!     SET THE INITIAL CONDITIONS (THIS SETS THE Y VECTOR TOO):
      CALL INITAL

!     SET THE INITIAL DERIVATIVES (FOR POSSIBLE PRINTING):
      CALL DERV

!     PRINT THE INITIAL CONDITIONS:
      CALL PRINT(NO)

!     SET THE INITIAL CONDITIONS FOR DVODE_F90:
      IF (MIXIT==0) THEN
!        USE NODEWISE ORDERING OF THE SOLUTION VARIABLE
!        (SENDS Y TO YV):
        CALL YMIX(YV)
        IF (MYPICTURES) THEN
          CALL JSTRUT(YV,NEQN,T,FCN)
        END IF
      ELSE
!        USE EQUATION ORDERING OF THE SOLUTION VARIABLE:
        YV(1:NEQN) = Y(1:NEQN)
        IF (MYPICTURES) THEN
          CALL JSTRUT(YV,NEQN,T,FCN)
          YV(1:NEQN) = Y(1:NEQN)
        END IF
      END IF

!     INITIALIZE THE INTEGRATION FLAGS:
      ITASK = 1
      ISTATE = 1

!     SET DVODE_F90 INTEGRATION OPTIONS:
      SPARSE = .TRUE.
      IF (SPARSE) THEN
        OPTIONS = SET_OPTS(SPARSE_J=.TRUE.,ABSERR=ATOL,RELERR=RTOL)
      ELSE
        OPTIONS = SET_OPTS(DENSE_J=.TRUE.,ABSERR=ATOL,RELERR=RTOL)
      END IF

      DO IOUT = 1, NOUT

        TOUT = TV + TP

!        RECOMPUTE SPARSITY STRUCTURE THE SECOND TIME THROUGH:
        IF (RECOMPUTE_SPARSITY .AND. IOUT==2) ISTATE = 3

!        PERFORM THE INTEGRATION:
        CALL DVODE_F90(FCN,NEQN,YV,TV,TOUT,ITASK,ISTATE,OPTIONS)

!        GATHER THE INTEGRATION STATISTICS FOR THIS CALL:
        CALL GET_STATS(RSTATS,ISTATS)

!        CHECK IF ERROR OCCURRED:
        IF (ISTATE<0) GO TO 10

!        TRANSFER THE SOLUTION VARIABLE TO LOCAL STORAGE:
        IF (MIXIT==0) THEN
!           USE NODEWISE ORDERING OF THE SOLUTION VARIABLES
!           (SENDS YV TO Y):
          CALL YUNMIX(YV)
          IF (MYPICTURES) THEN
            CALL JSTRUT(YV,NEQN,T,FCN)
!              Y WAS CHANGED DURING THE CALL TO JSTRUT ...
            CALL YUNMIX(YV)
          END IF
        ELSE
!           USE EQUATION ORDERING OF THE SOLUTION VARIABLES:
          Y(1:NEQN) = YV(1:NEQN)
          IF (MYPICTURES) THEN
            CALL JSTRUT(YV,NEQN,T,FCN)
            Y(1:NEQN) = YV(1:NEQN)
          END IF
        END IF
!        PRINT THE SOLUTION:
        T = TV
        CALL PRINT(NO)

      END DO

10    CONTINUE

!     CHECK IF AN ERROR OCCURRED IN VODE_F90:
      IF (ISTATE<0) THEN
        PRINT *, ' Stopping. DVODE_F90 returned ISTATE = ', ISTATE
        WRITE (NO,*) ' Stopping. DVODE_F90 returned ISTATE = ', ISTATE
        STOP
      END IF

!     PRINT THE FINAL INTEGRATION STATISTICS:
      NST = ISTATS(11)
      NFE = ISTATS(12)
      NJE = ISTATS(13)
      NLU = ISTATS(19)
      LENRW = ISTATS(17)
      LENIW = ISTATS(18)
      NNI = ISTATS(20)
      NCFN = ISTATS(21)
      NETF = ISTATS(22)
      NFEA = NFE
      NFEA = NFE - NJE
      WRITE (NO,90000) LENRW, LENIW, NST, NFE, NFEA, NJE, NLU, NNI, NCFN, NETF

!     FORMATS:
90000 FORMAT ('   Final statistics for this run:'/'   RWORK size =',I4, &
        '   IWORK size =',I4/'   Number of steps =',I5/'   Number of f-s   =', &
        I5/'   (excluding J-s) =',I5/'   Number of J-s   =', &
        I5/'   Number of LU-s  =',I5/'   Number of nonlinear iterations =', &
        I5/'   Number of nonlinear convergence failures =', &
        I5/'   Number of error test failures =',I5/)

!     TERMINATE EXECUTION:
      PRINT *, ' '
      PRINT *, ' The problem was successfully completed. '
      STOP
    END PROGRAM DEMO_HUM21

    SUBROUTINE FCN(NDUM,TV,YV,YDOT)

!     SUBROUTINE FCN IS AN INTERFACE BETWEEN SUBROUTINES DVODE_F90
!     AND DERV
!     NOTE THAT THE SIZE OF ARRAYS Y AND F IN THE FOLLOWING COMMON AREA
!     IS ACTUALLY SET BY THE CORRESPONDING COMMON STATEMENT IN THE MAIN
!     PROGRAM DEMO_HUM21.
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      COMMON /T/T, NSTOP, NORUN/Y/Y(64)/F/F(64)/MIX/MIXIT
!     THE NUMBER OF DIFFERENTIAL EQUATIONS IS AVAILABLE THROUGH COMMON /N/
      COMMON /N/NEQN
!     DEPENDENT VARIABLE AND DERIVATIVE VECTORS
      DIMENSION YV(*), YDOT(*)
!     TRANSFER THE INDEPENDENT VARIABLE, DEPENDENT VARIABLE VECTOR
!     FOR USE IN SUBROUTINE DERV:
      T = TV
      IF (MIXIT==0) THEN
!     (SENDS YV TO Y):
        CALL YUNMIX(YV)
      ELSE
        Y(1:NEQN) = YV(1:NEQN)
      END IF
!     EVALUATE THE DERIVATIVE VECTOR:
      CALL DERV
!     TRANSFER THE DERIVATIVE VECTOR FOR USE BY SUBROUTINE DVODE_F90:
      IF (MIXIT==0) THEN
!     (SENDS F TO YDOT):
        CALL DYMIX(YDOT)
      ELSE
        YDOT(1:NEQN) = F(1:NEQN)
      END IF
      RETURN
    END SUBROUTINE FCN

    SUBROUTINE INITAL

!  DYNAMICS OF A HUMIDIFICATION COLUMN

!  A HUMIDIFICATION COLUMN (A PACKED COLUMN WITH WATER FLOWING DOWN
!  AND AIR FLOWING UP) IS DIAGRAMMED BELOW

!                     L,TL(ZL,T)   V,Y(ZL,T),
!                                   TG(ZL,T)
!                          .           +
!                          .           .
!                          .           .
!                          +           .
!                          .  .  .  .  . Z = ZL
!                        .              .
!                       .                .
!                      .                  .
!                      .                  .
!                      .                  .
!                      .                  .
!                      .                  .
!                      .                  .
!                      . L,TL       V,Y,TG.
!                      .  .            +  .
!                      .  .            .  .
!                      .  +            .  .
!                      .................... Z+DZ
!                      .      .           .
!                      . H20  .           .
!                      .PHASE . ----+ H20 .
!                      .      .    AIR    .
!                      .      .   PHASE   .
!                      ....................  Z
!                      .  .            +  .
!                      .  .            .  .
!                      .  +            .  .
!                      . H20          AIR .
!                      .STREAM      STREAM.
!                      .(DOWN)       (UP) .
!                      .                  .
!                      .                  .
!                      .                  .
!                      .                  .
!                      .                  .
!                      .                  .
!                       .                .
!                        .              .
!                          .  .  .  .  . Z = 0
!                          .           +
!                          .           .
!                          .           .
!                          +           .
!                      L,TL(0,T)   V,Y(0,T),
!                                   TG(0,T)

!  THE VARIABLES AND PARAMETERS FOR THIS SYSTEM ARE

!     TG      GAS TEMPERATURE (UNITS OF THE MODEL VARIABLES ARE
!             DEFINED BELOW WITH THE NUMERICAL VALUES OF THE MODEL
!             PARAMETERS AND AUXILIARY CONDITIONS)

!     EV      INTERNAL ENERGY OF THE GAS STREAM
!             = (CVA*TG + Y*(CVV*TG + DHVAP))

!     EP      ENTHALPY OF THE GAS STREAM
!             = (CPA*TG + Y*(CPV*TG + DHVAP))

!     TL      LIQUID TEMPERATURE

!     Y       MOLE RATIO OF H20 IN THE GAS

!     YS      MOLE RATIO OF H20 IN THE GAS WHICH WOULD BE IN
!             EQUILIBRIUM WITH THE LIQUID AT TEMPERATURE TL

!     T       TIME

!     Z       AXIAL POSITION ALONG THE COLUMN

!     ZL      LENGTH OF THE COLUMN

!     V       DRY AIR MOLAR FLOW RATE

!     G       DRY AIR MOLAR HOLDUP

!     S       COLUMN CROSS SECTIONAL AREA

!     KY      MASS TRANSFER COEFFICIENT

!     AV      HEAT AND MASS TRANSFER AREAS PER UNIT VOLUME OF
!             COLUMN

!     H       HEAT TRANSFER COEFFICIENT

!     CVV     SPECIFIC HEAT OF WATER VAPOR AT CONSTANT VOLUME

!     CPV     SPECIFIC HEAT OF WATER VAPOR AT CONSTANT PRESSURE

!     CVA     SPECIFIC HEAT OF DRY AIR AT CONSTANT VOLUME

!     CPA     SPECIFIC HEAT OF DRY AIR AT CONSTANT PRESSURE

!     CL      SPECIFIC HEAT OF WATER

!     L       LIQUID (H20) MOLAR FLOW RATE

!     H       LIQUID (H20) MOLAR HOLDUP

!     DHVAP   HEAT OF VAPORIZATION OF WATER

!  THE DEPENDENT VARIABLES OF PARTICULAR INTEREST ARE Y(Z,T), TG(Z,T)
!  TL(Z,T).  IN ORDER TO COMPUTE THESE VARIABLES, BASIC CONSERVATION
!  PRINCIPLES ARE APPLIED TO THE AIR AND WATER STREAMS WITHIN THE
!  COLUMN OVER THE INCREMENTAL SECTION OF LENGTH DZ(SEE THE DIAGRAM)
!  AND THEN THE LIMIT OF DZ ---+ 0 GIVES THE MODEL DIFFERENTIAL EQUA-
!  TIONS.  SINCE THE MODEL IS DYNAMIC, TIME, T, AND AXIAL POSITION,
!  Z, APPEAR IN THE EQUATIONS AS INDEPENDENT VARIABLES AND THEREFORE
!  PARTIAL DIFFERENTIAL EQUATIONS(PDES) MUST BE USED.

!  THE PDES ARE DERIVED AS FOLLOWS.  A H20 BALANCE ON THE GAS PHASE
!  GIVES

!     (Y*G*S*DZ)  = V*Y  - V*Y     + KY*AV*(YS - Y)*S*DZ     (1A)
!               T      Z      Z+DZ

!  WHERE THE SUBSCRIPT T ON THE LHS DENOTES A PARTIAL DERIVATIVE WITH
!  RESPECT TO TIME, T.

!  THE PHYSICAL INTERPRETATION AND UNITS OF EACH OF THE TERMS IN
!  EQUATION (1A) IS INDICATED BELOW

!     (Y*G*S*DZ)
!               T

!        = (MOLS H20/MOL DRY AIR)*(MOLS DRY AIR/CM**3)*(CM**2)*
!          CM*(1/HR) = MOLS H20/HR ACCUMULATING IN THE GAS PHASE
!          WITHIN THE INCREMENTAL SECTION OF LENGTH DZ(SEE THE
!          DIAGRAM BELOW)

!     V*Y
!        Z

!        = (MOLS DRY AIR/HR)*(MOLS H20/MOL DRY AIR) = MOLS H20/HR
!          FLOWING INTO THE INCREMENTAL SECTION AT Z

!     V*Y
!        Z+DZ

!        = (MOLS DRY AIR/HR)*(MOLS H20/MOL DAY AIR) = MOLS H20/HR
!..           FLOWING OUT OF THE INCREMENTAL SECTION AT Z+DZ

!     KY*AV*(YS - Y)*S*DZ

!        = (MOLS DRY AIR/HR-CM**2)*(CM**2/CM**3)*(MOLS H20/MOL DRY
!           AIR)*(CM**2)*(CM) = MOLS H20/HR TRANSFERRED TO OR FROM
!           THE AIR STREAM IN THE INCREMENTAL SECTION (THE DIRECTION
!           OF TRANSFER DEPENDS ON THE SIGN OF (YS - Y))

!  IN THE LIMIT AS DZ ---+ 0, EQUATION (1A) BECOMES

!     Y   = -(V/(G*S))*Y  + (KY*AV/G)*(YS - Y)                (1)
!      T                Z

!  AN ENERGY BALANCE ON THE GAS PHASE GIVES

!     (CVA*TG + Y*(CVV*TG + DHVAP))*G*S*DZ)  =
!                                          T

!     (CPA*TG + Y*(CPV*TG + DHVAP))*V  -
!                                    Z                       (2A)

!     (CPA*TG + Y*(CPV*TG + DHVAP))*V     -
!                                    Z+DZ

!     H*AV*(TL - TG)*S*DZ + KY*AV*(YS - Y)*S*DZ*(CVV*TG + DHVAP)

!  THE PHYSICAL INTERPRETATION AND UNITS OF EACH OF THE TERMS IN
!  EQUATION (2A) IS INDICATED BELOW

!     (CVA*TG + Y*(CVV*TG + DHVAP))G*S*DZ
!                                        T

!        = (CAL/MOL DRY AIR-C)*(C) + (MOLS H20/MOL DRY AIR)*
!          (CAL/MOL H20-C)*(C) + CAL/MOL H20))*(MOLS DRY AIR/
!          CM**3)*(CM**2)*CM*(1/HR) = CAL/HR ACCUMULATING IN THE
!          GAS PHASE WITHIN THE INCREMENTAL SECTION

!     (CPA*TG + Y*(CPV*TG + DHVAP))*V
!                                    Z

!        = (CAL/MOL DRY AIR-C)*(C) + (MOLS H20/MOL DRY AIR)*
!          (CAL/MOL H20-C)*(C) + CAL/MOL H20))*(MOLS DRY AIR/HR)
!          = CAL/HR FLOWING INTO THE INCREMENTAL SECTION AT Z

!     (CPA*TG + Y*(CPV*TG + DHVAP))*V
!                                    Z+DZ

!        = (CAL/MOL DRY AIR-C)*(C) + (MOLS H20/MOL DRY AIR)*
!          (CAL/MOL H20-C)*(C) + CAL/MOL H20))*(MOLS DRY AIR/HR)
!          = CAL/HR FLOWING OUT OF THE INCREMENTAL SECTION AT Z+DZ

!     H*AV*(TL - TG)*S*DZ

!        = (CAL/HR-CM**2-C)*(CM**2/CM**3)*(C)*(CM**2)*CM = CAL/HR
!          TRANSFERRED BETWEEN THE H20 AND GAS PHASES DUE TO TEMP-
!          ERATURE DIFFERENCES(THE DIRECTION OF TRANSFER DEPENDS
!          ON THE SIGN OF (TL - TG))

!     KY*AV*(YS - Y)*S*DZ*(CVV*TG + DHVAP)

!        = (MOLS DRY AIR/HR-CM**2)*(CM**2/CM**3)*(MOLS H20/MOL DRY
!          AIR)*(CM**2)*(CM)*((CAL/MOL H20-C)*C + CAL/MOL H20) =
!          CAL/HR TRANSFERRED BETWEEN THE H20 AND GAS PHASES DUE TO
!          DIFFERENCES IN HUMIDITY

!  IN THE LIMIT AS DZ ---+ 0, EQUATION (2A) BECOMES

!     EV  = -(V/(G*S))*EP  + (H*AV/G)*(TL - TG)
!       T                Z                                    (2)

!                          + (KY*AV/G)*(YS - Y)*(CVV*TG + DHVAP)

!  THERE THE ENERGY GROUPS, EV(ENERGY AT CONSTANT VOLUME) AND EP
!  (ENERGY AT CONSTANT PRESSURE) HAVE BEEN USED TO SIMPLIFY THE
!  TERMS IN EQUATION (2).  EV AND EP ARE DEFINED IN THE TABLE OF
!  NOMENCLATURE GIVEN EARLIER.

!  AN ENERGY BALANCE ON THE LIQUID GIVES

!     (CL*TL*H*S*DZ)  = (CL*TL*L)     - (CL*TL*L)
!                   T            Z+DZ            Z           (3A)

!     -H*AV*(TL - TG)*S*DZ - KY(AV*(YS - Y)*S*DZ*(CVV*TG + HDVAP)

!  THE PHYSICAL INTERPRETATION AND UNITS OF EACH OF THE TERMS IN
!  EQUATION (3A) IS GIVEN BELOW

!     (CL*TL*H*S*DZ)
!                   T

!        (CAL/MOL H20-C)*(C)*(MOLS H20/CM**3)*(CM**2)*CM*(1/HR)
!        = CAL/HR ACCUMULATING IN THE H20 PHASE WITHIN THE
!        INCREMENTAL SECTION OF LENGTH DZ

!     (CL*TL*L)
!              Z+DZ

!        = (CAL/MOL H20-C)*(C)*(MOLS H20/HR) = CAL/HR FLOWING INTO
!          THE INCREMENTAL SECTION AT Z+DZ

!     (CL*TL*L)
!              Z

!        = (CAL/MOL H20-C)*(C)*(MOLS H20/HR) = CAL/HR FLOWING OUT OF
!          THE INCREMENTAL SECTION AT Z

!     H*AV*(TL - TG)*S*DZ

!        = (CAL/HR-CM**2-C)*(CM**2/CM**3)*(C)*(CM**2)*CM = CAL/HR
!          TRANSFERRED BETWEEN THE H20 AND GAS PHASES DUE TO TEMP-
!          ERATURE DIFFERENCES(THE DIRECTION OF TRANSFER DEPENDS
!          ON THE SIGN OF (TL - TG))

!     KY*AV*(YS - Y)*S*DZ*(CVV*TG + DHVAP)

!        = (MOLS DRY AIR/HR-CM**2)*(CM**2/CM**3)*(MOLS H20/MOL DRY
!          AIR)*(CM**2)*(CM)*((CAL/MOL H20-C)*C + CAL/MOL H20) =
!          CAL/HR TRANSFERRED BETWEEN THE H20 AND GAS PHASES DUE TO
!          DIFFERENCES IN HUMIDITY

!  IN THE LIMIT AS DZ ---+ 0, EQUATION (3A) BECOMES

!     TL  =  (L/(H*S))*TL  - (H*AV/(CL*H))*(TL - TG)
!       T                Z                                    (3)

!            - (KY*AV/(CL*H))*(YS - Y)*(CVV*TG + DHVAP)

!  AN EXTENSION OF THE PRECEDING SYSTEM IS ALSO SIMULATED IN THE
!  FOLLOWING CODING.  SPECIFICALLY, THE EXITING WATER TEMPERATURE
!  TL(0,T), IS SENSED, COMPARED WITH A SET POINT VALUE, TLSET,
!  AND THE DIFFERENCE (ERROR), E = TL(0,T) - TLSET, IS USED TO
!  ADJUST THE ENTERING AIR FLOW RATE, V, TO MOVE TL(0,T) TO TLSET.
!  THE ADJUSTMENT OF V IS ACHIEVED WITH A PROPORTIONAL-INTEGRAL(PI)
!  CONTROLLER.  THE FOLLOWING DIAGRAM ILLUSTRATES THE CONTROLLER
!  AT THE BOTTOM OF THE HUMIDIFICATION COLUMN

!                     L,TL(ZL,T)   V,Y(ZL,T),
!                                   TG(ZL,T)
!                          .           +
!                          .           .
!                          .           .
!                          +           .
!                          .  .  .  .  . Z = ZL
!                        .              .
!                       .                .
!                      .                  .
!                      .                  .
!                      .                  .
!                      .                  .
!                      .                  .
!                      .                  .
!                      . L,TL       V,Y,TG.
!                      .  .            +  .
!                      .  .            .  .
!                      .  +            .  .
!                      .................... Z+DZ
!                      .      .           .
!                      . H20  .           .
!                      .PHASE . ----+ H20 .
!                      .      .    AIR    .
!                      .      .   PHASE   .
!                      ....................  Z
!                      .  .            +  .
!                      .  .            .  .
!                      .  +            .  .
!                      . H20          AIR .
!                      .STREAM      STREAM.
!                      .(DOWN)       (UP) .
!                      .                  .
!                      .                  .
!                      .                  .
!                      .                  .
!                      .                  .
!                      .                  .
!                       .                .
!                        .              .
!                          .  .  .  .  . Z = 0
!                          .           +             PI
!                          .           .         CONTROLLER
!                          .           .          .........
!            TEMPERATURE .....         .          .       .
!              SENSOR    .   .......... ........... KC,TI .+--- TLSET
!                        .....         .          .       .
!                          .           .          .........
!                          .           .              .
!                          .          ...   ..        .
!                          .           ...... .........
!                          .          ...   ..
!                          .           . CONTROL VALVE
!                          +           .
!                      L,TL(0,T)   V,Y(0,T),
!                                   TG(0,T)

!  THE PURPOSE OF THE SIMULATION IS ESSENTIALLY TO DETERMINE, THE
!  VALUES OF THE CONTROLLER GAIN, KC, AND INTEGRAL TIME, TI, SO
!  THAT THE EXITING LIQUID TEMPERATURE, TL(0,T), IS CONTROLLED CLOSE
!  TO THE SET POINT, TLSET.

!  THE CONTROLLER EQUATIONS ARE

!     V = CV*SQRT(DP)*X                                         (4)

!     X = XSS + KC*(E + (1/TI)INT(E)DT)

!     E = TL(0,T) - TLSET

!     X       CONTROL VALVE STEM POSITION

!     XSS     X AT STEADY STATE

!     KC      CONTROLLER GAIN

!     TI      CONTROLLER INTEGRAL TIME

!     CVDP    PRODUCT OF THE CONTROL VALVE CONSTANT AND SQUARE
!             ROOT OF THE PRESSURE DROP ACROSS THE VALVE

!     TLSET   CONTROLLER SET POINT

!  THE MODEL PARAMETERS AND AUXILIARY CONDITIONS IN ENGLISH UNITS ARE
!  LISTED BELOW, FOLLOWED BY EQUIVALENT VALUES IN METRIC UNITS.  THE
!  CAN BE EXECUTED WITH EITHER SET BY SELECTING THE VALUE OF THE
!  VARIABLE METRIC IN THE PROGRAM (METRIC = 0 FOR ENGLISH UNITS,
!  METRIC = 1 FOR METRIC UNITS)

!  ******************************************************************

!   ENGLISH UNITS

!   H*AV = 50 BTU/(HR-FT**3-F)

!   KY*AV = 4.1 LB MOLS DRY AIR/(HR-FT**3)

!   Z = 8 FT, CL = 18 BTU/(LB MOL-F), DHVAP = 17000 BTU/(LB MOL)

!   CPV = 7.3 BTU/(LB MOL-F), CPA = 7.3 BTU/(LB MOL-F)

!   CVV = CPV - R = 7.3 - 1.987 = 5.3 BTU/(LB MOL-F)

!   CVA = CPA - R = 7.3 - 1.987 = 5.3 BTU/(LB MOL-F)

!   L = 50 LB MOLS/HR, S = 1 FT**2

!   H = 0.5 LB MOLS/FT**3 , G = 0.01 LB MOLS/FT**3

!   P = 10**(7.96681 - 3002.4/(378.4 + TL)) MM HG(TL IS IN F)

!   YS = P/(760 - P) LB MOLS H20/LB MOL DRY AIR

!   TG(0,T) = 110 F, TL(8,T) = 110 F, Y(0,T) = 0.01 LB MOLS H20/
!                                              LB MOL DRY AIR)
!   TG(Z,0) = TL(Z,0) = 110 F, Y(Z,0) = 0.01

!   KC = 0.02 (1/F), TI = 0.1 HR, CV*(DP**0.5) = 50 LB MOLS/HR,

!   XSS = 0.5, TLSET = 90 F

!  ******************************************************************

!   METRIC UNITS

!   H*AV = 0.8015 CAL/(HR-CM**3-C)

!   KY*AV = 0.06573 GM MOLS DRY AIR/(HR-CM**3)

!   Z = 243.8 CM, CL = 18 CAL/(GM MOL-C), DHVAP = 9443.6 CAL/GM MOL

!   CPV = 7.3 CAL/(GM MOL-C), CPA = 7.3 CAL/(GM MOL-C)

!   CVV = CPV - R = 7.3 - 1.987 = 5.3 CAL/(GM MOL-C)

!   CVA = CPA - R = 7.3 - 1.987 = 5.3 CAL/(GM MOL-C)

!   L = 22700 GM MOLS/HR, S = 929.03 CM**2

!   H = 0.008015 GM MOLS/CM**3, G = 0.0001603 GM MOLS/CM**3

!   P = 10**(7.96681 - 3002.4/(378.4 + 1.8*TL + 32)) MM HG
!                                                    (TL IS IN C)

!   YS = P/(760 - P) GM MOLS H20/GM MOL DRY AIR

!   TG(0,T) = 43.33 C, TL(8,T) = 43.33 C, Y(0,T) = 0.01 GM MOL H20/
!                                                  GM MOL DRY AIR)

!   TG(Z,0) = TL(Z,0) = 43.33 C, Y(Z,0) = 0.01

!   KC = 0.036 (1/C), TI = 0.1 HR, CV*(DP**0.5) = 22700 GM MOLS/HR

!   XSS = 0.5, TLSET = 32.22 C

!  ******************************************************************

!  THE CONVERSION FACTORS USED TO GO FROM ONE SET OF UNITS TO THE
!  OTHER ARE

!     1 BTU = 252.2 CAL

!     1 BTU/LB MOL-F = 1 CAL/GM MOL-C

!     1 LB = 454 GM

!     1 LB MOL = 454 GM MOL

!     1 F = 1/1.8 C

!     1 FT = 30.48 CM

!     1 FT**3 = 28316.8 CM**3

!     TF = 1.8*TC + 32

!  THE EXITING GAS COMPOSITION AND TEMPERATURE, Y(8,T) AND TG(8,T),
!  THE EXITING LIQUID TEMPERATURE, TL(0,T), AND THE GAS FLOW RATE,
!  G, ARE OF PARTICULAR INTEREST IN THIS SIMULATION.

!  DOUBLE PRECISION CODING IS USED
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)

!  COMMON AREA TO ESTABLISH LINKAGE WITH OTHER SUBROUTINES
      COMMON /T/T, NSTOP, NORUN/Y/Y(21), EV(21), TL(21), EI/F/YT(21), EVT(21), &
        TLT(21), E/S/YZ(21), EPZ(21), TLZ(21)/C/V, L, G, H, HLAV, KYAV, CL, &
        CPV, CPA, CVV, CVA, ZL, DHVAP, S, P1, P2, P3, P4, P5, P6, P7, TG(21), &
        EP(21), YS(21), KC, TI, CVDP, XSS, TLSET, X, IP, N, METRIC
      DOUBLE PRECISION L, KYAV, KC

!  THE NUMBER OF DIFFERENTIAL EQUATIONS IS IN COMMON /N/ FOR USE IN
!  THE PROBLEM SUBROUTINES

      COMMON /N/NEQN

!  COMMON AREA TO PROVIDE THE INPUT/OUTPUT UNIT NUMBERS TO OTHER
!  SUBROUTINES

!  COMMON/IO/ CONTAINS THE INPUT/OUTPUT UNIT (DEVICE) NUMBERS
      INTEGER NI, NO
      COMMON /IO/NI, NO

!  SELECT ENGLISH (METRIC = 0) OR METRIC(METRIC = 1) UNITS
      IF (NORUN==1) METRIC = 0
      IF (NORUN==2) METRIC = 1

!  N = NUMBER IS THE NUMBER OF SPATIAL NODES:
      N = (NEQN-1)/3
      IF (3*N+1/=NEQN) THEN
        WRITE (NO,90000)
        WRITE (0,90000)
        STOP
      END IF

!  SET THE MODEL PARAMETERS IN THE UNITS SELECTED:

!  ENGLISH UNITS:
      IF (METRIC==0) THEN
        L = 50.0D0
        G = 0.01D0
        H = 0.5D0
        HLAV = 50.0D0
        KYAV = 4.1D0
        CL = 18.0D0
        CPV = 7.3D0
        CPA = 7.3D0
        CVV = 5.3D0
        CVA = 5.3D0
        ZL = 8.0D0
        DHVAP = 17000.0D0
        S = 1.0D0

!  SET THE CONTROLLER PARAMETERS:
        KC = 0.02D0
        TI = 0.1D0
        CVDP = 50.0D0
        XSS = 0.5D0
        TLSET = 90.0D0

!  METRIC UNITS:
      ELSE
        L = 22700.0D0
        G = 0.0001603D0
        H = 0.008015D0
        HLAV = 0.8015D0
        KYAV = 0.06573D0
        CL = 18.0D0
        CPV = 7.3D0
        CPA = 7.3D0
        CVV = 5.3D0
        CVA = 5.3D0
        ZL = 243.8D0
        DHVAP = 9443.6D0
        S = 929.03D0

!  SET THE CONTROLLER PARAMETERS:
        KC = 0.036D0
        TI = 0.1D0
        CVDP = 22700.0D0
        XSS = 0.5D0
        TLSET = 32.22D0
      END IF

!  COMPUTE SOME CONSTANTS FOR USE IN OTHER SUBROUTINES:
      P2 = KYAV/G
      P3 = HLAV/G
      P4 = L/(H*S)
      P5 = HLAV/(CL*H)
      P6 = KYAV/(CL*H)

!  INITIAL CONDITIONS:
      DO I = 1, N
        Y(I) = 0.01D0
        IF (METRIC==0) THEN
          TL(I) = 110.0D0
          TG(I) = 110.0D0
        ELSE
          TL(I) = 43.33D0
          TG(I) = 43.33D0
        END IF
        EV(I) = CVA*TG(I) + Y(I)*(CVV*TG(I)+DHVAP)
      END DO
      EI = 0.0D0

!  INITIALIZE THE MODEL DERIVATIVES:
      CALL DERV
      IP = 0

90000 FORMAT (' ILLEGAL VALUE OF N IN INITIAL. STOPPING')

      RETURN
    END SUBROUTINE INITAL

    SUBROUTINE DERV

      IMPLICIT DOUBLE PRECISION (A-H,O-Z)

!  COMMON AREA TO ESTABLISH LINKAGE WITH OTHER SUBROUTINES
      COMMON /T/T, NSTOP, NORUN/Y/Y(21), EV(21), TL(21), EI/F/YT(21), EVT(21), &
        TLT(21), E/S/YZ(21), EPZ(21), TLZ(21)/C/V, L, G, H, HLAV, KYAV, CL, &
        CPV, CPA, CVV, CVA, ZL, DHVAP, S, P1, P2, P3, P4, P5, P6, P7, TG(21), &
        EP(21), YS(21), KC, TI, CVDP, XSS, TLSET, X, IP, N, METRIC
      DOUBLE PRECISION L, KYAV, KC

!  CONTROLLER EQUATIONS:
      E = TL(1) - TLSET
      X = XSS + KC*(E+(1.0D0/TI)*EI)
      IF (X<0.0D0) X = 0.0D0
      IF (X>1.0D0) X = 1.0D0
      V = CVDP*X
      P1 = V/(G*S)

!  MODEL ALGEBRA:
      DO I = 1, N
        TG(I) = (EV(I)-Y(I)*DHVAP)/(CVA+Y(I)*CVV)
        EP(I) = CPA*TG(I) + Y(I)*(CPV*TG(I)+DHVAP)
        IF (METRIC==0) THEN
          P = 10.0D0**(7.96681D0-3002.4D0/(378.4D0+TL(I)))
        ELSE
          P = 10.0D0**(7.96681D0-3002.4D0/(378.4D0+1.8D0*TL(I)+32.0D0))
        END IF
        YS(I) = P/(760.0D0-P)
      END DO

!  BOUNDARY CONDITIONS:
      Y(1) = 0.01D0
      IF (METRIC==0) THEN
        TL(N) = 110.0D0
        TG(1) = 110.0D0
      ELSE
        TL(N) = 43.330D0
        TG(1) = 43.330D0
      END IF
      EP(1) = CPA*TG(1) + Y(1)*(CPV*TG(1)+DHVAP)

!  CONSTRAINTS FOR THE DEPENDENT VARIABLES AND ASSOCIATED
!  VARIABLES:
      DO I = 1, N
        IF (Y(I)<0.0D0) Y(I) = 0.0D0
        IF (YS(I)<0.0D0) YS(I) = 0.0D0
      END DO

!  SPATIAL DERIVATIVES:
      CALL DSS020(0.0D0,ZL,N,Y,YZ,1.0D0)
      CALL DSS020(0.0D0,ZL,N,EP,EPZ,1.0D0)
      CALL DSS020(0.0D0,ZL,N,TL,TLZ,-1.0D0)

!  PARTIAL DIFFERENTIAL EQUATIONS:
      DO I = 1, N
        YT(I) = -P1*YZ(I) + P2*(YS(I)-Y(I))
        P7 = CVV*TG(I) + DHVAP
        EVT(I) = -P1*EPZ(I) + P3*(TL(I)-TG(I)) + P2*(YS(I)-Y(I))*P7
        TLT(I) = P4*TLZ(I) - P5*(TL(I)-TG(I)) - P6*(YS(I)-Y(I))*P7
      END DO

!  ZERO THE TIME DERIVATIVES TO MAINTAIN THE BOUNDARY CONDITIONS:
      YT(1) = 0.0D0
      EVT(1) = 0.0D0
      TLT(N) = 0.0D0
      RETURN
    END SUBROUTINE DERV

    SUBROUTINE PRINT(NO)

!  COMMON AREA TO ESTABLISH LINKAGE WITH OTHER SUBROUTINES
      COMMON /T/T, NSTOP, NORUN/Y/Y(21), EV(21), TL(21), EI/F/YT(21), EVT(21), &
        TLT(21), E/S/YZ(21), EPZ(21), TLZ(21)/C/V, L, G, H, HLAV, KYAV, CL, &
        CPV, CPA, CVV, CVA, ZL, DHVAP, S, P1, P2, P3, P4, P5, P6, P7, TG(21), &
        EP(21), YS(21), KC, TI, CVDP, XSS, TLSET, X, IP, N, METRIC

!  DOUBLE PRECISION CODING IS USED.  INDIVIDUAL VARIABLES AND ARRAYS
!  ARE TYPED BECAUSE SUBROUTINES JMAP AND EIGEN ARE CODED IN SINGLE
!  PRECISION, AND THEREFORE THEY REQUIRE THE USE OF SINGLE PRECISION
!  INPUTS
      DOUBLE PRECISION T, Y, EV, TL, EI, YT, EVT, TLT, E, YZ, EPZ, TLZ, V, L, &
        G, H, HLAV, KYAV, CL, CPV, CPA, CVV, CVA, ZL, DHVAP, S, P1, P2, P3, &
        P4, P5, P6, P7, TG, EP, YS, KC, TI, CVDP, XSS, TLSET, X

!  DIMENSION THE ARRAYS FOR THE PLOTTED SOLUTION, INDIVIDUAL TERMS
!  IN THE PDES:
      PARAMETER (NPT=101)
      DOUBLE PRECISION TLP(NPT), TGP(NPT), YP(NPT), VP(NPT), XP(NPT), &
        ERP(NPT), TP(NPT), TEP(21)
!     DOUBLE PRECISION TGLP(2, 21)

!  DIMENSION THE ARRAYS FOR SUBROUTINE JMAP AND EIGEN (SOME OF
!  WHICH ARE REAL OR SINGLE PRECISION).  NE IS THE TOTAL NUMBER OF
!  ODES(THE TOTAL LENGTH OF THE VECTORS IN COMMON/Y/ AND /F/)
      PARAMETER (NE=64)
      DOUBLE PRECISION A(NE,NE), WR(NE), WI(NE), Z(NE,NE), RW(NE)
      DOUBLE PRECISION SV(NE), SVOLD(NE), F(NE), FOLD(NE)
      INTEGER IW(NE)

!  THIS SECTION OF SUBROUTINE PRINT

!     (1)  CALLS SUBROUTINE JMAP TO MAP THE JACOBIAN MATRIX OF
!          NE-ODE SYSTEM.

!     (2)  CALLS A SERIES OF EISPACK ROUTINES VIA SUBROUTINE EIGEN
!          TO COMPUTE THE TEMPORAL EIGENVALUES OF THE ODE SYSTEM,
!          AND OPTIONALLY, THE ASSOCIATED EIGENVECTORS.

!  THE FOLLOWING EQUIVALENCE FACILITATES TRANSFER OF THE DEPENDENT
!  VARIABLE VECTORS IN /Y/ TO ARRAY SV, AND THE DERIVATIVE VECTOR
!  IN /F/ TO ARRAY F FOR USE IN THE SUBSEQUENT CALL TO SUBROUTINE
!  JMAP
      EQUIVALENCE (Y(1),SV(1)), (YT(1),F(1))

!  MONITOR OUTPUT
      WRITE (NO,*) NORUN, T

!  PRINT A DETAILED SOLUTION (THE INDIVIDUAL TERMS IN THE PDES AND
!  THE DEPENDENT VARIABLES) FOR IP = 1, 51, NPT CALLS TO SUBROUTINE
!  PRINT.  FIRST, UPDATE ALL OF THE MODEL CALCULATIONS BY A CALL
!  TO SUBROUTINE DERV(THE INITIAL-VALUE INTEGRATION THROUGH COMMON
!  /Y/ AND /F/ IS DONE CORRECTLY WITHOUT THIS UPDATE, BUT SOME OF THE
!  MODEL VARIABLES MAY BE ONE INTEGRATION TIME STEP BEHIND IN TIME,
!  T, WHEN THEY ARE PRINTED OUT IN SUBROUTINE PRINT IF THE FOLLOWING
!  CALL TO SUBROUTINE DERV IS NOT EXECUTED).  ALSO, STORE THE DEPEN-
!  DENT VARIABLE AND DERIVATIVE VECTORS FOR JACOBIAN MATRIX MAPPING
!  AND CALCULATION OF EIGENVALUES
      IP = IP + 1
      IF ((IP/=1) .AND. (IP/=51) .AND. (IP/=NPT)) GO TO 10
      CALL DERV

!  ***************************************************************
!  GAS-PHASE MATERIAL BALANCE
!  ************************************************************(1)

!     -(V/(G*S))*Y
!                 Z
      DO I = 1, N
        TEP(I) = -P1*YZ(I)
      END DO
      N2 = 2
      WRITE (NO,90000) T, (TEP(I),I=1,N,N2)
90000 FORMAT ('1',' T = ',F7.3,//,' -(V/(G*S))*YZ           ',5D11.3,/,14X, &
        6D11.3,//)

!  ************************************************************(2)

!     (KY*AV/G)*(YS - Y)
      DO I = 1, N
        TEP(I) = P3*(YS(I)-Y(I))
      END DO
      WRITE (NO,90001) (TEP(I),I=1,N,N2)
90001 FORMAT (' (KA*AV/G)*(YS - Y)      ',5D11.3,/,14X,6D11.3,//)

!  ************************************************************(3)

!     Y
!      T
      WRITE (NO,90002) (YT(I),I=1,N,N2)
90002 FORMAT (' YT                      ',5D11.3,/,14X,6D11.3,//)

!  ************************************************************(4)

!     Y
      WRITE (NO,90003) (Y(I),I=1,N,N2)
90003 FORMAT (' Y                      ',5D11.3,/,14X,6D11.3,//)

!  ***************************************************************
!  EQUILIBRIUM RELATIONSHIP
!  ************************************************************(5)

!     YS
      WRITE (NO,90004) (YS(I),I=1,N,N2)
90004 FORMAT (' YS                     ',5D11.3,/,14X,6D11.3,//)

!  ***************************************************************
!  GAS-PHASE ENERGY BALANCE
!  ************************************************************(6)

!     -(V/(G*S))*EP  = -(V/(G*S))*(CPA*TG + Y*(CPV*TG + DHVAP))
!                  Z                                           Z
      DO I = 1, N
        TEP(I) = -P1*EPZ(I)
      END DO
      WRITE (NO,90005) (TEP(I),I=1,N,N2)
90005 FORMAT (' -(V/(G*S))*EPZ         ',5D11.3,/,14X,6D11.3,//)

!  ************************************************************(7)

!     (H*AV/G)*(TL - TG)
      DO I = 1, N
        TEP(I) = P3*(TL(I)-TG(I))
      END DO
      WRITE (NO,90006) (TEP(I),I=1,N,N2)
90006 FORMAT (' -(H*AV/G)*(TL - TG)    ',5D11.3,/,14X,6D11.3,//)

!  ************************************************************(8)

!     (KY*AV/G)*(YS - Y)*(CVV*TG + DHVAP)
      DO I = 1, N
        TEP(I) = -P2*(YS(I)-Y(I))*(CVV*TG(I)+DHVAP)
      END DO
      WRITE (NO,90007) (TEP(I),I=1,N,N2)
90007 FORMAT (' (KY*AV/G)*(YS - Y)*(...)',5D11.3,/,14X,6D11.3,//)

!  ************************************************************(9)

!     EV  = (CVA*TG + Y*(CVV*TG + DHVAP))
!       T                                T
      WRITE (NO,90008) (EVT(I),I=1,N,N2)
90008 FORMAT (' EVT                    ',5D11.3,/,14X,6D11.3,//)
!  ************************************************************(10)

!     TG
      WRITE (NO,90009) (TG(I),I=1,N,N2)
90009 FORMAT (' TG                     ',5D11.3,/,14X,6D11.3,//)

!  ****************************************************************
!  LIQUID-PHASE ENERGY BALANCE
!  ************************************************************(11)

!     -(L/(H*S))*TL
!                  Z
      DO I = 1, N
        TEP(I) = P4*TLZ(I)
      END DO
      WRITE (NO,90010) (TEP(I),I=1,N,N2)
90010 FORMAT (' -(L/(H*S))*TLZ         ',5D11.3,/,14X,6D11.3,//)

!  ************************************************************(12)

!     -(H*AV/(CL*H))*(TL - TG)
      DO I = 1, N
        TEP(I) = -P5*(TL(I)-TG(I))
      END DO
      WRITE (NO,90011) (TEP(I),I=1,N,N2)
90011 FORMAT (' -(H*AV/(CL*H))*(TL-TG) ',5D11.3,/,14X,6D11.3,//)

!  ************************************************************(13)

!     -(KY*AV/(CL*H))*(YS - Y)*(CVV*TG + DHVAP)
      DO I = 1, N
        TEP(I) = -P6*(YS(I)-Y(I))*(CVV*TG(I)+DHVAP)
      END DO
      WRITE (NO,90012) (TEP(I),I=1,N,N2)
90012 FORMAT (' -(KY*AV/(CL*H))*(YS-Y)..',5D11.3,/,14X,6D11.3,//)

!  ************************************************************(14)

!     TL
!       T
      WRITE (NO,90013) (TLT(I),I=1,N,N2)
90013 FORMAT (' TLT                    ',5D11.3,/,14X,6D11.3,//)

!  ************************************************************(15)

!     TL
      WRITE (NO,90014) (TL(I),I=1,N,N2)
90014 FORMAT (' TL                     ',5D11.3,/,14X,6D11.3,//)

!  ****************************************************************

!  MAP THE JACOBIAN MATRIX OF THE NE ODES DEFINED IN SUBROUTINE
!  DERV, AND COMPUTE THEIR TEMPORAL EIGENVALUES.  NOTE THAT ARRAY
!  A CONTAINING THE NUMERICAL JACOBIAN MATRIX ON OUTPUT FROM JMAP
!  IS SINGLE PRECISION, I.E., IT IS NOT DECLARED IN THE PRECEDING
!  DOUBLE PRECISION STATEMENTS.  FURTHER EXPLANATION OF THIS POINT
!  IS GIVEN IN THE COMMENTS BEFORE THE FOLLOWING CALL TO SUBROUTINE
!  EIGEN
      CALL JMAP(NE,A,SV,SVOLD,F,FOLD)

!  SUBROUTINE EIGEN (CALLED BELOW) USES A SERIES OF EISPACK ROU-
!  TINES TO COMPUTE THE TEMPORAL EIGENVALUES, AND OPTIONALLY, THE
!  EIGENVECTORS OF THE NE-ODE SYSTEM JACOBIAN MATRIX.  NOTE THAT
!  ALL OF THE ARRAYS GOING INTO EIGEN (A, WR, WI, Z, RW, IW) ARE
!  SINGLE PRECISION.  THIS WAS DONE TO: (1) SAVE MEMORY SINCE HIGH
!  PRECISION IN THE CALCULATION OF EIGENVALUES IS GENERALLY NOT
!  REQUIRED AND (2) ALLOW THE USE OF THE SINGLE PRECISION EISPACK
!  ROUTINES CALLED BY EIGEN (THE PRECEDING COMMENTS SHOW THE AGE
!  OF SOME OF THIS CODING SINCE 'SAVING MEMORY' IS NO LONGER AN
!  ISSUE; ALSO THE SINGLE PRECISION EISPACK ROUTINES SHOULD BE
!  CONVERTED TO DOUBLE PRECISION TO AVOID THE MIXTURE OF SINGLE AND
!  DOUBLE PRECISION VARIABLES, I.E., AN IMPLICIT DOUBLE PRECISION
!  STATEMENT COULD THEN BE USED AS IN SUBROUTINES INITAL AND DERV)
      CALL EIGEN(NE,A,WR,WI,Z,RW,IW)

!  PLOT TG AND TL VS Z
      DO I = 1, N
!        TGLP(1, I) = TG(I)
!        TGLP(2, I) = TL(I)
        TEP(I) = ZL*DBLE(I-1)/DBLE(N-1)
      END DO
!     CALL TPLOTS(2,N,TEP,TGLP)
      WRITE (NO,90015) T
90015 FORMAT (' ',/,' 1 - TG(Z,T), 2 - TL(Z,T) VS Z, T = ',F7.3)

!  STORE THE NUMERICAL SOLUTION FOR PLOTTING (VS T)
10    TLP(IP) = TL(1)
      TGP(IP) = TG(N)
      YP(IP) = Y(N)
      VP(IP) = V
      XP(IP) = X
      ERP(IP) = E
      TP(IP) = T
!     WRITE TP, TLP, VP to a file for importing into Matlab.
      WRITE (7,FMT=90016) TP(IP), TLP(IP), VP(IP)
90016 FORMAT (3D12.4)

!  PLOT THE SOLUTION
      IF (IP<NPT) RETURN
      CALL TPLOTS(1,IP,TP,TLP)
      WRITE (NO,90017)
90017 FORMAT (' ',/,' TL(0,T) VS T                 ')
      CALL TPLOTS(1,IP,TP,TGP)
      WRITE (NO,90018)
90018 FORMAT (' ',/,' TG(8,T) VS T                 ')
      CALL TPLOTS(1,IP,TP,YP)
      WRITE (NO,90019)
90019 FORMAT (' ',/,' Y(8,T) VS T                  ')
      CALL TPLOTS(1,IP,TP,VP)
      WRITE (NO,90020)
90020 FORMAT (' ',/,' V(T) VS T                    ')
      CALL TPLOTS(1,IP,TP,XP)
      WRITE (NO,90021)
90021 FORMAT (' ',/,' X(T) VS T                    ')
      CALL TPLOTS(1,IP,TP,ERP)
      WRITE (NO,90022)
90022 FORMAT (' ',/,' E(T) VS T                    ')
      RETURN
    END SUBROUTINE PRINT

    SUBROUTINE YMIX(YVAR)

!     TRANSFORM THE SOLUTION FROM EQUATIONWISE ORDERING
!     TO NODEWISE ORDERING

!     DOUBLE PRECISION CODING IS USED
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)

      DIMENSION YVAR(*)

!     COMMON AREA TO ESTABLISH LINKAGE WITH OTHER SUBROUTINES
      COMMON /T/T, NSTOP, NORUN/Y/Y(21), EV(21), TL(21), EI/F/YT(21), EVT(21), &
        TLT(21), E/S/YZ(21), EPZ(21), TLZ(21)/C/V, L, G, H, HLAV, KYAV, CL, &
        CPV, CPA, CVV, CVA, ZL, DHVAP, S, P1, P2, P3, P4, P5, P6, P7, TG(21), &
        EP(21), YS(21), KC, TI, CVDP, XSS, TLSET, X, IP, N, METRIC
      DOUBLE PRECISION L, KYAV, KC

      INDEX = 2
      DO I = 1, N
        YVAR(INDEX) = Y(I)
        INDEX = INDEX + 3
      END DO
      INDEX = 3
      DO I = 1, N
        YVAR(INDEX) = EV(I)
        INDEX = INDEX + 3
      END DO
      INDEX = 4
      DO I = 1, N
        YVAR(INDEX) = TL(I)
        INDEX = INDEX + 3
      END DO
      YVAR(1) = EI

      RETURN
    END SUBROUTINE YMIX

    SUBROUTINE DYMIX(DYVAR)

!     TRANSFORM THE DERIVATIVE FROM EQUATIONWISE ORDERING
!     TO NODEWISE ORDERING

!     DOUBLE PRECISION CODING IS USED
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)

      DIMENSION DYVAR(*)

!     COMMON AREA TO ESTABLISH LINKAGE WITH OTHER SUBROUTINES
      COMMON /T/T, NSTOP, NORUN/Y/Y(21), EV(21), TL(21), EI/F/YT(21), EVT(21), &
        TLT(21), E/S/YZ(21), EPZ(21), TLZ(21)/C/V, L, G, H, HLAV, KYAV, CL, &
        CPV, CPA, CVV, CVA, ZL, DHVAP, S, P1, P2, P3, P4, P5, P6, P7, TG(21), &
        EP(21), YS(21), KC, TI, CVDP, XSS, TLSET, X, IP, N, METRIC
      DOUBLE PRECISION L, KYAV, KC

      INDEX = 2
      DO I = 1, N
        DYVAR(INDEX) = YT(I)
        INDEX = INDEX + 3
      END DO
      INDEX = 3
      DO I = 1, N
        DYVAR(INDEX) = EVT(I)
        INDEX = INDEX + 3
      END DO
      INDEX = 4
      DO I = 1, N
        DYVAR(INDEX) = TLT(I)
        INDEX = INDEX + 3
      END DO
      DYVAR(1) = E

      RETURN
    END SUBROUTINE DYMIX

    SUBROUTINE YUNMIX(YVAR)

!     TRANSFORM THE SOLUTION FROM NODEWISE ORDERING
!     TO EQUATIONWISE ORDERING

!     DOUBLE PRECISION CODING IS USED
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)

      DIMENSION YVAR(*)

!     COMMON AREA TO ESTABLISH LINKAGE WITH OTHER SUBROUTINES
      COMMON /T/T, NSTOP, NORUN/Y/Y(21), EV(21), TL(21), EI/F/YT(21), EVT(21), &
        TLT(21), E/S/YZ(21), EPZ(21), TLZ(21)/C/V, L, G, H, HLAV, KYAV, CL, &
        CPV, CPA, CVV, CVA, ZL, DHVAP, S, P1, P2, P3, P4, P5, P6, P7, TG(21), &
        EP(21), YS(21), KC, TI, CVDP, XSS, TLSET, X, IP, N, METRIC
      DOUBLE PRECISION L, KYAV, KC

      INDEX = 2
      DO I = 1, N
        Y(I) = YVAR(INDEX)
        INDEX = INDEX + 3
      END DO
      INDEX = 3
      DO I = 1, N
        EV(I) = YVAR(INDEX)
        INDEX = INDEX + 3
      END DO
      INDEX = 4
      DO I = 1, N
        TL(I) = YVAR(INDEX)
        INDEX = INDEX + 3
      END DO
      EI = YVAR(1)

      RETURN
    END SUBROUTINE YUNMIX

    SUBROUTINE DSTRUT(DFN,N,T,Y,F,FP,IA,IADIM,JA,JADIM,EMIN,IMIN,JMIN,IER)

!     APPROXIMATE THE JACOBIAN STRUCTURE USING DIFFERENCES

      IMPLICIT NONE
      INTEGER N, IA, IADIM, JA, JADIM, IMIN, JMIN, IER, I, J, K, JP1
      DOUBLE PRECISION T, Y, F, FP, EMIN, YJ, YPJ, RJ, AIJ

      DIMENSION Y(N), F(N), FP(N), IA(IADIM), JA(JADIM)
      EXTERNAL DFN

      IER = 0
      IMIN = 0
      JMIN = 0
      CALL DFN(N,T,Y,F)
      IA(1) = 1
      K = 1
      DO J = 1, N
        YJ = Y(J)
        YPJ = YJ*1.01D0
        RJ = ABS(YPJ-YJ)
        IF (RJ==0.0D0) IER = 131
        IF (RJ==0.0D0) GO TO 20
        Y(J) = YPJ
        CALL DFN(N,T,Y,FP)
        DO I = 1, N
          AIJ = ABS(FP(I)-F(I))/RJ
          IF ((AIJ<=EMIN) .AND. (I/=J)) GO TO 10
          IF (K>JADIM) IER = 130
          IF (K>JADIM) GO TO 20
          JMIN = K
          JA(K) = I
          K = K + 1
10      END DO
        JP1 = J + 1
        IF (JP1>IADIM) IER = 129
        IF (JP1>IADIM) GO TO 20
        IMIN = JP1
        IA(JP1) = K
        Y(J) = YJ
      END DO
      GO TO 30
20    CONTINUE
      WRITE (6,90000)
      WRITE (0,90000)
30    CONTINUE
90000 FORMAT (' Error occurred in DSTRUT.')

      RETURN
    END SUBROUTINE DSTRUT

    SUBROUTINE DSTPIC(N,IA,JA,MAXCL)

!     PRINT A PICTURE OF THE JACOBIAN STRUCTURE

      IMPLICIT NONE
      INTEGER N, IA, JA, MAXCL, KA, MAXCOL, NUMBLK, IBLOCK, IDONE, IBEGIN, &
        ILEFT, IDO, ICEASE, IROW, KAFILL, ILOWER, IHI, IJK, IDID, ICOL2, K, &
        ICOL, I, INUM, ILO, JFILL

      DIMENSION IA(1), JA(1), KA(130)

      WRITE (6,90000)
      MAXCOL = 130
!*
      MAXCOL = MAXCL
      IF (MAXCL<=0) MAXCOL = 130
      IF (MAXCL>130) MAXCOL = 130
!*
      NUMBLK = (N-1)/MAXCOL + 1
      IF (NUMBLK<1) GO TO 20
      IF (NUMBLK>100) GO TO 20
      IDONE = 0
      DO IBLOCK = 1, NUMBLK
        WRITE (6,90001) IBLOCK
        IBEGIN = IDONE + 1
        ILEFT = N - IDONE
        IDO = ILEFT
        IF (IDO>MAXCOL) IDO = MAXCOL
        ICEASE = IDONE + IDO
        DO IROW = 1, N
          KAFILL = ICEASE - IBEGIN + 1
          DO JFILL = 1, KAFILL
            KA(JFILL) = 0
          END DO
          DO ICOL2 = IBEGIN, ICEASE
            ICOL = ICOL2
            IF (IBLOCK>1) ICOL = ICOL2 - IBEGIN + 1
            ILO = IA(ICOL2)
            ILOWER = ILO - 1
            IHI = IA(ICOL2+1)
            INUM = IHI - ILO
            DO I = 1, INUM
              IJK = JA(ILOWER+I)
              IF (IJK==IROW) KA(ICOL) = 1
              IF (IJK==IROW) GO TO 10
            END DO
10          CONTINUE
          END DO
          IDID = IDO
          WRITE (6,90002) (KA(K),K=1,IDID)
        END DO
        IDONE = IDONE + IDID
      END DO
20    CONTINUE

90000 FORMAT (' THE JACOBIAN HAS THE FOLLOWING FORM.')
90001 FORMAT (' BLOCK NUMBER ',I6,' OF THE JACOBIAN ',//)
90002 FORMAT ((2X,130I1))

      RETURN
    END SUBROUTINE DSTPIC

    SUBROUTINE JSTRUT(YV,NEQN,T,FCN)

!     DETERMINE AND PRINT THE JACOBIAN PICTURE

      IMPLICIT NONE
      INTEGER NEQN, N, IADIM, JADIM, IA, JA, I, MAXCL, IER, IMIN, JMIN
      DOUBLE PRECISION YV, T, Y, F, FP, EMIN

      PARAMETER (N=64,IADIM=N+1,JADIM=N*N)

      DIMENSION YV(*)
      DIMENSION Y(N), F(N), FP(N)
      DIMENSION IA(IADIM), JA(JADIM)

      EXTERNAL FCN

      Y(1:NEQN) = YV(1:NEQN)
      DO I = 1, NEQN
        IF (Y(I)==0.0D0) Y(I) = 1.0D-10
      END DO
      EMIN = 0.0D0
      MAXCL = 130

      CALL DSTRUT(FCN,NEQN,T,Y,F,FP,IA,IADIM,JA,JADIM,EMIN,IMIN,JMIN,IER)
      CALL DSTPIC(NEQN,IA,JA,MAXCL)

      RETURN
    END SUBROUTINE JSTRUT

    SUBROUTINE ZDSS020(XL,XU,N,U,UX,V)

!  SUBROUTINE DSS020 IS AN APPLICATION OF FOURTH-ORDER DIRECTIONAL
!  DIFFERENCING IN THE NUMERICAL METHOD OF LINES.  IT IS INTENDED
!  SPECIFICALLY FOR THE ANALYSIS OF CONVECTIVE SYSTEMS MODELLED BY
!  FIRST-ORDER HYPERBOLIC PARTIAL DIFFERENTIAL EQUATIONS AS DIS-
!  CUSSED IN SUBROUTINE DSS012.  THE COEFFICIENTS OF THE FINITE
!  DIFFERENCE APPROXIMATIONS USED HEREIN ARE TAKEN FROM BICKLEY, W.
!  G., FORMULAE FOR NUMERICAL DIFFERENTIATION, THE MATHEMATICAL
!  GAZETTE, PP. 19-27, 1941, N = 4, M = 1, P = 0, 1, 2, 3, 4.  THE
!  IMPLEMENTATION IS THE **FIVE-POINT BIASED UPWIND FORMULA** OF
!  M. B. CARVER AND H. W. HINDS, THE METHOD OF LINES AND THE
!  ADVECTION EQUATION, SIMULATION, VOL. 31, NO. 2, PP. 59-69,
!  AUGUST, 1978

!  TYPE SELECTED REAL VARIABLES AS DOUBLE PRECISION
      DOUBLE PRECISION DX, R4FDX, U, UX, V, XL, XU, DBLE

      DIMENSION U(N), UX(N)

!  COMPUTE THE COMMON FACTOR FOR EACH FINITE DIFFERENCE APPROXIMATION
!  CONTAINING THE SPATIAL INCREMENT, THEN SELECT THE FINITE DIFFER-
!  ENCE APPROXIMATION DEPENDING ON THE SIGN OF V (SIXTH ARGUMENT).
      DX = (XU-XL)/DBLE(N-1)
      R4FDX = 1.D+00/(12.D+00*DX)
      IF (V<0.D+00) GO TO 10

!     (1)  FINITE DIFFERENCE APPROXIMATION FOR POSITIVE V
      UX(1) = R4FDX*(-25.D+00*U(1)+48.D+00*U(2)-36.D+00*U(3)+16.D+00*U(4)- &
        3.D+00*U(5))
      UX(2) = R4FDX*(-3.D+00*U(1)-10.D+00*U(2)+18.D+00*U(3)-6.D+00*U(4)+1.D+00 &
        *U(5))
      UX(3) = R4FDX*(+1.D+00*U(1)-8.D+00*U(2)+0.D+00*U(3)+8.D+00*U(4)-1.D+00*U &
        (5))
      NM1 = N - 1
      DO I = 4, NM1
        UX(I) = R4FDX*(-1.D+00*U(I-3)+6.D+00*U(I-2)-18.D+00*U(I-1)+10.D+00*U(I &
          )+3.D+00*U(I+1))
      END DO
      UX(N) = R4FDX*(+3.D+00*U(N-4)-16.D+00*U(N-3)+36.D+00*U(N-2)-48.D+00*U(N- &
        1)+25.D+00*U(N))
      RETURN

!     (2)  FINITE DIFFERENCE APPROXIMATION FOR NEGATIVE V
10    UX(1) = R4FDX*(-25.D+00*U(1)+48.D+00*U(2)-36.D+00*U(3)+16.D+00*U(4)- &
        3.D+00*U(5))
      NM3 = N - 3
      DO I = 2, NM3
        UX(I) = R4FDX*(-3.D+00*U(I-1)-10.D+00*U(I)+18.D+00*U(I+1)-6.D+00*U(I+2 &
          )+1.D+00*U(I+3))
      END DO
      UX(N-2) = R4FDX*(+1.D+00*U(N-4)-8.D+00*U(N-3)+0.D+00*U(N-2)+8.D+00*U(N-1 &
        )-1.D+00*U(N))
      UX(N-1) = R4FDX*(-1.D+00*U(N-4)+6.D+00*U(N-3)-18.D+00*U(N-2)+10.D+00*U(N &
        -1)+3.D+00*U(N))
      UX(N) = R4FDX*(+3.D+00*U(N-4)-16.D+00*U(N-3)+36.D+00*U(N-2)-48.D+00*U(N- &
        1)+25.D+00*U(N))
      RETURN
    END SUBROUTINE ZDSS020

