function [t,y] = demohum21
% Matlab script to plot the solution for the packed
% humidification column problem

temp = load('demohum21plot.dat','-ascii');
tp  = temp(:,1);
tlp = temp(:,2);
vp  = temp(:,3);
tgp = temp(:,4);
yp  = temp(:,5);
xp  = temp(:,6);
erp = temp(:,7);

figure
plot(tp,tgp)
title('Outlet Gas Temperature')
xlabel('t (hr)');
ylabel('TG(8,t) (C)');

figure
plot(tp,yp)
title('Mole Ratio')
xlabel('t (hr)');
ylabel('Y(8,t)');

figure
plot(tp,xp)
title('Control Valve Stem Position')
xlabel('t (hr)');
ylabel('X(t)');

figure
plot(tp,erp)
title('Inlet Temperature - TLSET')
xlabel('t (hr)');
ylabel('E(t)');

figure
plot(tp,tlp)
title('Inlet Temperature')
xlabel('t (hr)');
ylabel('TL(0,t) (C)');

figure
plot(tp,vp)
title('Velocity')
xlabel('t (hr)');
ylabel('V(t) (mols/hr)')
