function [t,y] = demop11
% Matlab script to plot the solution for the polymer problem

temp = load('demop11plot.dat','-ascii');
y = temp(:,1:end);
temp2 = load('demop11mesh.dat','-ascii');
z = temp2(:,1);
tpa = temp2(:,2);
y1 = y(:,1);
y2 = y(:,2);
y3 = y(:,3);
y4 = y(:,4);
y5 = y(:,5);
y6 = y(:,6);

figure
plot(z(1:20),y1(1:20),'.-',z(1:20),y2(1:20),'.-',z(1:20),y3(1:20),'.-',z(1:20),y4(1:20),'.-',z(1:20),y5(1:20),'.-')
%title('Polymer Problem: Profiles for t = 0:0.1:0.5')
axis([0,10,-10,450])

figure
plot(z,y6,'r.',z,tpa,'k')
%title('Polymer Problem: Steady state at t = 40.')
legend('Computed','Analytical')
axis([0,z(end),0,410])
