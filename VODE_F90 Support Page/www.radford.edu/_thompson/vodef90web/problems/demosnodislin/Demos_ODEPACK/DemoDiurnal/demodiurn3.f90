! DVODE_F90 demonstration program
! 2d diurnal kinetics problem
! Similar to the Sundials problems cvkvrf and cvkx
! Last change: 08/13/05

MODULE demo_diurn

  IMPLICIT NONE

! Define the numbers of points in the spatial grid and
! the maximum number of odes.
  INTEGER, PARAMETER :: MX=5, MZ=5, NEQMAX=2*MX*MZ
! ______________________________________________________________

! Problem Description

! An ODE system is generated from the following 2-species diurnal
! kinetics advection-diffusion PDE system in 2 space dimensions
! which represents a model of ozone production in the stratosphere:

! dc(i)/dt = Kh*(d/dx)**2 c(i) + V*dc(i)/dx + (d/dz)(Kv(z)*dc(i)/dz)
!                 + Ri(c1,c2,t)      for i = 1,2,   where
!   R1(c1,c2,t) = -q1*c1*c3 - q2*c1*c2 + 2*q3(t)*c3 + q4(t)*c2 ,
!   R2(c1,c2,t) =  q1*c1*c3 - q2*c1*c2 - q4(t)*c2 ,
!   Kv(z) = Kv0*exp(z/5) ,

! Kh, V, Kv0, q1, q2, and c3 are constants, and q3(t) and q4(t)
! vary diurnally. The species are oxygen singlet and ozone.
! The problem is posed on the square
!   0 .le. x .le. 20, 30 .le. z .le. 50 (all in km),
! with homogeneous Neumann boundary conditions, and for time t
!   0 <= t <= 518,400 sec (6 days).

! The PDE system is treated by biased upwind differences or by
! central differences on a uniform mesh.

! This version has an advection term in the x-direction added
! and is a modification of one of the problems in the reference
! below.

! Of historical interest is the fact that this problem and
! similar ones led to the development of the EPISODE family
! of ODE solvers of which the original f77 VODE ODE solver
! is a member.

! Reference:
! G.D. Byrne and A.C. Hindmarsh, Stiff ODE Solvers: A Review
! of Current and Coming Attractions, Journal of Computational
! Physics, Vol. 70, No. 1, May 1987, pp. 1-62.
! ______________________________________________________________

!     Declare the problem parameters.
      INTEGER mx2
      DOUBLE PRECISION q1,q2,q3,q4,a3,a4,freq,c3,dx,dz,cox, &
        coz,acox,dch,dcv0,vel,ccsol,ccder
      LOGICAL upwinding, jcp_parms, jcp_ics, drop_adv
      DIMENSION ccsol(NEQMAX),ccder(NEQMAX)

CONTAINS

      SUBROUTINE FCN(NEQ, T, CC, CDOT)

!     Direct the calculation of the time derivatives.

      IMPLICIT NONE
      INTEGER NEQ
      DOUBLE PRECISION T,CC,CDOT
      DIMENSION CC(NEQ), CDOT(NEQ)

!     Load the solution into local storage.
      ccsol(1:NEQ) = CC(1:NEQ)

!     Calculate the time derivatives.
      CALL DERV(T)

!     Load the time derivatives into the integrator array.
      CDOT(1:NEQ) = ccder(1:NEQ)
!
      RETURN
      END SUBROUTINE FCN

      SUBROUTINE inital(yinit)

!     Define the initial solution.

      IMPLICIT NONE
      INTEGER i,j,k,iz,iy,neq
      DOUBLE PRECISION yinit,z,z1,cz,x,x1,cx,cxcz,halfday,pi
      DIMENSION yinit(*)

      if (jcp_parms) then
!        Use the JCP parameter values for dch and dcv0.
         dch = 1.0d-2
         dcv0 = 1.0d-5
      else
!        Use the cvkryf values for dch and dcv0.
         dcv0 = 1.0d-8
         dch = 4.0d-6
      end if

      q1 = 1.63d-16
      q2 = 4.66d-16
      a3 = 22.62d0
      a4 = 7.601d0
      c3 = 3.7d16
      pi = 3.1415926535898d0
      halfday = 4.32d4
      vel = 1.0d-3
      mx2 = mx * 2
!     midy = mz * mx - mx
      dx = 20.0d0 / real(mx - 1)
      dz = 20.0d0 / real(mz - 1)
      cox = dch / dx**2
      acox = vel /(2.0d0 * dx)
      coz = 2.0d0 / dz**2
      freq = pi / halfday

      DO k = 1, mz

         z = 30.0d0 + real(k - 1) * dz
         z1 = 0.1d0 * (z - 40.0d0)
         z1 = z1**2

         if (jcp_ics) then
!           Use the JCP initial condition.
            cz = 1.0d0 - z1 + 0.5d0 * z1**2
         else
!           Use the Wittman initial condition.
            cz = 0.75d0 + 0.25d0 * tanh(10.0D0*z-400.0D0)
         end if

         iz = 2 * mx * (k - 1)

         DO j = 1, mx
            x = real(j - 1) * dx
            x1 = 0.1d0 * (x - 10.0d0)
            x1 = x1**2
            cx = 1.0d0 - x1 + 0.5d0 * x1**2
            iy = iz + 2 * (j - 1)
            cxcz = cx * cz
            yinit(iy + 1) = 1.0d6 * cxcz
            yinit(iy + 2) = 1.0d12 * cxcz
         END DO

      END DO

      neq = 2 * mx * mz
      DO i = 1, neq
         ccsol(i) = yinit(i)
      END DO

      RETURN
      END SUBROUTINE inital

      SUBROUTINE derv(t)
!
!     Compute the derivative of y(t).
!
      IMPLICIT NONE
      INTEGER j,k,iz,iy1,iy2
      DOUBLE PRECISION t,rkin1,rkin2,dcvu1,dcvu2,dcvl1,dcvl2,ach1, &
        ach2,c1,c2,czdl,czdu,zl,zu,dchr1,dchr2,dchl1,dchl2,dcval

      CALL diurn(t)

      DO k = 1, mz

         zl = 30.0d0 + (k - 1.5d0) * dz
         zu = zl + dz
         call dcv(zl,dcval)
         czdl = coz * dcval
         call dcv(zu,dcval)
         czdu = coz * dcval
         iz = mx2 * (k - 1)

         DO j = 1, mx
            iy1 = iz + 2 * (j - 1) + 1
            iy2 = iy1 + 1
            c1 = ccsol(iy1)
            c2 = ccsol(iy2)
            CALL chemr(c1, c2, rkin1, rkin2)
            IF (k /= 1) goto 10
            dcvu1 = ccsol(iy1 + mx2) - c1
            dcvu2 = ccsol(iy2 + mx2) - c2
            dcvl1 = - dcvu1
            dcvl2 = - dcvu2
            GOTO 30
   10       CONTINUE
            IF (k /= mz) goto 20
            dcvl1 = c1 - ccsol(iy1 - mx2)
            dcvl2 = c2 - ccsol(iy2 - mx2)
            dcvu1 = - dcvl1
            dcvu2 = - dcvl2
            GOTO 30
   20       CONTINUE
            dcvl1 = c1 - ccsol(iy1 - mx2)
            dcvl2 = c2 - ccsol(iy2 - mx2)
            dcvu1 = ccsol(iy1 + mx2) - c1
            dcvu2 = ccsol(iy2 + mx2) - c2
   30       CONTINUE
            ach1 = 0.0d0
            ach2 = 0.0d0
            IF (j /= 1) goto 40
            dchr1 = ccsol(iy1 + 2) - c1
            dchr2 = ccsol(iy2 + 2) - c2
            dchl1 = - dchr1
            dchl2 = - dchr2
            if (.not.drop_adv) then
               ach1 = ccsol(iy1+2) - ccsol(iy1)
               ach2 = ccsol(iy2+2) - ccsol(iy2)
            end if
            GOTO 60
   40       CONTINUE
            IF (j /= mx) goto 50
            dchl1 = c1 - ccsol(iy1 - 2)
            dchl2 = c2 - ccsol(iy2 - 2)
            dchr1 = - dchl1
            dchr2 = - dchl2
            if (.not.drop_adv) then
               ach1 = ccsol(iy1-2) - ccsol(iy1)
               ach2 = ccsol(iy2-2) - ccsol(iy2)
            end if
            GOTO 60
   50       CONTINUE
            dchl1 = c1 - ccsol(iy1 - 2)
            dchl2 = c2 - ccsol(iy2 - 2)
            dchr1 = ccsol(iy1 + 2) - c1
            dchr2 = ccsol(iy2 + 2) - c2

            if (upwinding) then
!              Use biased upwind differences.
               ach1 = 1.5d0*ccsol(iy1+2) - ccsol(iy1) - 0.5d0*ccsol(iy1-2)
               ach2 = 1.5d0*ccsol(iy2+2) - ccsol(iy2) - 0.5d0*ccsol(iy2-2)
            else
!              Use centered differences.
               ach1 = ccsol(iy1+2) - ccsol(iy1-2)
               ach2 = ccsol(iy2+2) - ccsol(iy2-2)
            end if

   60       CONTINUE

!           ccder(iy1) =
!           (czdu * dcvu1 - czdl * dcvl1) : 2nd order term wrt y (z)
!           + cox * (dchr1 - dchl1)       : 2nd order term wrt x
!           + acox * ach1                 : 1st order term wrt x
!           + rkin1                       : kinetics term
!           ccder(iy2) is similar.

            ccder(iy1) =(czdu * dcvu1 - czdl * dcvl1) + cox * &
            (dchr1 - dchl1) + acox * ach1 + rkin1
            ccder(iy2) =(czdu * dcvu2 - czdl * dcvl2) + cox * &
            (dchr2 - dchl2) + acox * ach2 + rkin2

         END DO

      END DO

      RETURN
      END SUBROUTINE derv

      SUBROUTINE diurn(t)

!     Compute the two diurnal rate functions at a given t.

      IMPLICIT NONE
      DOUBLE PRECISION t,s

      s = sin(freq * t)
      if (s <= 0.0d0) then
         q3 = 0.0d0
         q4 = 0.0d0
      else
         q3 = exp( - a3 / s)
         q4 = exp( - a4 / s)
      end if

      RETURN
      END SUBROUTINE diurn

      SUBROUTINE dcv(z,dcval)

!     Compute the vertical diffusion coefficient at a given z.

      IMPLICIT NONE
      DOUBLE PRECISION arg,z,dcval

      arg = 0.2d0 * z
      dcval = dcv0 * exp(arg)

      RETURN
      END SUBROUTINE dcv

      SUBROUTINE chemr(c1, c2, r1, r2)

!     Compute the 2 chemical kinetics rates at one spatial point.

      IMPLICIT NONE
      DOUBLE PRECISION c1,c2,r1,r2,qq1,qq2,qq3,qq4

      qq1 = q1 * c1 * c3
      qq2 = q2 * c1 * c2
      qq3 = q3 * c3
      qq4 = q4 * c2
      r1 = - qq1 - qq2 + 2.0d0 * qq3 + qq4
      r2 = qq1 - qq2 - qq4

      RETURN
      END SUBROUTINE chemr

      SUBROUTINE SPARSE_STRUCTURE(NEQ,IA,JA,IADIM,JADIM)

!     Define the sparse structure arrays.
!     (Not called by DVODE_F90)

      IMPLICIT NONE
      INTEGER J, K, IZ, IY1, IY2, MX2, NEQ, IA, JA, INTHISCOL, &
      IADIM, JADIM, NUMZ
      DIMENSION IA(*), JA(*)

!     Note:
!     We are using 7 full diagonals are used despite the fact
!     that some of them actually contain zeros.

      NUMZ = 0
      IADIM = NEQ + 1
      IA(1) = 1
      MX2 = 2 * MX
      DO 40 K = 1, MZ
         IZ = MX2 * (K - 1)
         DO 20 J = 1, MX
!           IY1/IY2 go: 1,2; 3,4; ...
            IY1 = IZ + 2 * (J - 1) + 1
            IY2 = IY1 + 1
            INTHISCOL = 0
!           Column IY1:
            IF (IY1 - MX2 > 0) THEN
               INTHISCOL = INTHISCOL + 1
               NUMZ = NUMZ + 1
               JA(NUMZ) = IY1 - MX2
            ENDIF
            IF (IY1 - 2 > 0) THEN
               INTHISCOL = INTHISCOL + 1
               NUMZ = NUMZ + 1
               JA(NUMZ) = IY1 - 2
            ENDIF
            IF (IY1 - 1 > 0) THEN
               INTHISCOL = INTHISCOL + 1
               NUMZ = NUMZ + 1
               JA(NUMZ) = IY1 - 1
            ENDIF
            IF (IY1 > 0) THEN
               INTHISCOL = INTHISCOL + 1
               NUMZ = NUMZ + 1
               JA(NUMZ) = IY1
            ENDIF
            IF (IY1 + 1 <= NEQ) THEN
               INTHISCOL = INTHISCOL + 1
               NUMZ = NUMZ + 1
               JA(NUMZ) = IY1 + 1
            ENDIF
            IF (IY1 + 2 <= NEQ) THEN
               INTHISCOL = INTHISCOL + 1
               NUMZ = NUMZ + 1
               JA(NUMZ) = IY1 + 2
            ENDIF
            IF (IY1 + MX2 <= NEQ) THEN
               INTHISCOL = INTHISCOL + 1
               NUMZ = NUMZ + 1
               JA(NUMZ) = IY1 + MX2
            ENDIF
            IA(IY1+1) = IA(IY1) + INTHISCOL
!           COLUMN IY2:
            INTHISCOL = 0
            IF (IY2 - MX2 > 0) THEN
               INTHISCOL = INTHISCOL + 1
               NUMZ = NUMZ + 1
               JA(NUMZ) = IY2 - MX2
            ENDIF
            IF (IY2 - 2 > 0) THEN
               INTHISCOL = INTHISCOL + 1
               NUMZ = NUMZ + 1
               JA(NUMZ) = IY2 - 2
            ENDIF
            IF (IY2 - 1 > 0) THEN
               INTHISCOL = INTHISCOL + 1
               NUMZ = NUMZ + 1
               JA(NUMZ) = IY2 - 1
            ENDIF
            IF (IY2 > 0) THEN
               INTHISCOL = INTHISCOL + 1
               NUMZ = NUMZ + 1
               JA(NUMZ) = IY2
            ENDIF
            IF (IY2 + 1 <= NEQ) THEN
               INTHISCOL = INTHISCOL + 1
               NUMZ = NUMZ + 1
               JA(NUMZ) = IY2 + 1
            ENDIF
            IF (IY2 + 2 <= NEQ) THEN
               INTHISCOL = INTHISCOL + 1
               NUMZ = NUMZ + 1
               JA(NUMZ) = IY2 + 2
            ENDIF
            IF (IY2 + MX2 <= NEQ) THEN
               INTHISCOL = INTHISCOL + 1
               NUMZ = NUMZ + 1
               JA(NUMZ) = IY2 + MX2
            ENDIF
            IA(IY2+1) = IA(IY2) + INTHISCOL
   20    END DO
   40 END DO
      JADIM = NUMZ

      RETURN
      END SUBROUTINE SPARSE_STRUCTURE

      SUBROUTINE DSTPIC(N, IA, JA, MAXCL)

!     Print a picture of the Jacobian structure.
!     (Not called by DVODE_F90)

!     Note: Not called unless MX=MZ=5.

      IMPLICIT NONE
      INTEGER N,IA,JA,MAXCL,KA,MAXCOL,NUMBLK,IBLOCK,IDONE,IBEGIN,ILEFT, &
      IDO,ICEASE,IROW,KAFILL,ILOWER,IHI,IJK,IDID,ICOL2,K,ICOL,I,INUM, &
      ILO,JFILL

      DIMENSION IA(1), JA(1), KA(130)

      WRITE(6, 99999)
      MAXCOL = 130
!*
      MAXCOL = MAXCL
      IF (MAXCL <= 0) MAXCOL = 130
      IF (MAXCL > 130) MAXCOL = 130
!*
      NUMBLK = (N - 1) / MAXCOL + 1
      IF (NUMBLK.LT.1) GOTO 70
      IF (NUMBLK > 100) GOTO 70
      IDONE = 0
      DO IBLOCK = 1, NUMBLK
         WRITE(6, 99998) IBLOCK
         IBEGIN = IDONE+1
         ILEFT = N - IDONE
         IDO = ILEFT
         IF (IDO > MAXCOL) IDO = MAXCOL
         ICEASE = IDONE+IDO
         DO 50 IROW = 1, N
            KAFILL = ICEASE-IBEGIN + 1
            DO JFILL = 1, KAFILL
               KA(JFILL) = 0
            END DO
            DO ICOL2 = IBEGIN, ICEASE
               ICOL = ICOL2
               IF (IBLOCK > 1) ICOL = ICOL2 - IBEGIN + 1
               ILO = IA(ICOL2)
               ILOWER = ILO - 1
               IHI = IA(ICOL2 + 1)
               INUM = IHI - ILO
               DO I = 1, INUM
                  IJK = JA(ILOWER + I)
                  IF (IJK == IROW) KA(ICOL) = 1
                  IF (IJK == IROW) GOTO 30
               END DO
   30          CONTINUE
            END DO
            IDID = IDO
            WRITE(6, 99997) (KA(K), K = 1, IDID)
   50    END DO
         IDONE = IDONE+IDID
      END DO
   70 CONTINUE

99999 FORMAT(' THE JACOBIAN HAS THE FOLLOWING FORM.')
99998 FORMAT(' BLOCK NUMBER ', I6, ' OF THE JACOBIAN ', //)
99997 FORMAT((2X, 130I1))

      RETURN
      END SUBROUTINE DSTPIC

      SUBROUTINE BANDEDIAJA(N, ML, MU, IA, LENIA, JA, LENJA, &
        SUBDS, NSUBS, SUPDS, NSUPS, IER)
! ..
! Build the sparse structure descriptor arrays for a banded
! matrix if the nonzero diagonals are known.
! (Not called by DVODE_F90)
! ..
!     Input:
!
!       N       = the order of the matrix (number of odes)
!       ML      = integer lower bandwidth
!       MU      = integer upper bandwidth
!       IA      = arrays of length LENIA
!       LENIA   = dimensioned length for IA. LENIA should
!                 equal N+1.
!       JA      = length of LENJA
!       LENJA   = dimensioned length of JA. A value of LENJA
!                 that will work is
!                 (SIZE(SUBS) + SIZE(NSUPS) + 1) * N.
!       SUBDS   = integer array containg the rows in
!                 which the sub diagonals begin, counting from
!                 the lowest sub diagonal
!       SUPDS   = integer array containg the columns
!                 in which the super diagonals begin, counting
!                 from the one nearest the main diagonal
!     Output:
!
!       IA     = IA descriptor array
!       LENIA  = minimum length to which dimensioned LENIA can
!                be reduced
!       JA     = JA descriptor array
!       LENJA  = mimimum length to which dimensioned LENJA can
!                be reduced
!       IER    = error flag with the following meanings
!                0 - successful return
!                1 - illegal value of NSUBS or NSUPS
!                2 - Some element of SUBDS is larger than
!                    ML + 1 or smaller than 2
!                3 - Some element of SUPDS is larger than
!                    MU + 1 or smaller than 2
!                4 - LENIA is too small
!                5 - LENJA is too small
!                6 - computed value of LENJA is incorrect
!                7 - N, ML, or MU is invalid
!
!    Note:
!    These IA and JA arrays may be supplied directly to DVODE_JAC
!    as follows:
!    CALL USERSETS_IAJA(IA(1:LENIA), LENIA, JA(1:LENIA), LENIA)
!    where LENIA and LENIB are the values returned by this
!    subroutine (not the dimensioned values).
! ..
        IMPLICIT NONE
! ..
! .. Arguments ..
        INTEGER, INTENT (IN) :: N, ML, MU, NSUBS, NSUPS
        INTEGER, INTENT (OUT) :: IER, IA(*), JA(*)
        INTEGER, INTENT (INOUT) :: LENIA, LENJA
        INTEGER, INTENT (IN) :: SUBDS(*), SUPDS(*)
! ..
! .. Local Scalars ..
        INTEGER :: I, J, K, KBEGIN, KFINI, KI, KJ, NP1, &
          NZ, NZB
! ..
! .. Intrinsic Functions ..
     INTRINSIC ALLOCATED, MAX, MIN, PRESENT
! ..
! .. FIRST EXECUTABLE STATEMENT BANDEDIAJA
! ..
     IER = 0
     NP1 = N + 1

     IF (N < 1 .OR. ML > N-1 .OR. ML < 0 .OR. MU > N-1 &
       .OR. MU < 0) THEN
        IER = 7
        RETURN
     END IF

     IF (NSUBS < 0 .OR. NSUBS > ML .OR. NSUPS < 0 .OR. NSUPS > MU) THEN
        IER = 1
        RETURN
     END IF

     IF (NSUBS > 0) THEN
        DO I = 1, NSUBS
           IF (SUBDS(I) > ML+1 .OR. SUBDS(I) < 2) THEN
              IER = 2
              RETURN
           END IF
        END DO
     END IF
     IF (NSUPS > 0) THEN
        DO I = 1, NSUPS
           IF (SUPDS(I) > MU+1 .OR. SUPDS(I) < 2) THEN
              IER = 3 
              RETURN
           END IF
        END DO
     END IF
     IF (LENIA < NP1) THEN
        IER = 4
        RETURN
     ELSE
        LENIA = NP1
     END IF

     NZ = (NSUBS + NSUPS + 1) * NP1 - 1
     IF (NSUBS /= 0) THEN
        DO I = 1, NSUBS
           NZ = NZ - SUBDS(I)
        END DO
     END IF
     IF (NSUPS /= 0) THEN
        DO I = 1, NSUPS
           NZ = NZ - SUPDS(I)
        END DO
     END IF

     IF (LENJA < NZ) THEN
        IER = 5
        RETURN
     ELSE
        LENJA = NZ
     END IF

     IA(1) = 1
     NZB = 0

!    For each column in the matrix...
     DO J = 1, N
!       Vertical extent of band = KBEGIN to KFINI.
!       KJ = number of nonzeros in column J.
        KBEGIN = MAX(J-MU,1)
        KFINI = MIN(J+ML,N)
        KJ = 0
!       Locate the row positions of the nonzeros in column J.
!       (Restrict attention to the band.)
        IA(J+1) = IA(J)
!       For each row in the intersection of the band with
!       this column ...
        DO K = KBEGIN, KFINI
!          Does column J intersect a super diagonal at (K,J)?
           IF (K < J) THEN
              DO I = NSUPS, 1, -1
                 KI = J + 1 - SUPDS(I)
                 IF (K == KI) THEN
                    KJ = KJ + 1
                    IA(J+1) = IA(J+1) + 1
                    NZB = NZB + 1
                    JA(NZB) = K
                    GOTO 10
                 END IF
              END DO
           ELSEIF (K == J) THEN
!             We are on the main diagonal.
              KJ = KJ + 1
              IA(J+1) = IA(J+1) + 1
              NZB = NZB + 1
              JA(NZB) = K
              GOTO 10
           ELSE
!             Does column J intersect a sub diagonal at (K,J)?
              DO I = NSUBS, 1, -1
                 KI = SUBDS(I) + J - 1
                 IF (K == KI) THEN
                    KJ = KJ + 1
                    IA(J+1) = IA(J+1) + 1
                    NZB = NZB + 1
                    JA(NZB) = K
                    GOTO 10
                 END IF
              END DO
           END IF
10         CONTINUE
        END DO
     END DO

     IF (NZB /= NZ) THEN
        IER = 6
        RETURN
     END IF

     RETURN
   END SUBROUTINE BANDEDIAJA

END MODULE demo_diurn
!******************************************************************

      PROGRAM DIURNTEST

!     Define the modules to be used.
      USE DVODE_F90_M
      USE demo_diurn

!     Declare the local arrays and parameters.
      IMPLICIT NONE
      INTEGER NST, NFE, NNI, NCFN, NETF, ISTATE, ITASK, IOUT, NJE,   &
        NOUT, NLU, NEQ, I, ML, MU, IX, ISTART, J, K, IY, IZ, KZ,     &
        IVTIME, IADIM, JADIM, MAXCL, NFEJ,                           &
        ISTATS(31), IA(NEQMAX+1), JA(7*NEQMAX), SUBDS(3), SUPDS(3),  &
        WHICH_COMPONENTS(5)
      DOUBLE PRECISION T, TOUT, DELTAT, TWOHR, OXSMAX, OXSMIN,       &
        OXSDIFF, HALFDAY, MAXH, DERMAX, DERMIN,                      &
        ATOL(NEQMAX), RTOL(1), RSTATS(22), Y(NEQMAX),  CPLOTX1(MX),  &
        CPLOTX2(MX), YDOT(NEQMAX)
      LOGICAL SPARSE, BANDED, DENSE, MATLAB_PLOTS, SUPPLY_STRUCTURE, &
        USE_SUBS
      REAL DVTIME, DVTIME1, DVTIME2

!     Declare the DVODE_F90 option structure.
      TYPE(VODE_OPTS) :: OPTIONS
!
!     Open the output files.
      OPEN(6,FILE  ='demodiurn3.dat')
      OPEN(7,FILE  ='demodiurnplot1.dat')
      OPEN(8,FILE  ='demodiurnplot2.dat')
      OPEN(9,FILE  ='demodiurndims.dat')
      OPEN(10,FILE ='demodiurntout.dat')
      OPEN(11,FILE ='demodiurnplot3.dat')
      OPEN(12,FILE ='demodiurnplot4.dat')
      OPEN(13,FILE ='demodiurnplot5.dat')

!     Define the five z nodes for which plot profiles
!     of the concentrations are desired.
      WHICH_COMPONENTS(1) = 1
      WHICH_COMPONENTS(2) = MZ / 4
      WHICH_COMPONENTS(3) = MZ / 2
      WHICH_COMPONENTS(4) = (3 * MZ) / 4
      WHICH_COMPONENTS(5) = MZ

!     Define the MATLAB plot flags and open the output files
!     (plots of concentrations vs time for fixed z).
      MATLAB_PLOTS = .TRUE.
      IF (MATLAB_PLOTS) THEN
         DO K = 1, 5
            WHICH_COMPONENTS(K) = MAX(WHICH_COMPONENTS(K),1)
            WHICH_COMPONENTS(K) = MIN(WHICH_COMPONENTS(K),MZ)
         END DO
!        OPEN(abc...) where
!           a  = species number
!           bc = z grid number
         OPEN(101,FILE ='demodiurnxt101.dat')
         OPEN(111,FILE ='demodiurnxt111.dat')
         OPEN(121,FILE ='demodiurnxt121.dat')
         OPEN(131,FILE ='demodiurnxt131.dat')
         OPEN(141,FILE ='demodiurnxt141.dat')
         OPEN(201,FILE ='demodiurnxt201.dat')
         OPEN(211,FILE ='demodiurnxt211.dat')
         OPEN(221,FILE ='demodiurnxt221.dat')
         OPEN(231,FILE ='demodiurnxt231.dat')
         OPEN(241,FILE ='demodiurnxt241.dat')
      END IF

!     Use biased upwinding for the advection term?
      upwinding = .TRUE.
      IF (upwinding) THEN
         WRITE(6,202)
      ELSE
         WRITE(6,203)
      END IF

!     Use which values for dcv0 and dch?
      jcp_parms = .FALSE.
      IF (jcp_parms) THEN
         WRITE(6,204)
      ELSE
         WRITE(6,205)
      END IF

!     Use which initial condition?
      jcp_ics = .TRUE.
      IF (jcp_ics) THEN
         WRITE(6,206)
      ELSE
         WRITE(6,207)
      END IF

!     Drop advection term at boundary?
      drop_adv = .TRUE.
      IF (drop_adv) THEN
         WRITE(6,212)
      ELSE
         WRITE(6,213)
      END IF

!     Supply banded diagonal information?
      USE_SUBS = .FALSE.

!     Jacobian linear algebra type?
      DENSE = .FALSE.
      SPARSE = .TRUE.
      BANDED = .FALSE.
      IF (DENSE) THEN
         WRITE(6,190)
      ELSEIF (BANDED) THEN
         WRITE(6,191)
      ELSEIF (SPARSE) THEN
         WRITE(6,192)
      ELSE
         WRITE(6,193)
         STOP
      END IF

!     Define the number of odes.
      WRITE(9,189) MX, MZ, (WHICH_COMPONENTS(K),K=1,5)
      IF (MX /= MZ) THEN
         WRITE(6,188)
         STOP
      END IF
      NEQ = 2 * MX * MZ
      IF (NEQ > NEQMAX) THEN
         WRITE(6,185)
         STOP
      END IF
      WRITE(6,208) MX, MZ

!     Define the initial solution and problem parameters.
      CALL INITAL(Y)
!     WRITE(6,187) (I,Y(I),I=1,NEQ)
      TOUT = 0.0D0
      CALL FCN(NEQ,TOUT,Y,YDOT)
      DERMAX = MAXVAL(YDOT)
      DERMIN = MINVAL(YDOT)
      WRITE(6,200) DERMAX,DERMIN

!     Plot the initial solution.
      IF (MATLAB_PLOTS) THEN
         ISTART = 0
!        Even components (species 2, ozone):
         DO IX = 1, MX
            DO IY = 1, MZ
               ISTART = ISTART + 1
!              Write to the Matlab plot file.
               IF (MATLAB_PLOTS) WRITE (11,196) Y(2*ISTART)
            END DO
         END DO
!        Odd components (species 1, oxygen singlet concentrations):
         ISTART = 0
         DO IX = 1, MX
            DO IY = 1, MZ
               ISTART = ISTART + 1
!              Write to the Matlab plot file.
               IF (MATLAB_PLOTS) WRITE (12,196) Y(2*ISTART-1)
            END DO
         END DO
      END IF

!     Write data for the plots of concentration vs time for
!     fixed z.
      IF (MATLAB_PLOTS) THEN
         TOUT = 0.0D0
         WRITE(10,196) TOUT
         DO KZ = 1, 5
            K = WHICH_COMPONENTS(KZ)
            IZ = 2 * MX * (K - 1)
            DO J = 1, MX
               IY = IZ + 2 * (J-1)
               CPLOTX1(J) = Y(IY + 1)
               CPLOTX2(J) = Y(IY + 2)
            END DO
            IF (KZ == 1) WRITE(101,196) (CPLOTX1(I), I=1,MX)
            IF (KZ == 1) WRITE(201,196) (CPLOTX2(I), I=1,MX)
            IF (KZ == 2) WRITE(111,196) (CPLOTX1(I), I=1,MX)
            IF (KZ == 2) WRITE(211,196) (CPLOTX2(I), I=1,MX)
            IF (KZ == 3) WRITE(121,196) (CPLOTX1(I), I=1,MX)
            IF (KZ == 3) WRITE(221,196) (CPLOTX2(I), I=1,MX)
            IF (KZ == 4) WRITE(131,196) (CPLOTX1(I), I=1,MX)
            IF (KZ == 4) WRITE(231,196) (CPLOTX2(I), I=1,MX)
            IF (KZ == 5) WRITE(141,196) (CPLOTX1(I), I=1,MX)
            IF (KZ == 5) WRITE(241,196) (CPLOTX2(I), I=1,MX)
         END DO
      END IF

!     Define the Jacobian bandwidths:
      ML = 2 * MAX(MX, MZ)
      MU = ML

!     Supply sparsity structure for the Jacobian.
      SUPPLY_STRUCTURE = .TRUE.
      IF (BANDED .OR. DENSE) THEN
         SPARSE = .FALSE.
         SUPPLY_STRUCTURE = .FALSE.
      END IF

      IF (USE_SUBS) THEN
         WRITE(6,214)
!        Specify the nonzero sub and super diagonals for
!        use in approximating the banded Jacobian.
!        SUBDS = rows in which sub diagonals start
!                counting from the bottom
!        SUPDS = columns in which super diagonals
!                start counting from one nerest
!                main diagonal
!        sub diagonal that starts at (ML+1,1):
         SUBDS(1) = ML + 1 
!        sub diagonal that starts at(3,1):
         SUBDS(2) = 3
!        sub diagonal that starts at (2,1):
         SUBDS(3) = 2
!        super diagonal that starts at (1,2):
         SUPDS(1) = 2
!        super diagonal that starts at (1,3):
         SUPDS(2) = 3
!        super diagonal that starts at (1,MU+1):
         SUPDS(3) = MU + 1
         WRITE(6,215) (SUBDS(I),I=1,3)
         WRITE(6,216) (SUPDS(I),I=1,3)
      END IF
      IF (SPARSE .AND. SUPPLY_STRUCTURE) THEN
         WRITE(6,210)
         CALL SPARSE_STRUCTURE(NEQ,IA,JA,IADIM,JADIM)
         WRITE(6,*) ' SPARSE_STRUCTURE says IADIM/JADIM = ', IADIM, JADIM
         IF (MX == 5) THEN
           CALL DSTPIC(NEQ, IA, JA, MAXCL)
         END IF
      ELSE
         WRITE(6,211)
      END IF

!     Define the absolute and relative error tolerances.
!     Use default Matlab tolerances:
!     RTOL(1) = 1.0D-3
!     ATOL(2:NEQ:2) = 1.0D-6
!     ATOL(1:NEQ:2) = 1.0D-6
!     Use SUNDIALS tolerances:
      RTOL(1) = 1.0D-5
      ATOL(1:NEQ) = 1.0D-3

      WRITE(6,201) RTOL(1), ATOL(1)

!     Define the initial time, the output increment, the
!     number of output points, and the first output point.
      HALFDAY = 4.32D4
!     Limit the maximum stepsize to one hour.
      MAXH = HALFDAY / 12.0D0
!     Define the output frequency.
!     Every 15 minutes:
      TWOHR = 7200.0D0
      DELTAT = TWOHR / 8.0D0
!     Define the number of days to be simulated.
!     Two days:
!     NOUT = 2*96
!     Six days:
      NOUT = 6*96

      T = 0.0D0
      TOUT = DELTAT

!     Calculate the minimum and maximum oxygen singlet
!     concentrations.
      OXSMAX = MAXVAL(Y(1:NEQ-1:2))
      OXSMIN = MINVAL(Y(1:NEQ-1:2))
      OXSDIFF = OXSMAX - OXSMIN
      WRITE(6,195) OXSMAX, OXSMIN, OXSDIFF
      WRITE(13,198) T, OXSMAX, OXSMIN, OXSDIFF

!     Initialize the integration flags.
      ITASK = 1
      ISTATE = 1

!     Set the remaining options.
      CALL CPU_TIME(DVTIME1)
      IF (USE_SUBS) THEN
         OPTIONS = SET_OPTS(SPARSE_J=SPARSE, BANDED_J=BANDED, DENSE_J=DENSE,   &
           ABSERR_VECTOR=ATOL(1:NEQ), RELERR=RTOL(1), NZSWAG=50000, HMAX=MAXH, &
           LOWER_BANDWIDTH=ML, UPPER_BANDWIDTH=MU,                             &
           SUB_DIAGONALS=SUBDS, SUP_DIAGONALS=SUPDS,                           &
           MA28_ELBOW_ROOM=10, MC19_SCALING=.TRUE., MA28_RPS=.TRUE.,           &
           USER_SUPPLIED_SPARSITY=SUPPLY_STRUCTURE)
      ELSE
         OPTIONS = SET_OPTS(SPARSE_J=SPARSE, BANDED_J=BANDED, DENSE_J=DENSE,   &
           ABSERR_VECTOR=ATOL(1:NEQ), RELERR=RTOL(1), NZSWAG=50000, HMAX=MAXH, &
           LOWER_BANDWIDTH=ML, UPPER_BANDWIDTH=MU,                             &
           MA28_ELBOW_ROOM=10, MC19_SCALING=.TRUE., MA28_RPS=.TRUE.,           &
           USER_SUPPLIED_SPARSITY=SUPPLY_STRUCTURE)
      END IF
      IF (SPARSE .AND. SUPPLY_STRUCTURE) THEN
!        Define the sparse structure arrays directly.
         CALL USERSETS_IAJA(IA, IADIM, JA, JADIM)
      END IF
      CALL CPU_TIME(DVTIME2)
      DVTIME = DVTIME2 - DVTIME1

!     Perform the integration.

      DO IOUT = 1, NOUT

         CALL CPU_TIME(DVTIME1)
         CALL DVODE_F90(FCN, NEQ, Y, T, TOUT, ITASK, ISTATE, OPTIONS)
         CALL CPU_TIME(DVTIME2)
         DVTIME = DVTIME + (DVTIME2 - DVTIME1)

!        Gather the integration statistics for this call.
         CALL GET_STATS(RSTATS,ISTATS)

!        Check if an error occurred and stop if it did.
         IF (ISTATE < 0) GOTO 175

!        Write data for the plots of concentration vs time
!        for fixed z.
         IF (MATLAB_PLOTS) THEN
            WRITE(10,196) TOUT
            DO KZ = 1, 5
               K = WHICH_COMPONENTS(KZ)
               IZ = 2 * MX * (K - 1)
               DO J = 1, MX
                  IY = IZ + 2 * (J-1)
                  CPLOTX1(J) = Y(IY + 1)
                  CPLOTX2(J) = Y(IY + 2)
               END DO
               IF (KZ == 1) WRITE(101,196) (CPLOTX1(I), I=1,MX)
               IF (KZ == 1) WRITE(201,196) (CPLOTX2(I), I=1,MX)
               IF (KZ == 2) WRITE(111,196) (CPLOTX1(I), I=1,MX)
               IF (KZ == 2) WRITE(211,196) (CPLOTX2(I), I=1,MX)
               IF (KZ == 3) WRITE(121,196) (CPLOTX1(I), I=1,MX)
               IF (KZ == 3) WRITE(221,196) (CPLOTX2(I), I=1,MX)
               IF (KZ == 4) WRITE(131,196) (CPLOTX1(I), I=1,MX)
               IF (KZ == 4) WRITE(231,196) (CPLOTX2(I), I=1,MX)
               IF (KZ == 5) WRITE(141,196) (CPLOTX1(I), I=1,MX)
               IF (KZ == 5) WRITE(241,196) (CPLOTX2(I), I=1,MX)
            END DO
         END IF

!        Print the integration statistics for this call.
         WRITE(6,194) IOUT
         NST  = ISTATS(11)
         NFE  = ISTATS(12)
         NJE  = ISTATS(13)
         NLU  = ISTATS(19)
         NNI  = ISTATS(20)
         NCFN = ISTATS(21)
         NETF = ISTATS(22)
         IF (BANDED) THEN
            IF (USE_SUBS) THEN
               NFEJ = 10 * NJE
            ELSE
               NFEJ = (ML + MU + 1) * NJE
            END IF
         ELSEIF (SPARSE .AND. .NOT.DENSE) THEN
            IF (SUPPLY_STRUCTURE) THEN
               NFEJ = 10 * NJE
            ELSE
               NFEJ = 10 * NJE + NEQ
            END IF
         ELSE
            NFEJ = NEQ * NJE
         END IF
         WRITE(6,179)
         WRITE(6,181) NST, NFE, NJE, NFEJ, NLU, NNI, NCFN, NETF, &
           ISTATS(23), ISTATS(24), ISTATS(25), ISTATS(26),       &
           ISTATS(29), ISTATS(30), ISTATS(31)
         IVTIME = DVTIME
         WRITE(6,209) IVTIME

         WRITE(6,183) TOUT
         WRITE(6,184) NEQ
!        Write the solution for this output point.
!        WRITE(6,182) (I,Y(2*I-1),Y(2*I),I=1,NEQ/2)
         CALL FCN(NEQ,TOUT,Y,YDOT)
!        WRITE(6,199) (I,YDOT(2*I-1),YDOT(2*I),I=1,NEQ/2)
         DERMAX = MAXVAL(YDOT)
         DERMIN = MINVAL(YDOT)
         WRITE(6,200) DERMAX,DERMIN
!        Calculate the minimum and maximum oxygen singlet
!        concentrations.
         OXSMAX = MAXVAL(Y(1:NEQ-1:2))
         OXSMIN = MINVAL(Y(1:NEQ-1:2))
         OXSDIFF = OXSMAX - OXSMIN
         WRITE(6,195) OXSMAX, OXSMIN, OXSDIFF
         WRITE(13,198) TOUT, OXSMAX, OXSMIN, OXSDIFF
!        Define the next output point.
         TOUT = TOUT + DELTAT
      END DO

  175 CONTINUE

!     Check if an error occurred in DVODE_F90.
      IF (ISTATE < 0) THEN
         WRITE(6,*) 'DVODE_F90 returned ISTATE = ', ISTATE
         PRINT *,   'DVODE_F90 returned ISTATE = ', ISTATE
      END IF

!     Print the final integration statistics.
      NST  = ISTATS(11)
      NFE  = ISTATS(12)
      NJE  = ISTATS(13)
      NLU  = ISTATS(19)
      NNI  = ISTATS(20)
      NCFN = ISTATS(21)
      NETF = ISTATS(22)
      IF (BANDED) THEN
         IF (USE_SUBS) THEN
            NFEJ = 10 * NJE
         ELSE
            NFEJ = (ML + MU + 1) * NJE
         END IF
      ELSEIF (SPARSE .AND. .NOT.DENSE) THEN
         IF (SUPPLY_STRUCTURE) THEN
            NFEJ = 10 * NJE
         ELSE
            NFEJ = 10 * NJE + NEQ
         END IF
      ELSE
         NFEJ = NEQ * NJE
      END IF
      WRITE(6,179)
      WRITE(6,181) NST, NFE, NJE, NFEJ, NLU, NNI, NCFN, NETF, &
        ISTATS(23), ISTATS(24), ISTATS(25), ISTATS(26),       &
        ISTATS(29), ISTATS(30), ISTATS(31)
      WRITE(6,183) TOUT
      WRITE(6,184) NEQ
!     Write the final solution and derivatives.
      WRITE(6,182) (I,Y(2*I-1),Y(2*I),I=1,NEQ/2)
      CALL FCN(NEQ,TOUT,Y,YDOT)
      WRITE(6,199) (I,YDOT(2*I-1),YDOT(2*I),I=1,NEQ/2)
      DERMAX = MAXVAL(YDOT)
      DERMIN = MINVAL(YDOT)
      WRITE(6,200) DERMAX,DERMIN

!     Release the DVODE_F90 work arrays.
      CALL RELEASE_ARRAYS

!     Plot the final solution.
      IF (MATLAB_PLOTS) THEN
!        Even components (species 2, ozone):
         ISTART = 0
         DO IX = 1, MX
            DO IY = 1, MZ
               ISTART = ISTART + 1
!              Write to the Matlab plot file.
               IF (MATLAB_PLOTS) WRITE (7,196) Y(2*ISTART)
            END DO
         END DO
!        Odd components (species 1, oxygen singlet concentrations):
         ISTART = 0
         DO IX = 1, MX
            DO IY = 1, MZ
               ISTART = ISTART + 1
!              Write to the Matlab plot file.
               IF (MATLAB_PLOTS)  WRITE (8,196) Y(2*ISTART-1)
            END DO
         END DO
      END IF

      IVTIME = DVTIME
      WRITE (6,197) IVTIME

!     Print the status of the run.
      PRINT *, ' '
      WRITE(6,*) ' Output for this run was written to demodiurn3.dat'
      PRINT *,   ' Output for this run was written to demodiurn3.dat'
      IF (ISTATE == 2) THEN
         WRITE(6,*) ' The integration was successful.'
         WRITE(6,*) ' To obtains plots for this run execute the Matlab mfile demodiurn3.m'
         PRINT *,   ' The integration was successful.'
         PRINT *,   ' To obtains plots for this run execute the Matlab mfile demodiurn3.m'
      ELSE
         WRITE(6,*) ' The integration was not successful.'
         PRINT *,   ' The integration was not successful.'
      END IF

!     Format Statements.
  179 FORMAT('  The cumulative statistics to this point follow.')
  181 FORMAT('  Integration statistics:',/                    &
      '     Number of integration steps              = ',I10/ &
      '     Total number of derivative evaluations   = ',I10/ &
      '     Number of Jacobian evaluations           = ',I10/ &
      '     Linear algebra derivative evaluations    = ',I10/ &
      '     Number of LU decompositions              = ',I10/ &
      '     Number of nonlinear iterations           = ',I10/ &
      '     Number of corrector convergence failures = ',I10/ &
      '     Number of error test failures            = ',I10/ &
      '  Sparse statistics:',/                                &
      '     Number of calls to MA28AD =',I10/                 &
      '     Number of calls to MA28BD =',I10/                 &
      '     Number of calls to MA28CD =',I10/                 &
      '     Number of calls to MC19AD =',I10/                 &
      '     Maximum MINIRN requested by MA28AD     = ',I10/   &
      '     Maximum MINICN requested by MA28AD     = ',I10/   &
      '     Maximum number of nonzeros in Jacobian = ',I10/)
  182 FORMAT(' Final DVODE_F90 Solution:',/, &
            10X,'Species 1',10X,'Species 2 ',/,(I5,2D15.5))
  183 FORMAT(' Current output time = ', D15.5)
  184 FORMAT(' Number of odes = ', I10)
  185 FORMAT(' NEQ is larger than NEQMAX.')
! 187 FORMAT(' Initial DVODE_F90 Solution:',/,(I5,D15.5))
  188 FORMAT(' MX and MZ must be equal in this version.')
  189 FORMAT(7I5)
  190 FORMAT(' The Jacobian will be treated as DENSE.')
  191 FORMAT(' The Jacobian will be treated as BANDED.')
  192 FORMAT(' The Jacobian will be treated as SPARSE.')
  193 FORMAT(' The Jacobian type was not unspecified.')
  194 FORMAT(' IOUT = ', I5)
  195 FORMAT(' Max. oxygen singlet concentration = ',D15.5,/, &
             ' Min. oxygen singlet concentration = ',D15.5,/, &
             ' Difference = ',D15.5)
  196 FORMAT((D25.15))
  197 FORMAT(' DVODE execution time (sec) = ', I10)
  198 FORMAT(4D25.15)
  199 FORMAT(' Final DVODE_F90 Derivatives:',/, &
            10X,'Species 1',10X,'Species 2 ',/,(I5,2D15.5))
  200 FORMAT(' Max/Min derivatives = ', 2D15.5)
  201 FORMAT(' Relative error tolerance = ', D15.5,/, &
             ' Absolute error tolerance = ', D15.5)
  202 FORMAT(' Biased upwind differences will be used.')
  203 FORMAT(' Centered differences will be used.')
  204 FORMAT(' The jcp parameters will be used for dcv0 and dch.')
  205 FORMAT(' The cvkryf parameters will be used for dcv0 and dch.')
  206 FORMAT(' The jcp initial condition will be used.')
  207 FORMAT(' The Wittman initial condition will be used.')
  208 FORMAT(' The spatial grid is ',I4,' by ',I4)
  209 FORMAT(' Cumulative execution time (sec) = ', I10)
  210 FORMAT(' Jacobian structure arrays will be supplied.')
  211 FORMAT(' Jacobian structure arrays will be determined numerically.')
  212 FORMAT(' Advection term will be dropped at the x-boundaries.')
  213 FORMAT(' Advection term will not be dropped at the x-boundaries.')
  214 FORMAT(' Starting locations of the diagonals will be provided.')
  215 FORMAT(' The sub diagonals start in rows:',/,3I10)
  216 FORMAT(' The super diagonals start in columns:',/,3I10)

!     Terminate execution.
      STOP
      END PROGRAM DIURNTEST
