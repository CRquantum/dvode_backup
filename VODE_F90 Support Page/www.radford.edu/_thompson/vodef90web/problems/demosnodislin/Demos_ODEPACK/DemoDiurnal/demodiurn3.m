function [t,y] = demodiurn3
% Matlab script to plot the solution for the 2d diurnal
% kinetics (demodiurn3) problem

clc

mxmz = load('demodiurndims.dat','-ascii');

% Initial solution profiles

tempu = load('demodiurnplot3.dat','-ascii');
tempv = load('demodiurnplot4.dat','-ascii');
mx = mxmz(1,1);
mz = mxmz(1,2);
u = reshape(tempu,mx,mz);
v = reshape(tempv,mx,mz);
x = linspace(0,20,mx);
z = linspace(30,50,mz);
figure
title('Diurnal Kinetics Problem. Species 2. Initial Solution')
surf(z,x,u)
%zlim([umin umax])
xlabel('z');
ylabel('x');
zlabel('Initial Species 2')
figure
title('Diurnal Kinetics Problem. Species 1. Initial Solution')
surf(z,x,v)
%zlim([vmin vmax])
xlabel('z');
ylabel('x');
zlabel('Initial Species 1')

% Final solution profile

tempu = load('demodiurnplot1.dat','-ascii');
tempv = load('demodiurnplot2.dat','-ascii');
u = reshape(tempu,mx,mz);
v = reshape(tempv,mx,mz);
umin = min(tempu);
umax = max(tempu);
vmin = min(tempv);
vmax = max(tempv);
if (umin > 0)
   umin = 0.99 * umin;
else
   umin = 1.01 * umin;
end
if (umax > 0)
   umax = 1.01 * umax;
else
   umax =  0.99 * umax;
end
if (vmin > 0)
   vmin = 0.99 * vmin;
else
   vmin = 1.01 * vmin;
end
if (umax > 0)
   vmax = 1.01 * vmax;
else
   vmax =  0.99 * vmax;
end
figure
title('Diurnal Kinetics Problem. Species 2. Final Solution')
surf(z,x,u)
zlim([umin umax])
xlabel('z');
ylabel('x');
zlabel('Final Species 2')
figure
title('Diurnal Kinetics Problem. Species 1. Final Solution')
surf(z,x,v)
zlim([vmin vmax])
xlabel('z');
ylabel('x');
zlabel('Final Species 1')

% Solutions c(x,z,t) for fixed z

which_components(1:5) = mxmz(3:7);

i = which_components(1);
zval = 30 + (i-1) * (20 / (mz - 1));
tout = load('demodiurntout.dat','-ascii');
ntout = length(tout);
tempx1 = load('demodiurnxt101.dat','-ascii');
tempx2 = load('demodiurnxt201.dat','-ascii');
u = reshape(tempx1,mx,ntout);
v = reshape(tempx2,mx,ntout);
figure
surf(u)
title(['Diurnal Kinetics Problem. c_1(x,z,t) for z = node no. ', ...
        num2str(i), ' (z = ', num2str(zval), ')'])
ylabel('x');
xlabel('t (x 0.25 hours)');
figure
surf(v)
title(['Diurnal Kinetics Problem. c_2(x,z,t) for z = node no. ', ...
        num2str(i), ' (z = ', num2str(zval), ')'])
ylabel('x');
xlabel('t (x 0.25 hours)');
i = which_components(2);
zval = 30 + (i-1) * (20 / (mz - 1));
tempx1 = load('demodiurnxt111.dat','-ascii');
tempx2 = load('demodiurnxt211.dat','-ascii');
u = reshape(tempx1,mx,ntout);
v = reshape(tempx2,mx,ntout);
figure
surf(u)
title(['Diurnal Kinetics Problem. c_1(x,z,t) for z = node no. ', ...
        num2str(i), ' (z = ', num2str(zval), ')'])
ylabel('x');
xlabel('t (x 0.25 hours)');
figure
surf(v)
title(['Diurnal Kinetics Problem. c_2(x,z,t) for z = node no. ', ...
        num2str(i), ' (z = ', num2str(zval), ')'])
ylabel('x');
xlabel('t (x 0.25 hours)');
i = which_components(3);
zval = 30 + (i-1) * (20 / (mz - 1));
tempx1 = load('demodiurnxt121.dat','-ascii');
tempx2 = load('demodiurnxt221.dat','-ascii');
u = reshape(tempx1,mx,ntout);
v = reshape(tempx2,mx,ntout);
figure
surf(u)
title(['Diurnal Kinetics Problem. c_1(x,z,t) for z = node no. ', ...
        num2str(i), ' (z = ', num2str(zval), ')'])
ylabel('x');
xlabel('t (x 0.25 hours)');
figure
surf(v)
title(['Diurnal Kinetics Problem. c_2(x,z,t) for z = node no. ', ...
        num2str(i), ' (z = ', num2str(zval), ')'])
ylabel('x');
xlabel('t (x 0.25 hours)');
i = which_components(4);
zval = 30 + (i-1) * (20 / (mz - 1));
tempx1 = load('demodiurnxt131.dat','-ascii');
tempx2 = load('demodiurnxt231.dat','-ascii');
u = reshape(tempx1,mx,ntout);
v = reshape(tempx2,mx,ntout);
figure
surf(u)
title(['Diurnal Kinetics Problem. c_1(x,z,t) for z = node no. ', ...
        num2str(i), ' (z = ', num2str(zval), ')'])
ylabel('x');
xlabel('t (x 0.25 hours)');
figure
surf(v)
title(['Diurnal Kinetics Problem. c_2(x,z,t) for z = node no. ', ...
        num2str(i), ' (z = ', num2str(zval), ')'])
ylabel('x');
xlabel('t (x 0.25 hours)');
i = which_components(5);
zval = 30 + (i-1) * (20 / (mz - 1));
tempx1 = load('demodiurnxt141.dat','-ascii');
tempx2 = load('demodiurnxt241.dat','-ascii');
u = reshape(tempx1,mx,ntout);
v = reshape(tempx2,mx,ntout);
figure
surf(u)
title(['Diurnal Kinetics Problem. c_1(x,z,t) for z = node no. ', ...
        num2str(i), ' (z = ', num2str(zval), ')'])
ylabel('x');
xlabel('t (x 0.25 hours)');
figure
surf(v)
title(['Diurnal Kinetics Problem. c_2(x,z,t) for z = node no. ', ...
        num2str(i), ' (z = ', num2str(zval), ')'])
ylabel('x');
xlabel('t (x 0.25 hours)');
