    MODULE DEMODB

! VODE_F90 DEMONSTRATION PROGRAM

! VODE_F90 IS USED TO SOLVE TWO OF THE ODEPACK DEMO PROBLEMS, ONE
! WITH A FULL JACOBIAN, THE OTHER WITH A BANDED JACOBIAN, WITH ALL
! 12 OF THE APPROPRIATE VALUES OF MF IN EACH CASE.

    CONTAINS

      SUBROUTINE F1(NEQ,T,Y,YDOT)
        IMPLICIT NONE
        INTEGER NEQ
        DOUBLE PRECISION T, Y, YDOT
        DIMENSION Y(2), YDOT(2)

        YDOT(1) = Y(2)
        YDOT(2) = 3.0D0*(1.0D0-Y(1)*Y(1))*Y(2) - Y(1)
        RETURN
      END SUBROUTINE F1

      SUBROUTINE JAC1(NEQ,T,Y,ML,MU,PD,NROWPD)
        IMPLICIT NONE
        INTEGER NEQ, ML, MU, NROWPD
        DOUBLE PRECISION T, Y, PD
        DIMENSION Y(2), PD(NROWPD,2)

        PD(1,1) = 0.0D0
        PD(1,2) = 1.0D0
        PD(2,1) = -6.0D0*Y(1)*Y(2) - 1.0D0
        PD(2,2) = 3.0D0*(1.0D0-Y(1)*Y(1))
        RETURN
      END SUBROUTINE JAC1

      SUBROUTINE F2(NEQ,T,Y,YDOT)
        IMPLICIT NONE
        INTEGER NEQ, I, J, K, NG
        DOUBLE PRECISION T, Y, YDOT, ALPH1, ALPH2, D
        DIMENSION Y(NEQ), YDOT(NEQ)
        DATA ALPH1/1.0D0/, ALPH2/1.0D0/, NG/5/

        DO J = 1, NG
          DO I = 1, NG
            K = I + (J-1)*NG
            D = -2.0D0*Y(K)
            IF (I/=1) D = D + Y(K-1)*ALPH1
            IF (J/=1) D = D + Y(K-NG)*ALPH2
            YDOT(K) = D
          END DO
        END DO
        RETURN
      END SUBROUTINE F2

      SUBROUTINE JAC2(NEQ,T,Y,ML,MU,PD,NROWPD)
        IMPLICIT NONE
        INTEGER NEQ, ML, MU, NROWPD, J, MBAND, MU1, MU2, NG
        DOUBLE PRECISION T, Y, PD, ALPH1, ALPH2
        DIMENSION Y(NEQ), PD(NROWPD,NEQ)
        DATA ALPH1/1.0D0/, ALPH2/1.0D0/, NG/5/

        MBAND = ML + MU + 1
        MU1 = MU + 1
        MU2 = MU + 2
        DO J = 1, NEQ
          PD(MU1,J) = -2.0D0
          PD(MU2,J) = ALPH1
          PD(MBAND,J) = ALPH2
        END DO
        DO J = NG, NEQ, NG
          PD(MU2,J) = 0.0D0
        END DO
        RETURN
      END SUBROUTINE JAC2

      SUBROUTINE EDIT2(Y,T,ERM)
        IMPLICIT NONE
        INTEGER I, J, K, NG
        DOUBLE PRECISION Y, T, ERM, ALPH1, ALPH2, A1, A2, ER, EX, YT
        DIMENSION Y(25)
        DATA ALPH1/1.0D0/, ALPH2/1.0D0/, NG/5/

        ERM = 0.0D0
        IF (T==0.0D0) RETURN
        EX = 0.0D0
        IF (T<=30.0D0) EX = EXP(-2.0D0*T)
        A2 = 1.0D0
        DO J = 1, NG
          A1 = 1.0D0
          DO I = 1, NG
            K = I + (J-1)*NG
            YT = T**(I+J-2)*EX*A1*A2
            ER = ABS(Y(K)-YT)
            ERM = MAX(ERM,ER)
            A1 = A1*ALPH1/REAL(I)
          END DO
          A2 = A2*ALPH2/REAL(J)
        END DO
        RETURN
      END SUBROUTINE EDIT2

    END MODULE DEMODB

!******************************************************************

    PROGRAM RUNDEMODB

      USE DVODE_F90_M
      USE DEMODB

      IMPLICIT NONE
      INTEGER I, IOPAR, IOUT, ISTATE, ITASK, ITOL, ISTATS, JSV, LENIW, LENRW, &
        LOUT, MBAND, METH, MF, MITER, ML, MU, NCFN, NEQ, NERR, NETF, NFE, &
        NFEA, NJE, NLU, NNI, NOUT, NQU, NST, MFSAVE
      DOUBLE PRECISION ATOL, DTOUT, ER, ERM, ERO, HU, RTOL, RSTATS, T, TOUT, &
        TOUT1, Y
      DIMENSION Y(25), RSTATS(22), ISTATS(31), ATOL(1), RTOL(1)
      TYPE (VODE_OPTS) :: OPTIONS

      OPEN (UNIT=6,FILE='demodb.dat')
      LOUT = 6
      TOUT1 = 1.39283880203D0
      DTOUT = 2.214773875D0
      NERR = 0
      ITOL = 1
      RTOL(1) = 0.0D0
      ATOL(1) = 1.0D-6

! FIRST PROBLEM (NONSTIFF AND STIFF DENSE OPTIONS)

      NEQ = 2
      NOUT = 4
      WRITE (LOUT,90000) NEQ, ITOL, RTOL(1), ATOL(1)
90000 FORMAT (/' DEMONSTRATION PROGRAM FOR DVODE PACKAGE'//// &
        ' PROBLEM 1..   VAN DER POL OSCILLATOR..'/ &
        '   XDOTDOT - 3*(1 - X**2)*XDOT + X = 0,','   X(0) = 2, XDOT(0) = 0'/ &
        '   NEQ =',I2/'   ITOL =',I3,'   RTOL =',D10.1,'   ATOL =',D10.1//)
      DO JSV = 1, -1, -2
        DO METH = 1, 2
          DO MITER = 0, 3
            IF (JSV<0 .AND. MITER==0) GO TO 30
            IF (JSV<0 .AND. MITER==3) GO TO 30
            MF = JSV*(10*METH+MITER)
            MFSAVE = MF
!              IF (MITER == 4) MF = JSV * (10 * METH + 6)
!              IF (MITER == 5) MF = JSV * (10 * METH + 7)
            WRITE (LOUT,90001) MFSAVE
90001       FORMAT (////' MF =',I4///6X,'T',15X,'X',15X,'XDOT',7X,'NQ',6X, &
              'H'//)
            T = 0.0D0
            Y(1) = 2.0D0
            Y(2) = 0.0D0
            ITASK = 1
            ISTATE = 1
            TOUT = TOUT1
            ERO = 0.0D0

            IF (MF==10 .OR. MF==21 .OR. MF==22 .OR. MF==-21 .OR. MF==-22) THEN
              IF (MF==10) OPTIONS = SET_OPTS(ABSERR=ATOL(1),RELERR=RTOL(1))
              IF (MF==21) OPTIONS = SET_OPTS(DENSE_J=.TRUE.,ABSERR=ATOL(1), &
                RELERR=RTOL(1),USER_SUPPLIED_JACOBIAN=.TRUE.)
              IF (MF==22) OPTIONS = SET_OPTS(DENSE_J=.TRUE.,ABSERR=ATOL(1), &
                RELERR=RTOL(1))
              IF (MF==-21) OPTIONS = SET_OPTS(DENSE_J=.TRUE.,ABSERR=ATOL(1), &
                RELERR=RTOL(1),USER_SUPPLIED_JACOBIAN=.TRUE., &
                SAVE_JACOBIAN=.FALSE.)
              IF (MF==-22) OPTIONS = SET_OPTS(DENSE_J=.TRUE.,ABSERR=ATOL(1), &
                RELERR=RTOL(1),SAVE_JACOBIAN=.FALSE.)
            ELSE
              OPTIONS = SET_OPTS(METHOD_FLAG=MF,ABSERR=ATOL(1),RELERR=RTOL(1))
            END IF

!              CALL SET_IAJA(F1, NEQ, T, Y)
            DO IOUT = 1, NOUT
              CALL DVODE_F90(F1,NEQ,Y,T,TOUT,ITASK,ISTATE,OPTIONS,JAC1)
              CALL GET_STATS(RSTATS,ISTATS)
              HU = RSTATS(11)
              NQU = ISTATS(14)
              WRITE (LOUT,90002) T, Y(1), Y(2), NQU, HU
90002         FORMAT (1X,D15.5,D16.5,D14.3,I5,D14.3)
              IF (ISTATE<0) GO TO 20
              IOPAR = IOUT - 2*(IOUT/2)
              IF (IOPAR/=0) GO TO 10
              ER = ABS(Y(1))/ATOL(1)
              ERO = MAX(ERO,ER)
              IF (ER<10000.0D0) GO TO 10
              WRITE (LOUT,90003)
90003         FORMAT (//' WARNING.. ERROR EXCEEDS 10000 * TOLERANCE'//)
              NERR = NERR + 1
10            TOUT = TOUT + DTOUT
            END DO
20          CONTINUE
            IF (ISTATE<0) NERR = NERR + 1
            NST = ISTATS(11)
            NFE = ISTATS(12)
            NJE = ISTATS(13)
            NLU = ISTATS(19)
            LENRW = ISTATS(17)
            LENIW = ISTATS(18)
            NNI = ISTATS(20)
            NCFN = ISTATS(21)
            NETF = ISTATS(22)
            NFEA = NFE
            IF (MITER==2) NFEA = NFE - NEQ*NJE
            IF (MITER==3) NFEA = NFE - NJE
            WRITE (LOUT,90004) LENRW, LENIW, NST, NFE, NFEA, NJE, NLU, NNI, &
              NCFN, NETF, ERO
90004       FORMAT (//' FINAL STATISTICS FOR THIS RUN..'/' RWORK SIZE =',I4, &
              '   IWORK SIZE =',I4/' NUMBER OF STEPS =', &
              I5/' NUMBER OF F-S   =',I5/' (EXCLUDING J-S) =', &
              I5/' NUMBER OF J-S   =',I5/' NUMBER OF LU-S  =', &
              I5/' NUMBER OF NONLINEAR ITERATIONS =', &
              I5/' NUMBER OF NONLINEAR CONVERGENCE FAILURES =', &
              I5/' NUMBER OF ERROR TEST FAILURES =',I5/' ERROR OVERRUN =', &
              D10.2)
30        END DO
        END DO
      END DO

! SECOND PROBLEM (NONSTIFF AND STIFF BANDED OPTIONS)

      NEQ = 25
      ML = 5
      MU = 0
      MBAND = ML + MU + 1
      NOUT = 5
      WRITE (LOUT,90005) NEQ, ML, MU, ITOL, RTOL(1), ATOL(1)
90005 FORMAT ('1'/' DEMONSTRATION PROGRAM FOR DVODE PACKAGE'//// &
        ' PROBLEM 2.. YDOT = A * Y , WHERE', &
        ' A IS A BANDED LOWER TRIANGULAR MATRIX'/ &
        '   DERIVED FROM 2-D ADVECTION PDE'/'   NEQ =',I3,'   ML =',I2, &
        '   MU =',I2/'   ITOL =',I3,'   RTOL =',D10.1,'   ATOL =',D10.1//)
      DO JSV = 1, -1, -2
        DO METH = 1, 2
          DO MITER = 0, 5
            IF (MITER==1 .OR. MITER==2) GO TO 60
            IF (JSV<0 .AND. MITER==0) GO TO 60
            IF (JSV<0 .AND. MITER==3) GO TO 60
            MF = JSV*(10*METH+MITER)
            MFSAVE = MF
!              IF (MITER == 4) MF = JSV * (10 * METH + 6)
!              IF (MITER == 5) MF = JSV * (10 * METH + 7)
            WRITE (LOUT,90006) MFSAVE
90006       FORMAT (////' MF =',I4///6X,'T',13X,'MAX.ERR.',5X,'NQ',6X,'H'//)
            T = 0.0D0
            DO I = 2, NEQ
              Y(I) = 0.0D0
            END DO
            Y(1) = 1.0D0
            ITASK = 1
            ISTATE = 1
            TOUT = 0.01D0
            ERO = 0.0D0

            IF (MF==24 .OR. MF==25 .OR. MF==-24 .OR. MF==-25) THEN
              IF (MF==24) OPTIONS = SET_OPTS(BANDED_J=.TRUE.,ABSERR=ATOL(1), &
                RELERR=RTOL(1),USER_SUPPLIED_JACOBIAN=.TRUE., &
                LOWER_BANDWIDTH=ML,UPPER_BANDWIDTH=MU)
              IF (MF==25) OPTIONS = SET_OPTS(BANDED_J=.TRUE.,ABSERR=ATOL(1), &
                RELERR=RTOL(1),LOWER_BANDWIDTH=ML,UPPER_BANDWIDTH=MU)
              IF (MF==-24) OPTIONS = SET_OPTS(BANDED_J=.TRUE.,ABSERR=ATOL(1), &
                RELERR=RTOL(1),USER_SUPPLIED_JACOBIAN=.TRUE., &
                LOWER_BANDWIDTH=ML,UPPER_BANDWIDTH=MU,SAVE_JACOBIAN=.FALSE.)
              IF (MF==-25) OPTIONS = SET_OPTS(BANDED_J=.TRUE.,ABSERR=ATOL(1), &
                RELERR=RTOL(1),LOWER_BANDWIDTH=ML,UPPER_BANDWIDTH=MU, &
                SAVE_JACOBIAN=.FALSE.)
            ELSE
              OPTIONS = SET_OPTS(METHOD_FLAG=MF,ABSERR=ATOL(1),RELERR=RTOL(1), &
                LOWER_BANDWIDTH=ML,UPPER_BANDWIDTH=MU)
            END IF

            OPTIONS = SET_OPTS(METHOD_FLAG=MF,ABSERR=ATOL(1),RELERR=RTOL(1), &
              LOWER_BANDWIDTH=ML,UPPER_BANDWIDTH=MU)
!              CALL SET_IAJA(F2, NEQ, T, Y)
            DO IOUT = 1, NOUT
!                 CALL DVODE_F90 (F2, NEQ, Y, T, TOUT, ITASK, ISTATE,  &
!                 JAC2, OPTIONS)
              CALL DVODE_F90(F2,NEQ,Y,T,TOUT,ITASK,ISTATE,OPTIONS,JAC2)
              CALL GET_STATS(RSTATS,ISTATS)
              CALL EDIT2(Y,T,ERM)
              HU = RSTATS(11)
              NQU = ISTATS(14)
              WRITE (LOUT,90007) T, ERM, NQU, HU
90007         FORMAT (1X,D15.5,D14.3,I5,D14.3)
              IF (ISTATE<0) GO TO 50
              ER = ERM/ATOL(1)
              ERO = MAX(ERO,ER)
              IF (ER<=1000.0D0) GO TO 40
              WRITE (LOUT,90003)
              NERR = NERR + 1
40            TOUT = TOUT*10.0D0
            END DO
50          CONTINUE
            IF (ISTATE<0) NERR = NERR + 1
            NST = ISTATS(11)
            NFE = ISTATS(12)
            NJE = ISTATS(13)
            NLU = ISTATS(19)
            LENRW = ISTATS(17)
            LENIW = ISTATS(18)
            NNI = ISTATS(20)
            NCFN = ISTATS(21)
            NETF = ISTATS(22)
            NFEA = NFE
            IF (MITER==5) NFEA = NFE - MBAND*NJE
            IF (MITER==3) NFEA = NFE - NJE
            WRITE (LOUT,90004) LENRW, LENIW, NST, NFE, NFEA, NJE, NLU, NNI, &
              NCFN, NETF, ERO
60        END DO
        END DO
      END DO
      WRITE (LOUT,90008) NERR
90008 FORMAT (////' NUMBER OF ERRORS ENCOUNTERED =',I3)
      STOP

    END PROGRAM RUNDEMODB
