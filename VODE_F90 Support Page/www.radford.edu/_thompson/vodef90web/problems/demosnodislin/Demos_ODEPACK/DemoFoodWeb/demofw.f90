! VODE_F90 demonstration program
! Food web problem. See the reference in the file demodiurndoc.

    MODULE DEMO_FOODWEB

      IMPLICIT NONE

!     Problem parameters and arrays:

      INTEGER NS, NP, MX, MY, MXNS, MP, MQ, MPSQ, MESHX, MESHY, NGX, NGY, &
        NGRP, MXMP, JGX, JGY, JIGX, JIGY, JXR, JYR, ICHECK

      DOUBLE PRECISION AA, EE, GG, BB, DPREY, DPRED, AX, AY, ACOEF, BCOEF, DX, &
        DY, ALPH, DIFF, COX, COY, CCALAN, CDALAN, CCRATE, ACHECK, BCHECK

      DIMENSION ACOEF(40,40), BCOEF(40), DIFF(40), COX(40), COY(40), JGX(21), &
        JGY(21), JIGX(50), JIGY(50), JXR(40), JYR(40), CCALAN(480), &
        CDALAN(480), CCRATE(1050), ACHECK(40,40), BCHECK(40,3), ICHECK(40)

    CONTAINS

      SUBROUTINE FCN(NEQ,T,CC,CDOT)

!     DIRECT THE CALCULATION OF THE TIME DERIVATIVES.

        IMPLICIT NONE
        INTEGER NEQ
        DOUBLE PRECISION T, CC, CDOT
        DIMENSION CC(NEQ), CDOT(NEQ)

!     LOAD THE SOLUTION INTO LOCAL STORAGE.
        CCALAN(1:NEQ) = CC(1:NEQ)
!     CALL DCOPY(NEQ,CC,1,CCALAN,1)

!     DEFINE THE TIME DERIVATIVES.
        CALL DERV(T)

!     LOAD THE TIME DERIVATIVES INTO INTEGRATOR ARRAY.
        CDOT(1:NEQ) = CDALAN(1:NEQ)
!     CALL DCOPY(NEQ,CDALAN,1,CDOT,1)

        RETURN
      END SUBROUTINE FCN

      SUBROUTINE DERV(T)

!     this routine computes the derivative of ccalan and returns
!     it in cdalan. the interaction rates were computed by calls
!     to derv,and these are saved in cc(neq+1),...,cc(2*neq)
!     for use in preconditioning (inactive: they are now saved
!     in ccrate. this is the fweb routine in the original program.

        IMPLICIT NONE
        INTEGER JX, JY, ICI, I, IDXL, IDXU, IC, IDYL, IDYU, IYOFF
        DOUBLE PRECISION T, X, Y, DCXLI, DCYUI, DCYLI, DCXUI

        DO JY = 1, MY

          Y = REAL(JY-1)*DY
          IYOFF = MXNS*(JY-1)
          IDYU = MXNS
          IF (JY==MY) IDYU = -MXNS
          IDYL = MXNS
          IF (JY==1) IDYL = -MXNS

          DO JX = 1, MX

            X = REAL(JX-1)*DX
            IC = IYOFF + NS*(JX-1) + 1

!         get interaction rates at one point (x,y)

!           call webr(x,y,t,ccalan(ic),ccalan(neq+ic))

            CALL WEBR(X,Y,T,CCALAN(IC),CCRATE(IC))

            IDXU = NS
            IF (JX==MX) IDXU = -NS
            IDXL = NS
            IF (JX==1) IDXL = -NS

            DO I = 1, NS

              ICI = IC + I - 1

!           do differencing in y

              DCYLI = CCALAN(ICI) - CCALAN(ICI-IDYL)
              DCYUI = CCALAN(ICI+IDYU) - CCALAN(ICI)

!           do differencing in x

              DCXLI = CCALAN(ICI) - CCALAN(ICI-IDXL)
              DCXUI = CCALAN(ICI+IDXU) - CCALAN(ICI)

!           collect terms and load cdot elements

!           cdalan(ici) = coy(i)*(dcyui - dcyli)
!    *                  + cox(i)*(dcxui - dcxli)
!    *                  + ccalan(neq+ici)

              CDALAN(ICI) = COY(I)*(DCYUI-DCYLI) + COX(I)*(DCXUI-DCXLI) + &
                CCRATE(ICI)

            END DO

          END DO

        END DO

        RETURN
      END SUBROUTINE DERV

      SUBROUTINE WEBR(X,Y,T,C,RATE)

!     this routine computes the interaction rates for
!     the species c(1),...,c(ns),at one spatial point
!     and at time t.
!     this is subroutine webr in the original program.

        IMPLICIT NONE
        INTEGER I
        DOUBLE PRECISION X, Y, T, C, RATE, FAC

        DIMENSION C(*), RATE(*)

        DO I = 1, NS
          RATE(I) = 0.0D0
        END DO
!     Uses a local copy of Daxpy
        DO I = 1, NS
          CALL MYDAXPY(NS,C(I),ACOEF(1,I),1,RATE,1)
        END DO
        FAC = 1.0D0 + ALPH*X*Y
        DO I = 1, NS
          RATE(I) = C(I)*(BCOEF(I)*FAC+RATE(I))
        END DO

        RETURN
      END SUBROUTINE WEBR

      SUBROUTINE INITAL(CC)

        INCLUDE 'demofwdoc'

!     set the problem parameters ns,mx,my,
!     and problem coefficients acoef,bcoef,
!     diff,alph,using parameters np,aa,
!     ee,gg,bb,dprey,dpred

        IMPLICIT NONE
        INTEGER I, IG, MPER, NGM1, LEN1, J, IYOFF, IOFF, ICI, M, NG, JX, JY
        DOUBLE PRECISION X, Y, ARGX, ARGY, CC

        DIMENSION CC(*)

        AX = 1.0D0
        AY = 1.0D0

!     begin setpar calculations.

        AA = 1.0D0
        EE = 1.0D4
        GG = 0.5D-6
        BB = 1.0D0
        DPREY = 1.0D0
        DPRED = 0.5D0
        ALPH = 1.0D0
        NS = 2*NP
        DO J = 1, NP
          DO I = 1, NP
            ACOEF(NP+I,J) = EE
            ACOEF(I,NP+J) = -GG
          END DO
          ACOEF(J,J) = -AA
          ACOEF(NP+J,NP+J) = -AA
          BCOEF(J) = BB
          BCOEF(NP+J) = -BB
          DIFF(J) = DPREY
          DIFF(NP+J) = DPRED
        END DO

!     end setpar calculations.

!     set remaining problem parameters. they were
!     originally set in the main program.

        MXNS = MX*NS
        DX = AX/REAL(MX-1)
        DY = AY/REAL(MY-1)
        DO I = 1, NS
          COX(I) = DIFF(I)/DX**2
          COY(I) = DIFF(I)/DY**2
        END DO

!     set remaining method parameters. they were
!     originally set in the main program.

        MP = NS
        MQ = MX*MY
        MPSQ = NS*NS
        MESHX = MX
        MESHY = MY
        MXMP = MESHX*MP
        NGX = 2
        NGY = 2
        NGRP = NGX*NGY

!     begin gset calculations.

!     call gset(meshx,ngx,jgx,jigx,jxr)
!     call gset(meshy,ngy,jgy,jigy,jyr)

!     the following calculations were originally
!     done in subroutine gset.

!     set arrays jgx,jigx,and jxr describing a uniform
!     partition of (1,2,...,m) into ngx groups.

        M = MESHX
        NG = NGX

        MPER = M/NG
        DO IG = 1, NG
          JGX(IG) = 1 + (IG-1)*MPER
        END DO
        JGX(NG+1) = M + 1

        NGM1 = NG - 1
        LEN1 = NGM1*MPER
        DO J = 1, LEN1
          JIGX(J) = 1 + (J-1)/MPER
        END DO
        LEN1 = LEN1 + 1
        DO J = LEN1, M
          JIGX(J) = NG
        END DO

        DO IG = 1, NGM1
          JXR(IG) = 0.5D0 + (REAL(IG)-0.5D0)*REAL(MPER)
        END DO
        JXR(NG) = 0.5D0*REAL(1+NGM1*MPER+M)

!     set arrays jgy,jigy,and jyr describing a uniform
!     partition of (1,2,...,m) into ngy groups.

        M = MESHY
        NG = NGY

        MPER = M/NG
        DO IG = 1, NG
          JGY(IG) = 1 + (IG-1)*MPER
        END DO
        JGY(NG+1) = M + 1

        NGM1 = NG - 1
        LEN1 = NGM1*MPER
        DO J = 1, LEN1
          JIGY(J) = 1 + (J-1)/MPER
        END DO
        LEN1 = LEN1 + 1
        DO J = LEN1, M
          JIGY(J) = NG
        END DO

        DO IG = 1, NGM1
          JYR(IG) = 0.5D0 + (REAL(IG)-0.5D0)*REAL(MPER)
        END DO
        JYR(NG) = 0.5D0*REAL(1+NGM1*MPER+M)

!     end gset calculations.

!     begin cinit calculations.

!     the following calculations were originally done
!     in subroutine cinit.

!     compute and load the vector of initial values.

        DO JY = 1, MY
          Y = REAL(JY-1)*DY
          ARGY = 16.0D0*Y*Y*(AY-Y)*(AY-Y)
          IYOFF = MXNS*(JY-1)
          DO JX = 1, MX
            X = REAL(JX-1)*DX
            ARGX = 16.0D0*X*X*(AX-X)*(AX-X)
            IOFF = IYOFF + NS*(JX-1)
            DO I = 1, NS
              ICI = IOFF + I
              CC(ICI) = 10.0D0 + REAL(I)*ARGX*ARGY
            END DO
          END DO
        END DO

!     end cinit calculations.

        RETURN
      END SUBROUTINE INITAL

      SUBROUTINE OUTWEB(T,C,NS,MX,MY,LUN)

!     this routine prints the values of the individual
!     species densities at the current time t.
!     the writestatements use unit lun.

        IMPLICIT NONE
        INTEGER NS, MX, MY, LUN, I, JX, JY
        DOUBLE PRECISION T, C

        DIMENSION C(NS,MX,MY)

        WRITE (6,90000) T
90000   FORMAT (/1X,79('-')/30X,'at time t = ',E16.8/1X,79('-'))

        DO I = 1, NS
          WRITE (6,90001) I
90001     FORMAT (' the species c(,i2,12h) values are')
          DO JY = MY, 1, -1
            WRITE (6,90002) (C(I,JX,JY),JX=1,MX)
90002       FORMAT (6(1X,G12.6))
          END DO
          WRITE (6,90003)
90003     FORMAT (1X,79('-'),/)
        END DO

        RETURN
      END SUBROUTINE OUTWEB

      SUBROUTINE DOUT(LOUT,T,M,YSOL,DYSOL,NEQ)

!     PRINT THE SOLUTION AT OUTPUT POINTS.

        IMPLICIT NONE
        INTEGER LOUT, M, NEQ
        DOUBLE PRECISION T, YSOL, DYSOL

        DIMENSION YSOL(*), DYSOL(*)

        CALL OUTWEB(T,YSOL,NS,MX,MY,LOUT)

        CALL WEBEQ(YSOL,DYSOL,NS,MX,MY,LOUT)

        RETURN
      END SUBROUTINE DOUT

      SUBROUTINE WEBEQ(CC,CD,LD1,LD2,LD3,LOUT)

        IMPLICIT NONE
        INTEGER LD1, LD2, LD3, LOUT, I, J, IAL, IER, JX, JY
        DOUBLE PRECISION CC, CD, DIFMX1, DIFMX2, SMAX1, SMAX2, DIF, RELDF1, &
          RELDF2, DERMAX

        DIMENSION CC(LD1,LD2,LD3), CD(LD1,LD2,LD3)

        DO I = 1, NS
          DO J = 1, NS
            ACHECK(I,J) = 0.0D0
          END DO
        END DO
        DO I = 1, NS
          ACHECK(I,I) = ACOEF(I,I)
        END DO
        DO I = 1, NP
          DO J = NP + 1, NS
            ACHECK(I,J) = ACOEF(I,J)
          END DO
        END DO
        DO I = NP + 1, NS
          DO J = 1, NS
            ACHECK(I,J) = ACOEF(I,J)
          END DO
        END DO
        DO I = 1, NS
          BCHECK(I,2) = -BCOEF(I)
        END DO

        IAL = 40
        CALL DECOMP(ACHECK,ACHECK,NS,IAL,ICHECK,BCHECK(1,3),LOUT,IER)
        IF (IER/=0) RETURN
        CALL SOLVE(ACHECK,BCHECK(1,2),ICHECK,NS,IAL,BCHECK(1,1))

        DIFMX1 = 0.0D0
        DIFMX2 = 0.0D0
        DERMAX = 0.0D0
        SMAX1 = 0.0D0
        SMAX2 = 0.0D0
        DO I = 1, NS
          DO JY = MY, 1, -1
            DO JX = 1, MX
              DIF = ABS(BCHECK(I,1)-CC(I,JX,JY))
              IF (I<=NP) THEN
                DIFMX1 = MAX(DIFMX1,DIF)
                SMAX1 = MAX(SMAX1,ABS(CC(I,JX,JY)))
              END IF
              IF (I>NP) THEN
                DIFMX2 = MAX(DIFMX2,DIF)
                SMAX2 = MAX(SMAX2,ABS(CC(I,JX,JY)))
              END IF
              DERMAX = MAX(DERMAX,ABS(CD(I,JX,JY)))
            END DO
          END DO
        END DO

        RELDF1 = 0.0D0
        RELDF2 = 0.0D0
        IF (SMAX1/=0.0D0) RELDF1 = DIFMX1/SMAX1
        IF (SMAX2/=0.0D0) RELDF2 = DIFMX2/SMAX2

        WRITE (6,90000) DIFMX1, DIFMX2, DERMAX
        WRITE (6,90002) RELDF1, RELDF2
        WRITE (6,90001) (I,BCHECK(I,1),I=1,NS)

90000   FORMAT (' dev. from prey  equil. sol. = ',D15.5,/, &
          ' dev. from pred. equil. sol. = ',D15.5,/, &
          ' maximum derivative magnitude = ',D15.5)
90001   FORMAT (' the alpha = 0 equilibrium valies follow: ',/,(2X,I5,D15.5))
90002   FORMAT (' rel. dev. from prey  equil. sol. = ',D15.5,/, &
          ' rel. dev. from pred. equil. sol. = ',D15.5)

        RETURN
      END SUBROUTINE WEBEQ

      SUBROUTINE DECOMP(A,UL,N,IA,IPS,SCALES,LOUT,IER)

!     FUNCTION          - LU DECOMPOSITION BY GAUSSIAN ELIMINATION
!     USAGE             - CALL DECOMP (A,UL,N,IA,IPS,SCALES,IER)
!     PARAMETERS
!                A      - INPUT MATRIX IN FULL STORAGE MODE.
!                UL     - L-U DECOMPOSITION OF MATRIX A IN PERMUTED
!                N      - ROW DIMENSION OF A FOR THE PRESENT CALL.
!                IA     - ROW DIMENSION OF A IN THE DIMENSION STATEMENT
!                           OF THE CALLING ROUTINE.
!                IPS    - PERMUTATION INDICES-ARRAY OF LENGTH N.(OUTPUT)
!                SCALES - ARRAY CONTAINING RECIPROCALS OF LARGEST ROW
!                           ELEMENTS.(OUTPUT)
!                IER    - ERROR PARAMETER
!                          =  0 INDICATES NORMAL RETURN
!                          = 129 INDICATES SINGULARITY

        IMPLICIT NONE
        INTEGER I, J, K, IA, IPS, LOUT, IER, N, NM1, IDXPIV, KP, IP, KP1
        DOUBLE PRECISION A, UL, SCALES, ROWNRM, BIG, PIVOT, EM, SIZEE

        DIMENSION A(IA,1), UL(IA,1), IPS(1), SCALES(1)

        IER = 0

!     INITIALIZE IPS,UL AND SCALES.

        DO I = 1, N
          IPS(I) = I
          ROWNRM = 0.0D0
          DO J = 1, N
            UL(I,J) = A(I,J)
            IF (ROWNRM-ABS(UL(I,J))) 10, 20, 20
10          ROWNRM = ABS(UL(I,J))
20        END DO
          IF (ROWNRM) 30, 40, 30
30        SCALES(I) = 1.0D0/ROWNRM
          GO TO 50
40        CALL ERRMSG(1,LOUT)
          SCALES(I) = 0.0D0
          IER = 129
50      END DO

!     GAUSSIAN ELIMINATION WITH PARTIAL PIVOTING.

        NM1 = N - 1
        DO K = 1, NM1
          BIG = 0.0D0
          DO I = K, N
            IP = IPS(I)
            SIZEE = ABS(UL(IP,K))*SCALES(IP)
            IF (SIZEE-BIG) 70, 70, 60
60          BIG = SIZEE
            IDXPIV = I
70        END DO
          IF (BIG) 90, 80, 90
80        CALL ERRMSG(2,LOUT)
          IER = 129
          GO TO 140
90        IF (IDXPIV-K) 100, 110, 100
100       J = IPS(K)
          IPS(K) = IPS(IDXPIV)
          IPS(IDXPIV) = J
110       KP = IPS(K)
          PIVOT = UL(KP,K)
          KP1 = K + 1
          DO I = KP1, N
            IP = IPS(I)
            IF (UL(IP,K)) 120, 130, 120
120         EM = -UL(IP,K)/PIVOT
            UL(IP,K) = -EM
            DO J = KP1, N
              UL(IP,J) = UL(IP,J) + EM*UL(KP,J)
            END DO
!     INNER LOOP.  USE MACHINE LANGUAGE CODING IF COMPILER
!     DOES NOT PRODUCE EFFICIENT CODE.
130       END DO
140     END DO
        KP = IPS(N)
        IF (UL(KP,N)) 160, 150, 160
150     CALL ERRMSG(2,LOUT)
        IER = 129
160     CONTINUE

        RETURN
      END SUBROUTINE DECOMP

      SUBROUTINE SOLVE(UL,B,IPS,N,IA,X)

!     FUNCTION          - ELIMINATION PART OF SOLUTION OF AX=B.
!                           FULL STORAGE MODE
!     USAGE             - CALL SOLVE(UL,B,IPS,N,IA,X)
!     PARAMETERS
!                UL     - THE RESULT,LU,COMPUTED IN THE SUBROUTINE
!                           DECOMP,WHERE L IS A LOWER TRIANGULAR
!                           MATRIX WITH ONES ON THE MAIN DIAGONAL. U IS
!                           UPPER TRIANGULAR. L AND U ARE STORED AS A
!                           SINGLE MATRIX A,AND THE UNIT DIAGONAL OF
!                           L IS NOT STORED
!                B      - B IS A VECTOR OF LENGTH N ON THE RIGHT HAND
!                           SIDE OF THE EQUATION AX=B
!                IPS    - THE PERMUTATION MATRIX RETURNED FROM THE
!                           SUBROUTINE DECOMP,STORED AS AN N LENGTH
!                           VECTOR
!                N      - ORDER OF A AND NUMBER OF ROWS IN B
!                IA     - NUMBER OF ROWS IN THE DIMENSION STATEMENT
!                           FOR A IN THE CALLING PROGRAM.
!                X      - THE RESULT X

        IMPLICIT NONE
        INTEGER IPS, N, IA, NP1, IP, I, J, IM1, IBACK, IP1
        DOUBLE PRECISION UL, B, X, SUM

        DIMENSION UL(IA,1), B(1), IPS(1), X(1)

        NP1 = N + 1

        IP = IPS(1)
        X(1) = B(IP)
        DO I = 2, N
          IP = IPS(I)
          IM1 = I - 1
          SUM = 0.0D0
          DO J = 1, IM1
            SUM = SUM + UL(IP,J)*X(J)
          END DO
          X(I) = B(IP) - SUM
        END DO

        IP = IPS(N)
        X(N) = X(N)/UL(IP,N)
        DO IBACK = 2, N
          I = NP1 - IBACK
!     I GOES (N-1),...,1
          IP = IPS(I)
          IP1 = I + 1
          SUM = 0.0D0
          DO J = IP1, N
            SUM = SUM + UL(IP,J)*X(J)
          END DO
          X(I) = (X(I)-SUM)/UL(IP,I)
        END DO

        RETURN
      END SUBROUTINE SOLVE

      SUBROUTINE ERRMSG(IWHY,LOUT)

!     FUNCTION          - ERROR MESSAGE GENERATION FOR DECOMP.
!     USAGE             - CALL ERRMSG(IWHY,LOUT)
!     PARAMETERS
!                IWHY   - ERROR TYPE WHERE
!                            1 = ZERO ROW SINGULARITY
!                            2 = ZERO COLUMN SINGULARITY

        IMPLICIT NONE
        INTEGER IWHY, LOUT

90000   FORMAT (' MATRIX WITH ZERO ROW IN DECOMP.')
90001   FORMAT (' SINGULAR MATRIX IN DECOMP. ZERO DIVIDE IN SOLVE.')

        IF (IWHY==2) GO TO 10

        WRITE (LOUT,90000)
        GO TO 20

10      WRITE (LOUT,90001)

20      RETURN
      END SUBROUTINE ERRMSG

      SUBROUTINE MYDAXPY(N,DA,DX,INCX,DY,INCY)

! Compute a constant times a vector plus a vector

!     Description of Parameters
!     Input:
!     N    - number of elements in input vector(s)
!     DA   - double precision scalar multiplier
!     DX   - double precision vector with N elements
!     INCX - storage spacing between elements of DX
!     DY   - double precision vector with N elements
!     INCY -  storage spacing between elements of DY
!     Output:
!     DY - double precision result (unchanged if N <= 0)

!     Overwrite double precision DY with double precision DA*DX + DY.
!     For I = 0 to N-1, replace  DY(LY+I*INCY) with DA*DX(LX+I*INCX) +
!       DY(LY+I*INCY),
!     where LX = 1 if INCX >= 0, else LX = 1+(1-N)*INCX, and LY is
!     defined in a similar way using INCY.

        IMPLICIT NONE
        INTEGER N, IX, IY, INCX, INCY, M, I, MP1, NS
        DOUBLE PRECISION DX(*), DY(*), DA

!***FIRST EXECUTABLE STATEMENT MYDAXPY
!     IF (N <= 0 .OR. DA == 0.0D0) RETURN
        IF (N<=0 .OR. ABS(DA)<=0.0D0) RETURN
        IF (INCX==INCY) IF (INCX-1) 10, 20, 40

!     Code for unequal or nonpositive increments.

10      IX = 1
        IY = 1
        IF (INCX<0) IX = (-N+1)*INCX + 1
        IF (INCY<0) IY = (-N+1)*INCY + 1
        DO I = 1, N
          DY(IY) = DY(IY) + DA*DX(IX)
          IX = IX + INCX
          IY = IY + INCY
        END DO
        RETURN

!     Code for both increments equal to 1.

!     Clean-up loop so remaining vector length is a multiple of 4.

20      M = MOD(N,4)
        IF (M==0) GO TO 30
!     DO I = 1, M
!        DY(I) = DY(I) + DA * DX(I)
!     END DO
        DY(1:M) = DY(1:M) + DA*DX(1:M)
        IF (N<4) RETURN
30      MP1 = M + 1
        DO I = MP1, N, 4
          DY(I) = DY(I) + DA*DX(I)
          DY(I+1) = DY(I+1) + DA*DX(I+1)
          DY(I+2) = DY(I+2) + DA*DX(I+2)
          DY(I+3) = DY(I+3) + DA*DX(I+3)
        END DO
        RETURN

!     Code for equal, positive, non-unit increments.

40      NS = N*INCX
        DO I = 1, NS, INCX
          DY(I) = DA*DX(I) + DY(I)
        END DO
        RETURN
      END SUBROUTINE MYDAXPY

    END MODULE DEMO_FOODWEB

!******************************************************************

    PROGRAM FOODWEB

      USE DVODE_F90_M
      USE DEMO_FOODWEB

      IMPLICIT NONE

!     Note:
!     NEQMAX is the maximum allowable number of odes. If NEQMAX is
!     increased,be sure to make the same increase in the header
!     declarations.
      INTEGER, PARAMETER :: NEQMAX = 480

      DOUBLE PRECISION T, TOUT, ATOL, RTOL, RSTATS, Y
      INTEGER NST, NFE, LENRW, LENIW, NNI, NCFN, NETF, NFEA, ISTATS, ISTATE, &
        ITASK, IOUT, NJE, NOUT, NLU, NPLOT, NEQ
      DIMENSION ATOL(1), RTOL(1), RSTATS(22), ISTATS(31)
      LOGICAL SPARSE
      TYPE (VODE_OPTS) :: OPTIONS

!     SOLUTION AND DERIVATIVE:
      DIMENSION Y(NEQMAX)

!     OPEN THE OUTPUT FILE AND THE PLOT FILES:
      OPEN (6,FILE='demofw.dat')

!     NEQ = NUMBER OF ODES:
      NP = 15
      MX = 4
      MY = 4
      NS = 2*NP
      NEQ = NS*MX*MY
      IF (NEQ>NEQMAX) THEN
        WRITE (6,90005)
        STOP
      END IF
!     INITIAL SOLUTION:
      CALL INITAL(Y)

!     SET DVODE_F90 JACOBIAN OPTION:
      SPARSE = .TRUE.

!     ABSOLUTE AND RELATIVE ERROR TOLERANCES:
      ATOL(1) = 1.0D-6
      RTOL(1) = 1.0D-6

!     INITIAL TIME AND FIRST OUTPUT POINT:
      T = 0.0D0
      TOUT = 1.0D-8

!     NUMBER OF OUTPUT TIMES:
      NOUT = 10
      NOUT = MIN(NOUT,101)

!     PRINT THE INITIAL CONDITIONS:

!     WRITE(6,182) (I,Y(I),I=1,NEQ)

!     INITIALIZE THE INTEGRATION FLAGS:
      ITASK = 1
      ISTATE = 1

!     SET REMAINING VODE_F90 OPTIONS:

      OPTIONS = SET_OPTS(SPARSE_J=SPARSE,ABSERR=ATOL(1),RELERR=RTOL(1), &
        MXSTEP=100000,NZSWAG=20000)
      NPLOT = 0

!     PERFORM THE INTEGRATION:

      DO IOUT = 1, NOUT

        CALL DVODE_F90(FCN,NEQ,Y,T,TOUT,ITASK,ISTATE,OPTIONS)

!        GATHER THE INTEGRATION STATISTICS FOR THIS CALL:
        CALL GET_STATS(RSTATS,ISTATS)

!        CHECK IF AN ERROR OCCURRED:
        IF (ISTATE<0) GO TO 10

        IF (NPLOT<101) NPLOT = NPLOT + 1
        TOUT = TOUT*10.0D0

      END DO

10    CONTINUE

!     CHECK IF AN ERROR OCCURRED IN VODE_F90:
      IF (ISTATE<0) THEN
        PRINT *, 'DVODE_F90 returned ISTATE = ', ISTATE
        WRITE (6,*) 'DVODE_F90 returned ISTATE = ', ISTATE
      END IF

!     PRINT THE FINAL INTEGRATION STATISTICS:
      NST = ISTATS(11)
      NFE = ISTATS(12)
      NJE = ISTATS(13)
      NLU = ISTATS(19)
      LENRW = ISTATS(17)
      LENIW = ISTATS(18)
      NNI = ISTATS(20)
      NCFN = ISTATS(21)
      NETF = ISTATS(22)
      NFEA = NFE
      NFEA = NFE - NJE
      WRITE (6,90000) LENRW, LENIW, NST, NFE, NFEA, NJE, NLU, NNI, NCFN, NETF
      WRITE (6,90003) TOUT
      WRITE (6,90004) NEQ
!     WRITE(6,181) (I,Y(I),I=1,NEQ)

      CALL RELEASE_ARRAYS

!     FORMATS:
90000 FORMAT ('   Final statistics for this run:'/'   RWORK size =',I4, &
        '   IWORK size =',I8/'   Number of steps =',I8/'   Number of f-s  =', &
        I8/'   (excluding J-s) =',I8/'   Number of J-s  =', &
        I8/'   Number of LU-s =',I5/'   Number of nonlinear iterations =', &
        I8/'   Number of nonlinear convergence failures =', &
        I8/'   Number of error test failures =',I8/)
90003 FORMAT (' Final integration time = ',D15.5)
90004 FORMAT (' Number of odes = ',I10)
90005 FORMAT (' NEQ is larger than NEQMAX.')

!     TERMINATE EXECUTION:
      PRINT *, ' '
      IF (ISTATE==2) THEN
        PRINT *, ' The integration was successful. '
      ELSE
        PRINT *, ' The integration was not successful. '
      END IF
      STOP

    END PROGRAM FOODWEB
