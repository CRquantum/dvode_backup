!
!-----------------------------------------------------------------------
! demonstration program for lsodpk.
! ode system from ns-species interaction pde in 2 dimensions.
!
! this is the version of 6 august 1990.
!
! this version is in double precision.
!-----------------------------------------------------------------------
! this program solves a stiff ode system that arises from a system
! of partial differential equations.  the pde system is a food web
! population model, with predator-prey interaction and diffusion on
! the unit square in two dimensions.  the dependent variable vector is
!
!         1   2        ns
!   c = (c , c , ..., c  )
!
! and the pde-s are as follows..
!
!     i               i      i
!   dc /dt  =  d(i)*(c    + c   )  +  f (x,y,c)  (i=1,...,ns)
!                     xx     yy        i
!
! where
!                  i          ns         j
!   f (x,y,c)  =  c *(b(i) + sum a(i,j)*c )
!    i                       j=1
!
! the number of species is ns = 2*np, with the first np being prey and
! the last np being predators.  the coefficients a(i,j), b(i), d(i) are
!
!   a(i,i) = -a  (all i)
!   a(i,j) = -g  (i .le. np, j .gt. np)
!   a(i,j) =  e  (i .gt. np, j .le. np)
!   b(i) =  b*(1 + alpha*x*y)  (i .le. np)
!   b(i) = -b*(1 + alpha*x*y)  (i .gt. np)
!   d(i) = dprey  (i .le. np)
!   d(i) = dpred  (i .gt. np)
!
! the various scalar parameters are set in subroutine setpar.
!
! the boundary conditions are.. normal derivative = 0.
! a polynomial in x and y is used to set the initial conditions.
!
! the pde-s are discretized by central differencing on a mx by my mesh.
!
! the ode system is solved by lsodpk using method flag values
! mf = 10, 21, 22, 23, 24, 29.  the final time is tmax = 10, except
! that for mf = 10 it is tmax = 1.0e-3 because the problem is stiff,
! and for mf = 23 and 24 it is tmax = 2 because the lack of symmetry
! in the problem makes these methods more costly.
!
! two preconditioner matrices are used.  one uses a fixed number of
! gauss-seidel iterations based on the diffusion terms only.
! the other preconditioner is a block-diagonal matrix based on
! the partial derivatives of the interaction terms f only, using
! block-grouping (computing only a subset of the ns by ns blocks).
! for mf = 21 and 22, these two preconditioners are applied on
! the left and right, respectively, and for mf = 23 and 24 the product
! of the two is used as the one preconditioner matrix.
! for mf = 29, the inverse of the product is applied.
!
! two output files are written.. one with the problem description and
! and performance statistics on unit 6, and one with solution profiles
! at selected output times (for mf = 22 only) on unit 8.
!-----------------------------------------------------------------------
! note.. in addition to the main program and 10 subroutines
! given below, this program requires the linpack subroutines
! dgefa and dgesl, and the blas routine daxpy.
!-----------------------------------------------------------------------
! reference..
!     peter n. brown and alan c. hindmarsh,
!     reduced storage matrix methods in stiff ode systems,
!     j. appl. math. & comp., 31 (1989),pp. 40-91;
!     also l.l.n.l. report ucrl-95088, rev. 1, june 1987.
!-----------------------------------------------------------------------
!
